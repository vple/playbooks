# State
A key factor in controlling the complexity of your software is how well you manage state. Reasoning about state is challenging and taxing—as much as possible you want to minimize or simplify this kind of reasoning.

State pops up everywhere. At the code level, your functions, methods, and classes track state through primitives and objects. This happens with counters, maps, objects, etc.

```typescript
interface MyObject {
  id: number;
}

// A counter!
let nextId: number = 0;

const createObject = () => {
  return {
    id: nextId++,
  };
}
```

At the service and system level, state typically involves tracking and responding to events or actions. Handling an individual action might seem straightforward, but it often triggers a variety of side effects and internal state changes.

Suppose you log in to your favorite service. This changes your client state—your device should now show that you're logged in. In order to decide what to render, backend services must have tracked various information about you—which messages you've received or read, which videos to recommend, etc.

The act of logging in can also trigger a variety of other state changes. The service may record that when you logged in, and on which device. More mature services will also detect potentially suspicious activity, triggering them to verify that it was really you who just logged in.

As you've likely experienced, state gets frustrating or challenging when it changes in ways that you don't expect. This happens when your system somehow gets in a bad state, when objects are unexpectedly mutated, etc.

## Thinking About State
When is state easy to reason about and when is it difficult?

For me, the first thing that comes to mind is that easy-to-reason state has few variables. Hard-to-reason state has a lot.

```typescript
let var1 = 0;
let var2 = 1;

const myFunction = (): number => {
  const ret = var1;
  const next = var1 + var2;
  var1 = var2;
  var2 = next;
  return ret;
}
```

This code seems awkward, but it is not too hard to figure out that it returns sequential Fibonacci numbers. It is relatively easy to reason about—there are only two independent variables, derived variables are straightforward, and you can track each subsequent function call in your head.

You could imagine that a code snippet involving 10 independent variables would be much harder to track.

Here's a different example:

```typescript
import billingClient from ...;

const checkout = async (user: User, items: Item[]) => {
  const total =
    items
	  .map(item => item.price)
	  .reduce((sum, current) => sum + current, 0);
  
  const paymentMethod = await billingClient.requestPaymentMethod(user);
  await billingClient.charge(user, paymentMethod, total);
  
  await sendConfirmation(user, items);
}
```

This function is (oversimplified) checkout code—we calculate the total price, ask the user how they want to pay, charge them, and then send a receipt. This is also easy to reason about. It's missing error handling, but the high-level approach seems reasonable.

However, as any payments team will tell you, checking out is not simple. So why is this code so simple even though payments are complex?

Yes, we cut a lot of corners since it's an example. Yes, our naming is doing a lot of the lifting. But the key point is that there is a lot of state that we don't have to keep track of. (Answers such as abstraction, separation of concerns, services/microservices, etc. are also valid and getting at a similar point.)

We aren't worried if the user has a default credit card, if they want to pay with Apple Pay vs. Google Pay vs. something else, if a credit card gets declined, etc. Our checkout method only has to worry about determining a payment method, then charging that payment method.

