# TeamCity Log
## 2019-07-06
### REST API Investigation
- https://www.jetbrains.com/help/teamcity/rest-api.html

### IntelliJ Integration
Didn't investigate, but this seems interesting.

- https://blog.jetbrains.com/teamcity/2017/10/teamcity-integration-with-intellij-based-ides/
- https://confluence.jetbrains.com/display/TCD18/IntelliJ+Platform+Plugin