# TeamCity Roadmap
## Things To Investigate
### Notifications
- https://confluence.jetbrains.com/display/TCD18/Subscribing+to+Notifications
- https://teamcity.office.yext.com/profile.html?notificatorType=tcSlackBuildNotifier&item=userNotifications

### IntelliJ Integration
- https://blog.jetbrains.com/teamcity/2017/10/teamcity-integration-with-intellij-based-ides/
- https://confluence.jetbrains.com/display/TCD18/IntelliJ+Platform+Plugin

### Favorite Builds
- https://teamcity.office.yext.com/favoriteBuilds.html