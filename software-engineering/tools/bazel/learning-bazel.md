# Learning Bazel

1. [Concepts & Terminology](https://docs.bazel.build/versions/master/build-ref.html)

## Miscellaneous
- [Exploring the IntelliJ Bazel Plugin's Sync Process](https://blog.bazel.build/2019/09/29/intellij-bazel-sync.html)