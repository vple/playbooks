# Bazel Log
## 2019-02-03
### Learning About Toolchains
- [Understanding Extensions](https://docs.bazel.build/versions/master/skylark/concepts.html) (`.bzl` files)
- [Skylark Rules](https://docs.bazel.build/versions/master/skylark/rules.html)
	- Only partially read this, as it's more important if you actually want to create a rule.
- [Platforms](https://docs.bazel.build/versions/master/platforms.html)
- [Toolchains](https://docs.bazel.build/versions/master/toolchains.html)

### Read External Dependencies
- https://docs.bazel.build/versions/master/external.html

## 2019-02-02
### Read Concepts & Terminology
- https://docs.bazel.build/versions/master/build-ref.html 

### Upgraded Bazel
- Current v0.20, installed via homebrew.
- Tried to upgrade with homebrew; no new version found (current is v0.22.0).
- Uninstalled, `brew uninstall bazel`.
- Installed v0.22.0 via [bazel instructions](https://docs.bazel.build/versions/master/install-os-x.html).