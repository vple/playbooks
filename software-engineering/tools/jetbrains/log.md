# JetBrains Log

## 2019-07-24
- [Toolbox App](https://www.jetbrains.com/toolbox/app/)
  - An app for managing JetBrains-related tools and projects, including multiple versions of your IDE.