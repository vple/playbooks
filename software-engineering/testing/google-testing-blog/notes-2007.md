# Google Testing Blog 2007

## January 2007
### [Better Stubbing in Python](https://testing.googleblog.com/2007/01/better-stubbing-in-python.html)
- In Python, you can stub out calls to a built-in function by taking it in as a method parameter (similar to dependency injection).
  - Comes at a cost of adding in an extra param.
- This is applicable to other languages as well, but might not be as nice as Python if they don't treat functions as first class.

## February 2007
### *[Naming Unit Tests Responsibly](https://testing.googleblog.com/2007/02/tott-naming-unit-tests-responsibly.html)*
- Unit tests can be written so that the class under test implicitly forms a sentence with the name of the test.
- This pattern emphasizes responsibilities instead of inputs and outputs.
- When it's hard to form a sentence, suggests that the test is in the wrong place.

### [Improving the accuracy of tests by weighting the results](https://testing.googleblog.com/2007/02/improving-accuracy-of-tests-by-weighing.html)
- https://www.stickyminds.com/article/improving-accuracy-tests-weighing-results
- Describes testing by weighting different parts of the result.
- Not sure I follow the application of the idea. What kind of tests is this actually useful for?

### [Testapalooza](https://testing.googleblog.com/2007/02/testapalooza.html)
- Brief comments on how Google ran an internal conference to share knowledge on testing.