# Java Resources

## Best Practices
- [Java SE 8 Optional, a pragmatic approach](https://blog.joda.org/2015/08/java-se-8-optional-pragmatic-approach.html), Stephen Colebourne
  - Recommendations on how to use `Optional`.