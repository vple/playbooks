# Learning Rust

Many of the resources here can be found on the [Learn Rust](https://www.rust-lang.org/learn) page.

## [The Rust Programming Language](https://doc.rust-lang.org/book/)
This book is the book for learning rust.