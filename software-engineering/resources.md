# Software Engineering Resources
Unorganized software engineering resources.

## General
- [A Startup Founder’s Guide to Smells in Outsourced App Development](https://ctoasaservice.org/2019/07/08/a-startup-founders-guide-to-smells-in-outsourced-app-development/), Marc Adler
    - High-level, low-technical overview of a variety of software engineering best practices and smells—code and architecture, development lifecycle/workflow, data access, security, performance.

## Interviewing
### Interviewers
- [How to interview engineers](https://defmacro.substack.com/p/how-to-interview-engineers)

### Interviewees
- [Tech Interview Handbook](https://yangshun.github.io/tech-interview-handbook/)

## Compensation
- [Typical Employee Equity Levels](https://www.holloway.com/g/equity-compensation/sections/typical-employee-equity-levels)
	- Sample equity levels for series A and B companies.
- [TLDR Options](https://tldroptions.io/)

### Negotiation
- [How to Negotiate a Higher Salary](https://blog.blackswanltd.com/the-edge/how-to-negotiate-a-higher-salary)

## Growth & Development
- [The Skills Poor Programmers Lack](https://justinmeiners.github.io/the-skills-programmers-lack/), Justin Meiners
    - These seem to address the jump from "writes working code" to "understands intent behind decisions." Article will make sense to more senior programmers, but may need to be translated/explained for more junior programmers.
- [A Senior Engineer's Checklist](https://littleblah.com/post/2019-09-01-senior-engineer-checklist/), Little Blah
    - A list of things useful for software engineers, especially senior ones, to do. I agree with them.
    - via [Hacker News](https://news.ycombinator.com/item?id=20914236)

## Code Review
- [Words are Hard](https://www.youtube.com/watch?v=l3tkAbsJ_9U), Peter Sobot
- [Unlearning toxic behaviors in a code review culture](https://medium.com/@sandya.sankarram/unlearning-toxic-behaviors-in-a-code-review-culture-b7c295452a3c), Sandya Sankarram
- [How to Make Your Code Reviewer Fall in Love with You](https://mtlynch.io/code-review-love/)

## Software Development
### Commit Messages
- [My favourite Git commit](https://fatbusinessman.com/2019/my-favourite-git-commit)
    - Example of an informative git commit.

### Miscellaneous
- [Avoid Else, Return Early](https://blog.timoxley.com/post/47041269194/avoid-else-return-early)

### Patterns
- [Strategy Pattern with Dependency Injection](https://ufukhaciogullari.com/blog/strategy-pattern-with-dependency-injection/), Ufuk Hacioğullari
    - Explains how to use the resolver pattern to inject and dynamically determine strategies.

### Quality
- [Managing technical quality in a codebase.](https://lethain.com/managing-technical-quality/)

## Web Applications
- [Web security essentials - Sessions and cookies](https://www.sohamkamani.com/blog/2017/01/08/web-security-session-cookies/)

### Front End
- [JavaScript Modules: A Beginner’s Guide](https://www.freecodecamp.org/news/javascript-modules-a-beginner-s-guide-783f7d7a5fcc/)
- [JavaScript modules](https://v8.dev/features/modules#mjs)

## Writing
- [How (some) good corporate engineering blogs are written](https://danluu.com/corp-eng-blogs/)