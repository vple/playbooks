# Version Control

## Commit Messages
Commit messages should provide the context needed for a reader to understand the commit itself.

https://fatbusinessman.com/2019/my-favourite-git-commit