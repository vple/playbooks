# Naming

**The type of an object should never be part of its name, unless the type is somehow relevant to the code.**

```java
// Bad
List<Foo> fooList;

// Good
List<Foo> foos;
```