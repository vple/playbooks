# Development Cycle

Your development cycle is the workflow that you'll use everyday to accomplish tasks. **The key to being an effective individual contributor is figuring out how to continually optimize this workflow for speed, quality, and correctness.**

Regardless of the specific work you do, the overall structure of this cycle is the same.

1. Problem Assessment
2. Experimentation & Problem Solving
3. Implementation
4. Review
5. Launch

The first phase is problem assessment. Through whatever means, you have a problem or task to do. In this phase, you are aiming to (1) understand the problem, (2) understand the desired result, and (3) determine a tentative approach for solving the problem.

The second phase is experimentation and problem solving. Here, you take the ideas you had from the first phase and confirm that they're possible. You're not worried about quality or thoroughness here. Your aim is to determine if your approach is viable and acceptable.

Once you've confirmed that you have a reasonable approach, you move into implementation. Here, you implement your solution, at the appropriate quality level.

The next phase is the review phase. Here, your work gets reviewed and tested. This phase is typically done by someone else. If your work has issues, those need to go back to the appropriate phase and get reworked.

Finally, once everything is good, you launch your work. Typically, this means deploying to production.

## General Principles


## Problem Assessment

## Experimentation & Problem Solving