# Play Winning Chess

Yasser Seirawan  
[Amazon](https://www.amazon.com/Play-Winning-Chess-Yasser-Seirawan-ebook/dp/B009E3HDAY)

## Definitions

- **rank**—A row.
- **file**—A column.
- **gambit**—A voluntary sacrifice of a piece or pawn in the opneing, with the idea of a lead in development and a subsequent attack as compensation.
- **open file**—A file free of pawns.
- **good bishop**—A bishop free of obstructions.
- **bad bishop**—A (self) blocked bishop.
- **fianchettoed bishop**—A bishop on b2/g2 (for white) or b7/g7 (for black).
- **zugzwang**—A situation where you'd prefer to do nothing.
- **open position**—A position with a minimal number of pawns in the center.
- **closed position**—A position where the center has lots of pawns, congesting the board.

## Principles

With each move, you want to be improving your force, development, pawn structure, and/or space control.

### Force

There are two ways to gain an advantage in force: having more overall pieces or more pieces in a particular area of the board.

You gain an advantage in force by relying on tactics to win you extra pieces. These include:

- pins
- forks
- traps
	- attacking the f pawn
	- discovered attacks
	- playing for a devastating check

Although traps work well when they pay off, you shouldn't aim for traps. Good opponents will see them and push you into a worse position.

### Time

> An advantage in time denotes a situation in which you can bring your pieces to a particular part of the board faster than your opponent can.

When you end up with a time advantage, you capitalize on it by attacking. Time advantages are only temporary, and your opponent can catch up to you if you don't make use of your advantage.

You generally don't want to move a piece more than once during the opening. The following moves don't help it to develop further, which means you're losing tempo.

Developing your queen too early is often a mistake. When your opponent attacks it, you'll be forced to move it and lose tempo.

### Space

Space refers to territory. The board starts divided into two equal halves. The player who controls (has a piece that can attack) more of their opponent's territory has a space advantage.

Space advantages allow you to be more mobile than your opponent. If your opponent is especially cramped, you have a lot of extra time to move and reposition your pieces.

When you have less space, you should try to trade pieces. This opens up space on the board. The pieces you trade are also typically less effective than your opponent's (since you have less space). Conversely, you often don't want to trade when ahead on space.

### Pawn Structure

> The health and the positioning of all the pawns is called *pawn structure.*

Pawn structures dictate the strategies that both players can go for.

Types of pawns:

- **weak pawn**—A pawn on an open file that isn't defended by another pawn.
- **pawn islands**—Single or groups of pawns that are separated from other (same color) pawns by at least one file.
- **doubled pawns**—Pawns that end up on the same file because one made a capture.
- **tripled pawns**—Same as doubled pawns, but with three pawns.
- **isolated pawns**—A pawn without protecting pawns on either side.
- **backward pawns**—A pawn that can no longer be supported by allied pawns because it's the last to advance.
- **good pawn**—Any pawn that cramps the opponent and is safe from attack.
- **passed pawn**—A pawn with no enemy pawns in front or on either side. A pawn that can no longer be attacked by enemy pawns.
- **pawn chain**—A group of pawns on a diagonal.

Principles:

- When your opponent has a weak pawn, you want to focus on attacking and capturing that pawn.
- Before attacking a weak pawn, gain control of the square directly in front of it.
- You can help keep weak pawns safe by ensuring that you have nearby pawns to protect it.
- In the endgame, it's better to have fewer pawn islands (i.e. a more connected pawn structure).
- Doubled pawns are often weak because they have less mobility. They are sometimes okay in the center since they can still control space.
- Tripled pawns are bad.
- Attack pawn chains at their base.

Open positions allow for high mobility and faster attacks. With open positions, it's important to get the king to safety as quickly as possible. Open positions also allow rooks to be more effective. Pieces often lead attacks in open positions.

Closed positions make the game play more slowly since both players must maneuver around the pawns. In closed positions, it's okay to delay castling a bit since the king is harder to attack. To attack in a closed position, attack where you have the most space to maneuver. You also want to try to open up a file for your rooks. Pawns generally lead attacks in closed positions.

### Game Phases

There are three phases of a game: the opening, the midgame, and the endgame.

#### Opening

#### Endgame

In the endgame, you want to develop your king and use it to control space. In earlier phases you want to leave your king in safety, but in the endgame there are much fewer pieces and the king is consequently much stronger.

Covered scenarios: king & queen vs lone king, king & rook vs lone king.

### Pieces

- Rooks need open files to be effective.
- When placing rooks and queens on an open file, it's often better to lead with the rook to protect the queen.
- Bishops need open diagonals to be effective. Keeping your pawns and bishops on opposite colors helps to keep your bishop effective.
- Knights are more effective when they've been moved to more advanced (forward) squares.

## Follow-Up Ideas

### Creating Undefended Pieces

If a piece is defended only by a small number (e.g. 1) of defenders, you might be able to get the piece to become undefended. This is done by attacking the defenders, forcing them to move and therefore stop defending the target.

### Tempo Manipulation

How can you manipulate tempo? A move can cause your opponent to "lose tempo," but what does it actually mean to be lost?

In particular:

- How much tempo do you gain? You got a free move, but not moves will necessarily have the same worth.
- How much tempo do you have if you repeatedly cause your opponent to lose tempo?
- Can you aggressively distort tempo? Are there moves that either force your opponent to move multiple times or that make it feel as if you moved multiple times?
- Can you defensively distort tempo? When you are forced to lose tempo, are there moves you can do that mitigate it?
- Can you disguise tempo loss? If you wanted / needed to move a piece and your opponent attacks that piece, do you really lose tempo if you get to move it to where you wanted?

### File Control

It's harder for a rook to get on a file if the file is already contested by the opposing rook. So, you want to take over the file first by getting your rook there first.