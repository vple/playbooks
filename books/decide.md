# Decide

## 3. Prioritizing Tasks in Relation to Results

Prioritize tasks as (A) gain tasks, (B) recorded prevent pain tasks, and (C) prevent pain tasks. Gain tasks are the most important tasks. Tasks that you want to explicitly prevent pain on should be recorded.

There are two types of goals—consumption and creation. Consumption goals provide short-term rewards, while creation goals are more meaningful and long term. Both are important to keep life balanced.

## 4. Energy and Motivation Decide How You Will Get Yours

Procrastination can be useful and harmful. It's useful when it frees up energy for other things, lets you get things done quickly, etc. It's harmful when it stresses you out or causes lower quality work. The rule of thumb is that it's okay to procrastinate on things where quality doesn't matter.

## 5. What Understanding the Value of Time Can Do for Your Life
Accomplish gain by putting your goals on your calendar. This defends your time for you to work on goals.

Start by breaking down your goal into tasks. Identify the first action that you can take and schedule time for it, no matter how small.