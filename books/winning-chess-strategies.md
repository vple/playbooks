# Winning Chess Strategies
Yasser Seirawan
[Amazon](https://www.amazon.com/Winning-Chess-Strategies-ebook/dp/B004Y6AI30)

## The Importance of Strategy
There are two types of advantages: static (long-term) and dynamic (temporary). Strategy aims to create one or more static advantages:

- More material
- Superior piece mobility
- Superior pawn structure
- More territory (space)
- Safe king position

## Making the Most of a Material Advantage
### Overwhelming Your Opponent

> 1. After you gain material, immediately consolidate your position by developing all your pieces and ensuring your King's safety.
  2. Once you have consolidated your position, seek out new goodies to devour.
  3. If you have a large material advantage, don't hesitate to give up some of your gains if doing so will stop enemy threads while allowing you to remain equal or ahead in material.

### Throwing Away a Material Advantage