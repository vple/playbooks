# How to Text a Girl
Chase Amante

## Introduction

## The Four Kinds of Texters
### Clueless Boring Questions Guy
No idea how to text girls, what to put in a text, or what girls are like. Doesn't think about how a girl would respond to his message. This results in asking poor questions that girls don't respond to.

Texts are often boring questions, such as:

- What's up?
- How's it going?
- How was your weekend?
- What are you doing?
- Do you have plans?

### Endless Conversations Guy
This guy engages girls in conversation rather than asking boring questions, but treats text conversations like normal conversations. The conversations don't go anywhere, and so nothing happens.

### Really, Incredibly Witty and Interesting Guy
These guys realize that endless conversations are boring. Instead they're interesting by having humor and being more of a bad boy than a nice guy. They do much better than the other two guys, but can still run into issues when setting up dates.

### Just Gets It Guy
This guy keeps things simple and to the point. He doesn't aim to be witty or interesting.

## Ground Rules of Texting
1. 	Faulty models are your responsibility to fix, not women's.
2. 	Phone numbers are easy.  
	Getting a girl's number doesn't mean anything--it's just contact information, like an email.
3. 	Emotions don't "stick."  
	A girl won't remember the emotions she had when you met. Assume she's going about her normal day, and imagine how she might receive your first text in those circumstances.
4.	People want you to reduce their cognitive load, not pile on.
5.	You must keep your eye on the ball.
6.	Girls talk because they like to talk.
7. 	Women want men who are "just friends."
8.	Women cannot "get to know you" over text.

## How to Text a Girl
### Objectives
There are only two objectives when texting: build rapport and comfort or set up a meeting. The latter is the main objective, which the former helps you achieve.

You want the girl to know your objective--it should be clear what your text is about. Be direct and straightforward; don't beat around the bush. Keep your objective in mind. Be precise and concise.

### Warm vs. Cold
Warm texting: texting a girl who is thinking about you or expecting to hear from you at the time you text her. Cold texting: texting a girl who isn't expecting your text.

A warm text feels normal, even if it's lighter on details and more straightforward. Cold texts can be confusing because a girl has to figure out why you're texting them. It's important to structure cold texts so they don't come out of the blue.

### Structuring Texts
All new, cold text conversations must always have four elements:

1.	A greeting.
2.	The girl's name.
3.	A piece of new information.
4.	Something showing consideration for her.

Full example:
> Gabby, hey. Running a bit behind, sorry; will be there closer to 2:30. Still cool?

Greeting + name examples:
> Hey Lily, hope your weekend was good =)

> Katie, morning!

> Hi Melanie!

Information examples:
> Sitting here in gridlock... this city has the worst traffic ever!

> Had the most amazing shrimp of my life last night... I can still taste it.

> Thinking we need to get together some time soon.

Consideration examples:
> How's your week looking?

> How was your test?

> What's your schedule looking like this week?

Emotes and exclamation points are usually more positive than negative. They convey intent better over textual mediums.

## Opening up the Hood

## What to Do When She Won't Text Back

## What to Do When She Flakes

## Calls -- For When Texts Don't Work

## Texting When It's Been a While

## Texting Girls You Met Online

## When to Throw the Ball in Her Court

## Example Text Conversation

## Where the Learning Curve Lies

## Conclusion