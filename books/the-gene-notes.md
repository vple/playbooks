# The Gene: An Intimate History
Siddhartha Mukherjee  
[Amazon](https://www.amazon.com/dp/B017I25DCC)

From the book, an outline of a hitchhiker's guide for a post-genomic world:

1. A gene is the basic unit of hereditary information.
2. The genetic code is universal.
3. Genes influence form, function, and fate, but these influences typically do not occur in a one-to-one manner.
4. Variations in genes contribute to variations in features, forms, and behaviors.
5. When we claim to find "genes for" certain human features or functions, it is by virtue of defining that feature narrowly.
6. It is nonsense to speak about "nature" or "nurture" in absolutes or abstracts. Whether nature—i.e., the gene—or nurture—i.e., the environment—dominates in the development of a feature or function depends, acutely, on the indivudal feature and the context.
7. Every generation of humans will produce variants and mutants; it is an intextricable part of our biology.
8. Many human diseases—including several illnesses previously thought to be related to diet, exposure, environment, and chance—are powerfully influenced or caused by genes.
9. Every genetic "illness" is a mismatch between an organism's genome and its environment.
10. In exceptional cases, the genetic incompatibility may be so deep that only extraordinary measures, such as genetic selection, or directed genetic interventions, are justified.
11. There is nothing about genes or genomes that makes them inherently resistant to chemical and biological manipulation.
12. A triangle of considerations—extraordinary suffering, highly penetrant genotypes, and justifiable interventions—has, thus far, constrained our attempts to intervene on humans.
13. History repeats itself, in part because the genome repeats itself. And the genome repeats itself, in part because history does.