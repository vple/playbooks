# Difficult Conversations
Douglas Stone, Bruce Patton, Sheila Heen

- [The Problem](#the-problem)
  - [1. Sort Out the Three Conversations](#1-sort-out-the-three-conversations)
- [The "What Happened?" Conversation](#the--what-happened---conversation)
  - [2. Stop Arguing About Who's Right: _Explore Each Other's Stories_](#2-stop-arguing-about-who-s-right---explore-each-other-s-stories-)
  - [3. Don't Assume They Meant It: _Disentangle Intent from Impact_](#3-don-t-assume-they-meant-it---disentangle-intent-from-impact-)
  - [4. Abandon Blame: _Map the Contribution System_](#4-abandon-blame---map-the-contribution-system-)
- [The Feelings Conversation](#the-feelings-conversation)
  - [5. Have Your Feelings _(Or They Will Have You)_](#5-have-your-feelings---or-they-will-have-you--)
- [The Identity Conversation](#the-identity-conversation)
  - [6. Ground Your Identity: _Ask Yourself What's at Stake_](#6-ground-your-identity---ask-yourself-what-s-at-stake-)
- [Create a Learning Conversation](#create-a-learning-conversation)
  - [7. What's Your Purpose? _When to Raise It and When to Let Go_](#7-what-s-your-purpose---when-to-raise-it-and-when-to-let-go-)
  - [8. Getting Started: _Begin from the Third Story_](#8-getting-started---begin-from-the-third-story-)
  - [9. Learning: _Listen from the Inside Out_](#9-learning---listen-from-the-inside-out-)
  - [10. Expression: _Speak for Yourself with Clarity and Power_](#10-expression---speak-for-yourself-with-clarity-and-power-)
  - [11. Problem-Solving: _Take the Lead_](#11-problem-solving---take-the-lead-)
  - [12. Putting It All Together](#12-putting-it-all-together)
- [Ten Questions People Ask About _Difficult Conversations_](#ten-questions-people-ask-about--difficult-conversations-)

## The Problem
### 1. Sort Out the Three Conversations
There are three conversations that really go on during a difficult conversation.

The "What Happened?" conversation is focused around disagreement over what has happened or what should happen. This is where most time is spent. There are three parts to it.

The first is, on the surface, about figuring out facts. The problem is that we come in with the assumption that we're correct. Instead, we should be focusing on figuring out what's important, not what's true.

The second is arguing over intentions. We tend to assume we know others' intentions, and we tend to assume others' intentions are bad when we don't know them. In reality, we can't know other peoples' intentions and should avoid making unfounded assumptions.

The third is blaming others. Focusing on blame, however, doesn't accomplish much. Instead, it's better to figure out how to move forwards.

The feelings conversation deals with issues over people's feelings, how they should be expressed, and whether or not they are valid. We'll always have strong feelings, so the real issue is how we handle them.

The identity conversation is focused on figuring out what the situation means to us. It's about whether or not our actions line up with the image we have of ourselves in our heads.

## The "What Happened?" Conversation
### 2. Stop Arguing About Who's Right: _Explore Each Other's Stories_
### 3. Don't Assume They Meant It: _Disentangle Intent from Impact_
### 4. Abandon Blame: _Map the Contribution System_

## The Feelings Conversation
### 5. Have Your Feelings _(Or They Will Have You)_

## The Identity Conversation
### 6. Ground Your Identity: _Ask Yourself What's at Stake_

## Create a Learning Conversation
### 7. What's Your Purpose? _When to Raise It and When to Let Go_
### 8. Getting Started: _Begin from the Third Story_
### 9. Learning: _Listen from the Inside Out_
### 10. Expression: _Speak for Yourself with Clarity and Power_
### 11. Problem-Solving: _Take the Lead_
### 12. Putting It All Together

## Ten Questions People Ask About _Difficult Conversations_