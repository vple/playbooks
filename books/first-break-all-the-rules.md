# First, Break All The Rules
Gallup
[Amazon](https://www.amazon.com/First-Break-All-Rules-Differently-ebook/dp/B01E7M6INO/)

## Introduction
The ideas behind great management are plain and direct. However, they can be hard to actually implement. They require discipline, focus, trust, and a willingness to individualize.

Great managers approach things differently, doing things such as:

- helping employees develop according to they already are
- treating each person differently
- being close friends with employees
- not trying to change people
- trusting others

## 1. The Measuring Stick
### What do we know to be important but are unable to measure?
We know that it's important to build an environment that attracts, focuses, and keeps talented employees. But we don't really know how to measure this.

### How can you measure human capital?
To determine what makes a great workplace, you need to identify the elements that attract *only* talented employees, as opposed to any employee.

Gallup's data revealed questions that engaged (loyal and productive) employees answered positively but everyone else answered neutrally or negatively. There are 12 items, referred to as Q12, that capture the most information and the most important information.

> 1. I know what is expected of me at work.
> 2. I have the materials and equipment I need to do my work right.
> 3. At work, I have the opportunity to do what I do best every day.
> 4. In the last seven days, I have received recognition or praise for doing good work.
> 5. My supervisor, or someone at work, seems to care about me as a person.
> 6. There is someone at work who encourages my development.
> 7. At work, my opinions seem to count.
> 8. The mission or purpose of my company makes me feel my job is important.
> 9. My associates or fellow employees are committed to doing quality work.
> 10. I have a best friend at work.
> 11. In the last six months, someone at work has talked to me about my progress.
> 12. This last year, I have had opportunities at work to learn and grow.

An important part here is the extreme qualifiers (e.g. "best"). Without these qualifiers, these items lose their ability to discriminate. We need questions where people don't always strongly agree.

### Does the measuring stick link to business outcomes?
Essentially, yes. Each of the twelve items links to at least one of productivity, profitability, retention, and customer satisfaction. 

An employee's immediate manager most directly influences items that are linked to turnover—people leave managers, not companies. The employee's relationship with this manager determines *how long they stay* and *how productive they are*.

Of the twelve items, some are more consistently linked to business outcomes and more actionable. These six are the most powerful:

> 1. I know what is expected of me at work.
> 2. I have the materials and equipment I need to do my work right.
> 3. At work, I have the opportunity to do what I do best every day.
> 4. In the last seven days, I have received recognition or praise for doing good work.
> 5. My supervisor, or someone at work, seems to care about me as a person.
> 6. There is someone at work who encourages my development.

### What do these discoveries mean for one particular company?
Since things change rapidly, one of the most valuable things a company can have is employees' "benefit of the doubt." If a company has this, their employees will give initiatives a fighting chance, rather than believing it will fail.

Ultimately, it's the immediate manager that affects an employee's answers to the Q12 items. So, managers need to learn how to address these items to improve results. **It's important that these are addressed in the correct order,** as these items build upon one another.

### Why is there an order to the 12 items?
There are four stages for an employee to climb to the point where they get fully engaged. (The book likens them to climbing a mountain.)

Base Camp and Camp 1 are the foundation and most important. If those needs aren't met then employees get burned out and leave, regardless of how they respond to the items in Camps 2 and 3.

#### Base Camp: "What do I get?"
Here, an employee wants to know what is expected of them. These include logistics, such as salary/compensation, commute, equipment, etc. The two Q12 items here are:

> 1. I know what is expected of me at work.
> 2. I have the materials and equipment I need to do my work right.

#### Camp 1: "What do I give?"
As an employee improves, they want to know how they are doing at their job. They are focused on their individual contribution and how others perceive their contribution. This includes if they are in the right role to excel, if they actually are excelling, etc. The Q12 items here are:

> 3. At work, I have the opportunity to do what I do best every day.
> 4. In the last seven days, I have received recognition or praise for doing good work.
> 5. My supervisor, or someone at work, seems to care about me as a person.
> 6. There is someone at work who encourages my development.

Each of these addresses a different concern:

- Q3: If you feel you're doing well in your role.
- Q4: If others value your individual performance.
- Q5: If others value you as a person.
- Q6: If someone is prepared to invest in your growth.

#### Camp 2: "Do I belong here?"
At this stage, an employee wants to know if their environment matches up with what they want. This often means that your peers and align with your basic values.

> 7. At work, my opinions seem to count.
> 8. The mission or purpose of my company makes me feel my job is important.
> 9. My associates or fellow employees are committed to doing quality work.
> 10. I have a best friend at work.

#### Camp 3: "How can we all grow?"
The final stage deals with how everyone gets better. It's only possible to innovate at this level once you've progressed through the other stages. Innovation is novelty that can be applied.

> 11. In the last six months, someone at work has talked to me about my progress.
> 12. This last year, I have had opportunities at work to learn and grow.

#### The Focus of Great Managers
The first six items are the most important focus for great managers. But this requires handling responsibilities that seem contradictory. Great managers must set consistent expectations while treating each person uniquely, ensure people are in good roles while challenging them to grow, etc.

## 2. The Wisdom of Great Managers
### What is the revolutionary insight shared by all great managers?
The insight is that each person is unique, with their own set of motivations, ways of thinking, and style for relating to others. They fundamentally can't be remolded and changed. Instead, great managers capitalize on what people are good at and inclined towards.

### What are the four basic roles of a great manager?
> The manager role is to reach inside every employee and release his unique talents into performance.

Managers are catalysts—they speed up the interaction between employee talents and company goals, as well as between employee talents and customer needs. This allows them to produce results.

In order to do so, and to get positive responses from Q1-6, managers must be able to do four things well:

1. Select a person.  
	When selecting people, managers must know how much of them can be changed. They need to know the difference between talent, skills, and knowledge. They must also be able to ask questions that reveal a candidate's true talents, not those they are trying to show.
2. Set expectations.  
	This includes keeping employees focused on performance today. It also includes knowing where to require conformity and where employees should exercise their own style, balancing standardization and efficiency with flair and originality.
3. Motivate the person.  
	This means understanding who to spend your time with, how to deal with someone's weaknesses and strengths, and giving praise.
4. Develop the person.  
	This involves knowing what to say when employees need career advice, how close you should get to people, and what you actually owe people.

Managers and leaders are both important, but different, roles. Managers look inward into individuals, understanding how each person can best contribute. Leaders look outward, looking to see how to move things forward and thinking in bigger picture terms.

### How do great managers play these roles?

- Select for talent, not just experience, intelligence, or determination.
- Set expectations by defining correct outcomes, not correct steps.
- Motivate people by focusing on their strengths, not their weaknesses.
- Develop people by helping them find the right fit, not the next rung on the ladder.

## 3. The First Key: Select for Talent
### Why does every role performed at excellence require talent?
Talent—a recurring pattern of thought, feeling, or behavior that can be productively applied. The importance is recurring; talents are behaviors that you do often. There are many possible talents, such as being able to remember names or color-coding wardrobes. 

Every role, to be done excellently, requires talent. 

### Why is talent more important than experience, brainpower, and willpower?
Experience, intelligence, and willpower[^talent] are important. But they're not the most important thing. It's most important to realize that there are many talents and you need to select for the right talents. They're prerequisites to being able to perform well.

[^talent]: Willpower is classified as a talent.

This is because talent cannot be taught, only selected.[^learning-talent] Additionally, talents are the driving force behind job performance. Talents are how people perceive the world, and affect how they react to things.[^mindset]

[^learning-talent]: Although it probably is impossible to teach talent in a reasonable time period, it's hard for me to agree with the idea that talent can't be learned at all. The argument later on is that you can improve your competency to be able to handle things that you are terrible at. I don't want to believe that people, and in particular me, cannot foster new talents. It may be impossible to develop a specific talent, but it seems wrong that you cannot develop any more talents. Likely, thoroughly developing talent relies on time and a need for the talent.

[^mindset]: It's unclear to me how close talent, as used here, is to Carol Dweck's mindset.

### How much of a person can the manager change?
Basically, nothing. People don't change much, and managers should focus on drawing out what they have.

The claim here is that people solidify how they think in their adolescent years, reinforcing certain neural paths. If people don't develop a certain talent in these years, they won't develop it.

People are still able to change—they can learn skills, knowledge, and change their values. They can also learn to at least cope in areas where they have no neural connections. But the areas with no competency cannot be turned into talent.

Instead of trying to change people, help them discover their hidden talents and teach them new skills & knowledge.

### What is the difference between skills, knowledge, and talents?
Skills and knowledge can be easily taught, but talent cannot.

**Skills** are "how-tos" of a role, capabilities that can be transferred from one person to another. Skills are best taught by breaking down the entire action into steps, which are then taught and reconstructed into the whole. Skills are best learned through practice.

Knowledge consists of factual knowledge and experiential knowledge. **Factual knowledge** clearly consists of facts and includes things such as the rules of double-entry bookkeeping, safety regulations, and features of a product. This knowledge can and should be taught.

**Experiential knowledge** isn't as tangible, and has to be obtained by the individual. This is done by making sense of past experiences, drawing out patterns and connections. This knowledge includes practical things such as processes for obtaining certain results. It also includes conceptual things, such as understanding what you value.

**Talents** are the recurring patterns of thought, feeling, or behavior that result from the inclinations you've developed over time. They typically act as filters, making you good in corresponding areas. For example, a love for precision is a talent—it's not a skill or knowledge, but having this drives you to be able to excel in areas requiring precision.

Skills and knowledge are transferable from person to person, but they are limited to specific situations. Talent is transferable between different situations, but are very difficult to teach.

#### Three Kinds of Talent
**Striving** talents explain *why* someone does the things they do. They motivate people to keep working in certain areas.

**Thinking** talents explain *how* someone operates—how they think, how they weight options, and how they make decisions.

**Relating** talents explain *who* someone is. They determine who someone trusts, who they form relationships with, who they confront or ignore. They define how someone interacts with others.

### Why are great managers so good at selecting for talent?
It can be hard to identify talent, especially since many people don't know what their talents are. It's also hard to pick talent when interviewing, since people will present themself in the best possible way they can.

To figure out what talents to look for, think about what and how people will be working while on the job. This involves understanding the culture of the company. The talents that people have should enable them to succeed at the role you have in mind for them.

Consider the expectations you have and how the person will be supervised. Consider the people they will have to work with, especially your own style.

This can be overwhelming. It helps to look for one critical talent in each of the three talent categories and to start with just those as a foundation. Then, once they're identified, don't compromise on them.

To confirm that you've identified the right talents, look at the best people you have. They will exhibit the talents needed to succeed, and *only* they will show these talents—by definition, worse performers won't have them. 

## 4. The Second Key: Define the Right Outcomes
### Why is it so hard to manage people well?
As a manager, you have to get people to do what you want when you are not there to tell them to do it. This means you have less control (over work getting completed) than the people who report to you. **You have no direct control—you have remote control.** At the same time, you're still obligated to produce results.

In addition to managing by remote control, each person will respond to you differently. Great managers have additional constraints—they don't believe that people change much and they believe the organization exists to perform and produce results. As a result, great managers are skeptical about handing authority down. They have to be in control and focus on performance, but cannot force people to perform in the same way.

The solution to this problem is to define the right outcomes and to let each person find their own path to the result. Standardizing the ends allows you to not standardize the means. This also encourages people to take responsibility for their accomplishments.

### Why do so many managers try to control their people?
There are several temptations that lead managers to try to control people.

#### Temptation: Perfect People
Here, the manager wants everyone to be perfect and believes there's a best way to perform every role. But this is inefficient, especially when it goes against peoples' talents. It also prevents people from developing, as they don't explore and figure things out on their own. It also narrows down the decisions they make. In the long run, employees suffer.

#### Temptation: My People Don't Have Enough Talent
As a response to a lack of talent, people respond with lots of processes and procedures in the hopes of making a role idiot proof. The solution here is to select for talent.

> If you don't select for talent, then you *shouldn't* give people leeway.

#### Temptation: Trust Is Precious—It Must Be Earned
Some people, for whatever reason, may have a fundamental distrust of others. But this results in reports losing trust in you. Additionally, there's no line when people will suddenly become trustworthy.

#### Temptation: Some Outcomes Defy Definition
Sometimes it seems like you can't define an appropriate outcome for something. The real problem here is that the outcome is difficult to define, but not impossible. You just need to work more to figure out the outcome.

Not all outcomes are in terms of hard metrics. They can also include things like emotional outcomes.

### When and how do great managers rely on steps?
Great managers still use steps. They're obligated to output performance. The following rules of thumb help to determine whether or not steps are appropriate:

- Don't break the bank: employees must follow certain required steps for all aspects of their role that deal with accuracy or safety.
- Standards: employees must follow required steps when those steps are part of a company or industry standard.
- Don't let the creed overshadow the message: required steps are useful only if they do not obscure the desired outcome.
- There are no steps leading to customer satisfaction: required steps only prevent dissatisfaction. They cannot drive customer satisfaction.

There are four levels that customers expect out of the companies they deal with. They build on one another; lower-levels must be met before customers will care about higher levels.

1. Accuracy. Customers expect the service or product they receive to match up with their expectations.
2. Availability. Customers want to be able to use the service at appropriate times.
3. Partnership. Customers want you to listen and be responsive to them. They want you to be on the same side of the table.
4. Advice. Customers want you to help them learn.

Crucially, the first two are easy for other companies to replicate and cannot create satisfaction. Not meeting them creates dissatisfcation. The latter two are where satisfaction is created, but it's very hard to do so if your employees are following a formula. Instead, you have to focus employees to emotional outcomes like partnership and advice.

### How do you know if the outcomes are right?
An important part of using outcomes is having the right outcomes. Here are some guidelines to finding the right outcomes.

- What is right for your customers?
- What is right for your company?
- What is right for the individual?

## 5. The Third Key: Focus on Strengths
### How do great managers release each person's potential?
Great managers focus on each person's strengths, finding ways to work around their weaknesses.

### Why is it so tempting to try to fix people?
It's tempting to try to fix people because you think you can just unlock their potential, but this isn't something you should do. This implies that everyone has the same potential and no individuality. Moreover, trying to get people to develop talents in the areas that they lack will just be a frustrating experience for them. It also focuses the manager on the wrong things in the relationship (weaknesses) rather than the right things (strengths). Most importantly, you're putting blame on the report for being unable to improve.

### How do great managers cultivate excellent performance so consistently?
Everyone has talents that can be used productively. You need to figure out how to use your peoples' talents productively.

You determine your employees' strengths, talents, goals, dreams, etc. by working closesly with them, taking notes, and paying attention over time.

### Why do great managers break the Golden Rule?
Instead, treat each person as they would like to be treated. The best way to find this out is to ask. There are many things you can ask about:

- The person's goals.
- The person's expected career trajectory.
- Personal goals that the person is comfortable sharing with you.
- How often they want to talk about progress.
- Preferred form of praise (private/public, written/verbal).
- Most memorable recognition they've received, and why it was so memorable.
- How they prefer to learn.
- Mentors or partners that have helped them, and what these people did to help.

### Why do great managers play favorites?
Effective managers spend the most time with their most productive employees. This is because they are most concerned with being *catalysts*. They want to unleash someone's talents, done by setting a unique set of expectations; highlighting and perfecting each person's unique style; and running interference for that person.

In contrast, weaker managers spend the most time with their underperforming employees. This is because they see their role as controlling or instructing.

Focusing on strong performers is good because it encourages desired behaviors. By neglecting strong-performing employees, you're sending out a message that their good behaviors aren't as important to you. This will result in getting less of that behavior. People will change their behavior in order to receive more attention.

Focusing on your best also gives you other benefits. First, it's the fairest thing for you to do for your employees. The better their performance is, the more time and attention they should receive from their manager.

Second, it's the best way for you to learn about excellence. To know what makes your best the best, you need to pay attention to your best! Not your worst. The things that your best performers do are not necessarily opposite of what your worst performers are doing, so you can only learn it by watching them.

Finally, investing in your best is the only way to reach excellence. Although averages are good for estimating, you don't want to use them for managing. Focus on helping your best people become excellent, rather than your worst people just meeting the bar.

### How do great managers turn a harmful weakness into an irrelevant nontalent?
Poor performance must be confronted, and it must be confronted quickly. Typically, people perform poorly due to "mechanical" causes, such as not having the resources or information they need, or "personal" causes, such as a death in the family. As a manager, you should look to these causes first, even though they can be difficult to solve.

There are two questions that help identify other performance problems. First, can the person be trained to have improved performance? The answer to this is always yes if the person is lacking certain skills or knowledge. Second, is the nonperformance caused by the manager tripping the wrong trigger? As a manager, it's your responsibility to find and hit the right switches for each employee, letting their talent come forward.

If the genuine answer to both questions is no, the person has a talent problem. They're not in the right role, as their nontalents become weaknesses that won't let them succeed there. They need to be put in a different role.

Nontalents are areas that you don't have talent in; areas where you routinely struggle and don't find any satisfaction. They become weaknesses when they're required in order to succeed in a role. When someone has a weakness, there are three ways to deal with it:

1. Devise a support system.
2. Find a complementary partner.
3. Find an alternative role.

A support system is the fastest way to fix a weakness. It involves changing something, such as an existing process, a layout, etc., that makes someone's nontalent no longer required to do their job well.

Finding a partner means finding someone whose strengths offset your weaknesses. This allows both of you to focus on your strengths without having to worry about your weaknesses. An important part of this is that each person in the partnership realizes that they aren't perfect. This is really important, since otherwise you won't be able to trust each other and function as a team. 

This isn't what conventional teams do today. Ideally, on a productive team, each person knows what role they play best and perform that role most of the time. Teams are built around *individual excellence*; each person should have the right role and each individual's strengths & weaknesses should be balanced.

Finding an alternative role needs to happen when you spend most of your time trying to manage around someone's weaknesses. You've made a casting error and need to fix that error, not the person.

## 6. The Fourth Key: Find the Right Fit
### What's wrong with the old career path?
Eventually, employees will ask where they should go next in their career. There's no single right answer. The right approach is to help each person find the right fit.

The classic approach to career development is to always move upward. This fails when you get to the point where the Peter Principle applies—you're now incompetent, but you can't move down to a lower position. There are three bad assumptions that cause this approach to not work:

1. Each rung is a slightly more complex version of the previous rung. In reality, one rung doesn't necessarily lead to another.
2. The conventional path creates conflict. Instead of having conflict, managers can convey meaningful prestige for every role performed excellently.
3. Varied experiences supposedly make an employee more attractive. These are seen as the driving force behind an employee's career. Instead, the driving force for a career change should be that the employee has a new career in mind.

### Why do we keep promoting people to their level of incompetence?
We continue to assume that success on one rung implies success on the next rung. This is often because we don't know what is trainable and what is not. There are no distinctions between skills, knowledge, and talent, which makes it hard for us to speak precisely.

### How to solve the shortage of respect
No matter what conclusion the manager reaches (for career development), the employee will still want to move up. The benefits at higher levels are more appealing than lower levels. This is addressed by performing heroes in every role. Every role performed excellently should be respected.

It takes 10-18 years to reach world-class competency in many professions. In order to get employees to reach world-class performance, companies must find ways to encourage employees to continue developing their expertise. An effective way of doing so is by defining graded levels of achievement for each role.

These efforts won't work if pay structures don't match up. Ideally, companies should pay employees in direct proportion to the amount of expertise they show in their current role. This is complicated by some roles being more important that others. However, you can still **broadband**—make pay ranges at each level overlap with the pay range of the next level up. This allows you to express the idea that a role performed excellently can be more valuable than a higher role performed averagely. Within a role, you can also provide compensation that values excellence differently from average performance. It also forces employees to decide why they want to move up, as moving up will involve a pay cut.

If your company's structure prevents you from doing these things, you should find a way to revolt against the structure. Find ways to give people routes towards growth and prestige.

### What is the force driving the new career?
Today, employers no longer guarantee lifelong employment. They only guarantee lifelong employability, where they guarantee that the skills that they teach will help an employee get employed anywhere. It assumes that an employee's energy should come from an employee's desire to benefit themself—to fill themselves with attractive experiences.

Although important, this isn't the key to a healthy career. The key is self-discovery—energy for a healthy career comes from discovering the talents that you already have. The employee's responsibility is to ask themselves if the role thrills them, if they seem to learn things quickly, if they're good in the role, and if the role brings them strength and satisfaction. They need to listen to the clues that indiciate that the role matches their talents.

Great managers use self-discovery differently:

1. They make self-discovery a central role, making it an explicit expectation of each employee.
   Help them look in the mirror. Every Sunday night, the employee should think about how they're feeling about the week. If their reaction is positive, they should think about what they're looking forward to. If the reaction is negative, they should think about what the role is lacking.
2. They emphasize that self-discovery is not for fixing non-talents.
   The point isn't to try to fill in skill gaps. The point is to learn about yourself so you can take better control of your career and make more informed decisions.

Managers help by leveling the playing field, holding up the mirror, and creating a safety net. 

To be good at holding up the mirror, you have to be good at giving performance feedback to help employees understand their style and what's possible with that style. You do this by having constant feedback (meaning regular). Each time, you start with a brief review of past performance to help employees think about their style and to spark conversation about the talents and nontalents that created this style. After this review, the conversation shifts to the future and how the employee can use their style to be more productive. This feedback is always given in private, one on one.

The most effective managers say that you should build personal relationships with your people. Getting to know them means knowing the practicalities and dramas of their personal life. You don't have to intervene, but you do have to know and care.

Great managers use trial periods to create a safety net where employees can focus on self-learning and career learning without having to worry as much about consequences. If you use a trial period, you should only use them for people who have shown talent and interest in a role. You need to be precise about the details. Additionally, it needs to be clear that the employee will be moved back to their previous role if either them or you is unhappy with the fit.

### How do great managers terminate someone and still keep the relationship intact?
It's hard to deal with struggling employees. As a manager, there are a number of decision to make:
- What level of performance is unacceptable?
- How long is too long at that level?
- Have you done enough to help with training, motivation, support systems, or complementary partnering?
- Should you break the news all at once, or should you give the employee a probationary period?
- When the final conversation happens, what words will you use?

Some managers evade these problems by hiring someone else or keeping their employees at arm's length. This isn't the way to go—instead, you need to show tough love. Tough love is a mindset that forces you to deal with issues early and directly while allowing you to keep your relationship intact.

Tough means not compromising on excellence. The answer to "What level of performance is unacceptable?" is any level that hovers around average with no trend upward. The answer to "How long is too long at that level?" is not very long.

Love still involves handling issues early, but doing it in a way that removes bitterness and ill will. This is done by understanding talent, which is important because it frees the manager from blaming the employee. Otherwise, since you told your employee what to do and they didn't do it, you end up thinking about them as stupid, weak-willed, disobedient, or disrespectful. By focusing on talents and non-talents, you can discuss how the employees non-talents make them a misfit in their current role, and what kind of role would fit their strengths.

Still, tough love doesn't make things easy. You want to make sure you understand your employees better than they understand themselves, which you accomplish by talking to them regularly and by having clear rationale and consistent language. Great managers believe that they always have the right to believe they know an employee better than the employee knows themselves, and that they should always help to get an employee what's right for them rather than what they want.

Manager assisted career suicide—sometimes someone knows they're performing poorly, and on some level they want help. This results in them subsconsciously putting themselves in situations where they'll struggle. If you think this is happening, you should put them out of their misery.

## 7. Turning the Keys: A Practical Guide
The four keys—select for talent, define the right outcomes, focus on strengths, and find the right fit—are the ways that managers use to turn talent into performance. The following are suggestions to turn these keys that you can take and integrate with your own style.

### Interviewing For Talent—What are the right questions to ask?
#### Make sure the talent interview stands alone.
The talent interview should be its own interview. It should discover if a candidate's recurring thought patterns, feelings, or behaviors match up with the job you're offering. Make it clear that your goal is to learn about their talents, and that the interview will be more structured and focused, less banter.

#### Ask a Few Open-Ended Questions and Then Try to Keep Quiet
You want the talent interview to allow someone to reveal themself through the choices they make. The talent interview should mirror, verbally, what the candidate will face on the job, behaviorally. They can respond in a myriad number of ways on the job—the important thing is how they *consistently* respond.

To get at this, ask open-ended questions and don't telegraph a "correct" direction. After asking the question, pause and remain silent. If they ask you to explain, deflect the question and say that you're interested in their interpretation. You want them to apply their filters. Whatever they say, believe them—even if you want to hear something else.

#### Listen for Specifics
Use past behavior to help predict future behavior. For this, you can rely on "Tell me about a time when you..." questions. 

For these questions you should be listening for specific examples—specific by time, by person, or by event. You also only want to give credit to their top-of-mind response. If it's a recurring behavior, it shouldn't be hard to think up of an example where they did this. Although you're listening for a specific example, you haven't explicitly asked for a specific example. If they don't give specifics, don't probe them for specifics. Additionally don't judge them on the response quality or details—the purpose of this isn't to find someone with a good memory. Instead, judge on specific and top of mind.

#### Clues to Talent
There are often small clues to what someone's talented at. There are two in particular that are good to focus on in interviews; if you direct your questions in those directions, you should be able to help reveal their talents.

##### Rapid Learning
You learn new things in steps. Sometimes, no matter how hard you practice, you are forced to keep relying on those steps. With other activities, things become smooth and the steps fall away. In the latter, what you're learning is just following a mental pattern that you're already used to.

Ask what kind of roles someone's been able to learn quickly. Also ask what kind of activities come easily to them now. 

##### Satisfactions
Look for what gives someone the greatest satisfaction. Figure out what gives them strength and what they find fulfilling. Those answers will tell you what they can keep doing week after week.

#### Know What To Listen For
Like others, great managers have a list of their favorite questions. However, great managers ask questions to which they know how their top performers respond.

To develop these questions, you can ask your current employees and see if the stronger performers answer differently. Or, ask the question to all new applicants, then see how they end up performing.

### Performance Management—How do great managers turn the last three keys every day with every employee?
The remaining three keys can't be treated in isolation. They need to be turned at the same time. The best managers do so by following a performance management routine that forces them to stay focused on each person's performance. Although the routines can be different, they have similar elements:

1. They are simple. 
   They avoid bureaucracy and allow the manager to focus on what to say to each employee and how to say it.
2. The routine forces frequent interaction between the manager and the employee.
   At a minimum, this should be semi-anually. More frequent interactions keep events more fresh in each party's mind. This allows employees to remember how they felt, and this allows managers to remember the specific things they'd change. This also allows you to bring up poor performance little by little over time.
3. The routine is focused on the future.
   You do have to talk about past performance to highlight someone's style or needs. However, the main focus is on the future, focusing on goals, metrics, the path to those goals, and how the manager can help.
4. The routine asks the employee to keep track of their own performance and learnings.
   A routine should have the employee keeping track of their own goals, successes, and discoveries. The employee should be able to share their short-term goals, but everything else can be private. The point of this is to allow the employee to be aware of how they themselves are doing.

#### The Strengths Interview
This is to be done once a year, or shortly (1-2 weeks) after someone gets hired. Spend about an hour answering these questions:

1. What did you enjoy most about your previous work experience?
   What brought you here? / What keeps you here?
2. What do you think your strengths are? (skills, knowledge, talent)
3. What about your weaknesses?
4. What are your goals for your current role? (with scores and timelines)
5. How often would you like to meet with me to discuss your progress?
   Are you the kind of person who will tell me how you're feeling, or will I have to ask?
6. Do you have any personal goals or commitments you would like to tell me about?
7. What is the best praise you have ever received?
   What made it so good?
8. Have you had any really productive partnerships or mentors?
   Why do you think these relationships worked so well for you?
9. What are your future growth and career goals?
   Are there any particular skills you want to learn?
   Are there some specific challenges you want to experience?
   How can I help?
10. Is there anything else you want to talk about that might help us work well together?

The main purpose of this is to learn about the employee's strengths and goals, *as they perceive them*. Write down what they say, even if you disagree. In order to help someone, you have to understand where they're coming from. Schedule performance planning meetings using the interval they gave in answer 5.

#### Performance Planning Meetings
To prepare for this meeting, have your employee write their answers to these three questions in advance:

1. What actions have you taken?
   Their answer should reflect performance over the last period, including quantitative metrics if available.
2. What discoveries have you made?
   This can be as a result of classes, something they did on the job, a book, whatever. Encourage them to keep track of their own learning.
3. What partnerships have you built?
   Any relationship, new or existing, internal or external, is fair game. They should take responsibility for their constituency within the company.

Ask them these questions at the beginning of the meeting and jot down their answers. It's nice if they share their written version with you, but don't demand it. Use their answers as a starting point to discuss their performance over the last period.

After around 10 minutes, direct the conversation towards the future:

4. What is your main focus?
   What are your primary goal(s) for the next period?
5. What new discoveries are you planning?
   What specific discoveries are they hoping to make over the next period?
6. What new partnerships are you hoping to build?
   How will they grow their constituency over the next period?

Recommend that they write down their answers. You should discuss their answers, agree to them, and keep your copy. These will be your specific expectations over the next period.

Use this template with each period. With the patterns that show up, help perfect their style, set appropriate expectations, and discuss what you can do to run interference.

#### Career Discovery Questions
At some point, employees will want to talk about their career options. They may want to know where you think they should go next. This will be handled over many different conversations, at different times. You need to ensure that, over time, two things happen.

First, the employee becomes increasingly clear about their skills, knowledge, and talents. Second, they need to know what the next step will entail and why they think they would be good at it. They must come to this understanding on their own. However, some questions can help prompt their thinking:

1. How would you define success in your current role? Can you measure it? Here's what I think.
2. What do you that makes you as good as you are? What does this tell you about your skills, knowledge, and talents? Here is what I think.
3. Which part of your current role do you enjoy the most? Why?
4. Which part of your current role are you struggling with? What does this tell you about your skills, knowledge, and talents? What can we do to manage around this? (Training, positioning, support system, partnering.)
5. What would be the perfect role for you? Imagine you're in that role. It's 3 p.m. on a Thursday. What are you doing? Why would you like it so much? Here is what I think.

### Can an employee turn these keys?
Managers can't make an employee productive. They're just catalysts who speed things up. But this all requires major effort from the employee. This is what great managers expect from employees:

- Look in the mirror every chance you get.
  Use feedback to increase your understanding of who you are and how others perceive you.
- Muse.
  Spend 20-30 minutes every month and reflect on the past few weeks. What did you accomplish? What did you learn? What did you love / hate? What does this say about you and your talents?
- Discover yourself.
  Use a deeper understanding of yourself to put yourself in better situations where you will shine.
- Build your constituency.
  Identify what kind of relationships work well for you, then seek them out.
- Keep track.
  Build your own record of your learnings and discoveries.
- Catch your peers doing something right.