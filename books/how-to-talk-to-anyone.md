# How To Talk To Anyone
Leil Lowndes

## Part 1: How to Intrigue Everyone Without Saying a Word
1. How to Make Your Smile Magically Different
2. How to Strike Everyone as Intelligent and Insightful by Using Your Eyes
3. How to Use Your Eyes to Make Someone Fall in Love with You
4. How to Look Like a Big Winner Wherever You Go
5. How to Win Their Heart by Responding to Their "Inner Infant"
6. How to Make Someone Feel Like an Old Friend at Once
7. How to Come Across as 100 Percent Credible to Everyone
8. How to Read People Like You Have ESP
9. How to Make Sure You Don't Miss a Single Beat

## Part 2: How to Know What to Say Ater You Say "Hi"

## Part 3: How to Talk Like a VIP
24. How to Find Out What They Do (Without Even Asking!)

### 25. How to Know What to Say When They Ask, "What Do You Do?"
Rather than directly answering with your job, answer how your job benefits other people.

- Cerberus engineer: I help people get started on Yext.

### 26. How to Sound Even Smarter Than You Are
Increase your vocabulary--replace "overused" words that you use every day. These are words like smart, nice, pretty, or good. This can be done gradually--you just need ~50 new words in your vocabulary, and you can look up one synonym you like per day.

Examples: 
- smart: ingenious, resourceful, adroit, shrewd
- pretty/great: stunning, elegant, ravishing
- wonderful: splendid, superb, extraordinary, magnificent, remarkable, glorious 

#### My Words
- good
- great
  - fantastic
- smart
- sucks