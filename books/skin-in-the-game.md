# Skin in the Game
Nassim Nicholas Taleb
[Amazon](https://www.amazon.com/Skin-Game-Hidden-Asymmetries-Daily/dp/B077BSK9LC)

## Book 1: Introduction
This book is about four topics:

1. uncertainty and the reliability of knowledge (bullshit detection)  
	This is the difference between theory and practice, between shallow and true expertise.
2. symmetry in human affairs—fairness, justice, responsibility, reciprocity  
	If you have rewards, you should get some of the risks. If you inflict risk on others, you should pay some price if they are harmed. Responsibility should be fairly shared.
3. information sharing in transactions  
	How much information should or shouldn't you share with others?
4. rationality in complex systems and in the real world  
	Rationality here is, statistically, what will help you survive.

> But never engage in detailed overexplanations of _why_ something is important: one debases a principle by endlessly justifying it.

_A nice thought on teaching._

### Prologue, Part 1: Antaeus Whacked
### Prologue, Part 2: A Brief Tour of Symmetry
### Prologue, Part 3: The Ribs of the Incerto