# MCDP 1 Warfighting
USMC

## 1. The Nature of War
> Everything in war is simple, but the simplest thing is difficult. The difficulties accumulate and end by producing a kind of friction that is inconceivable unless one has experienced war. --Carl von Clausewitz

Before we can talk about how to approach war, we first have to understand its nature. We have to understand the physical, mental, and moral components and challenges.

