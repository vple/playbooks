# Unmasking The Face
Paul Ekman, Wallace V. Friesen  
[Amazon](https://www.amazon.com/Unmasking-Face-Wallace-V-Friesen-ebook/dp/B002NGO5IC)

## 2. Why Mistakes Are Made In Understanding Facial Expressions of Emotion
Facial expressions provide messages, but you need to be able to recognize the different signals to be able to understand the message. There are three types of signals: **static**, permanent aspects of the face; **slow**, gradual changes in facial expression over time (typically with age); **rapid**, caused by facial muscles and displaying for at most a few seconds. This book focuses on rapid signals, as they are the most controllable and what we typically use to read people.

***Facial expressions can be misread because they do more than just express emotion.*** They can relate to moods, where feelings involve last over a longer period. They can also be used to show *emblematic messages*, which have a very specific meaning understood within a culture (such as a wink). Expressions can also be used to as *conversation punctuators*. Being able to recognize emotions well also means being able to distinguish them from these other expressions.

***Facial expressions aren't read when people don't see each others' faces.*** There are many reasons for not seeing others' faces (typically practical or cultural). Regardless of the reason, you can't read someone's face if you don't see it.

***Facial expressions get lost amongst the other information you're processing.*** When talking, you're exposed primarily to visual and auditory information. There's a lot of it, and you will naturally pay attention to some more than others. Additionally, you cannot filter out some information, such as auditory information—you can't turn off your ears. With all of this other information, it can be hard to recognize what you're seeing from facial expressions.

Finally, ***facial expressions can be consciously controlled***. This means that people may show false or distorted expressions that must be recognized as not being fully genuine.

## 3. Research on Facial Expressions of Emotion
Before analyzing facial expressions, we want to know that analyzing these expressions will actually tell us something. There are a number of concerns covered by various studies.

***People can identify emotions from expressions***, as opposed to just recognizing if someone was feeling positive or negative. When people consistently read an expression in the same way, this implies that there are shared properties in that expression. We don't know all of the emotions that are commonly recognized, but the six covered in this book are widely recognized. There are also several blends of these emotions, allowing for a much greater range of expression.

***People can accurately identify emotions from expressions.*** Above we're concerned with if people agree on the meaning of an expression. Here we want to know if their understanding is correct. Many studies have to approach this somewhat indirectly, but they do show that people are able to roughly categorize what others are feeling.

***There are universal facial expressions for some emotions.*** Studies show that, regardless of culture, everyone has the same expressions for certain emotions (including those in this book). It is worth noting that the cause of the emotion can differ between cultures. Additionally, cultures differ in how they control the expressions that they express.

This book focuses on research in recognizing expressions. The main approach in recognizing expressions is to classify how three regions of the face move to express certain emotions. These regions are the brow/forehead, eyes/lids and root of the nose, and the lower face.

It also focuses on how people control their expressions. There's nothing conclusive yet, but it's believed that when someone has *nonverbal leakage* when they try to conceal an emotion.

## 4. Suprise
> Surprise is the briefest emotion. It is sudden in its onset. If you have time to think about the event and consider whether or not you are surprised, then you are not. You can never be surprised for long, unless the surprising event unfolds new surprising elements. It doesn't linger. When you cease being surprised, its disappearance is often as sudden as was it [sic] onset.

Surprise is triggered from *unexpected* and *misexpected* events. Unexpected events are those that you weren't anticipating. Misexpected events are those where you were anticipating something else to happen. Almost anything can be surprising, but it lasts only until you've evaluated what happened.

Surprise often quickly transitions into another emotion, based on how you've evaluated the event. Although surprise itself is neutral, we tend to classify our surprise as positive or negative based on the following emotion. Since the surprise expression is so quick, it often blends into the following expression.

### Appearance

#### Brow
When surprised eyebrows get raised—they are curved and high. This can cause horizontal wrinkles on the forehead. This expression can occur without other common surprise expressions. When it does, it isn't always communicating surprise, such as when someone is expressing doubt or questioning.

#### Eyes
Eyes are opened wide when surprised. In particular, you can see the sclera (white of the eye) *above* the iris.

#### Lower Face
The jaw can drop when surprised, as if it fell open.

#### Intensity
The brows and eyes can be affected a bit more when someone is more surprised. The biggest indicator of intensity tends to be how much the jaw drops.

#### Types of Surprise
Different combinations of areas of surprise imply slightly different meanings:

- *questioning surprise*—expressed with brows and eyes
- *astonished/amazed surprise*—expressed with eyes and lower face
- *dazed/less interested surprise*—expressed with brows and lower face
- *surprise*—all three regions

## 5. Fear
## 6. Disgust
## 7. Anger
## 8. Happiness
## 9. Sadness
## 10. Practice Faces
## 11. Facial Deceit
## 12. Checking Your Own Facial Expression
## 13. Conclusion