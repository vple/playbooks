# Grit
Angela Duckworth  
[Amazon](https://www.amazon.com/Grit-Power-Passion-Perseverance/dp/B01D3AC5VU)

## What Grit Is And Why It Matters
### 1. Showing Up
> Who are the people at the very top of your field? What are they like? What do you think makes them special?

Part of excelling involves tackling challenges that exceed your current skill level. But not everyone persists through those challenges—some people drop out. Those people who do succeed don't do so because they think they have what it takes. Instead, they know what direction they want to go in and find pleasure from continually working to move in that direction. They persist regardless of how difficult things are.

This trait is referred to as "grit." It's not correlated with talent. Grit is the best indicator of who will persist or remain in a certain role over time.

### 2. Distracted By Talent
We like and tend to believe that those who are talented are the ones who are most likely to be successful. But really, talent is just where you start. Where you end up is determined by the effort you put in.

Since most people believe talent is responsible for success, they tend to fixate on talent metrics. But really, to help people succeed, it's more important to show them that their current talent isn't what matters—it's how they apply their time and effort.

### 3. Effort Counts Twice
> For if we think of genius as something magical, we are not obliged to compare ourselves and find ourselves lacking. ... To call someone "divine" means: "here there is no need to compete." —Nietzsche

Our accomplishments tend to be the results of countless individual elements, most (or all) of which are ordinary. But we still attribute this to talent or genius because, as Nietzsche says, this gives us an escape and excuse for us not reaching that same level of accomplishment.

Talent x effort = skill. Skill x effort = achievement. Your natural talent determines how quickly you can learn skills. But it doesn't determine how much you will achieve. It just changes how fast you can get there. Effort is really what matters, as it's what is needed to develop skills and accomplish achievements.

Grit is where we are persistent in working towards a goal. We keep coming back to work on it every day, no matter how things went on previous days. Grit is about stamina.

### 4. How Gritty Are You?
|      | Not at all like me | Not much like me | Somewhat like me | Mostly like me | Very much like me |
| ---- | ---- | ---- | ---- | ---- | ---- |
| 1. New ideas and projects sometimes distract me from previous ones. | 5 | 4 | 3 | 2 | 1 |
| 2. Setbacks don't discourage me. I don't give up easily. | 1 | 2 | 3 | 4 | 5 |
| 3. I often set a goal but later choose to pursue a different one. | 5 | 4 | 3 | 2 | 1 |
| 4. I am a hard worker. | 1 | 2 | 3 | 4 | 5 |
| 5. I have difficulty maintaining my focus on projects that take more than a few months to complete. | 5 | 4 | 3 | 2 | 1 |
| 6. I finish whatever I begin. | 1 | 2 | 3 | 4 | 5 |
| 7. My interests change from year to year. | 5 | 4 | 3 | 2 | 1 |
| 8. I am diligent. I never give up. | 1 | 2 | 3 | 4 | 5 |
| 9. I have been obsessed with a certain idea or project for a short time but later lost interest. | 5 | 4 | 3 | 2 | 1 |
| 10. I have overcome setbacks to conquer an important challenge. | 1 | 2 | 3 | 4 | 5 |

Your average score is a measure of your grit. The 10th percentile is 2.5, 50th percentile is 3.8, and 90th percentile is 4.7.

Grit is actually composed of two factors: passion and perseverance. The odd numbers measure your passion and the even numbers measure your perseverance. Typically, people score a bit higher on perseverance.

> Do you have a life philosophy?

Your passion is what you want to accomplish in your life. It's the ultimate goal that you want to accomplish in life. Consistently following your passion over time means all of the smaller goals you work towards ultimately helps you move towards your main goal.

A common source of problems for people is that they their goals are nonexistent or unfocused. They don't know what intermediate goals they need to accomplish, or their goals are too scattered. Although it's likely impossible to aim for a single top-level goal, you should aim for a very small number. For a professional goal, ideally you only have one goal.

Persistence comes from consistently working towards that top goal. It doesn't mean sticking to all of your goals, and in fact you should be able to be flexible about your lower level goals. If they don't or can't pan out, you can still reach your ultimate goal—you just need to find a different route through other goals.

Put in other words, passion combines:

- "Degree to which he works with distant objects in view (as opposed to living from hand to mouth). Active preparation for later life. Working toward a definite goal."
- "Tendency not to abandon tasks for mere changeability. Not seeking something fresh because of novelty. Not 'looking for a change.'"

Perseverance combines:

- "Degree of strength of will or perseverance. Quiet determination to stick to a course once decided upon."
- "Tendency not to abandon tasks in the face of obstacles. Perseverance, tenacity, doggedness."

### 5. Grit Grows
How genetic is grit? It has some influence from genetics, but no more than typical personality traits. Comparing grit to age shows that older people tend to have more grit, indicating that it's some combination of your generation and your age affects your grit.

One of the main drivers of grit is necessity—we don't change unless we need to.

Generally, when we quit something, it's for one of the following reasons:

- I'm bored.
- The effort isn't worth it.
- This isn't important to me.
- I can't do this, so I might as well give up.

There are four core traits that gritty people have: interest, capacity to practice, purpose, and hope. Each of these combats the reasons above.

## Growing Grit From The Inside Out
### 6. Interest
Common advice is to follow your passion. People are more satisfied and do a better job when they work on something they're interested in. But many people don't have a passion and don't know how to find theirs.

In actuality, finding a passion often involves spending several years explording different interests. The one that they end up finding a passion for is the one that they end up spending all of their waking thoughts about. Finding a passion takes time. Instead of trying to find a perfect passion for you, you should just look for one that has potential.

> One of the huge mistakes people make is that they try to *force* an interest on themselves. —Jeff Bezos

Childhood is often too early to know what you want to do. Your interests are also not discovered from introspection, but from interacting with the outside world. Often, discovering an interest is unnoticed by the discoverer.

After discovering an interest, you then spend a much longer period of time with interest development. Part of this period involves having encounters that continually retrigger your attention. 

This is aided by having encouraging supporters, especially at the start of developing an interest. These supporters make your initial learning pleasant and rewarding. Overbearing influences reduce motivation.

### 7. Practice
Deliberate practice involves just not more time spent practicing, but the quality of the practice. This involves focusing on a single aspect to practice on, fully concentrating on your practice, quickly getting useful feedback, and reflecting on how you did.

Deliberate practice is often mentally demanding, which leads to people taking naps after practice.

### 8. Purpose
Purpose: the intention to contribute to the well-being of others. Many people start with a self-interest and eventually transition to including a purpose for others. Gritty people tend to have purpose. Having personal and prosocial interests tends to help you in the long run.

There are two key parts to someone developing purpose. They have to see someone else showing that it's possible to achieve something on behalf of others. They also to realize that they, personally, can make a difference.

Ways to cultivate purpose:

1. Reflect on how your work can contribute positively to society.
2. Think about how you can make small changes in your work to make it more connected to your core values.
3. Find inspiration in a purposeful role model.

### 9. Hope
Typically, we see hope as believing that tomorrow will be better. For grit, hope means believing that our efforts can improve our future.

A factor in this is learned helplessness, where people who believe they can't control their suffering subsequently tend to feel hopeless in future scenarios. This tends to be developed in adolescence.[^rebel] Experiencing adversity during adolescence helps you deal with adversity differently later on.

[^rebel]: Is this why teenagers rebel? To learn optimism / resilience?

Resilient people are also often optimistic. They see their suffering as temporary and caused by specific things. In contrast, pessismists view suffering as hard-coded and intrinsic to them. How we interpret things just depends on how we present them to ourselves.

> Whether you think you can, or think you can't—you're right. —Henry Ford

We can demonstrate hope in both our language and in our actions. It's important to do both, otherwise the message we give to others will be incongruent.

## Growing Grit From The Outside In
### 10. Parenting For Grit
> What can I do to encourage grit in the people I care for?

It's important to both show people that you care for them, but also that you are going to push them to be their best. They need to be loved and accepted, but also taught. Being supportive and being demanding are both required. The person's interests come first, but they may not be the best judge of what to do, how hard to work, or when to give up on things.

> Number one, a parent needs to set a stage that proves to the child, "I'm not trying to just have you do what I say, control you, make you be like me, make you do what I did, ask you to make up for what I didn't do."

You can help cultivate grit by finding things for people to do that fall right outside of their comfort zones.

### 11. The Playing Fields Of Grit
Grit is enhanced through structured extracurriculars. There's no conclusive evidence for this, but kids doing extracurriculars generally fare better on any metric. It's unclear if gritty people do extracurriculars or if extracurriculars build grit, but Duckworth suspects that it's a mix of both.

As with learned helplessness, people can also develop learned industriousness. This is where they learn that their hard work leads to rewards. 

### 12. A Culture Of Grit
> If you want to be grittier, find a gritty culture and join it. If you're a leader, and you want the people in your organization to be grittier, create a gritty culture.

People will naturally fall in line with the standards of the group, so people will become grittier if they join a gritty culture. Creating that culture involves relentless communication and continually experimenting to improve things.

One activity, used by Anson Dorrance, is the Beep Test. This is where athletes must progressively run laps at a faster and faster pace, dictated by a beep. The coach tells the athletes that the beep test proves to the coach how much self-discipline they have from training (since last year) and/or the mental toughness to handle the increasing difficulty.

Another activity is to require team members to memorize literary quotes that reflect core values. They are tested regularly and expected to understand the quotes.