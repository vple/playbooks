# Software Engineer Book Recommendations

## Technical
- [Professor Frisby's Mostly Adequate Guide to Functional Programming](https://mostly-adequate.gitbooks.io/mostly-adequate-guide/)  
  It's important to understand the concepts behind functional programming, as they can be used in many places outside of FP. These techniques can help you write cleaner, more correct code. Understanding concepts such as function signatures will also help you design much better methods. Good to read once you're looking into how to write better code, after becoming comfortable with software development.
- Structure and Interpretation of Computer Programs  
  For engineers interested in a deeper, more technical understanding of code. While you don't have to be senior, you should have a more mature understanding of code. Recommended to have some exposure to functional programming first, as it will help with understanding some of the concepts.

## Process
- The Phoenix Project  
  Useful for senior individual contributors and beyond. This book will help you to see where inefficiencies are in your team's workflow, as well as how your time and effort can be better applied.