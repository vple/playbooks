# Software Engineering Manager Book Recommendations

## Essentials
- The Coaching Habit  
  A quick, easy read. Presents a simple framework to coaching. While it's ideally read early on in your management career, it can be applicable any time.
- Extreme Ownership  
  Explains a handful of concepts on what it means to be a leader, based on learnings from Navy SEALs. Valuable for understanding the responsibility and ownership that you need to bear as a leader.
- The Phoenix Project  
  While it's ostensibly a book on DevOps, it also shows what good management is capable of doing. The lessons taught about the nature of work are extremely important to understand when it comes to management.
- An Elegant Puzzle
  Gives a good high-level overview of key management scope, as well as many tactics and ways of thinking on how to manage these different topics effectively.

## People
- Managing Humans  
  Easy, anecdotal read with various tools to keep in your toolkit. Includes patterns you'll find yourself in, how to approach certain situations, and general tools for interacting with people. Good for new/early managers.
- Trillion Dollar Coach  
  Coaching practices used by Bill Campbell, legendary Silicon Valley coach. Applicable for any manager, but likely has further meaning as you improve as a manager.
- First, Break All The Rules  
  Data-backed findings on what makes effective management. May not be necessary if your organization already has strong management practices.

## Leadership
- The Dichotomy of Leadership  
  Sequel to Extreme Ownership. Dives more in depth into those concepts and focuses on how there are unreasonable extremes for any principle.

## Management Mechanics
- Toyota Kata
  Good to read for managers who are now taking their role more seriously and want to improve their processes to create a productive environment.