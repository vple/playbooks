# Accelerate
Nicole Forsgren, Jez Humble, Gene Kim
[Amazon](https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations-ebook/dp/B07B9F83WM/)

## Quick Reference: Capabilities To Drive Improvement
### Continuous Delivery
1. Version control
2. Deployment automation
3. Continuous integration
4. Trukn-based development
5. Test automation
6. Test data management
7. Shift left on security
8. Continuous delivery

### Architecture Capabilities
9. Loosely coupled architecture
10. Empowered teams

### Product and Process Capabilities
11. Customer feedback
12. Value stream
13. Working in small batches
14. Team experimentation

### Lean Management and Monitoring Capabilities
15. Change approval processes
16. Monitoring
17. Proactive notification
18. WIP limit
19. Visualizing work

### Cultural Capabilities
20. Westrum organizational culture
21. Supporting learning
22. Collaboration among teams
23. Job satisfaction
24. Transformational leadership

## Part 1: What We Found
### 1. Accelerate
<!-- TODO -->

### 2. Measuring Performance
Measuring performance in software is hard. People typically focus on outputs, rather than outcomes. They also focus on individual or local measures instead of global ones. This causes issues where the metric isn't properly aligned with desired results or where there ends up being conflict between different divisions.

Instead, we want to focus on global outcomes and to focus on outcomes over outputs. We use four measures for delivery performance:

1. delivery lead time
2. deployment frequency
3. mean time to restore service (MTTR)
4. change fail rate

**Lead time** is the time to go from a customer making a request to the request being satisfied. There are two parts to this lead time: the time to design and validate a product or feature, and the time to deliver the feature to customers. The first part, design, is hard to measure and has high variability. The second part, delivery, is easier to measure and has less variability. Shorter delivery lead times are better—they give more feedback and allow for faster incident response.

Product delivery lead time is measured as the time it takes to go from code committed to code successfully running in production.

**Deployment frequency** relates to batch size. Smaller batches result in shorter cycle times and less variability, in addition to a number of other benefits. Batch size is hard to measure, so deployment frequency is used as a proxy.

Lead time and deployment frequency focus on *tempo*. The other two metrics focus on *stability*.

**Mean time to restore** is how fast service can be restored when there is a failure. **Change fail percentage** is the percentage of production changes that fail, result in degraded service, or require remediation.

Strong performers consistently do well in all four of these categories. Software delivery performance actually predicts organizational performance and noncommercial performance.

Once these metrics are in place, we can use them to drive decisions on how to improve team performance.

### 3. Measuring and Changing Culture
There are three levels of organizational culture: basic assumptions, values, and artifacts.

**Basic assumptions** are the things that members of the org have interpreted from relationships, events, and activities. They are typically hard to "see," instead being the things that we just know and have trouble articulating.

**Values** are more visible and can be discussed and debated, if people are aware of them. People view and interpret relationships, events, and activities through their values. Values also help define social norms. Values are typically what people think of when they think about culture.

**Artifacts** are the most visible level, often made up of mission statements, formal procedures, rituals, etc.

Ron Westrum has defined various characteristics about organizational cultures. There are three main types of organizational cultures:

- **Pathological**, defined by fear and threats. Information is typically hoarded or held for political reasons.
- **Bureaucratic**, focused on rules. People within departments are focused on doing things by the book, and specifically their book.
- **Generative**, focused on performance. Everything is focused on good performance.

Organizational culture predicts how information flows through the organization. Good information:

- Provides answers to questions that the receiver needs answered.
- Is timely.
- Is presented in a way that can be effectively used by the receiver.

Good information flow leads to safe and effective operations, as well as high-tempo and high-consequence environments. Good organizational cultures also predict performance outcomes.

#### Measuring Culture
Westrum's culture model can be measured with strongly worded Likert-type questions:

- Information is actively sought.
- Messengers are not punished when they deliver news of failures or other bad news.
- Responsibilities are shared.
- Cross-functional collaboration is encouraged and rewarded.
- Failure causes inquiry.
- New ideas are welcomed.
- Failures are treated primarily as opportunities to improve the system.

#### Westrum Organization Culture
This culture requires trust and cooperation between people. Having a good organizational culture enables better decision making, both in terms of quality of decisions and in terms of being able to more easily reverse bad ones. These cultures also do a better job with their people, since issues are discovered faster and addressed. In fact, culture predicts software delivery performance, organizational performance, and more job satisfaction.

Continuous delivery and lean management positively impact culture.

### 4. Technical Practices
Technical practices are key to achieving better software releases. This chapter focuses on continuous delivery practices, which improve software delivery performance, organizational culture, and other things such as team burnout or deployment pain.

Continuous delivery allows changes to get into production safely, quickly, and sustainably. There are five key principles behind continuous delivery:

- Build quality in.  
  Deal with problems when they're cheap to detect and to resolve.
- Work in small batches.  
  Ensure that each batch of work is actually delivering the value that we expect it to. Keep the cost of making changes low.
- Computers perform repetitive tasks; people solve problems.  
- Relentlessly pursue continuous improvement.  
  Improvement is part of everybody's daily work.
- Everyone is responsible.  
  Set organizational goals where everyone is responsible for ensuring that outcomes are achieved.

Implementing continuous delivery requires *comprehensive configuration management*, *continuous integration*, and *continuous testing*.

There are nine capabilities that drive continuous delivery. Some of these are discussed here; others later on.

- Version control.  
  System and application configuration should also be version controlled, not just application code itself.
- Test automation.  
  Tests should be reliable. Developers should focus on acceptance tests.
- Test data management.  
  Teams should have adequate test data and should be able to acquire test data as needed. Test data should not be a bottleneck on testing.
- Trunk-based development.  
  Keep development in sync with master as much as possible.
- Information security.  
  Integrate information security into the delivery process.

### 5. Architecture
#### Types Of Systems
- greenfield—a new system that hasn't been released
- system of engagement—used directly be end users
- system of record—used to store business-critical information, particularly where data consistency and integrity are critical
- custom software developed by another company
- custom software developed in-house
- packaged, commercial off-the-shelf software
- embedded software running on a manufactured hardware device
- software with a user-installed componenet
- non-mainframe software running on servers operated by another company
- non-mainframe software running on self-owned servers
- mainframe software

Low performers typically have custom software developed by other companies and are more likely to be working on mainframe systems. No other factor has a significant correlation between system type and delivery performance.

#### Deployability and Testability
<!-- TODO -->

### 6. Integrating Infosec into the Delivery Lifecycle
<!-- TODO -->

### 7. Management Practices for Software
<!-- TODO -->

### 8. Product Development
<!-- TODO -->

### 9. Making Work Sustainable
<!-- TODO -->

### 10. Employee Satisfaction, Identity, and Engagement
<!-- TODO -->

### 11. Leaders and Managers
<!-- TODO -->

## Part 2: The Research
### 12. The Science Behind This Book
<!-- TODO -->

### 13. Introduction to Psychometrics
<!-- TODO -->

### 14. Why Use a Survey
<!-- TODO -->

### 15. The Data for the Project
<!-- TODO -->

## Part 3: Transformation
### 16. High-Performance Leadership and Management
<!-- TODO -->