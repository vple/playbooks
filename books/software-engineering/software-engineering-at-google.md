# Software Engineering at Google

## 1. What Is Software Engineering?

- Involves an understanding of time, scale, and trade-offs at play.
- What is the expected lifespan of your code?
  - One-time scripts have different requirements from "forever" systems.
  - _How can this be communicated to non-technical stakeholders?_
- Sustainable: you can react to any change (technical or business) for the expected lifespan of your software.
  - You must be _capable of_, but may choose not to based on other constraints.
- As the org, projects, and systems grow, does it become more efficient at producing software?
  - Software engineers & leaders are responsible for sustainability & management.
  - Includes organization, product, and development workflow.
- Transitioning a project from start --> long lifespan can be painful, especially if it is done infrequently.
  - Ability to regularly upgrade contributes/leads to sustainability.
- "Happens to work" vs. "Is maintainable"
  - Hyrum's Law: With a sufficient number of users of an API, it doesn't matter what is promised in the contract: all observable behaviors of the system will be depended on by somebody.
  - Are you aware of your assumptions & dependencies?
  - Change isn't always necessary, but it's important to be capable of it.

### Levelling (Personal Thoughts)

#### ENG 1 --> ENG 2

- Should be competent in company methodologies and procedure.
- Not expected to understand how to be sustainable, but should be introduced to it.
- Should be introduced to the concept of sustainability.
- Must follow sustainable best practices relating to programming/implementation. "Happens to be" sustainable.
  - Avoids making things less sustainable.
  - Avoids rookie / "obviously bad" / immediately wrong or exploitable stuff.
    - Enumerate these (case by case learning).
    - Begin to identify "bad" things.
- Begin to recognize assumptions.
  - Aware of Hyrum's Law.
- Happens to be sustainable (thanks to guidance), but is not intentionally sustainable.

#### ENG 2 --> ENG 3

- Intentionally sustainable for all individual / programming / implementation tasks.
  - Includes technical concerns, "reasonable" edge cases (doesn't disregard them).
  - Begins to understand system sustainability (happens to be sustainable).
    - Doesn't break stuff.
  - Does not have to be sustainable around product shifts.

#### ENG 3 --> ENG 4

- Intentionally sustainable at a system level. Including product changes.
- Unintentionally sustainable at an ecosystem level.
- Must be able to identify unsustainable systems.
  - Also, make/push for appropriate change based on judgment.


#### Meta Sustainability

- You want people to start recognizing and starting initiatives around meta sustainability.
- Eliminating human / manual verification.
- Roughly, people should be able to be meta sustainable about things ~2 levels below them. (Ideally, just 1 level up.)
- Initiatives decided based on reliability (with best effort) and/or total (non-scaling) effort.

### Scale & Efficiency

- Cost: Is this growing (sub) linearly?
  - Human cost.
  - Software resources (compute, memory, storage, bandwidth).
  - Development resources.
  - Codebase.
    - Cost for full build? Fresh pull? Language upgrade? Build system?
- Policy scaling.
  - How much does a single engineer have to keep up with as an org scales?
  - Example: deprecation. If you farm out work when deprecating something, this scales based on # of deprecations & teams.
    - Instead, have the deprecating team be responsible for migrating everyone.
      - This team will develop the most expertise, which also lets them do things faster and more efficiently.
  - Expertise scales.
    - Pattern matching, deeper understanding, scriptable.
    - Less wasted effort.
      - You don't have to ramp up and then "throw away" the now-useless knowledge.
      - In other words, reduce temporary efforts.
  - Beyonce rule: If you liked it, you should have put a CI test on it.
    - In general: reduce friction points preemptively.
    - Give people a technical format to express what's important to them.
  - Spread knowledge + expertise.
    - Shared forums = good.
    - Clearing stumbling blocks provides values.
  - Improve after pain points.
  - Codebase scaling factors: expertise, stability, conformity, familiarity, policy.
  - Shift left --> it's less expensive.

### Trade Offs
  - There should be a decider.
    - You want "consensus". Consensus = everyone understands the reasoning behind a decision.
    - You aren't trying for unanimity.
  - Cost: financial, resource, personnel, transaction, opportunity, societal.
  - Health of an organization: cash on hand, but also productive members.
    - Lucrative fields: personnel >> financial.
    - Good as an indicator.
  - Decisions should ideally be:
    - Because it must be done.
    - Because it's the best known course based on available information.
  - Weighing data:
    - Try to get comparable quantities. Measurable and/or estimable.
    - Suble / hard-to-measure data is hard to deal with. Treat it with priority & care and do the best you can.
  - You can provide ratios (e.g. cost of memory to cost of storage) to help people weigh decisions. You can do this even when you can't provide the actual numbers.
  - Be able to admit mistakes.
  - Be able to revise data, when there is new data.

## 2. How to Work Well on Teams

- Software development is a team endeavor.
  - You need to understand your own reactions, behaviors, and attitudes to work well on a team.
  - Core principles: humility, respect, trust.
- People want to hide their work when they feel insecure.
- Working alone adds unnecessary failure and reduces growth potential.
  - Hard to determine if you're on the right track.
  - Early sharing allows early detection.
  - Sharing reduces bus factor.
    - Ideally, have at least good documentation. Also have a primary & secondary owner.
- Social skill pillars: humility, respect, trust.
  - Humility: You're not the most important. You make mistakes. You can improve.
  - Respect: Others matter. Treat them well, appreciate what they bring to the table.
  - Trust: Believe others are competent and will do the right thing.
- Relationships get things done. Relationships also outlast projects.
- Tactic: introduce an idea with a team in advance before applying it. Ask them to try it out.
- You are not your code.
- Blameless postmortems.
  - Also, documented failures.
- Googley:
  - Thrives in ambiguity
  - Values feedback
  - Challenges status quo
  - Puts the user first
  - Cares about the team
  - Does the right thing

## 3. Knowledge Sharing

> Your organization should be able to answer most of its own questions. To achieve that, you need both experts who know the answers to those questions _and_ mechanisms to distribute their knowledge, which is what we'll explore in this chapter. [...] Most importantly, however, your organization needs a _culture of learning_, and that requires creating the psychological safety that permits people to admit to a lack of knowledge.

- Obstacles to learning:
  - lack of psychological safety
  - information islands
    - information fragmentation
    - information duplication
    - information skew
  - single point of failure
    - One person does everything, so others don't learn it.
  - all or nothing expertise
  - parroting
  - haunted graveyards
- Documentation is very scalable.
  - There are trade-offs: it may be more generalized and less individually applicable.
  - Has a maintenance cost.
- Tribal knowledge: the gap between what people know and what is documented.
- Targeted human help is also useful. Experts can:
  - Draw on all of their knowledge.
  - Assess what is relevant to someone's problem.
  - Determine if documentation is still relevant.
  - Figure out where to find information.
  - Forward you to a more knowledgeable source.
- **Documentation and tribal/expert help are complements.** Neither is perfect alone.

### Psychological Safety

- Make it safe to fail.
- Give people mentors.
  - Google mentors are not part of the team/reporting line.
- Antipatterns
  - https://www.recurse.com/manual#sub-sec-social-rules
  - No feigned surprised.
  - No "well-actually"s.
  - No back-seat driving.
  - No suble "-isms" (racism, ageism, etc.).

### Growing Knowledge

- Always be learning. Always be asking questions.
- Context is important.
  - Dive deeper. Figure out questions you should be asking.
- **Chesterson's fence: before removing or changing something, first understand why it's there.**

### Scale Questions

- When you learn something from a one-on-one discussion, **write it down.**
- Others will have the issue too--share what you write down.

#### Community-Based Help

- Group chats.
  - Often devoted to topics or teams.
    - Topics often have more experts, leading to faster responses.
    - Teams are smaller, feeling safer for newcomers.
  - Better for quick back-and-forth exchanges.
- Mailing lists.
  - Similar to group chats.
  - Easily searchable / archivable.
  - Can be indexed.
  - Better for more complicated/contextual questions.
- Stack Overflow-esque systems.

#### Scaling Your Knowledge (Teach Stuff)

- Expertise is a scale, not binary. There are many opportunities to teach.
- Office Hours
  - Most helpful for very ambiguous questions (someone doesn't know where to start) or for specialized problems with little documentation.
  - Idea: All new projects & initiatives must have office hours. Frequent to start, scaling down as appropriate.
- Tech Talks & Classes
  - Classes are best when:
    - A topic is complicated enough to be frequently misunderstood.
    - The topic is relatively stable.
    - Topic benefits from having teachers available to provide personalized help / answer questions (as opposed to self-serve formats).
    - There's enough demand to offer the class regularly.
  - Above reasons are because classes involve a lot of work to make. Should consider when a class is actually appropriate.
- Writing Documentation
  - The first time you learn something is the best time to see how existing materials can be improved.
  - Make it easy for people to find documents on their own. Undiscoverable documents are close to not existing.
  - **Have a feedback mechanism.**
    - Let people indicate if something is outdated or inaccurate.
- Reviewing Code
  - Code reviews are a good learning opportunity.
  - See readability below.

#### Scaling Organization Knowledge

- Cultivate a knowledge-sharing culture.
  - Be kind and respectful. 
  - Toxicity is hard to recover from. Don't be toxic.
- Incentives & recognition should support knowledge sahre.
  - For example, explicity note this on the career ladder. Could include points for:
    - Growing future leaders.
    - Sustaining & developing the organization.
  - Kudos.
- Establish canonical sources of information. Centralized, company-wide information that both standardizes & propagates expert knowledge.
  - Intended for things relevant to everyone.
  - Localized information is different and can be kept more locally.
  - More work than localized, but has broader benefits.
  - To maintain trust in the information, they need to be actively maintained and vetted by subject matter experts.
  - Examples:
    - Developer guides.
      - Not expected to be read front to back, but still useful for linking specific bits of information.
    - go/goto links.
    - Codelabs.
    - Static analysis tools.
- Disseminating information.
  - Some knowledge is helpful, but not critical.
  - The sharing medium should match the formality of the information being delivered.
    - More formal = things like being maintained, always up to date, etc.
  - Examples:
    - Newsletters.
      - EngNews, privacy/security, greatest hits (outages).
      - Good for non-mission critical.
      - Engagement is better when less frequent but more interesting content. Don't be spammy.
    - Communities.

#### Readability
  - Readability is both code readability and programming language best practices.
    - Language idioms, code structure, API design, appropriate common libraries, documentation, test coverage.
  - Process: code reviews require readability approval. (When org is large enough for this to make sense.)
    - Someone must be certified to give readability approval.
    - Someone certified ("having readability") consistently writes clear, idiomatic, maintainable code.
    - Certification involves submitting changes to readability reviewers. Over time, information transfers. Certified once they are sufficiently writing readable code.
  - High bar for readability reviewers: you must both have deep language expertise and be able to teach it well.
  - Mentoring and cooperative, not adversarial.
  - Reviewers should provide rationale (Chesterson's fence). Reviewees should ask questions if unclear.
  - Exposes engineers to more than tribal knowledge.
  - Involves a lot of effort, but the belief is that it's worth it in the long term. Heavyweight process.
    - Costs:
      - Friction for teams without a readability certified member.
      - Can add code review rounds.
      - Human-driven, so has scaling limitations.