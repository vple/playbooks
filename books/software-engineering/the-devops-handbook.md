# The DevOps Handbook
Gene Kim, Jez Humble, Patrick Debois, John Willis
[Amazon](https://www.amazon.com/DevOps-Handbook-World-Class-Reliability-Organizations/dp/1942788002/)

## Part I—The Three Ways
DevOps results from applying principles from physical manufacturing and leadership to IT and software development. It pulls on Lean, Theory of Constraints, the Toyota Production System, resilience engineering, learning organizations, safety culture, human factors, and others. It also pulls on high-trust management cultures, servant leadership, and organizational change management.

### 1. Agile, Continuous Delivery, and the Three Ways
This chapter introduces the theory of Lean Manufacturing and the Three Ways.

A fundamental concept in Lean Manufacturing is the **value stream**—"the sequence of activities an organization undertakes to deliver upon a customer request," or "the sequence of activities required to design, produce, and deliver a good or service to a customer, including the dual flows of information and material."

In manufacturing, value streams start when an order is received and raw materials are released. The focus is on creating a smooth and even flow of work. This is done by having small batch sizes, reducing work in progress, preventing rework (due to defects), and optimizing systems for global goals.

In technology, value streams convert a business hypothesis into a technology-enabled service that delivers value to the customer. Input starts when work is accepted into a backlog. Value is only created when services are running in production. This means deploys have to work well. They need to be fast, non-disruptive, and compliant.

The focus of the book is on *deployment lead time*. This is a subset of the value stream, starting when a change is checked into version control and ending when the change is successfully running in production. At this point it is providing value to the customer and generating feedback and telemetry. This phase typically involves testing and operations, and aims to be predictable and mechanistic. The previous phase involves design and development, and is more volatile. Ideally, deployment lead time is in the order of minutes.

Lead time is different from *processing time*. The former starts when the request is made and ends when the request is fulfilled. Processing time starts when work is actually begun.

Another key metric is percent complete and accurate (%C/A). It's a measure of quality—what percentage of the time do downstream customers receive usable work? Usable work is "usable as is"—information doesn't have to be corrected and missing information doesn't have to be supplied.

#### The Three Ways: The Principles Underpinning DevOps
The first way is the flow from development to operations to the customer. 

The second way is the fast and constant flow of feedback. This feedback comes from any point, and development should find out as quickly as possible about it.

The third way is creating a generative, high-trust culture that relies on a dynamic, disciplined approach to risk-taking. This facilitates learning and continual improvement.

### 2. The First Way: *The Principles of Flow*
- Make work visible.  
  Typically done with things such as kanban boards.
- Limit work in process.  
  It's hard to understand the impact of disruption in technology. Having lots of items in progress just degrades the results of all of them.
- Reduce batch sizes.
  Large batches, where you finish one piece at a time, is just multitasking. It has the same issues—slow time for first completed result, hard to switch gears on error.
- Reduce handoffs.
  Handoffs result in lost knowledge and communication inefficiency. Reduce them by automation or by reorganizing teams to have more influence.
- Continually identify and elevate constraints.  
  Find the bottlenecks in the system. Figure out how to improve them.
- Eliminate hardships and waste in the value stream.  
  In manufacturing, waste consists of inventory, overproduction, extra processing, transportation, waiting, motion, and defects. In technology, it consists of partially done work, extra processes, extra features, task switching, waiting, motion, defects, nonstandard/manual work, and heroics.

### 3. The Second Way: *The Principles of Feedback*
Feedback is about making sure information rapidly flows back so that problems can be identified and fixed more quickly and easily.

- Wok safely within complex systems.  
  Complex systems are those that can't be fully understood by a single individual. They tend to have some unpredictable behavior. Work in these systems is safer when:
  - Problems in design and operation are revealed.
  - Problems are swarmed and solved.
  - Local knowledge is used globally in the org.
  - Leaders create other leaders that grow these capabilities.
- See problems as they occur.  
  Have fast, frequent, and high quality information. Iterate quickly and get feedback from all stages of the value stream.
- Swarm and solve problems to build new knowledge.  
  Toyota Andon cord. This prevents problems from propagating, prevents new work from causing issues, and prevents repeated errors.
- Keep pushing quality closer to the source.  
  Keep standards, responsibilities, and decision-making close to the team doing the work.
- Enable optimizing for downstream work centers.  
  End customers aren't your only customers. Internal customers that use your work are also very important.

### 4. The Third Way: *The Principles of Continual Learning and Experimentation*
Create a culture of continual learning and experimentation. This will create individual knowledge, which then needs to become team and organizational knowledge. This is done through a high-trust culture that promotes learning.

- Enable organizational learning and a safety culture.  
  There are three types of organizations: pathological (lots of fear), bureaucratic (process), and generative (seeks and shares information). Generate information and be blameless, e.g. with blameless postmortems.
- Institutionalize the improvement of daily work.  
  Improving daily work is more important than daily work itself.
- Transform local discoveries into global improvements.  
  Personal learnings need to be able to benefit the rest of the organization. Tacit, difficult to transfer knowledge needs to become explicit and codified. This allows others to benefit from that knowledge as well. This happens, for example, by making things both more accessible and discoverable (searchable).
- Inject resilience patterns into our daily work.  
  Low performing organizations compensate by adding inefficient bulk. Instead, continually improve operations. Do this by adding tension and engineering resilience. This can be done by artificially introducing constraints or failures to discover improvements or failure modes.
- Leaders reinforce a learning culture.  
  Traditional leaders lead by making the right decisions. Instead, leaders should enable their team to discover greatness. This involves setting a clear direction and iteratively creating goals to move towards it. Progress is constantly evaluated to do better with each step.

## Part II—Where To Start
### 5. Selecting Which Value Stream to Start With
The value stream you select affects the difficulty of transformation, who is involved, and organizational structure. It's important to pick projects that will most improve the organization. There are many factors in evaluating value streams.

#### Value Stream Selection
##### Greenfield vs. Brownfield
A **greenfield** project is a new project or initiative, with new applications/infrastructre and few constraints. It's often easier to start with a greenfield project since there's less in the way. Greenfield devops projects are usually pilots to demonstrate feasibility of certain technologies or tools.

A **brownfield** project is one involving existing products or services that have already been in use. They often come with technical debt and few tests.

##### Systems of Record vs. Systems of Engagement
A **system of record** focuses on correctness of transactions and data. They usually have a slower pace of change and have to follow regulatory and compliance requirements. The organization usually focuses on "doing it right" with these systems.

A **system of engagement** is usually a customer-facing system. They usually change much faster, supporting fast feedback loops and experimentation to meet customer demand. The organization usually focuses on "doing it fast" with these systems.

Devops enables doing things both right and fast. Changing systems is often most limited by the system that is most difficult to safely change, which is often a system of record.

#### Introducing Change
- Start with sympathetic and innovative groups.  
  Focus on a few areas of the organization, making sure that those initiatives succeed. Focus on groups with innovators and early adopters.
- Demonstrate early wins and broadcast success.  
  Break larger goals into small, incremental steps. This enables faster progress and helps reveal bad value stream selection.
- Grow credibility, influence, and support.  
  - Start with innovators and early adopters. Ideally, find those who are also respected and have influence; this gives credibility.
  - Build a critical mass and a silent majority. It's okay if teams aren't visible; this grows influence so that it's harder for initiatives to be jeopardized.
  - Identify holdouts. Finally, tackle the high profile detractors.

### 6. Understanding the Work in Our Value Stream, Making it Visible, and Expanding it Across the Organization
After identifying a value stream, we need to understand how value is delivered to the customer. Who does what work, what steps will improve flow.

#### Identify The Teams Supporting Our Value Stream
Figure out who needs to work together to create value. Typically this includes:
- product owner
- development—the team developing the functionality
- qa—the team that ensures the service functions as expected
- operations—the team that maintains the product environment and ensures that service levels are met
- infosec—the team that secures systems and data
- release managers—people managing and coordinating production deployment and release processes
- technology execs, value stream manager—the person ensuring the value stream meets the customer's needs

#### Create A Value Stream Map
Document the value stream to the level of detail needed to understand the areas of the value stream that are affecting fast flow, short lead times, and reliable outcomes. Ideally, get the people with authority to change the appropriate portions. The value stream map should include lead time, value added (processing time), and %C/A if possible.  

Focus on areas where:
- Work must wait for weeks, or even months. Often this can involve getting appropriate environments, change approval, and security review.
- Significant rework is generated or received.

Use the metrics in the value stream map to decide where improvement efforts should be focused. After identifying that metric, investigate to understand that problem better. Create an ideal future value stream map. Aim to achieve that map by some date (typically 3-12 months). Leadership should help to define the future state and help with evaluating whether or not the team is making progress with each step.

#### Create A Dedicated Transformation Team
Devops transformations are often in conflict with ongoing business operations. Devops requires disruption and innovation. 

This is accomplished by creating a dedicated transformation team that operates outside of the rest of the org. The dedicated team is held responsible for a clearly defined, measurable, system-level result. Free the team from many restrictions that the rest of the org follows. To form this team:

- Have members be focused 100% on devops transformation. Don't split time between devops and current responsibilities.
- Select team members who are generalists, with skills across many domains.
- Select team members with longstanding, mutually respectful relationships with the org.
- Create a separate physical space for the team, giving them some isolation.

Keep improvement planning horizons short. Planning horizons should be on the order of weeks. Having short planning and iteration intervals gives:

- Flexibility and the ability to reorder/replan.
- Shorter delays from when work is done to improvements realized. This helps with positive reinforcement.
- Faster learning.
- Less risk of the project getting killed before it shows results.

Reserve 20% of development and operations cycles to pay off technical debt. Refactor, invest in automation, architecture, and non-functional requirements.

#### Use Tools to Reinforce Desired Behavior
Use tools to help make things visible and create alignment. This includes things such as using the same backlog and a single chat app.

### 7. How to Design Our Organization and Architecture with Conway's Law in Mind
> Organizations which design systems...are constrained to produce designs which are copies of the communication structures of these organizations.... The larger an organization is, the less flexibility it has and the more pronounced the phenomenon. —Conway's Law

How teams are organized will affect the software we make. Teams need to be organized so that Conway's Law works to our advantage.

#### Organizational Archetypes
- **Functional-oriented organizations** optimize for expertise, division of labor, or reducing cost.  
  They centralize expertise. This helps with career growth and skill development. They typically have tall hierarchical structures. This is usually how operations is structured.
- **Market-oriented organizations** optimize for responding quickly to customer needs.  
  They are often flat and have multiple, cross-functional disciplines (marketing, engineering, etc.). This can lead to redundancies in the org. This is usually how organizations that adopt devops operate.
- **Matrix-oriented organizations** attempt to combine functional and market-oriented organizations.  
  They are often complicated and fail to achieve the goals of either orientation.

##### Functional Orientation
The most visible consequence of functional orientations are long lead times, especially for complex activities with lots of handoffs. People performing work also often lack visibility into how their work relates to overall goals, resulting in a lack of motivation. This gets worse when a team serves multiple value streams and is forced to prioritize between them. This often results in all projects becoming slow.

It is still possible to create an effective organization with functional orientation. A key here is that everyone in the value stream has customer outcomes as a shared goal. Teams need to be able to get what they need from operations reliably and quickly. There needs to be a high-trust culture that lets departments work effectively and there needs to be enough slack in the system to let high-priority work get completed quickly.

##### Market Orientation
To get devops outcomes, we want to reduce functional orientation and enable market orientation. This lets us have many small teams that work safely and independently. At the extreme, a market-oriented team handles feature development, testing, securing, deploying, and supporting the service in production. 

Achieving market orientation can be done by embedding functional engineers and skills into teams. It can also be done by providing these capabilities through automated and self-service platforms.

#### Testing, Operations, and Security as Everyone's Job, Every Day

#### Enable Every Team Member To Be A Generalist
When people over-specialize, you get silos. Specialists are necessary, but it's important that they don't get "frozen in time" where they can only contribute to one area of a value stream.

A countermeasure is to enable and encourage everyone to be a generalist. This is done by providing opportunities for engineers to learn all skills necessary to build and run systems that they're responsible for and by regularly rotating people through different roles. Full stack engineers are often expected to have a general understanding of the entire application stack—application code, databases, operating systems, networking, cloud.

Cross-training and growing engineering skills enables generalists to do orders of magnitude more work than specialist counterparts. Prefer T-shaped people to I-shaped people. Even better are E-shaped people.[^e-shaped] Valuing people for existing skills or performance in current role inadventantly reinfoces a fixed mindset. We want to:

[^e-shaped]:  
  https://culturecartography.wordpress.com/2012/07/26/business-trend-e-shaped-people-not-t-shaped/  
  https://www.leadingagile.com/2017/02/e-shaped-staff/

- encourage learning
- help people overcome learning anxiety
- ensure people have relevant skills
- ensure people have a career road map
- promote positive disruption

#### Fund Not Projects, But Services And Products
Create stable service teams with funding to execute their own strategy and road map. We want to value organizational and customer outcomes, preferably with minimum output. This differs from traditional measurement, which is whether the project is within budget, time, and scope.

This is in contrast to where teams are assigned to a project and then reassigned when the project is completed. This leads to negative effects, such as developers not understanding the long-term consequences of the decisions that they make.

#### Design Team Boundaries In Accordance With Conway's Law
One of the largest challenges with growing organizations is maintaining effective communication and coordination. Team organization affects this, by Conway's Law.

Bad configurations are where teams are split by function or architectural layer, as they require lots of communication and coordination but still result in a lot of rework. There are disagreements over specs, poor handoffs, and idling.

We want small, independent, productive teams. They should be sufficiently decoupled from other teams.

##### Create Loosely-Coupled Architectures To Enable Developer Productivity And Safety
With tightly coupled architectures, small changes result in large scale failures. People working in one part of the system must constantly coordinate with others. Testing the system also requires integrating changes from many developers and systems. This is hard organizationally, and in terms of spinning up test environments. The overall effect is long lead times and low developer productivity.

Service-oriented architectures are composed of loosely coupled services with bounded contexts. Developers should be able to understand and update a service's code without understanding internals of peer services. Services interact strictly through APIs. Bounded contexts ensure that services are compartmentalized and have well-defined interfaces.

##### Keep Team Sizes Small (Two Pizza Rule)
Small teams reduce the amount of inter-team communication.

- Team has a clear, shared understanding of the system.
- Limits the growth rate of the product or service, ensuring that the team maintains a shared understanding.
- Decentralizes power and enables autonomy.
- People can gain leadership by leading one of these teams without jeopardizing the business if there are catastrophic consequences.

### 8. How to Get Great Outcomes by Integrating Operations into the Daily Work of Development
We want to enable market-oriented outcomes, with small teams. This is hard if operations are centrally and functionally oriented. We can change this by integrating ops into dev teams, which ends up making both better. 

Using a centralized operations team, there are three main strategies:

- Enable developers to self-serve.
- Embed ops engineers into service teams.
- Assign ops liasons when embedding ops is not possible.

#### Create Shared Services To Increase Developer Productivity
Operations needs a set of centralized platforms and tooling services that any dev team can use to be more productive. This includes production-like environments, deployment pipelines, automated testing tools, production telemetry dashboards, etc. These services are ideally automated and available on demand. Developers shouldn't need to open a ticket and wait for someone to manually perform work.

Internal teams are not required to use these platforms and services. Platform teams have to win over teams, sometimes competing with external vendors. This tension helps ensure that internal services are the easiest and most appealing. The goal is to make the internal platform the easiest and safest platform to use. They should also look for internal toolchains that are widely adopted and decide which one to support centrally—this is easier than building one from scratch.

Platform teams can also provide coaching and consulting to elevate how tools and technologies are used. Shared services enable standardization, allowing teams to be familiar with things even when they switch to other teams.

If the organization has restricted, approved tools, this restriction can be lifted for a the transformation team. This allows for experimentation.

#### Embed Ops Engineers Into Our Service Teams
Embedding ops engineers reduces other teams' reliance on centralized operations. The embedded ops engineer prioritizes based on the product teams that they are in, rather than ops teams focusing only on their own problems. The benefit is not just getting someone ops-minded on the team; this also helps to cross-train knowledge between teams.

As ops knowledge and capabilities spread, ops engineers can transition to other projects or engagements.

#### Assign An Ops Liaison To Each Service Team
If embedding isn't possible, you can also assign a designated liaison. The designated ops member needs to understand:

- the product functionality and why it's being built
- how it works in terms of operations and scalability
- how to monitor and collect metrics to ensure progress, success, or failure
- departures from previous architectures and patterns, and why
- extra needs for infrastructure
- feature launch plans

The liaison should still participate in some team rituals, such as stnadups.

#### Integrate Ops Into Dev Rituals
Integrating ops engineers into dev rituals helps them better understand the development culture and workflow. There are many ways to do so:

- Invite ops to dev standups.  
  This helps ops be aware of what's going on within development. It's especially important for roadblocks.
- Invite ops to dev retrospectives.  
  This lets ops engineers benefit from new learnings. Ops can provide feedback from the ops side, enabling developers to understand their downstream actions. This feedback can also help uncover issues that dev would have had a harder time finding. Retrospective work falls into a few broad categories of improvement work: fixing defects, refactoring, automating manual work.
- Make relevant ops work visible on shared kanban boards.  
  This makes dev aware of what ops needs to do.

## Part III—The First Way: The Technical Practices Of Flow
### 9. Create the Foundations of Our Deployment Pipeline
Fast and reliable flow involves ensuring that we have production-like environments at every stage of our value stream. These environments should be automatically created, via stored configuration, and self-serviced. We want to always be able to recreate the production environment based on what's in version control.

Failing to do this makes it hard to accurately test and find issues. This makes it hard to coordinate deployments and slows down the entire development cycle, as changes take much longer to reach production due to errors being detected so late in the cycle.

To help ensure that things make it to production, add to the definition of "done"—something done should be tested, working, and deployed to production.

#### Enable On Demand Creation Of Dev, Test, And Production Environments
Chaotic, disruptive, and catastrophic software releases often occur when the first time we see our code in a production-like environment is when the code is actually run in production. When test environments are created, they should be created quickly enough that they're suitable for teams to use in testing. 

There should be a common build mechanism to create all environments. This involves defining and automating creation of good environments.

Automation should be used for any and all of:

- copying virtualized environments
- building automated environment creation processes from bare metal
- using infrastructure as code configuration tools (puppet, chef, ansible, etc.)
- using automated operating system config tools
- assembling an environment from virtual images or containers
- spinning up new environments in a public cloud, private cloud, or other platform

#### Create A Single Repository For The Entire System
Everything should be in source control—including environments. Anything and everything should be checked in, including:

- application code and dependencies
- scripts for schemas, reference data, etc.
- environment creation tools and artifacts
- files to create containers
- supporting automated tests and manual test scripts
- scripts for packaging code, deploys, database migrations, environment provisioning
- cloud configuration files
- scripts or configuration for infrastructure that supports multiple services

#### Make Infrastructure Easier To Rebuild Than To Repair
Being able to rebuild an application rather than repairing it allows us to just restart the application when there's an issue. This also enables *immutable infrastructure*—all changes to production must be from recreating the environment from scratch. No manual changes are allowed.

### 10. Enable Fast and Reliable Automated Testing
Testing needs to be regular and frequent. It should be done by the developers themselves, as they have the most context. Frequent testing also ensures that developers learn about their mistakes quickly, when the context is still fresh. This enables solutions to be implemented faster and allows for more learnings.

Testing also has to be automated. Otherwise, the cost (time and money) will constantly increase as more code is written. The more rigorous your testing practices, the better your systems and deployments will be. These can include:

- All changes have accompanying automated tests.
- The continuous build is always kept passing.
- Test coverage monitoring is added and continually increases over time.
- Policy and testing guides are written, shared, and used throughout the organization.

Everyone should understand that it's their responsibility to help keep the deployment pipeline running. 

#### Continuously Build, Test, And Integrate Our Code And Environments
A **deployment pipeline** ensures that all code checked in to version control is automatically built and tested in a production-like environment. This allows build, test, and integration errors to be found as soon as changes are introduced. In turn, this should let us fix issues quickly. This is enabled by having automated test suites, which are run as part of the deployment pipeline.

Successfully creating a deployment pipeline involves creating automated build and test processes that run *in dedicated environments*. Keeping this separate ensures:

- Build and test processes can run all the time, regardless of what individual engineers are doing or not doing.
- Separate processes require that we understand all dependencies to build, package, run, and test our code. Otherwise, we'd run into reproducibility problems (works on a developer machine but not in prod).
- Application can be packaged to allow repeatable installation of code and configurations into an environment.
- Applications can be packaged into deployable containers.
- Environments can be created that are similar to production.

These environments can also self-service builds for UAT.

#### Build A Fast And Reliable Automated Validation Test Suite
We want to continuously run our automated tests—this makes it easy to identify which changes broke the build. We'll also find out during the day, rather than having to wait for a nightly build. There are various types of automated tests.

- **unit tests**  
  Test a single method, class, or function in isolation. They typically stub out external dependencies to have fast and stateless tests.
- **acceptance tests**  
  These test the whole application to ensure that higher-level functionality is as designed. They show that the application does what the customer expects. Having virtual or simulated remote services is an architectural requirement for fast and reliable acceptance tests.
- **integration tests**  
  These make sure that applications interact correctly with other production applications and services. These can be brittle, so it's better to run them on builds that have passed unit and acceptance.

It can help to make test coverage visible, regardless of the coverage metric (number of tested classes, lines of code tested, permuations tested, etc.). We can also require minimum test coverage requirements.

When errors occur, we want them to show up as early as possible in testing. This is why test sequencing matters—have faster and more reliable tests (unit) run before slower and more unreliable tests (acceptance and integration). Errors should also be caught with the fastest category of testing possible—catch a bug in unit testing over other tests if possible. Acceptance tests that are hard to write and maintain are likely a result of overly coupled architecture.

Make sure tests can run quickly and in parallel. It helps to make tests hermetic for this. Any build that passes the tests should be useable for exploratory testing. Testers should use the latest build that passes all tests.

Test-driven development helps make sure that we have reliable automated testing. This involves writing automated tests that validate when the expected behavior, which will start out failing. Then write code to make the test pass. As many manual tests as possible should be automated, but make sure that they're reliable. Tackle flaky or other unreliable tests.

We also want performance testing, so we can better understand how our services will operate under load. A way to do this is to run many copies of an acceptance test in parallel. Performance results should be logged and evaluated against previous results, even failing if there's too much deviation.

All important attributes of the system should be validated, even if they aren't functional. These can include availability, scalability, capacity, security, etc. Some of these are filled by correct environment configuration, so we should have tests that environments are built and configured correctly.

#### Pull Our Andon Cord When The Deployment Pipeline Breaks
Have a Toyota Andon cord. When the deployment pipeline is broken, everyone should be aware of the issue and be able to help fix the problem. Everyone should be empowered to roll back commits to get things back to green. People's jobs are not to write code, but to run a service. It can help to have highly visible indicators so everyone can see when things are failing.

When later stages of the pipeline fail, add tests at an earlier stage to catch future regressions.

It's important to pull the cord and fix issues when they occur. Otherwise things can pile up, leaving us in a much more complex state.

### 11. Enable and Practice Continuous Integration
Integrating—collecting and merging everyone's changes together. Integration issues often require a lot of work to get back into a deployable state. When merging is painful, people will do less of it, and this problem will get worse. This is more prone to happen when working in long-lived private branches—feature branches. In addition to merge issues, developers are getting delayed feedback.

#### Adopt Trunk-Based Development Practices
Continuous integration ensures that merging changes into master (trunk) is a regular part of everyone's daily work. Checking into master regularly reduces the size of the changes that need to be integrated. Frequent commits also ensures that automated tests get run on the most up to date code. It also forces us to keep our commits smaller. This also helps to eliminate the need for an explicit test and stabilization phase at the end of projects.

Deployment pipelines may be configured to reject commits that leave us in an undeployable state. These are called *gated commits*.

### 12. Automate and Enable Low-Risk Releases
We want to make it easy to deploy our changes into production. Although we have tests and continuous integration, deployments may still be manual, time-consuming, painful, tedious, and/or error-prone. When this happens, we'll do it less often, leading to a downward spiral.

We make it easy by enabling promotion production of any build that passes our automated testing and validation processes. This can be done on demand, with a button push, or automatically.

#### Automate Our Deployment Process
This involves first automating the deployment process. This requires figuring out all the steps involved, which can also involve doing a value stream mapping exercise. Once the steps are figured out, identify, simplify, and automate as many manual steps as possible. This might include:

- packaging code for deployment
- creating pre-configured VM images or containers
- automating deployment and configuration of middleware
- copying packages or files onto production servers
- restarting servers, applications, or services
- generating configuration files from templates
- running automated smoke tests
- running testing procedures
- scripting and automating database migrations

Where possible, remove steps. Especially for slow steps. Also aim to reduce handoffs to minimize errors and loss of context. Figuring all of this out will require development to work closely with operations. When we're done, we should:

- Deploy the same way to every environment.
- Smoke test deployments.
- Ensure consistent environments are maintained.

Once deployment is automated, enable self-serve deployments. Deploys should be able to be done by dev or ops, ideally without manual steps or handoffs. This affects build steps, test steps, and deploy steps—everyone should be able to do these three actions in any environment (where they have access).

Deploys can also become a part of the deployment pipeline. Our pipeline needs to:

- Ensure that packages created during continuous integration are suitable for production deploys.
- Show readiness of production environments at a glance.
- Provide a push-button, self-service method to get any suitable version of packaged code into production.
- Automatically record which commands, which machines, who was involved, and outputs. This is for auditing and compliance.
- Run smoke tests.
- Provide fast feedback for the deployer so they can see if their deploy succeeded.

This gives us a "deploy code" button that lets us safely and easily promote changes to production.

#### Decouple Deployments From Releases
Traditionally, releases are driven by a marketing launch date. When deployments occur solely with releases, this causes a lot of scrambling to ensure that things work as intended. This often requires manual and risky fixes. This is dealt with by decoupling the idea of deployments and releases.

A **deployment** is the installation of a specific version of software to a given environment. It may or may not be associated with releasing a feature to customers.

A **release** is when a feature or features become available to all or a segment of customers. Releasing functionality should not require changing application code.

There are two broad types of release patterns we can use.

##### Environment-Based Release Patterns
An **environment-based release pattern** involves running multiple environments. One of them receives live traffic. The other(s) receives no traffic. Releases are done by moving traffic to the other environment. They involve no changes to application code.

###### Blue-Green Deployments
This refers to having two production environments. One serves customer traffice. To release a new version, we deploy to the inactive environment. Changes can be tested and confirmed there. When everything's working, we switch over. The two environments are referred to as green and blue.

This pattern is simple and easy to apply to existing systems.

There can be challenges when changes depend on a common database—it can't always support two versions of an application. This can be dealth with by also having a blue and green database. Releases involve making a backup, restoring it to the to-be-live environment, and switching over. However, rolling back is harder—transactions have to be replayed.

Alternatively, decouple database changes from releases. Only make additive changes, never mutate existing objects, and don't assume which version is in production.

###### Canary Releases
A canary release involves promoting to successively larger and more critical environments. If there are issues, our systems will ideally fail before reaching the most impactful environment.

One way to do this is to segment the customer base. The least critical could be internal-only, the next critical is a small set of users, and the last is all users. The release has to be successful on each segment before moving on.

###### Cluster Immune System
This expands on canary releases. Production monitoring systems are linked to release processes and will roll back code when user-facing performance falls outside of an acceptable range. This helps guard against issues that are hard to test and it reduces time needed to respond to issues.

##### Application-Based Patterns
An **application-based release pattern** involves modifying our applications to selectively release and expose functionality through configuration changes. Application-based patterns involve dev work.

###### Implement Feature Toggles
The primary way to have application-based patterns is to have feature toggles. This lets us individually turn features on and off. They can also control which features are visible and available to certain segments. They are normally implemented by wrapping code in a conditional, based on some configuration setting.

Feature toggles enable:

- Easy rollbacks. Just toggle the feature.
- Graceful performance degradation. Toggle features to reduce quality of service delivered.
- Increase resilience through service-oriented architectures. Code can still be deployed even though its dependencies aren't ready; just hide it behind a toggle.

Automated tests should run with all features on.

###### Perform Dark Launches
Dark launching involves deploying features into production without making them accessible to users. 

This can be done well in advance, allowing us to test with production-like loads. This can be done by e.g. calling into a new feature, logging and discarding results. This can also be done with a fragment of users (e.g. 1%). This gets better with business telemetry, as we can see immediately if our features validate our business assumptions.

### 13. Architect for Low-Risk Releases
Every successful company has near-death experiences due to architectural issues. When this happens, the company needs to migrate and rewrite parts of their architecture to end up with something more suitable. This is especially needed with overly tight architectures, where seemingly small changes can have unexpected and difficult to manage consequences.

A way to do this is with a *strangler application* pattern, where an interface is placed in front of existing functionality and can control whether old or new functionality is being used under the hood. When using this pattern, it can be useful to have versioned APIs, allowing the service to be modified without impacting callers. It's especially useful for migrating a monolithic or tightly coupled application to a more loosely coupled one. Loosely coupled architectures with well-defined interfaces allow developers to make changes productively and safely.

Many organizations start with a monolithic architecture—these are often the best choice early on in a product's life cycle. As the produdt evolves, the architecture will need to evolve with it.

|      | Pros | Cons |
|------|------|------|
| Monolithic v1<br>(All functionality in one application) | <ul><li>Simple at first</li><li>Low inter-process latencies</li><li>Single codebase, one deployment unit</li><li>Resource-efficient at small scales</li></ul> | <ul><li>Coordination overhead increases as team grows</li><li>Poor enforcement of modularity</li><li>Poor scaling</li><li>All-or-nothing deploy (downtime, failures)</li><li>Long build time</li></ul> |
| Monolithic v2<br>(Sets of monolithic tiers: "front end presentation," "application server," "database layer") | <ul><li>Simple at first</li><li>Join queries are easy</li><li>Single schema, deployment</li><li>Resource-efficient at small scales</li></ul> | <ul><li>Tendency for increased coupling over time</li><li>Poor scaling and redundancy (all or nothing, vertical only)</li><li>Difficult to tune properly</li><li>All-or-nothing schema management</li></li> |
| Microservice<br>(Modular, independent, graph relationship vs. tiers, isolated persistence) | <ul><li>Each unit is simple</li><li>Independent scaling and performance</li><li>Independent testing and deployment</li><li>Can optimally tune performance (caching, replication, etc.)</li></ul> | <ul><li>Many cooperating units</li><li>Many small repos</li><li>Requires more sophisticated tooling and dependency management</li><li>Network latencies</li></ul> |

## Part IV—The Second Way: The Technical Practices Of Feedback
The second way is focused on getting fast and continuous feedback from operations to development. This involves shortening and amplifying feedback loops and ensuring that information makes it to everyone in the value stream. We want to find and fix problems earlier. Operations knowledge should make it back to development. Everyone should be able to get feedback on work, see relevant information, and test features and hypotheses. Telemetry is created and used to see where problems are.

### 14. Create Telemetry to Enable Seeing and Solving Problems
There will always be something that goes wrong. When they do, we often don't have the information that we need to solve the problem. Lack of information forces us to use brute force methods—rebooting servers. When we do have information, we can it to focus our problem solving efforts.

We enable this by creating **telemetry**—an automated communications process by which measurements and other data are collected at remote points and are subsequently transmitted to receiving equipment for monitoring. 

Enable developers to add telemetry as part of their daily work. Collect that information, aggregate it, and graph it. Overlay the graphs with deployment times so it's clear when things get deployed.

#### Create Our Centralized Telemetry Infrastructure
Generating, logging, and managing telemetry isn't new. However, it's important to generate telemetry that allows us to understand how our system is behaving as a whole. This involves breaking down information silos so that everyone has access to relevant data.

Once we have monitoring and logging, we enable other capabilities:

- graphing and visualization
- anomaly detection
- proactive alerting and escalation

A modern monitoring architecture has two main components. One, it has data collection at the business logic, application, and environments layer. These are events, logs, and metrics. Two, it has an event router that stores that data. The event router turns data into useful metrics by, e.g., counting them. With metrics, we can now understand what is occurring in our systems and build on top of this data.

Telemetry should also include data from our deployment pipeline. This includes:

- when tests pass or fail
- deployments to any environment
- time to execute builds and tests

Ensure that it's easy to enter and retrieve information from our telemetry infrastructure. Ideally, this is done via self-service APIs. 

We want telemetry for tells us about when, where, and how for anything of interest. It should be independent—we should be able to analyze our telemetry without having the corresponding application on hand.

#### Create Application Logging Telemetry That Helps Production
We need to make sure that we are creating sufficient telemetry. This requires engineers to create production telemetry as part of their daily work, for new and existing services. Every feature should be instrumented.

Telemetry is used in many ways.

- Developers temporarily create more telemetry to diagnose problems.
- Ops engineers use telemetry to diagnose production problems.
- Infosec and auditors use telemetry to confirm effectiveness of required controls.
- Product managers use telemetry to track business outcomes, feature usage, or conversion rates.

To support these different use cases, use appropriate logging levels. Some of these can be used to trigger alerts.

- **DEBUG**  
  Anything that happens in the program, used for debugging. Typically disabled in production and temporarily enabled when troubleshooting.
- **INFO**  
  User-driven actions or system specific actions (e.g. "beginning credit card transaction").
- **WARN**  
  Conditions that potentially become an error. These should usually initiate alerts and troubleshooting. Other log messages will help us understand what triggered the warn message.
- **ERROR**  
  Error conditions.
- **FATAL**  
  Information about why the program must terminate.

We want to make sure that all potentially significant application events generate logging entries. These include:

- Authentication/authorization decisions (including logoff)
- System and data access
- System and application changes (especially privileged changes)
- Data changes—adding, editing, or deleting data
- Invalid input (possible malicious injection, threats, etc.)
- Resources (RAM, disk, CPU, bandwidth, etc.—anything with hard/soft limits)
- Health and availability
- Startups and shutdowns
- Faults and errors
- Circuit breaker trips
- Delays
- Backup success/failure

To better interpret and give meaning for these entries, we would ideally create hierarchical categories for our logging. This includes non-functional attributes (performance, history) and feature-related attributes (search, ranking).

#### Use Telemetry To Guide Problem Solving
High performers use a disciplined approach to solving problems. Telemetry enables us to focus our investigation, through questions such as:

- What evidence do we have from our monitoring that a problem is actually occurring?
- What are the relevant events and changes in our applications and environments that could have contributed to the problem?
- What hypotheses can we formulate to confirm the link between the proposed causes and effects?
- How can we prove which of these hypotheses are correct and successfully effect a fix?

#### Enable Creation Of Production Metrics As Part Of Daily Work
Ideally, a single line of code is all that's needed to create a new metric that shows up in a common dashboard for everyone to see. Generated graphs should have overlays of production changes so we can see if issues are caused by production changes.

#### Create Self-Service Access To Telemetry And Information Radiators
After creating telemetry, we need to make sure this information radiates out to anyone who needs it. They shouldn't need to have privileged access or to open a ticket to see a graph.

Make production telemetry highly visible—put it in a central area. This information can be shared not just internally, but even with external customers. Creating transparency shows that we don't have anything to hide and helps build trust.

#### Find And Fill Any Telemetry Gaps
Identify gaps that prevent us from detecting and resolving incidents. We need metrics from multiple levels:

- business level  
  sales transactions, revenue of sales transactions, user signups, churn rate, A/B testing results, etc.
- application level  
  transaction times, user response times, application faults, etc.
- infrastructure level  
  web traffic, CPU load, disk usage, etc.
- client software level (e.g. javascript, mobile)  
  application errors, crashes, user-measured transaction times, etc.
- deployment pipeline  
  build pipeline status, change deployment lead times, deployment frequencies, test environment promotions, environment status

Displayed business metrics should be actionable. Otherwise they're just vanity metrics—collect them but don't display them. Link metrics to business outcomes as early as possible and measure how these metrics change as we deploy changes.

Infrastructure metrics should enable us to know when issues are infrastructure-related. They should let us know what applications and services might be affected. Links between infrastructure and services can be registered automatically and then dynamically discovered via tools like Zookeeper.

#### Books
- *The Art of Monitoring*, James Turnbull

### 15. Analyze Telemetry to Better Anticipate Problems and Achieve Goals
Once we have telemetry, we want to use it to anticipate problems. This is typically done by discovering variance in our metrics, ever-weaker failure signals, and by using statistical techniques.

**Outlier detection**—detecting abnormal running conditions from which significant performance degradation may well result. A simple approach to outlier detection is to determine the "current normal," then find things that don't fit that normal. If normal is computed, rather than explicitly defined, we don't necessarily have to know what proper behavior is.

A common use of outlier detection is to alert people. We want our alerts to be accurate and to minimize false-positives. Otherwise we end up in alert fatigue where our alerts end up getting ignored. Better alerts are created by increasing the signal-to-noise ratio; focus on outliers that matter.

#### Statistical Analysis
##### Means And Standard Deviations
A simple statistical technique is to look at the *mean* and *standard deviation* for some data.

With a Gaussian (normal, bell curve) distribution, the first, second, and third standard deviations contain 68%, 95%, and 99.7% of the data. We could, for example, set an alert if we are somewhere that is more than three standard deviations away from our mean.

##### Non-Gaussian Distributions
Not all data sets follow a Gaussian distribution. If we try to apply Gaussian outlier detection to them, our detection will be way off. This happens with long-tail distributions. Many data sets have a chi squared distribution.

We can still find outliers in Non-Gaussian data sets with **anomaly detection**—the search for items or events that do not conform to an expected pattern. Some techniques include:

- *Smoothing*
  Used for time series data. This involves using rolling averages to reduce short-term fluctuations and highlight long-term trends and cycles.
- *Fast Fourier Transforms*  
- *Kolmogorov-Smirnov test*  
  Useful for finding similarities or differences in periodic/seasonal data.

It can be useful to find people in marketing or business intelligence, as they likely already have the skillsets to understand our data.

#### Instrument And Alert On Undesired Outcomes
Analyze recent severe incidents in order to figure out what telemetry would have allowed earlier and faster detection or diagnosis. Also look for metrics that confirm when effective fixes are implemented. These metrics might show up at a variety of different levels—application, OS, database, or network. Repeating this allows us to find ever-weaker failure signals.

### 16. Enable Feedback So Development and Operations Can Safely Deploy Code
Providing faster and more frequent feedback, as well as reducing batch sizes, increases safety and confidence in deploys. Otherwise people will be afraid to deploy, as they'll be afraid of breaking everything. There are a few steps towards improving deployments.

1. Add telemetry. This at least lets people see what they broke. It also enables them to more quickly fix the issues that come up.
2. Peer reviews help everyone write better code.
3. Automated tests help find errors before deployment.
4. Increasingly smaller pieces of code are checked in.

#### Use Telemetry To Make Deployments Safer
We want to actively monitor production telemetry whenever someone performs a production deployment. This involves actively monitoring metrics associated with our feature during the deployment, ensuring that we haven't broke our own or another service. This telemetry helps us catch issues that our deployment pipeline failed to catch.

If an issue does occur, we quickly restore service. This can be done with a *fix forward* (leaving the issue and deploying code to fix it) or with a *roll back* (switching to the previous deploy). Fix forwards can be dangerous, but it is safer with automated testing, fast deployment, and sufficient telemetry. Overlaying deployments and change events onto metric graphs helps correlate changes to issues.

#### Dev Shares Pager Rotation Duties With Ops
There will always be unexpected issues at inconvenient times. When unresolved, these cause downstream issues. The problem is that these issues aren't always communicated back upstream to the developers.

This is dealt with by having everyone in the value stream share downstream responsibilities. This can be done by putting developers on pager rotation. This also helps developers see that business impact goes beyond makeing something as done. Whenever developers get production feedback, they get closer to the customer and can produce better work.

#### Have Developers Follow Work Downstream
It helps developers when they see how customers actually use their software. Developers should follow their work downstream and see how the customer actually uses it.

#### Have Developers Initially Self-Manage Their Production Service
Even with production-like environments, systems may behave in unexpected ways the first time they are released. This can be difficult for operations to deal with. Developers should be responsible for managing their own services when these services are first launched. Operations can help with consulting on best practices.

This can be made a requirement before ops will manage a service. This would likely come with launch requirements that the service must fulfill before handing it off to ops. These can include:

- defect counts and severity
- type/frequency of page alerts
- monitoring coverage
- system architecture
- deployment process
- production hygiene

It's also good at this point to determine if there are or will be any compliance objectives.

If services already in production have issues, operations should have a *service handback mechanism* to return responsibility back to development. Dev can pass it back to ops once the service is in a good state again. Operations shouldn't be in a state where they have to manage a fragile service while technical debt buries them.

### 17. Integrate Hypothesis-Driven Development and A/B Testing into Our Daily Work
> Success requires us to not only deploy and release software quickly, but also to out-experiment our competition.

Features underperform if they aren't achieving intended business goals. We want to figure out as quickly as possible if our features are doing what they are intended to do. This doesn't happen when features take a long time to develop and when improvements to features are out-prioritized by other goals. When building features, we want to quickly determine if the feature should be built and why.

#### A/B Testing
##### A Brief History
There are two major categories of marketing strategies—*direct response marketing* and *mass/brand marketing*. A/B testing was developed as part of direct response marketing.

In the past, direct response marketing involved sending physical mail. Prospects were given an action item (calling back, returning a card, placing an order). Campaigns were used to figure out which offer had the highest conversion rates. Each campaign typically cost tens of thousands of dollars, but they justified themselves with increased conversion rates.

##### Feature Testing
The most common A/B technique is to see how visitors react to a control (A) or a treatment (B). Statistical analysis allows us to determine if the chere is a causal link between the treatment and the outcome. These are also referred to as online controlled experiments or split tests.

It's possible to run tests with multiple variables, allowing us to see how they all interact. This is called multi-variate testing.

There's a decent chance that many features have negligible or zero impact. In actuality there's a cost, as the feature must be built. There's an additional cost as well—this time could have been used building more useful features.

##### Releases
Fast and iterative A/B testing is enabled by being able to deploy on demand. Feature toggles allow us to determine which version customers see. Telemetry is needed at all levels of the stack.

##### Feature Planning
Product owners should think about A/B releases and testing as tools for evaluating their hypotheses. They should get in the habit of validating their guesses with data from actual users, complete with a hypothesis, anticipated outcome, and threshhold for success.

### 18. Create Review And Coordination Processes to Increase Quality Of Our Current Work
We want to reduce risk of changes before they get made. A significant part of this is relying less on reviews, inspections, and approvals that are far away from the developer—both in terms of time and in terms of organization. We want developers to be able to review their own work as much as possible.

#### Overly Controlling Changes
Generally, when issues occur, they're attributed to change control failure or testing failure. In actuality, the issue results from low-trust, command-and-control cultures.

When change failures occur, we end often add more controls—additional questions that need to be answered, more and higher-level authorizations, and more lead time for change approvals. These add friction, increase batch sizes, and increase deployment lead times—all things that makes it harder for dev and ops to produce good work.

The people closest to a problem know the most about it. So, we actually want to bring implementers closer to change authorizers.

#### Effective Control Practices
##### Coordination And Scheduling
The amount of coordination and scheduling decreases as systems are less tightly coupled. There will still some conflicts; these are often well-addressed by having chat rooms where people can communicate as needed. In more complex situations, we may need scheduled changes.

##### Peer Review
> Ask a programmer to review ten lines of code, he'll find ten issues. Ask him to do five hundred lines, and he'll say it looks good.

**Code reviews** allow fellow engineers to scrutinize changes before they get made. Code reviews should be done prior to pushing code. Higher-risk areas may benefit from review from a subject matter expert. Guidelines include:

- All changes must be reviewed before they are pushed.
- Everyone should monitor team members' commit streams to identify potential conflicts.
- Define what qualifies as a high-risk change. These may require input from a subject matter expert.
- Changes that are too large to easily reason about should be split up into multiple, smaller commits.

Tracking code review statistics also helps to ensure that changes aren't getting rubber stamped. Sampling and inspecting specific code reviews is also a good way to do this.

Code reviews can be done in a variety of ways:

- pair programming
- over the shoulder—a developer looks over the author's shoulder as the author explains the code
- email pass-around—reviewers are automatically notified
- tool-assisted code review

**Pair programming** is where two engineers work together at the same workstation. The *driver* is writing the code; the *navigator, observer,* or *pointer* is the person reviewing the work as it gets written. The observer can also focus on the strategic direction, allowing the driver to focus on tactical pieces. Pair programming is also a good way of transferring skills. Pair programming tends to slightly decrease net speed, but massively increases quality.

##### Good And Bad Pull Requests
A good pull request:

- Provides enough context for the reader.  
  It should document what the change is intended to do. There should be sufficient detail about why the change is being made.
- Tags engineers who should look at the request.  
  Mentors and/or subject matter experts should review the change.
- Explains what the changes are and/or why they're important.  
  Describe how the change is made.
- Exposes the author's thinking.  
- Identifies risks/countermeasures.
- Has good discussion about the change.  
  This is enabled by sufficient context.
- Links to bad or unexpected issues during deployment.

#### Fearlessly Cut Bureaucratic Processes
We want to streamline approval processes to reduce lead times. A metric to help detect this is the number of meetings and work tickets required for a release.

## Part V—The Third Way: The Technical Practices Of Continual Learning And Experimentation
### 19. Enable and Inject Learning into Daily Work
Even with checklists and runbooks, we'll still have unexpected and even catastrophic accidents. Working safely requires self-diagnostics, self-improvements, and the ability to detect problems, solve them, and make solutions available throughout the org. Learning needs to be dynamic, allowing mistakes today to translate to learnings that don't occur again in the future. All of this creates a resilient organization.

#### Establish A Just, Learning Culture
When accidents happen, the response to them has to be seen as just. Otherwise, people will be afraid instead of mindful when they do safety-critical work. Accidents aren't caused by people—they're caused by design problems in the systems that we use. We should be trying to maximize organizational learning and showing that we value when people share problems in daily work. There are main two parts to this—post-mortems and controlled introduction of production failures. The main mental shift is that failures are embraced.

#### Schedule Blameless Post-Mortem Meetings After Accidents Occur
Whenever accidents and significant incidents occur, conduct a *blameless* post-mortem after the incident is resolved. Have it as soon as possible after the accident. The post-mortem should:

- Include a timeline and gather details from multiple perspectives on failures.  
  - These are ideally supported by chat logs and telemetry metrics.
  - The narrative should also detail investigation paths that were followed and resolutions considered.
- Empower all engineers to give detailed accounts of their contributions towards failures.
- Enable and encourage people who make mistakes to educate the rest of the org on how to not make those mistakes in the future.
- Accept that good decisions are determined in hindisght.
  - Disallow the phrases "would have" and "could have" at the meeting.
  - Focus on why the decision made sense at the time it was made.
- Propose countermeasures to prevent similar accidents from occurring in the future and ensure those measures are owned and followed up on.
  - These can include automated tests, more telemetry, identifying categories of change, rehearsing during game day exercises.

Certain stakeholders should be at the meeting:

- People involved in decisions that contributed to the problem.
- People who identified the problem.
- People who responded to the problem.
- People who diagnosed the problem.
- People who were affected by the problem.
- Anyone else interested in attending the meeting.

> We're trying to prepare for a future where we're as stupid as we are today.

#### Publish Our Post-Mortems As Widely As Possible
After post-mortem meetings, widely announce the notes and related information. Place it in a centralized place where anyone in the org can access it and learn from it.

Etsy Morgue—tool to easily record aspects of each incident:

- scheduled vs unscheduled incident
- post-mortem owner
- relevant IRC chat logs
- relevant JIRA tickets for corrective actions, with due dates
- links to customer posts

#### Decrease Incident Tolerances To Find Ever-Weaker Failure Signals
As you get better at handling problems, look for smaller problems. Do this by amplifying failure signals.

Organizations are typically structured as a *standardized model* or an *experimental model*. Work should never be treated as entirely standardized.

#### Redefine Failure And Encourage Calculated Risk-Taking
Leaders should continually reinforce that people should be comfortable and responsible for surfacing and learning from failures. Number of failures will likely increase—you want the rate of failure to decrease.

#### Inject Production Failures To Enable Resilience And Learning
Regularly perform tests to make sure our systems fail gracefully. Designing failure modes helps you understand which failures are acceptable and anticipated, rather than having things be left to chance. Once failure modes are defined, they need to be tested to ensure that they're working as intended. This can be done by adding faults to production environments and by rehearsing large-scale failures.

#### Institute Game Days To Rehearse Failures
*Resilience engineering*—an exercise designed to increase resilience through large-scale fault injection across critical systems. Teams simulate and rehearse accidents on game days, giving them practice as to how to handle them in real life.

Game days involve defining and executing drills. Problems and difficulties are identified, addressed, and tested again. At the scheduled time, execute the outage and see how the team responds. These help expose *latent defects*—problems that only appear when your system has faults.

These exercises should be conducted in an increasingly intense and complex way—they should become part of an average day, not an extraordinary event.

### 20. Convert Local Discoveries into Global Improvements
This chapter focuses on mechanisms that enable new learnings and improvements to be captured and shared throughout the org.

#### Use Chat Rooms And Chat Bots To Automate And Capture Organizational Knowledge
Chat rooms can be used for more than fast communication—they can be used to trigger automation.

One technique is to have tools create transparency and document work. This is done, for example, by having a bot that can trigger production deploys. This enables everyone to see what's happening (as opposed to a script being run locally), engineers can see what's happening on day 1, people are more likely to ask for help, and organizational learning is accumulated. Everything is discoverable.

> Even when you're new to the team, you can look in the chat logs and see how everything is done. It's as if you were pair-programming with them all the time.

Anything automated or done regularly can be turned into a chat bot action. These include:

- deploys and reverts
- checking service health
- muting alerts when services go into maintenance
- pulling up smoke test logs
- taking servers out of rotation

#### Automate Standardized Processes In Software For Re-Use
Lots of knowledge gets documented as prose. Engineers that don't know about these documents, or don't have the time to implement them, create their own tools and processes. These end up fragile, insecure, and unmaintainable.

Instead, this knowledge has to be turned into an executable form. These should be checked in, so everyone has access to them. Having them executable makes it easy for others to adopt these processes.

#### Create A Single, Shared Source Code Repository For Our Entire Organization
A single repository allows you to check in configuration standards, deployment tools, testing standards and tools, deployment pipeline tools, monitoring and analysis tools, tutorials and standards. It also ensures everyone's on the most up to date version and that everyone has access to new tools.

#### Spread Knowledge By Using Automated Tests As Documentation And Communities Of Practice
When code is tested, the tests serve as documentation and show other engineers how to use the library. Shared libraries ideally have a single team that supports it, designating an expert for the team. Only one version should be used in production. The library owner would also be responsible for migrating versions.

#### Design For Operations Through Codified Non-Functional Requirements
These include ensuring:

- Sufficient production telemetry in applications and environments.
- Being able to track dependencies.
- Services that are resilient and degrade gracefully.
- Forward and backward compatibility between versions.
- The ability to archive data to manage the size of production data sets.
- Ability to search and understand log messages across services.
- Ability to trace requests from users through multiple services.
- Simple, centralized runtime configurations.

#### Build Reusable Operations User Stories Into Development
When operations work can't be fully automated or self-service, we should make it as repeatable and deterministic as possible. This is done by standardizing needed work, automating as much as possible, and documenting it.

Automate building servers. Define handoffs as clearly as possible. Make it clear what work is required, who is needed to perform it, what the steps to completing it are, etc. Create ops user stories and plan for them just as for developer work.

#### Ensure Technology Choices Help Achieve Organizational Goals
Define the technologies that operations will support and not support. Identify technologies that slow down workflow, create high levels of unplanned work, create high levels of support requests, or are inconsistent with desired architectural outcomes.

### 21. Reserve Time to Create Organizational Learning and Improvement
*Improvement blitzes*, or *kaizen blitzes*, are focused periods of time to address a particular issue. Typically they last a few days. We want to adopt this idea for software engineering.

#### Improvement Practices
One way of running these blitzes is by having teams come to work with devops (coaches and engineers) for a certain period of time. During that time, the two groups aim to solve a problem that development has been struggling with.

It's also useful to institutionalize rituals to pay down technical debt. This can be done by scheduling day or week long sessions where everyone finds and fixes problems that they care about. No feature work is allowed. These should improve daily work, e.g. by solving daily workarounds. You can also have blitzes where everyone focuses on the same topic.

#### Learning
Everyone should be enabled to teach and to learn. The method of learning and teaching isn't important—it's important that organizational time is dedicated towards it. This includes methods such as classes, trainings, conferences, workshops, and mentoring.

Also encourage learning from peers. This can be done by e.g. jointly doing code reviews.

Encourage engineers to both go to conferences, but to also share their experiences from them.

#### Spreading Practices
Spread practices by having internal coaching and consulting. You can go further and have things like internal certification as well. There can be subject matter experts for any subject.

Google had a weekly *Testing on the Toilet* periodical. Their testing certification established three levels of automated testing: quickly establishing baseline metrics, setting a policy and reaching automated coverage goals, and striving towards a long-term coverage goal.

## Part VI—The Technological Practices Of Integrating Information Security, Change Management, And Compliance
We want to acheive not only devops goals, but also infosec goals. This can be done by automating controls and integrating them with our deployment pipeline. All process can be augmented with automated controls, rather than treating it as a separation of duties.

### 22. Information Security as Everyone's Job, Every Day
Problems come up when infosec is siloed outside of devops. Instead, we want to integrate infosec with everyone who works in the technology value stream.
<!-- TODO -->

### 23. Protecting the Deployment Pipeline, and Integrating into Change Management and Other Security and Compliance Controls
<!-- TODO -->