# The Rust Programming Language
Steve Klabnik, Carol Nichols
[Online](https://doc.rust-lang.org/book/)

## Introduction
> Rust is proving to be a productive tool for collaborating among large teams of developers with varying levels of systems programming knowledge. Low-level code is prone to a variety of subtle bugs, which in most other languages can be caught only through extensive testing and careful code review by experienced developers. In Rust, the compiler plays a gatekeeper role by refusing to compile code with these elusive bugs, including concurrency bugs. By working alongside the compiler, the team can spend their time focusing on the program’s logic rather than chasing down bugs.

It seems like Rust has classified types of bugs that people make. Is it possible to take this knowledge and spread it to others, even if the language they use doesn't have the same safety that Rust does?

## 1. Getting Started
### Installation
`rustup` is a CLI for managing Rust versions and tools.

To install `rustup`:
```
$ curl https://sh.rustup.rs -sSf | sh
```

To update `rustup`:
```
$ rustup update
```

### Hello, World!
Rust file naming uses snake_case. Rust files end with the `.rs` extension.

Code runs from a main function. The main function can have parameters.
```
fn main() {
    println!("Hello, world!");
}
```

- Indentation is always 4 spaces. 
- Things that end in `!` call a Rust _macro_, not a function. `println!` calls a Rust macro.
- The `rustc` CLI takes in the name of a source file and compiles the code into an executable with a similar name.

### Hello, Cargo!
`cargo` is Rust's build system and package manager.

Commands:

- `cargo new [project name]`
- `cargo build` to compile the project.
- `./target/debug/[project name]` to run compiled binary.
- `cargo run` to compile and run in one step.
- `cargo check` ensures that the code compiles, but doesn't produce a binary. It's faster than `cargo build`.
- `cargo build --release` creates a release binary by including optimizations when compiling.

- Packages of code are called _crates_.
- `cargo` expects code to live in a `src` directory.

## 3. Common Programming Concepts
### Variables and Mutability
- By default, Rust variables are immutable.
- `let` declares an immutable variable.
- `let mut` declares a mutable variable.
- Variables don't need to explicitly declare their type.
- `const` declares a constant; constants must explicitly declare type.

Variables can be shadowed by declaring a new variable with the same name. This creates a new variable entirely, and the previous value is no longer accessible.
```
fn main() {
    let x = 5;

    let x = x + 1;

    let x = x * 2;

    println!("The value of x is: {}", x);
}
```

Because this effectively creates a new variable, you can change the type of the new variable.

### Data Types
Rust is statically typed. Type declarations are optional if they can be inferred by the compiler.

There are four _scalar types_: integers, floating-point numbers, Booleans, and characters. There are two primitive _compound types_, which group multiple values into a single type: tuples and arrays.

- An integer overflow will lead to a panic in debug mode. It results in wrapping in release mode, but you shouldn't depend on the wrapping behavior.
- Tuple values can also be accessed via index, e.g. `x.0`, `x.1`, or `x.2`.
- Creating an array: `let a: [i32; 5] = [1, 2, 3, 4, 5];` or `let a = [3; 5];`.

### Functions
- Functions (and variables) in Rust use snake_case.
- A _statement_ is an instruction that performs an action, but doesn't return a value.
- An _expression_ evaluates to a resulting value.
  - A way to differentiate between statements and expressions: only expressions can be assigned to variables.
  - Expressions don't have ending semicolons. Adding a semicolon would turn the expression into a statement.
- Functions can return a value early with `return`, but typically the last expression will be returned implicitly (without a semicolon).

### Control Flow
- `if` conditions don't need to be in parentheses.
- You can assign results from `if` expressions to a variable.
- `loop` allows you to loop infinitely.
- You can create half-open ranges, e.g. 1..5 (1 to 4, inclusive).

## 4. Understanding Ownership
### What is Ownership?
- Every value has a variable that is its _owner_.
- Each value can only have one owner at a time.
- When an owner goes out of scope, the value is dropped.
- Assigning a variable to another variable is a _move_. The original variable becomes invalidated (out of scope).
- Rust design choice: by default, no deep copies are created. Automatic copying has little runtime cost.
- `clone` performs a deep copy, and may be expensive.
- Anything that is represented only on the stack has no difference between shallow and deep copy, so reassignment results in a copy. This is indicated on the type with the `Copy` trait. All simple scalar values and tuples of simple scalar values are `Copy` types.
- Passing a value into a function is similar to assigning the value to a variable. Non-`Copy` types become invalidated in the original block once they are passed into a function.
- Ownership is always moved when a value is assigned to another variable. A variable going out of scope will have its value cleaned up by `drop`, unless the value has already been moved and owned by a different variable.

### References and Borrowing
- Defining a function to take a reference to a value allows it use the object without taking ownership of the value.
- A reference to a value is written as `&foo`. The dereference operator is `*`.
- A reference as a function parameter is referred to as _borrowing_.
- References are immutable; you can't modify the referenced object.
- A mutable reference can be created with `&mut foo`. However, there can only be one mutable reference per piece of data per scope. This prevents data races.
- Creating a separate scope allows you to have multiple mutable references. The separate scope prevents simultaneous mutable references.
- Mutable references also cannot be used simultaneously with immutable references.
- A reference's scope continues to the last place that reference is used. As long as the previous references are out of scope, you can create a mutable reference.
- Dangling references produce compile-time exceptions in Rust.

### The Slice Type
- You can create a slice with `&foo[3..7]`.
- Generally, methods should take in string slices rather than `String`s.

## 5. Using Structs to Structure Related Data
### Defining and Instantiating Structs
- Field init shorthand allows for easier instantiation of structs where field names match with variable names.
- `..foo` syntax allows for setting unspecified fields to the same value of the given instance of the struct.
- Tuple structs allow for naming tuples to differentiate different types.
- Structs can store references, but that requires lifetimes (chapter 10).

### Method Syntax
- Methods on structs are declared and defined within an `impl` for that struct.
- Rust methods are selfish.
- Methods may take ownership, borrow, or mutably borrow `self`, just as other functions do.
- Rust has automatic referencing and dereferencing, so you don't need to worry what you are calling a method on.
- Methods without self are _associated functions_ and called via the `::` operator.
- Structs can have multiple `impl` blocks.

## 6. Enums and Pattern Matching
### Defining an Enum
- Rust enums are essentially sealed classes.
- `Option<T>` is an enum and is automaticaly included. It has two variants: `Some<T>` and `None`.

### The `match` Control Flow Operator
- `match` is exhaustive; all possible values must be covered.
- `_` is the syntax for default.

### Concise Control Flow with `if let`
- `if let` allows single-case `match` with less boilerplate. However, it isn't exhaustive, which could matter for future changes.

## 7. Managing Growing Projects with Packages, Crates, and Modules
### Packages and Crates
- A _crate_ is a binary or library. The _crate root_ is a file that makes up the root module of the crate.
- A _package_ consists of one or more crates that provide a set of functionality.
- Packages contain a `Cargo.toml` that describes how to build its crates.
- A package must contain at least one crate, and may have at most one library crate.
- `src/main.rs` is always the crate root of a binary crate with the same name as its package.
- `src/lib.rs` is always the crate root of a library crate with the same name as its package.
- Cargo passes crate roots to `rustc` in order to build libraries/binaries.

### Defining Modules to Control Scope and Privacy
- A _module_ organizes code within a crate for readability and reuse.
- It's kind of like organizing the files and directories within a file system.

### Paths for Referring to an Item in the Module Tree
- A _path_ defines how to find an item in a module tree.
- An _absolute path_ starts with `crate`.
- A _relative path_ starts from the current module.
- Modules are also used to define privacy boundaries.
- Rust is private by default.

### Bringing Paths into Scope with the `use` Keyword
- It's idiomatic to bring a function's parent module into scope with `use`, rather than the function itself. This makes it clear that the function isn't locally defined.
- For other things (structs, enums, etc.), it's idiomatic to specify the full path.
- `pub use` is used to re-export a name. The main intent is to develop code with one internal structure, while a exposing a different external structure for external programmers.

## 8. Common Collections
### Storing Lists of Values with Vectors
- The `vec![a, b, c]` macro allows for easier initialization of vectors.
- When a vector is dropped, its contents are dropped.
- Using enums allow you to store different types within a vector.

### Storing UTF-8 Encoded Text with Strings
- The compiler can coerce `&String` to `&str`.
- You can't index into a string because UTF-8 characters aren't always a single byte (and strings are represented as vectors of bytes).
- There are lots of ways to represent parts of a string: bytes, scalar values, and grapheme clusters.
- You can create string slices, but you have to be careful as the code will panic at runtime if you slice at a non-char boundary.
- Instead, you often want to use `.chars()` or `.bytes()` to iterate through a string.

### Storing Keys with Associated Values in Hash Maps
- Once owned values are inserted into a hash map, the hash map owns them.

## 13. Functional Language Features: Iterators and Closures
### Closures: Anonymous Functions that Can Capture Their Environment
- A closure is an anonymous function that can be stored as a variable or passed as an argument.
- Closures have their own unique anonymous type. Two closures will never have the same type.
- All closures implement at least one of `Fn`, `FnMut`, or `FnOnce`.
- `FnOnce` takes ownership of any captured values. It can only be called once, because it can only take ownership once.
- `FnMut` mutably borrows ownership of captured values.
- `Fn` immutably borrows ownership of captured values.

### Processing a Series of Items with Iterators
- Iterators must be declared as mutable, as they get consumed.

### Comparing Performance: Loops vs. Iterators
- Iterators don't add additional overhead compared to loops.