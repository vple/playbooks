# The Storyteller's Secret
Carmine Gallo  
[Amazon](https://www.amazon.com/Storytellers-Secret-Speakers-Business-Legends-ebook/dp/B012NCOC38)

This book breaks down the qualities that various speakers use to make them effective speakers.

## Storytellers Who Ignite Our Inner Fire

### 1. What Makes Your Heart Sing?
> The greatest waste is an unfulfilled idea that fails to connect with audiences, not because it's a bad idea, but because it's not packaged in a way that moves people.

People get excited about technology when it's tied to humanity. Steve Jobs used that passion and excitement to appeal to audiences. To be an inspiring storyteller, you need to have a passion that you want to share with your audience.

### 2. From T-Shirt Salesman to Mega Producer

Telling a good story involves continually refining it. Rejections should be taken as feedback on how to improve the story, allowing you to tailor it to the specific audiences that you'll be speaking to.

### 3. Conquering Stage Fright to Sell Out Yankee Stadium

Speaking well involves practicing and believing that you can do it.

### 4. A Rock Star Rediscovers His Gift in the Backstory of His Youth

Rely on your backstory to help create your stories.

### 5. Change Your Story, Change Your Life

The obstacles in your life give you a chance to grow. Overcoming them gives a classic narrative for your stories.

### 6. The Power in Your Personal Legend

Use a classic three part narrative—the hero's journey.

1. Trigger Event
2. Transformation
3. Life Lesson

### 7. A Coffee King Pours His Heart into His Business

Set forth a vision that people can believe and rally around.

### 8. We're Not Retailers with a Mission, We're Missionaries Who Retail

Tie what you're trying to do with a meaning. This gives people a reason why they want to believe and follow you.

### 9. If You Can't Tell It, You Can't Sell It

You need to bring people along an emotional journey for them to care. Building a story typically involves:

1. Grabbing their attention, e.g. with a question or challenge.
2. Give them an emotional experience with a relevant story leading to resolving the challenge.
3. Call to action.

*This is somewhat similar to the specific speeches that Dale Carnegie talks about, but here various aspects of the speeches seem to be merged into one.*

## Storytellers Who Educate

### 10. How a Spellbinding Storyteller Received TED's Longest Standing Ovation

Stories help win people over and build rapport. Accordintly, most of your talk should be taken up by story.

*This lines up with Carnegie saying that 3/4 of a talk should be example. Gallo recommends 65% as story.*

### 11. Turning Sewage into Drinking Water

> A "good story," however, offers "canon and breach." The "breach" is the page-turner, the unexpected twist that keeps you glued to the page or the screen.

Expectancy Violations Theory: When people behave outside of expected norms, they "violate" expected bheavior. This is uncomfortable for people, but can be used to positive effect in stories—when you're uncomfortable, you're feeling an emotion. Additionally, speakers appear more attractive, credible, and persuasive; audiences can't avoid novelty.

### 12. What You Don't Understand Can (and Does) Hurt You

Don't alienate an audience with stats or jargon. Use stories and analogies to help convey information and relate abstract and/or complex ideas.

### 13. The $98 Pants That Launched an Empire

Sharing background stories behind a product or idea helps people relate and buy into the idea more.

### 14. Japan Unleashes Its Best Storytellers to Win Olympic Gold

Your brand consists of stories.

### 15. A Funny Look at the Most Popular TED Talk of All Time

> The end of laughter is followed by the height of listening. —Jeffrey Gitomer

Humor helps with stories. It disarms the audience, gets them to pay attention, and focuses them on what you say next.

There are many ways to add humor:

- "off-beat" and unexpected jokes
- exaggerating a normal (though typically not expressed) opinion or feeling
- adopting a different point of view
- (slight) self-deprecation
- retelling a humorous situation

### 16. Dirt, Cigars, and Sweaty Socks Put a Marketer on the Map

Tie your content and story together with the channel you're sharing your story through. This is more or less being aware of your audience.

### 17. A Burger with a Side of Story

> My hope is that throught storytelling I'm able to name some things that you already knew in your heart, but had not named. —Danny Meyer

Sharing stories is how you can exemplify and emphasize a more abstract concept. This helps, for example, team members to understand and internalize the behaviors you want them to follow.

*Harari echoes this in Sapiens when he says that collective fictions are how humans coordinate and form shared beliefs.*

## Storytellers Who Simplify

### 18. If Something Can't Be Explained on the Back of an Envelope, It's Rubbish

> Complexity is your enemy. Any fool can make something complicated. It's hard to make something simple. —Richard Branson

Concisely state the key message behind your idea.

### 19. The Evangelizer in Chief

Use the rule of three. It:

1. Gives you a template to use.
2. Makes it easier for the audience to follow.
3. Helps persuade and drive action.

### 20. A Film Mogul's Granddaughter Cooks Up Her Own Recipe for Success

When you talk, you want to be approachable. Use simple language, be personable, and talk as if you're having a conversation. Be passionate and smile.

### 21. The Storytelling Astronaut Wows a TED Audience

Add visuals, either with actual pictures or by painting a picture with descriptive language.

### 22. "Dude's Selling a Battery" and Still Inspires

Make your message simple and easy to follow. The target comprehension level is for an 8th grader or below.

### 23. An Entrepreneur Makes *Shark Tank* History

Be concise. You should be able to get your message across quickly and clearly, within 60 seconds.

## Storytellers Who Motivate

### 24. Find Your Fight
Reframe your personal narratives to create identity. This also gives your life and stories purpose and meaning.

### 25. The Hospital Steve Jobs Would Have Built
Use stories to motivate your teams. Use them to illustrate your mission, purpose, and vision. People need to be able to understand what their job accomplishes.

### 26. A Hotel Mogul Turns 12,000 Employees into Customer Service Heroes
Stories help build a culture, which in turn inspires people to be loyal and to do a good job. The Wynn hotels do this by encouraging employees to share stories about their good customer experiences.

> Does anybody have a story about a great customer experience they'd like to share?

### 27. A Revolutionary Idea That Took Off on the Back of a Napkin
Don't like this chapter.

### 28. When Amy Lost Her Legs, She Found Her Voice
> If my life were a book and I was the author, how would I want the story to go?

Use tension and triumph to heighten stories. This often follows a three-part structure. The first part introduces the character, setting, and a turning point. The second part increases the tension and obstacles. The third part is where the hero finds what they need to triumph.

### 29. From Hooters to the C-Suite—A Former Waitress Shares Her Recipe for Success
Share your background with others to build a personal connection.

### 30. Trading Wall Street Riches for the Promise of a Pencil
Good stories must maintain attention. You need to identify which parts people pay attention to and which parts they don't pay attention to.

### 31. The Ice Bucket Challenge Melts the Hearts of Millions
Provide specific, tangible, and concrete details in your stories.

### 32. His Finest Hour—180 Words That Saved the World
There's no need to be fancy—use simple words that are accessible to your audience.

## Storytellers Who Launch Movements

### 33. Great Storytellers Are Made, Not Born
Practice rhetorical techniques—analogy, metaphor, repetition (anaphora), etc.

### 34. Millions of Women "Lean In" After One Woman Dares to Speak Out
Don't persuade with data; persuade with your story.

### 35. The 60-Second Story That Turned the Wine World on Its Side
Blend facts with narrative to make your point.

### 36. From My Heart Rather Than from a Sheet of Paper
Pixar story formula:

1. Once there was a _____.
2. Every day he _____.
3. Until one day _____.
4. Because of that _____.
5. Because of that _____.
6. Until finally _____.
7. Ever since then _____.

### 37. Story, Story, Story
The story always come first.