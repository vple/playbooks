# Power
Jeffery Pfeffer
[Amazon](https://www.amazon.com/gp/product/B003V1WSZK/)

## Introduction
Being politically savvy and seeking power are related to career success and managerial performance. Managers primarily interested in power were the most effective out of three groups, both at achieving influence and accomplishing their jobs. The other two groups were primarily interested in being liked or personal goal achievement.

You should want power for a few reasons:

1. It's related to living a longer and healthier life.
2. Power, and the visibiility / stature that comes with it, can produce wealth.
3. Power is a part of leadership and necessary to get things done.

There are three major obstacles to gaining power.

### Believing the World is a Just Place
Many people believe that the world is fair, with people getting results based on their merit. Additionally, when they see others getting results that they believe don't come from merit, they assume those people are only experiencing a temporary success.

This prevents people from learning from all situations and all people, particularly for the situations or people they don't respect. This also causes people to not recognize the importance of building a power base.

### Leadership Literature
Lots of leadership literature omits the power plays that the leaders themselves used to get to the top. The authors of these books often believe they're being truthful, but they've already developed the ability to present themselves well and will emphasize their positive qualities over their negative ones. As they're in power, they get to also write history to make themselves look good. Finally, people miscorrelate results—they believe that someone's success is often an indicator of positive qualities and behaviors.

### Yourself
You're your own worst enemy. People preserve their self-esteem my preemptively surrendering or putting obstacles in front of themselves. If you intentionally do something that reduces your chances at success, you can then use that as an excuse when you fail instead of confronting your shortcomings.

## 1. It Takes More Than Performance
Positive job performance doesn't guarantee career success or that you will maintain a position of power. Moreover, poor performance won't necessarily cost you your job. People assume that good performance is sufficient to get power and avoid organizational problems. As a result, they leave this to chance and don't manage their careers.

> Job performance matters less for your evaluation than your supervisor's commitment to and relationship with you.

There are also times when outstanding job performance may hurt your ability to get promoted. Your boss may be willing to reward you financially, but they may be reluctant to do things that would result in them losing you. The key here is to manage up.

### Get Noticed
People in power are busy with their own concerns and responsibilities. Your first responsibility is to make sure those above you know who you are and what you're doing. You do this by telling them. If people don't know or care about you, they won't consider you when picking someone for a senior role.

This relates to the "mere exposure effect." With other factors being equal, people prefer and choose what's familiar to them. 

### Define Performance Metrics
You won't do equally well at all aspects of your work. Emphasize the areas where you do well. Define performance in a way that makes you look good. There are limits to how much you can do, but you can highlight things that look good for you and bad for your competition.

### Know What Matters To Your Boss
People assume they know what matters, but they often don't. Instead you should ask them, regularly, what's most important and what they think you should be doing. Additionally, asking for assistance in a way that still demonstrates competence is a good way to flatter those above you.

### Make Others Feel Better About Themselves
> The surest way to keep your position and to build a power base is to help those with more power enhance their positive feelings about themselves.

This also means that you shouldn't criticize your boss, particularly if the criticism will be sensitive or an issue that your boss is insecure about. Worry about your relationship with your boss at least as much as your job performance. You should use flattery; most people underestimate and underutilize it.

## 2. The Personal Qualities That Bring Influence
To develop qualities that allow you to gain and hold onto influence, you need to deal with three obstacles:

1. Believe that personal change is possible.
2. See yourself, your strengths, and your weaknesses as objectively as possible.
3. Understand the most important qualities for building a power base so that you can focus on those.

When improving yourself, you may recognize where you need to improve but not how to do so. Get advice from others who are good at what you want to become.

### Personal Qualities That Build Power
There are seven qualities associated with producing personal power. They're split into two categories. The *will* qualities involve a drive for tackling challenges. 

- **ambition**  
	Amibition is needed to drive the effort, hard work, persistence, and sacrifices needed to reach success. Ambition helps people avoid giving up or giving in to irritations.
- **energy**
	Energy helps build influence by being contagious, enabling you to work the long hours you need, and by signaling to others that you're committed and loyal.
- **focus**
	The same effort is much more powerful when it's focused. You can be focused in a particular industry or company, having a deeper understanding and network. Or, you could be focused on certain activities and skills. You might also be focused on activities in your position that are most critical and impactful.

The *skill* qualities involve the ability to turn that ambition into accomplishment. 

- **self-knowledge**
	This involves reading, learning, and reflecting on your experiences. Most important is reflection—it's impossible to learn without it.
- **confidence**
	Appearing confident and knowledgeable helps you build influence. Others will also assume that you have power because you are confident. If you aren't confident about what you deserve and want, you'll be more reluctant to push for it and end up less successful overall.
- **empathy**
	This involves putting yourself in others' positions. This involves being able to read others' thoughts and feelings. It's important to recruit others to your side, or to at least reduce their opposition.
- **capacity to tolerate conflict**
	Most people can't deal with conflict. Being able to handle it yourself gives you a leg up.

## 3. Choosing Where To Start
Not all departments are equal for gaining power. People typically make the mistake of joining departments with the organization's current core activity or product. This isn't always a good idea since that's where you'll have the most talented competition and most established career paths. It's easier to move up quickly by going to niches where you can develop leverage.

One source of departmental power is having *unit cohesion*. This was developed, e.g., via socialization rituals (running the projector, preparing briefing books, etc.) that provide skills and knowledge while also developing communication and trust.

Another source is to provide critical resources or to solve critical organizational problems.

### Diagnosing Developmental Power
This concerns figuring out which departments have the most power within an organization.

- relative pay  
	Starting salaries and salaries for senior positions show relative power.
- physical location  
	Physical proximity to others in power signals power and gives power via more access. 
- positions
	The department representation within boards and committees represents those departments' relative power.

### Trade-Offs
You need to decide between joining a powerful organization with a lot of talented competition, or taking the risks with pioneering a new path. Your approach should depend on how much of a risk taker you are, as well as if you're satisfied being carried by a powerful tide or if you want to get ahead of it and create your own space.

## 4. Getting In: Standing Out and Breaking Some Rules
> You need to get over the idea that you need to be liked by everybody and that likability is important in creating a path to power, and you need to be willing to put yourself forward.

The worst that can happen when you ask is usually being turned down. Developing a bad reputation often won't happen, and still doesn't outweigh the benefits from taking this risk. People often avoid help because:

- They're taught to be self-reliant.
- They're afraid of rejection and the self-esteem hit.
- They ask based on the odds that help will be given.

However, people underestimate the odds of others helping them. They focus on the cost of the request and forget the factors that make saying no costly. Asking is also flattering—it affirms that you have something useful that others want from you. This is best done by focusing on the other person's importance and accomplishments, as well as reminding them of commonalities.

> Many people believe that they can stand out and be bold once they have become successful and earn the right to do things differently. But once you are successful and powerful, you don't need to stand out of worry about the competition. It's early in your career when you are seeking initial positions that differentiating yourself from the competition is most important.

To stand out from competition, you need to build your personal brand and promote yourself. Playing by the rules or established practices favors those already in power. When developing power, you need to stand out and break away from the rules in a way that makes you stand out and makes you memorable.

This doesn't necessarily mean being likable. People are univerally assessed on warmth and competence. However, to appear competent, it helps to seem a bit tough or mean.

## 5. Making Something out of Nothing: Creating Resources
Acquiring power requires having resources to reward friends and punish enemies. Having these resources only gives you power if you use them strategically to help others, gaining their support and favor. Anything that people want or need is a resource: money, jobs, information, social support, friendship, or help with their job.

Since resources correlate to power, you should (1) choose jobs with greater direct control of budget or staff and (2) your power stems from your position and the resources you control.

You should start building your power base immediately, regardless of your current position. You can:

- Provide attention and support.
- Do small but important tasks.
- Build a resource base inside and outside your organization.
- Leverage your association with a prestigious institution.

## 6. Building Efficient and Effective Social Networks
Networking: "Behaviors that are aimed at building, maintaining, and using informal relationships that possess the (potential) benefit of facilitating work-related activities of individuals by voluntarily gaining access to resources and maximizing...advantages."

Networking behaviors typically include building, maintaining, or using either internal or external contacts. By being in contact with others, they're more likely to think about you when they need someone for a particular role.

People don't spend enough time networking because the effort involved, because they find it distasteful, or because they undervalue the importance of social relationships. In reality, networking doesn't take that much time and effort—it takes thought and planning. Figure out who you want or need to meet, then figure out how to build those connections.

Not everyone is equally useful for networking. Often weak social ties are more likely to connect you to new people, organizations, and information. These weak ties are only useful if they are casual acquaintances that are *able* and *willing* to link you into diverse networks. A good approach is to know lots of people from different circles, but not necessarily know people well enough to develop close ties with them.

It helps you to associate with high-status people. However, this also means you can't easily move down to lower status people, as that would cause you to lose your own status. A way to gain status is to start an organization that's so compelling that high-status people join the project.

It's also important to be in a central position in your network. You can assess this by determining how much information and communication goes through you. You can build this centrality by how you structure your organization, or via physical location.

### Trade-Offs
The amount of time and how you network should depend on how much your job requires building relationships and the type of knowledge most useful for the job. 

There's two types of knowledge—explicit, codified knowledge and implicit, tacit knowledge. The former is good for projects where knowledge needs to be readily transferred once you get it; you want a large network of weak ties. The latter is good for projects where you need people to explain their expertise; you want a smaller network of close ties.

Additionally, loose ties are better for product efforts involving doing new things. Strong, tacit ties are better where you can anticipate the needed information and the projects rely on existing competencies.

> A large network of weak ties is good for innovation and locating information, while a small network of strong ties is better suited to exploiting existing knowledge and transferring tacit skills.

## 7. Acting and Speaking with Power
The secret of leadership is the ability to play a role, to pretend, to be skilled in the theatrical arts. This includes projecting assurance, especially if you aren't sure what you're doing. This is important since it affects others' emotions, and eventually becomes self-reinforcing.

A useful thing is to display anger instead of sadness or remorse, as it makes you appear more competent, smart, and dominant. 

Nonverbals matter. You can also change your appearance, via your clothes and hairstyle. Pay attention to your hair, style of clothes, and colors. You can also "move with power," taking advantage of your posture. Powerful people take up space. Your gestures should also be decisive; making them short and forceful, not long and circular. Eye contact is important as well.

When you need to display certain emotions, you can rely on a memory where you felt the emotion to help you actually show it.

Verbals. If you need it, take time to collect your thoughts and composure before responding. Interruption signals power on the side of the person interrupting.

There are more subtle aspects too. One indicator of power is the ability to win direct contests. The second is to set the agenda or the process for dealing with issues. The third is determining the rules for how people interact when debating these topics.

Creating images and emotions makes your language more powerful and able to persuade people despite reason. There are also several conventions you can draw on:

- Us versus them.
- Pause for emphasis, inviting approval or applause.
- Use a list of three items.
- Use contrastive pairs, with mirroring structure.
- Avoid scripts or notes.

Use humor appropriately.

## 8. Building a Reputation: Perception Is Reality
Accomplishments are important, but so is your reputation. Building your image and reputation helps maintain your position once you get it.

People for impressions of you within the first few seconds or milliseconds. This is based off your behavior and what they know about you, but also your facial expression, posture, voice, and appearance. This is often a reasonably stable impression. These impressions also predict other future evaluations. How people interpret what they see depends on their prior expectations.

To construct an image, you have to think strategically about the elements you want in your reputation. This can include traits such as dedication or intelligence.

Once you have this, you want to develop your image in the media. You do this by being persistent until you get a foot in. To build relationships with media people, be helpful and accessible.

There is a dilemma with self-promotion: too little and you seem incompetent when it comes to advocating for yourself, too much and you seem too arrogant. The way around this is to get others to speak well about you. Even if they're clearly in your camp (or even if you hired them), this will still increase others' perceptions of you.

Showing some weaknesses, as long as they aren't major ones, is also good. Psychologically, others will accept you more since, despite your flaws, they are putting in the effort to work with you.

## 9. Overcoming Opposition and Setbacks
People will push back if forced into a certain behavior. One way to deal with these people is to leave them a graceful way out. You can also rely on strategic outplacement—moving someone to a better job elsewhere where they won't bother you. Giving others things that makes them feel better works to your advantage.

Always keep the end goal in mind. If you know what victory looks like and what you need to get there, don't get distracted fighting other battles. This includes not taking things personally and making sure important relationships work.

Be persistent. People will get worn down and give in. Additionally, you'll have opportunities as environments change. You also want to move first and gain initiative.

Other advice for dealing with opposition:

- You can set up rewards and punishments to shape the behavior of others.
- Make your objectives seem compelling.

When dealing with setbacks:

- Don't give up. You can overcome embarrassment by talking to others, helping you to regain focus.
- Continue to do what made you successful.
- Act as if things are fine and under control. You can share what happened, but maintain an attitude that you'll do well in the end.

## 10. The Price of Power

### Visibility and Public Scrutiny
With power, you'll be much more in the public eye. This makes things harder for you because:

- The presence of others decreases performance on tasks involving new learning or novel / difficult activities.
- Your effort gets split, as some of it is spent managing your impression.
- To continue to "look good," people stop taking risks.

### The Loss of Autonomy

### The Time and Effort Required

### Trust Dilemmas
The more powerful you are, the more people will want your job. This makes it harder to trust people.

### Power as an Addictive Drug

## 11. How—and Why—People Lose Power
Holding on to power is becoming harder to do, but not impossible.

Power leads to people more actively trying to obtain what they want, and feeling less inhibited as they do so. This results in overconfidence, insensitivity, stereotyping, and seeing people as tools. People lose power when this spills out and they neglect the needs of others who can cause them problems.

Not letting power go to your head can help you maintain your position. You want to maintain perspective and balance. It helps to periodically expose yourself to a social circle that doesn't care about your position.

> Unless you understand yourself pretty well, you're going to lose control of yourself.

Another reason people lose power is because they misplace or give too much trust to others. They've become overconfident and less observant, tending to trust what others tell them. It helps to watch someone's actions when trying to determine how much to trust them.

Lack of patience will also cost you your position. You have to do a lot of events and deal with lots of feedback that take away from your job. It's tempting to lash out, but some of these people are capable of taking you down.

People also get tired. If you want to stay where you are, you need to have the energy, stamina, and vigilance to keep going.

When you do leave, do so gracefully. It's possible and desirable to leave before the party's over, leaving others with fond memories of you.

## 12. Power Dynamics: Good for Organizations, Good for You?
Power dynamics are typically good for you, but perceived power dynamics are detrimental to the organization. You shouldn't worry about that, since your employer probably doesn't worry about you.

It may also be impossible to separate power dynamics from hierarchical structures. Hierarchies form in pretty much every society. Many people have trouble with hierarchical relationships (both having or lacking control).

Having power is also useful for getting things done.

## 13. It's Easier Than You Think
The first step in building a path to power is to pick an environment that fits your aptitudes and interests, where you can be successful technically and politically. This means:

- Being brutally honest about your strengths, weaknesses, and preferences.
- Not getting trapped into following the crowd because others are.
- Being objective about yourself, the job, and the risks and opportunities.