# How to Read a Book—Overview
Mortimer J. Adler, Charles Van Doren  
[Amazon](https://www.amazon.com/How-Read-Book-Classic-Intelligent/dp/0671212095)

## Summary

This book teaches how to read, understand, and think critically about a book that is worth reading—a book that has something to teach us and deserves to be read actively. This is primarily done through analytical reading, a method of reading to deconstruct and evaluate an author's arguments.

## Concepts

### A Case for Good Reading

Reading is both a primary means of gaining understanding about the world and something that we must learn to do individually. This involves learning to read actively and understanding what we read.

### Levels of Reading

1. Elementary Reading
2. Inspectional Reading 
3. Analytical Reading
4. Syntopical Reading

### Analytical Reading

The main purpose of analytical reading is to gain an understanding of a book by answering four basic questions. Each question can be answered by following certain rules. All questions & rules below are quoted directly; bolding is mine.

1. What is the book about as a whole?
	- **Rule 1.** You must know what kind of book you are reading, and you should know this as early in the process as possible, preferably before you begin to read.
	- **Rule 2.** State the unity of the whole book in a single sentence, or at most a few sentences (a short paragraph).
	- **Rule 3.** Set forth the major parts of the book, and show how these are organized into a whole, by being ordered to one another and to the unity of the whole.
	- **Rule 4.** Find out what the author's problems were.
2. What is being said in detail, and how?
	- **Rule 5.** Find the important words and through them come to terms with the author.
	- **Rule 6.** Mark the most important sentences in a book and discover the propositions they contain.
	- **Rule 7.** Locate or construct the basic arguments in the book by finding them in the connection of sentences.
	- **Rule 8.** Find out what the author's solutions are.
3. Is the book true, in whole or in part?
	- **Rule 9.** You must be able to say, with reasonable certainty, "I understand," before you can say any one of the following things: "I agree," or "I disagree," or "I suspend judgment."
	- **Rule 10.** When you disagree, do so reasonably, and not disputatiously or contentiously.
	- **Rule 11.** Respect the difference between knowledge and mere personal opinion by giving reasons for any critical judgment you make.
4. What of it?
