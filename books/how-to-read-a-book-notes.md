# How to Read a Book—Notes
Mortimer J. Adler, Charles Van Doren  
[Amazon](https://www.amazon.com/How-Read-Book-Classic-Intelligent/dp/0671212095)

## The Dimensions of Reading
### 1. The Activity and Art of Reading

> This is a book for readers and for those who wish to become readers ... people who are still accustomed ... to gain a large share of their information about and their understanding of the world from the written word.

Although there are new mediums for gaining information (radio and television in the book, the internet today), that knowledge does not necessarily help us understanding things. For one, we get overwhelmed with facts. Two, these media sources package their information to make the listener believe they are thinking and making up their mind, but they are really taking in a pre-packaged opinion.

#### Active Reading

When we read, we can read more or less actively. The more active the reading the better. A reader is "better if he demands more of himself and of the text before him."

Reading is often viewed as a passive activity, since it involves *receiving* communication. But receiving doesn't mean passive; a catcher in baseball is also receiving. Pitchers and batters send communications; catchers and fielders receive them. All are active. Even more, catching involves catching every kind of pitch. Likewise, reading involves catching every kind of communication.

> Successful communication occurs in any case where what the writer wanted to have received finds its way into the reader's possession.

Even more, pitchers and catchers are only successful if they cooperate. Writers with good control make it easier for a reader to catch their meaning. Likewise, readers must work to fully catch a piece of writing. Unlike a baseball, a piece of writing can be only partially received. The degree to which it's received depends on the effort and skill of the reader.

Exactly what active reading entails will be discussed throughout the book.

#### The Goals of Reading: Information vs. Understanding

Success in reading a book isn't determined just by being able to receive everything the writer intended to communicate. Success involves *gaining understanding* through reading the book.

When you read, you either understand everything the author intended or you don't. If you do, you may have gained information. But you didn't gain understanding, because you already understood everything the author had to say.

If you don't understand the book, you will hopefully realize that you don't understand it all. It contains something that can increase your understanding. The aim of reading is then to "gradually lift yourself from *a state of understanding less to one of understanding more.*"

> Thus we can roughly define what we mean by the art of reading as follows: the process whereby a mind, with nothing to operate on but the symbols of the readable matter, and with no help from outside, elevates itself by the power of its own operations. The mind passes from understanding less to understanding more. The skilled operations that cause this to happen are the various acts that constitute the art of reading.

So, there's a difference between reading for information and reading for understanding. Reading for understanding can only take place when (1) there's an "initial inequality in understanding" between the writer and the reader and (2) the reader is able to "overcome this inequality in some degree," even if they can't fully reach the same understanding as the writer.

> [T]his book is about the art of reading for the sake of increased understanding.

#### Reading as Learning: Learning by Instruction vs. Learning by Discovery

> To be informed is to know simply that something is the case. To be enlightened is to know, in addition, what it is all about: why it is the case, what its connections are with other facts, in what respects it is the same, in what respects it is different, and so forth.

The difference between information and enlightenment is that "enlightenment is achieved only when, in addition to knowing what an author says, you know what he means and why he says it." Being informed is a prerequisite to enlightenment, but you shouldn't stop at being informed. People typically make this mistake when they are widely read but not well-read, which happens when they don't read critically. They make the mistake of believing that reading and listening are relatively effortless, which is not true of reading for understanding.

In actuality, reading (for understanding) involves a lot of effort. Reading is a form of learning through instruction, where one person teaches another via speech or writing. This is in contrast to learning through discovery, where we gain knowledge without being taught. But the two are really two sides of the same coin—instruction is just aided discovery. Learning through discovery involves thinking, observation, memory, and imagination—all things requiring effort. But then so too does reading, a form of aided discovery.

*I agree that reading for understanding requires effort, but the argument here doesn't seem very well-phrased. You do have to construct and follow a train of thought, but I think this comparison needs to be more fleshed out and tied more closely with being widely read.*

#### Present and Absent Teachers

When we learn through listening, there's normally a teacher present who is able to answer our questions without us needing to think. With reading, the teacher is absent. You must answer yourself any questions that you ask of a book. If we're going to read books on our own, we must know how to be able to learn from a book.

### 2. The Levels of Reading

There are four levels of reading. They're cumulative; each one builds upon the previous levels.

The first level is Elementary Reading, named because it's learned in elementary school. By mastering elementary reading, you go from nonliteracy to beginning literarcy. The focus here isn't on meaning or implications. It's just on being able to determine the actual words that are written. 

The reader is trying to answer "What does the sentence say?" Elementary reading is generally a level we've all passed, but may return to, for example, when trying to read something in a foreign language.

The second level is Inspectional Reading, characterized by an emphasis on time. The reader aims to get the most out of a book within a certain time period. Usually this period is short, and definitely too short to get everything out of the book. The aim of inspectional reading is to get a surface level understanding of the book, which can still include a lot. 

The reader is trying to answer questions along the lines of "What is the book about?" After an inspectional reading, the reader should be able to answer, "What kind of book is it—a novel, a history, a scientific treatise?" Without an inspectional reading, it's harder to understand the book as you read it because you're trying to develop a superficial knowledge of the book at the same time.

The third level is Analytical Reading. It's the best reading you can do. Unlike inspectional reading, it's the "best and most complete reading that is possible given unlimited time." Analytical reading is done for the sake of understanding, and isn't necessary if your goal is information or entertainment.

The reader is trying to ask many, organized questions about what they're reading. This is the main focus of the book and expanded on later. Analytical reading is very active.

The fourth level is Syntopical Reading. This is comparative reading—the reader reads several books and compares them to one another and to their common subject. "With the help of the books read, the syntopical reader is able to construct an analysis of the subject that *may not be in any of the books.*" This form of reading involves the most effort, even if the materials being read are relatively easy and unsophisticated.

### 3. The First Level of Reading: Elementary Reading

*This chapter is generally irrelevant to the main point of the book, and likely included for completeness.*

#### Stages of Learning to Read

There are four distinguishable stages in a child's progress towards mature reading ability. 

The first is reading readiness, which includes various kinds of reading preparation. Trying to teach a child who isn't ready to read is usually self-defeating, since they will be frustrated and may continue to dislike reading throughout life. Delaying to teach a child reading is not an issue, other than parent concern.

The second involves reading very simple materials. Kids in this stage typically learn 3-400 words in their first year. By the end of this stage, children can read simple books independently and with enthusiasm. Adler points out that it's somewhat magical that a child suddenly goes from seeing meaningless symbols on a page to being able to read a simple sentence in a span of 2-3 weeks.

The third stage involves rapid vocabulary building and improved ability to figure out unfamiliar words through context clues. Children also learn to read for different purposes and in different areas of content, e.g. science or language arts. They learn that reading is something that can be done recreationally.

The fourth stage is refinement and enhancement of the previous skills. Students are able to carry over and contrast concepts between different pieces of writing. This stage is typically reached by a person's early teens. As of the 1970s, many did not reach this level.

#### Stages and Levels

The four stages above are all stages of the elementary reading level. Each stage is typically completed by kindergarten, first grade, fourth grade, and ninth grade, respectively. At this point, a reader is "mature" in the sense that they are capable of reading almost everything, but they still don't know how to read past the elementary level.

Chapter 1 focused on defining reading for understanding, and pointed out that a reader must be able to gain understanding from a book. This is only possible when a reader is able to read independently and learn on their own. In turn, it's not possible to truly learn to read during the elementary reading stages, since children are aided by present teachers. Once they are able to read without aid, children are able to begin to become really good readers.

#### Higher Levels of Reading and Higher Education

*This section is essentially Adler describing and complaining about the state of reading levels in the 70s.*

As of the 1970s, many high schools and colleges had to provide remedial reading classes since students did not fully master elementary reading. However, these were just remedial classes—they weren't classes that taught reading beyond the elementary level. Adler claims that a good high school should produce competent analytical readers and that a good college should produce competent syntopical readers. *(This is probably much more accurate as of 2018, but I don't know to what degree.)* Instead, students had to spend another four years in grad school to reach the syntopical level. This adds up to 20 years just to learn to read.

### 4. The Second Level of Reading: Inspectional Reading

Inspectional reading is a true level of reading, distinct from both elementary and analytical reading. Because inspectional reading builds on elementary reading, you can't read inspectionally if you struggle with understanding words, syntax, or grammar.

Learning inspectional reading involves learning the two types of inspectional reading. Experienced readers perform both simultaneously, but it's easier for beginning readers to treat them as two separate steps.

#### Inspectional Reading I: Systematic Skimming or Pre-reading

Suppose you have a book. You don't know if you want to read the book, although you suspect it might deserve an analytical reading. You also don't have much time. What do you do?

You must skim, or pre-read the book. The main goal of this is to figure out if the book is worth reading, but it can also tell you lots of other things about the book. Even if you decide the book is not worth reading, you'll be able to understand the author's main point and argument and what kind of book it is.

Learning to skim isn't hard. It's just picking out the key parts of the book. Here are some suggestions:

1. Look at the title page and the preface. This allows you to figure out the scope or aim of the book, as well as the angle the author is taking on the subject. This also gives you an idea of the subject and allows you to categorize the book.
2. Study the table of contents, as if it is a road map.
3. Check the index. This lets you estimate the range of topics covered and see the author's references. You can find and read passages on crucial terms, such as those terms with many references. These passages may also lead you to the crux or key points of the book.
4. Read the publisher's blurb. These blurbs may be (partially) written by the authors, and they'll try to summarize the main points of their book. Even if it's just fluff, that may indicate that there's not much of substance in the book either. If you've already decided that the book isn't worth reading, you can put it aside at this point.
5. Look at the chapters that seem to be pivotal to the book's argument. If the chapters have summary statements at the start or end, read those carefully.
6. Turn the pages, reading occasional paragraphs. Don't read more than a few pages at once. Look for the main point or argument. Make sure to read the last few pages of the main book (and not the epilogue).

#### Inspectional Reading II: Superficial Reading

> In tackling a difficult book for the first time, read it through without ever stopping to look up or ponder the things you do not understand right away.

Here, you want to pay attention to what you can understand and gloss over the parts that you don't immediately grasp. This will give you a much better understanding on your second reading. Even if you never revisit it, understanding part of a tough book is better than not understanding it at all.

Avoid things like looking up words, checking out the footnotes, etc. Done prematurely, they make your reading harder. Doing so makes you forget parts of the book and lose sight of the whole. You'll miss the forest for the trees.

*My initial understanding is that this sounds impossible, particular for lengthy books. I think the intent here is more in line with step 6 of the first type of inspectional reading. Presumably it's more along the lines of lightly reading each chapter for its main (understandable) points, without worrying about not fully understanding the author's more nuanced arguments. In this way it can also be done at the same time as the first type.*

#### On Reading Speeds

The two inspectional reading steps are both done rapidly, no matter how long or difficult the book is.

The actual reading speed should vary depending on the book. This involves knowing when different speeds are appropriate. For example, inspectional readings are often done much faster than analytical readings. However, even during analytical readings, there are some parts of the book that are less relevant and should be read quickly. The meatier portions will be difficult and should be read slowly.

#### Fixations and Regressions

*It wasn't included in the notes earlier, but Adler's view on speed reading courses is that they're just remedial reading classes.*

Speed reading courses have noticed that people have certain fixtures and regressions when they read. They may continue to sub-vocalize the words they're reading. Their eyes may stop at multiple points along a line, or even return to phrases or sentences that were already read. 

Your mind can grasp sentences and paragraphs much faster than this. These courses then focus on getting rid of these fixtures and regressions. This can be done fairly easily by sweeping your fingers across the line you're reading while forcing your eyes to follow your fingers.

#### The Problem of Comprehension

Does reading faster affect comprehension? Speed reading classes generally advertise that comprehension will go up with reading speed, although that is likely just because the reader is concentrating more (and therefore comprehending more). However, this is still a limited comprehension where the reader can only answer simple questions.

Often it can take a significant amount of time to comprehend weightier portions of a text. Comprehension can't occur without an analytical reading.

#### Summary of Inspectional Reading

There's no single right speed to read at. Reading something quickly is only valuable when the book isn't really worth reading.

> Every book should be read no more slowly than it deserves, and no more quickly than you can read it with satisfaction and comprehension.

For most people, speed is not their main issue. Skimming a book is always a good idea, especially if you don't already know if your book is worth reading. It helps even when you intend to analytically read a book, as it lets you understand the book's form and structure.

Don't try to understand every word or page of a difficult book on your first pass. You can, and should, be superficial. This prepares you to read it well the second time.

### 5. How to Be a Demanding Reader

The thing that keeps you awake when you read a book is whether or not it makes a difference to you that you read the book you have in hand. If you want to grow somehow from reading, you have to stay awake. You're making an effort for which you expect to be repaid.

#### The Essence of Active Reading: The Four Basic Questions a Reader Asks

> Ask questions while you read—questions that you yourself must try to answer in the course of reading.

The core of active reading is asking the right questions, in the right order. There are four main questions to ask about any book:

1. What is the book about as a whole? This involves determining the book's main theme and how it's divided into essential sub-themes or topics. 
2. What is being said in detail, and how? These are the points, claims, and arguments that the author is using to construct the messages in their topics and themes.
3. Is the book true, in whole or in part? Once you understand the author's argument, you're obligated (if you're reading seriously) to make up your own mind about the author's argument.
4. What of it? This is the significance of the book. Is the author's message actually important? Is there anything you should do to follow up on the book?

These are the core questions that you need to answer through analytical reading, and will be elaborated on throughout the rest of the book. Getting in the habit of asking these questions makes you a demanding reader. Being able to answer these questions precisely and accurately makes you proficient in the art of reading.

#### How to Make a Book Your Own

Demanding readers both ask questions and answer them as they read. This is much easier to do with a pencil in hand, writing in your book.

Buying a book makes it your property. But you don't truly own the book until you've ingrained it into yourself. Adler claims this is best done by writing in it. Additionally, writing helps you to be able to articulate your thoughts. It also helps you remember the author's thoughts.

> The person who says he knows what he thinks but cannot express it usually does not know what he thinks.

Some suggested ways to mark up a book:

1. Underlining major points and important or forceful statements.
2. Vertical lines in the margin, either to emphasize an already-underlined statement or to indicate a passage too long to be underlined.
3. Symbols (e.g. stars) in the margin, used to emphasize the most important statements or passages in the book. Earmarking these pages helps you quickly find them later. These should be used sparingly.
4. Numbers in the margin, used to sequence the points the author is making in an argument.
5. Numbers of other pages in the margin, to indicate where to find related points in the book. "Cf", meaning "compare" or "refer to", can be used to indicate the other page numbers.
6. Circling key words and phrases.
7. Writing in the margins or at the top / bottom of the page, used for writing questions, summarizing arguments, etc. 
8. Writing in the endpapers in the back of the book, used to make an index of the author's main points.
9. Writing in the endpapers in the front of the book, used to outline the entire book.

#### Three Kinds of Note-making

##### Structural

During inspectional reading, we try to answer what the book is about and how the book is structured. These notes can be made on the contents and title pages.

##### Conceptual

During analytical reading, you need to answer questions about the truth (accuracy) and significance of the book. These are conceptual notes.

##### Dialectical

This is the "shape of the discussion" that the authors are "having" when doing syntopical reading. Since this concerns several books, it needs to be recorded separately.

#### From Many Rules to One Habit

Learning to read involves doing a number of separate acts altogether as a single act. But in order to do so, you must first learn to do them separately. This is challenging and frustrating, especially as individually they may make less sense. Adler compares this to skiing—you have to learn a number of new skills, with the ultimate goal being to just look in front of you and enjoy the ride. He's offering encouragement here—learning to read takes time and patience, but it is possible and will pay off in the long run.

## The Third Level of Reading: Analytical Reading

*Analytical reading involves a number of rules, which will be introduced over several chapters.*

### 6. Pigeonholing a Book

#### The Importance of Classifying Books

> **Rule 1.** You must know what kind of book you are reading, and you should know this as early in the process as possible, preferably before you begin to read.

This involves distinguishing between fiction and non-fiction, and even further dividing what you're reading. A work of fiction can be a novel, a poem, a play, etc. Non-fiction spans a number of topics, such as history and science. The main reason for doing so is that different types of books follow different patterns and modes of thought or argument. Classifying books prepares you for what to expect, which in turn helps with understanding the book.

> Hence, as books differ in the kinds of knowledge they have to communicate, they proceed to instruct us differently; and, if we are to follow them, we must learn to read each kind in an appropriate manner.

Classifying books can often be done during an inspectional reading. The title, subtitle, contents, etc. hint towards what kind of book it is. Many book titles are fairly specific, and can already tell you a broad framework for the book. Authors also go through the trouble of making the titles and subtitles descriptive.

#### Practical vs. Theoretical Books

> The practical has to do with what works in some way, at once or in the long run. The theoretical concerns something to be seen or understood. [...] Theoretical books teach you *that* something is the case. Practical books teach you *how* to do something you want to do or think you should do.

*Most of this chapter is giving examples of different types of books, as well as some of their distinguishing characteristics.*

### 7. X-raying a Book

Every book has a skeleton; the analytical reader's job is to find that skeleton.

> **Rule 2.** State the unity of the whole book in a single sentence, or at most a few sentences (a short paragraph).

In other words, summarize what the book is about—summarize its purpose, theme, or main point. Being able to summarize concisely demonstrates that you actually understood the singular point of the book.

Good authors often give you a reasonable idea of the book's plot via a subtitle, preface, book jacket blurb / summary, or some other means. Adler gives a number of examples of plots for various classic books.

> **Rule 3.** Set forth the major parts of the book, and show how these are organized into a whole, by being ordered to one another and to the unity of the whole.

A good book is complex (i.e. made up of parts). Fully understanding the book means being able to identify the distinct parts and how they relate to one another to form a whole. Each part is a unit on its own, but these parts are also all related and merge to form a more cohesive argument. 

Put another way, understanding the pieces of the book means being able to create an outline of the book. The level of detail you go into when outlining the book depends on how worthwhile it is to reach that level of detail. Often a book is already roughly outlined for you, e.g. by how it's divided into chapters.

Rules 2 and 3 are really flip sides of the same coin. The first looks at the book holistically. The second looks at how the parts of the book create that whole, or the book's complexity.

#### The Reciprocal Arts of Reading and Writing

> The reader tries to *uncover* the skeleton that the book conceals. The author starts with the skeleton and tries to *cover it up*.

#### Discovering the Author's Intentions

> **Rule 4.** Find out what the author's problems were.

This adds another dimension to rules 2 and 3. The author had a reason for writing the book; they were dealing with certain questions. The book is, presumably, their answer to those questions. Understanding this lets you better understand why the book has a certain unity and why the book is structured a particular way. *In other words, why did the author write this book?*

#### The First Stage of Analytical Reading

The first four rules make up the first stage of analytical reading: outlining a book's structure. They let a reader answer the first basic question about a book—*what is the book about as a whole?*

### 8. Coming to Terms with an Author

*In the upcoming chapters, we look at the rules for the second stage of analytical reading. As stated, they are intended to apply to expository (non-fiction) works.*

> [A] term is the basic element of communicable knowledge.

#### Words vs. Terms

> The main point is that *one* word can be the vehicle for *many* terms, and *one* term can be expressed by *many* words.

We communicate with words, but words can have multiple and different meanings. Here, a term refers to a a word used unambiguously, or a word with a specific meaning. A writer and reader have come to terms if they use a given word and understand it to have the same meaning. More simply, when an author uses jargon or has a specific meaning or connotation for a word, we need to be able to understand that meaning in order to be able to understand the author.

> **Rule 5.** Find the important words and through them come to terms with the author.

This is the first rule for the second stage of analytical reading. This stage focuses on interpreting and understanding a book's contents or message.

#### Finding the Key Words

The words than an author uses in a special way are important for them, and consequently for us. If you're unfamiliar with the subject, the most important words will tend to be those that give you trouble. That's because the majority of words used will be regular words that we're already used to; the ones that don't make sense are the ones that likely have a special significance and meaning. 

Authors will often call attention to key words as well, by stressing them in some way. They might be marked typographically (e.g. with italics). They might be explicitly discussed and explained.

Additionally, each field has its own technical vocabulary. Having familiarity with a field will help identify the corresponding jargon.

> You can spot them *positively* through having some acquaintance with the field, or *negatively* by knowing what words must be technical, because they are not ordinary.

#### Finding the Meanings

After finding a key word, you need to find its meaning(s) to actually understand the term(s). This is done by comparing different usages of the word and by using the surrounding context. This is unfortunately a tricky process, and can involve both trial-and-error and backtracking to refine your understanding.

### 9. Determining an Author's Message

Authors make *propositions*—declarations on an author's judgment about something. It's something they believe to be true or false. Propositions are expressions of personal opinion that an author should persuade us to accept.

Since we must be persuaded, authors also make arguments. Arguments are founded on premises. If (and only if) the premises and the argument's logic are sound, the argument's conclusion must also be correct.

#### Finding the Key Sentences

> **Rule 6.** Mark the most important sentences in a book and discover the propositions they contain.

Somewhat similar to key words, important sentences are those that require more effort for you to interpret. However, this means that they are important *for you*. They may not be important *for the author*, although they are likely to be because you'll probably struggle more with the most important things the author has to say.

> From the author's point of view, the important sentences are the ones that express the judgments on which his whole argument rests.

Again, authors may point out key sentences typographically or by some other means of calling attention to them. Important sentences also often contain important terms.

#### Finding the Key Propositions

There are a few ways to help make sure you've understood a proposition. When you've understood a proposition, you should be able to rephrase it in your own words. Not being able to do so just means that you're parroting the author; you don't understand the concept enough to extend it or reframe it. You also understand a proposition if you can come up with a real world example that satisfies the proposition.

#### Finding the Arguments

> **Rule 7.** Locate or construct the basic arguments in the book by finding them in the connection of sentences.

Arguments are composed of collections of sentences, but there's no clear writing unit that an argument can be tied to. This is unlike propositions and terms, which are respectively tied to sentences and words. Instead, arguments often have to be pieced together from propositions located in various places.

> **Rule 7, rephrased.** Find if you can the paragraphs in a book that state its important arguments; but if the arguments are not thus expressed, your task is to construct them, by taking a sentence from this paragraph, and one from that, until you have gathered together the sequence of sentences that state the propositions that compose the argument.

Good authors try to make their arguments clear. They will also often summarize their arguments, such as at the end of a chapter.

Still, there are bad or hard to follow arguments. The points of the argument may be spread out, making the overall point hard to follow. Some points may be skipped, which can be used by misleading authors to present skewed arguments.

As a reader, your job is to be find and succinctly construct the arguments.

#### Finding the Solutions

> **Rule 8.** Find out what the author's solutions are.

Rule 4 deals with determining what problems the author was trying to solve. Rule 8 involves assessing how well the author's arguments addressed those problems. In particular, what is the resulting state? What's solved, what's unsolved, what new problems have come up?

#### The Second Stage of Analytical Reading

This next set of rules makes up the second stage of analytical reading—interpreting the contents of the book. This answers the second basic question—*what is being said in detail, and how?*

### 10. Criticizing a Book Fairly

> The activity of reading does not stop with the work of understanding what a book says. *It must be completed by the work of criticism, the work of judging.*

This is the last stage of analytical reading. Readers are involved in a conversation with the author, and are obligated to respond to the author's message. In other words, the reader should form their own opinion. This chapter discusses how to do so *fairly*.

#### Teachability as a Virtue

> A person is wrongly thought to be teachable if he is passive and pliable. On the contrary, teachability is an extremely active virtue. No one is really teachable who does not freely exercise his power of independent judgment. He can be trained, perhaps, but not taught. *The most teachable reader is, therefore, the most critical.*

At the outset, it was implied that a good book teaches the reader. For someone to be teachable, they shouldn't criticize a teaching until they understand it. However, this isn't to say that they shouldn't critique it at all; in fact, they are obligated to be critical of what they've been told.

#### The Importance of Suspending Judgment

> **Rule 9.** You must be able to say, with reasonable certainty, "I understand," before you can say any one of the following things: "I agree," or "I disagree," or "I suspend judgment."

This rule is presented as common sense, although people often don't follow it. Additionally, it's important to notice that disagreeing is not the only form of criticism. You can agree or suspend judgment, the latter implying that you're not convinced.

This third phase of analytical reading must come after the first two, since you must understanding the book in order to have an opinion on it. Criticism is irrelevant if the critic hasn't read the book. A critic understands the author's position if they can explain the author's argument, using their (the critic's) own words. If they can't, they haven't internalized the argument.

There's another subtle point—you need to make sure your understanding is based off of your best effort to read the book. Your understanding might be incomplete if you only read part of the book. Additionally, a book may have external context that needs to be understood in order to fully understand the book's argument.

#### The Importance of Avoiding Contentiousness

> **Rule 10.** When you disagree, do so reasonably, and not disputatiously or contentiously.

When critiquing, your objective should be to come to the truth. It shouldn't be to win an argument against another person, as that doesn't help in the long run.

#### On the Resolution of Disagreements

> **Rule 11.** Respect the difference between knowledge and mere personal opinion by giving reasons for any critical judgment you make.

People can disagree for many reasons. The important part about disagreeing is to make sure that the dispute is one that can actually be resolved.

People might disagree because they have different beliefs. That's fine; they are *able to agree* even if they *choose not to agree*. They might also disagree because one person is uninformed or misinformed. That can be resolved by providing the critic with the information they lack.

The ultimate goal of reading for understanding is to gain knowledge. Providing reasons for your argument shows that your critique comes from reasoning about facts or opinions, rather than from passing judgment.

### 11. Agreeing or Disagreeing with an Author

At this point, you've understood the book you've read. The only time it's acceptable to critique without understanding the book is if you can argue that the book was poorly written and therefore not readable. If you agree with the book, there's nothing left to do. So, let's assume you disagree.

#### Prejudice and Judgment

Although you're following the rules on how to critique fairly, you're still human and have emotions and biases. To address these:

1. Recognize the emotions you're bringing to the table.
2. State your assumptions. Both sides have their own assumptions, and need to be able to understand and accept where the other is coming from.
3. Aim for impartiality. You can do this by at least trying to take the other point of view.

There are certain lines of reasoning that help avoid prejudices and judgment. It's recommended that your critique stays within one (or more) of these types of arguments:

1. The author is uninformed.
2. The author is misinformed.
3. The author's reasoning is incorrect (although their premises are).
4. The author's reasoning is incomplete.

In the first three cases, we argue against the author by pointing out where they are lacking or wrong, then identifying how this affects their conclusion. If your critique doesn't fall under the first three cases, you're forced to agree with the author's conclusion, even if you don't like it.

In the last case, the author hasn't fully addressed the issues that they set out to address. To critique incomplete reasoning, you need to be able to point out precisely why the author's argument is incomplete.

#### The Third Stage of Analytical Reading

The last stage of analytical reading allows you to answer the last two basic questions: *Is it true?* and *What of it?*

*There are additional parts and chapters to this book.*