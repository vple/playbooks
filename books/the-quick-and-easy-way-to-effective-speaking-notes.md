# The Quick and Easy Way to Effective Speaking
Dale Carnegie, revised by Dorothy Carnegie  
[Amazon](https://www.amazon.com/Quick-Easy-Way-Effective-Speaking-ebook/dp/B07C5BC7Z9)

*A lot of the notes here are actually quoted directly from the book. I just can't style things the way I want to because markdown.*

> When we are unable to say clearly what we mean, through nervousness, timidity, or foggy thought processes, our personality is blocked off, dimmed out and misunderstood. —Dorothy Carnegie

## Fundamentals of Effective Speaking

### 1. Acquiring the Basic Skills

#### Take heart from the experience of others.

Lots of people have learned to speak well.

#### Keep your goal before you.

> In almost any subject, your passion for the subject will save you. —William James

Stay focused on the goal you want to accomplish.

#### Predetermine your mind to success.

Believe you can do it. Envision how a good talk will look and feel.

#### Seize every opportunity to practice.

### 2. Developing Confidence

> Fear defeats more people than any other one thing in the world. —Emerson

Public speaking lets us deal with out fears and insecurities, and in doing so we can gain confidence.

#### Get the facts about fear of speaking in public.

1. You are not unique in your fear of speaking in public.
2. A certain amount of stage fright is useful!
3. Many professional speakers have assured me that they never completely lose all stage fright.
4. The chief cause of your fear of public speaking is simply that you are unaccustomed to speak in public.

#### Prepare in the proper way.

> It was obvious almost at once that he had not planned his speech. [...] He made his talk as Rousseau says a love letter should be written: he began without knowing what he was going to say, and finished without knowing what he had said.

##### Never memorize a talk word for word.

> Once a victim of this type of mental dope addiction, the speaker is hopelessly bound to a time-consuming method of preparation that destroys the effectiveness of the platform.

It takes time to memorize a talk, but more importantly you become stiff and can't deliver it well.

> I don't like to hear a cut-and-dried sermon. When I hear a man preach, I like to see him act as if he were fighting bees. —Abe Lincoln

##### Assemble and arrange your ideas beforehand.
##### Rehearse your talk with your friends.

You can practice ideas for your talk in casual conversation. Your friends don't even have to know that you're practicing for a speech—you can just tell it to them as part of a story.

##### Predetermine your mind to success.
##### Lose yourself in your subject.

You need to really believe in the message you're trying to convey. If you don't, you can get there by delving into the message in more detail, focusing on how what you have to say will help your audience.

##### Keep your attention off negative stimuli that may distract you.

Don't dwell on negative things, especially right before your talk.

##### Give yourself a pep talk.

#### Act Confident

### 3. Speaking Effectively the Quick and Easy Way

> [T]hey were talking about themselves, about their most embarrassing moments, their most pleasant memory, or how they met their wives or husbands.

#### Speak about something you have earned the right to talk about through experience or study.

Talk about things you know about, rather than topics you're unfamiliar with.

##### Tell us what life has taught you.

##### Look for topics in your background.

> You can be sure you have the right topic for you if you have lived with it, made it your own through experience and reflection.

Use your life as inspiration. You already have lots of stories, but here are some topics for inspiration:

- Early years and upbringing.
- Early struggles to get ahead.
- Hobbies and recreation.
- Special areas of knowledge.
- Unusual experiences.
- Beliefs and convictions.
	- Don't speak only about general ideas—include specific details too.

#### Be sure you are excited about your subject.

> [I]f someone stood up and directly opposed your point of view, would you be impelled to speak with conviction and earnestness in defense of your position? If you would, you have the right subject for you.

#### Be eager to share your talk with your listeners.

There are three parts to a speech: you, your message, and your audience. For the speech to be successful, you have to win over your audience. You do this when you can share your passion with them.

## Speech, Speaker, and Audience

*These are the three elements of every talk.*

### 4. Earning the Right to Talk

This chapter deals with the speech itself. You want your talk to be rich; to have specific examples and to be interesting to the audience. But how do you craft your content?

#### Limit your subject.

Talking about a wide variety of topics just dilutes your talk, making it harder to follow and remember. It also ensures that you can't do each topic justice. A rule of thumb is to stick to one or two main points for a talk up to 5 minutes long and at most four or five main points for a talk up to 30 minutes long.

#### Develop reserve power.

Reserve power is the "backup" knowledge and experience you have on your subject. The vast majority of it won't be used in your talk, but it can help you adjust your talk on the fly. You might want to shift the focus of your talk based on the circumstances (e.g. based on something the previous speaker said). You also might draw on that knowledge to answer questions from the audience. 

You develop reserve power by trying to answer more detailed questions that dive into your subject.

> Why do I believe this? When did I ever see this point exemplified in real life? What precisely am I trying to prove? Exactly how did it happen?

#### Fill your talk with illustrations and examples.

> Only stories are really readable. —Rudolf Flesch

Popular articles utilize narratives and anecdotes to make themselves readable. There are five ways to make your speech more illustrative: humanize, personalize, specify, dramatize, and visualize.

##### Humanize Your Talk

Use examples that people will care about. These can be personal experiences based on people you know, stories about topics that people are interested in, etc.

##### Personalize Your Talk By Using Names

##### Be Specific—Fill Your Talk With Detail

Answer the five Ws: when, where, who, what, and why? But don't fill your talk with too much detail, otherwise you'll just bore people. Answer the 5 Ws, but keep your answers concise.

##### Dramatize Your Talk By Using Dialogue

##### Visualize By Demonstrating What You Are Talking About

You can do this, for example, with a visual demonstraction (e.g. acting something out).

#### Use concrete, familiar words that create pictures.

Words that create pictures are concrete things that can be visualized. Physical objects (battles, bull fights, combats of gladiators) and actions (hanging, burning) generally are picturable. Abstract concepts aren't.

### 5. Vitalizing the Talk

> Vitality, aliveness, enthusiasm—these are the first qualities I have always considered essential in a speaker.

You, the speaker, are how you bring a speech to life.

#### Choose subjects you are earnest about.

> If a speaker believes a thing earnestly enough and says it earnestly enough, he will get adherents to his cause.

Some ways to find things you're earnest about: talk about things you really believe in or learn about a topic that you think is interesting.

#### Relive the feelings you have about your topic.

Share how your subject makes you feel; don't hide your emotions. Present things as if you're (still) a part of them, not as if you're a bystander.

#### Act in earnest.

Even if you don't feel like it, act like you're eager and ready to give the talk.

### 6. Sharing the Talk with the Audience

> I visit a town or city and try to arrive there early enough to see the postmaster, the barber, the hotel manager, the principal of the schools, some of the ministers, and then go into the stores and talk with people, and see what has been their history and what opportunities they had. Then I give my lecture and talk to those people about the subjects that apply to them locally. —Dr. Russell Conwell

#### Talk in terms of your listener's interests.

In order to identify what your listeners care about, think about how your message will impact your audience's life. How will the message you share improve their life?

#### Give honest, sincere appreciation.

But if you can't give genuine appreciation, don't give any at all!

#### Identify yourself with the audience.

> As soon as possible, preferably in the first words you utter, indicate some direct relationship with the group you are addressing.

This is often done by sharing some commonality you have with the audience. It doesn't always have to directly come from you today; it can also come from your past or the communities you were a part of.

Another technique is to use names of people you've met. If you do, you need to make sure you understand how those people relate to the rest of the audience.

You can also use the word "you" when speaking. This makes it feel as if you're talking more directly to each member of the audience. However, "we" is better than "you" if you're in a situation where you might come across as condescending or lecturing.

#### Make your audience a partner in your talk.

There are a number of techniques to get people interested:

- ask questions and get audience responses
- get people on their feet
- repeat a sentence or phrase
- have them answer your questions (e.g. by taking a poll of the audience)
- have them vote on something
- have them help you solve a problem.

#### Play yourself down.

> Indeed, one of the best ways for a speaker to endear himself to an audience is to play himself down.

Some tactics to do so include sharing or implying that you feel you're outclassed (by the audience), nervous / anxious, unsure of how you can enrich your audience, etc.

> The surest way to antagonize an audience is to indicate that you consider yourself to be above them.

## The Purpose of Prepared and Impromptu Talks

There are two types of talks you can give: prepared and impromptu talks. Additionally, every talk has a purpose:

1. To persuade or get action.
2. To inform.
3. To impress and convince.
4. To entertain.

A talk needs to be organized in different ways for each purpose. Additionally, you also want to make sure that the purpose of your talk aligns with what your audience is expecting.

### 7. Making the Short Talk to Get Action

The magic formula for giving a persuasive talk is to: give an example, give your point, then give your reason. This is applicable both in formal presentations and throughout your daily life.

#### Give your example, an incident from your life.

This is the major portion of your talk, taking around 3/4 of your time. You want to use a single example from your personal life that will help illustrate the point you want to make.

> Audiences are not interested in apologies or excuses, real or simulated.

It's best to start your talk and to hook the audience by just diving into the details of your talk. Common mistakes include:

- Explaining how you picked your topic.
- Showing that you're unprepared.
- Presenting your topic as if you're a lecturer.

Instead, go directly into your story. For example:

- "In 1942, I found myself on a cot in a hospital"
- "Yesterday at breakfast my wife was pouring the coffee and ..."
- "Last July I was driving at a fast clip down Highway 42 ..."

Fill your example with appropriate details and relive the experience, as described before.

#### State your point, what you want the audience to do.

1. Make the point brief and specific.
2. Make the point easy for listeners to do.
3. State the point with force and conviction.

In particular, make the point easy and actionable. Give very specific steps as to what people should do.

#### Give the reason or benefit the audience may expect.

- Be sure the reason is relevant to the example.
- Be sure to stress one reason—and one only.

### 8. Making the Talk to Inform

#### Restrict your subject to fit the time at your disposal.

#### Arrange your ideas in sequence.

#### Enumerate your points as you make them.

#### Compare the strange with the familiar.

#### Turn a fact into a picture.

#### Avoid technical terms.

> It is a good practice to pick out the least intelligent-looking person in the audience and strive to make that person interested in your argument.

#### Use visual aids.

However, don't overdo it. Make sure that your speech is still the main focus and that you maintain clarity and audience attention.

### 9. Making the Talk to Convince

#### Win confidence by deserving it.

You need to be sincerely interested in your message and deliver it with character. 

#### Get a yes-response.

Carnegie talks about using a yes-chain and getting your audience to agree to a series of points that you're making. His real point seems to be to start with some common ground and then lead to your point.

This is especially important when what your audience may not agree with what you're trying to convince them of. Instead, start with your commonalities, then introduce your differences as something to be objectively discussed.

#### Speak with contagious enthusiasm.

> When your aim is to convince, remember it is more productive to stir emotions than to arouse thoughts.

#### Show respect and affection for your audience.

#### Begin in a friendly way.

You don't have to take an adversarial approach just because you're trying to convince your audience. Instead, relate your topic to something your audience already believes or understands. This allows you to talk about it on the "same side" as your audience. Additionally, as you do this, you want to avoid raising contradicting or opposing ideas.

### 10. Making Impromptu Talks

> The ability to assemble one's thoughts and to speak on the spur of the moment is even more important, in some ways, than the ability to speak only after lengthy and laborious preparation.

#### Practice impromptu speaking.

You can do drills that get you used to thinking on the spot. One game involves taking turns talking for a full minute on random topics. Another game, the "linkage technique," involves multiple people constructing a story.

#### Be mentally ready to speak impromptu.

Actively think about what you'd say if you had to give an impromptu talk on particular subjects. By prepping in advance, you'll be ready for when you actually do have to talk.

#### Get into an example immediately.

1. You will free yourself at once of the necessity to think hard about your next sentence, for experiences are easily recounted even in an impromptu situation.
2. You will get into the swing of speaking, and your first-moment jitters will fly away, giving you the opportunity to warm up to your subject matter.
3. You will enlist the attention of your audience at once.

#### Speak with animation and force.

#### Use the principle of here and now.

Relate your talk to what's actually going on. This typically means talking about the audience, the occasion, or amplifying something that a previous speaker talked about.

#### Don't talk impromptu—give an impromptu talk.

Although the talk is on the spot, it still needs to be coherent. Make sure to stay on topic and not to wander.

## The Art of Communicating

### 11. Delivering the Talk

#### Crash through your shell of self-consciousness.

The hardest part of speaking naturally is getting rid of the mental blocks we have on how others will perceive us. It can feel weird to expose ourselves, but that's actually what people respond to—when people show their emotions and personalities.

#### Don't try to imitate others—be yourself.

#### Converse with your audience.

> And that is the first essential of good talking: a sense of communication.

#### Put your heart into your speaking.

#### Practice making your voice strong and flexible.

There are many ways to add depth to your talk:

- volume, pitch variation, pace
- vocabulary, imagery, expression

## The Challenge of Effective Speaking

### 12. Introducing Speakers, Presenting and Accepting Awards

> The speech of introduction serves the same purpose as a social introduction. It brings the speaker and the audience together, establishes a friendly atmosphere, and creates a bond of interest between them.

#### Thoroughly prepare what you are going to say.

There are three things you need to hit:

1. Subject of the speaker's talk.
2. Their qualifications to speak on the subject.
3. Their name.

#### Follow the T-I-S formula.

1. Topic. Give the exact title of the talk.
2. Importance. Tie the topic into the audience.
3. Speaker. Introduce the speaker, making sure to cover how they relate to the topic.

#### Be enthusiastic.

When you reach their name, there are three crucial parts:

1. Pause. A slight pause before saying their name. This helps build up anticipation.
2. Part. Make sure you pronounce their first and last names separately.
3. Punch. Say their name emphatically.

Also, make sure you say the speaker's name to the audience. Don't turn to look at the speaker until after you've finished saying their name.

#### Be warmly sincere.

#### Thoroughly prepare the talk of presentation.

For awards:

1. Tell why the award is made.
2. Tell why the group's interested in the person receiving the award.
3. Tell why the award is deserved and how the group feels about the recipient.
4. Congratulate the recipient.

#### Express your sincere feelings in the talk of acceptance.

When receiving an award:

1. Give a warmly sincere "thank you."
2. Give credit to those who helped you.
3. Tell what the award means to you. Show the award, say how you'll use it.
4. Sincerely thank the audience again.

### 13. Organizing the Longer Talk

There are three parts: getting the audience's attention, the body, and the conclusion.

#### Get attention immediately.

As discussed before, jump into your talk. There are many ways to get attention:

- Begin your talk with an incident—example.
- Arouse suspense.
- State an arresting fact.
- Ask for a show of hands.
- Promise to tell the audience how they can get something they want.
- Use an exhibit.

#### Avoid getting unfavorable attention.

- Do not open with an apology.
- Avoid the "funny" story opening.

#### Support your main ideas.

##### Use statistics.

> Statistics, of themselves can be boring. They should be judiciously used, and when used they should be clothed in a language that makes them vivid and graphic.

##### Use the testimony of experts.

Verify what you're going to say:

1. Is the quotation accurate?
2. Is it a topic that the expert is knowledgeable about?
3. Is the expert known and respected by the audience?
4. Is the statement based on first-hand knowledge? (As opposed to personal interest or bias.)

##### Use analogies.

##### Use a demonstration with or without an exhibit.

#### Appeal for action.

> The close is really the most strategic point in a talk, what one says last, the final words left ringing in the ears when one ceases—these are likely to be remembered longest.

Bring your talk to a close. Don't end meekly, either by saying you've run out of things or by continuing to talk despite having run out.

Two common ways to end a talk are by summarizing and asking the audience to take a certain action.

### 14. Applying What You Have Learned

Formal speeches and daily conversations are pretty similar. In formal speeches you typically have just one goal (see part 3), but in daily conversations they blend together.

#### Use specific detail in everyday conversation.
#### Use effective speaking techniques in your job.
#### Seek opportunities to speak in public.
#### You must persist.
#### Keep the certainty of reward before you.