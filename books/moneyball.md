# Moneyball: The Art of Winning an Unfair Game
Michael Lewis
[Amazon](https://www.amazon.com/Moneyball-Art-Winning-Unfair-Game-ebook/dp/B000RH0C8G)

> At the bottom of the Oakland experiment was a willingness to rethink baseball: how it is managed, how it is played, who is best suited to play it, and why.

This involves searching for inefficiencies, or effectively knowledge. By finding things that are under or overvalued, you can take advantage of this in the long run.

## 1. The Curse of Talent
> It wasn't merely that he didn't like to fail; it was as if he didn't know how to fail.

This happens with "talent" who are accustomed to being the best. Things can't always go right, but it's important for them to know what to do when they don't. Rather than moving on, they dwell on the mistakes and implode.

## 2. How to Find a Ballplayer
> High school pitchers also had brand-new arms, and brand-new arms were able to generate the one asset scouts could measure: a fastball's velocity. The most important quality in a pitcher was not his brute strength but his ability to deceive, and deception took many forms.

In a way, metrics are good. But over-relying on a metric for things it doesn't show is bad. Additionally, the answer with a lack of metrics shouldn't be to depend more on the one you have, but to get more metrics.

> There was, for starters, the tendency of everyone who actually played the game to generalize wildly from his own experience. People always thought their own experience was typical when it wasn't. There was also a tendency to be overly influenced by a guy's most recent performance: what he did last was not necessarily what he would do next. Thirdly—but not lastly—there was the bias toward what people saw with their own eyes, or thought they had seen. The human mind played tricks on itself when it relied exclusively on what it saw, and every trick it played was a financial opportunity for someone who saw through the illusion to the reality.

> The 2002 draft was to be the first science experiment Billy Beane performed upon amateur players.

Interestingly, so far, the focus seems to be exclusively on individual talent. This was acknowledge a bit by one of the coaches before, and perhaps baseball is a more individual-oriented sport. There should definitely be a correlation with individual talent, but to what extend can individual success predict team success with individuals that you have to learn to work with?

> He's looking for Lark's results on the psychological test given by Major League Baseball to all prospects.

Baseball uses psychological tests to evaluate likeliness of good/stable behavior, as well as to judge for positive qualities such as competitiveness, leadership, conscientiousness, etc. How do they actually evaluate this? Is it useful?

## 3. The Enlightenment
> He was able to instantly forget any failure and draw strength from every success. He had no concept of failure. And he had no idea of where he was.

How can you create the feeling of invincibility, and to get people to move on beyond the things on their mind? Doubt is important to prevent overconfidence, but doubt is also the thing that prevents you from being able to be you. Part of it is almost certainly expectations—there's no need to doubt yourself if you have no expectations.

So where are the expectations from? Some are external and some are internal.

External expectations result from your image, and in particular how you perceive others will view you. Of course, others will always form an image of you, but if you don't know about it then there's nothing to get in your head about. Internal expectations come from wanting more from yourself.

It's known, but when you're in a stay of play you have much different expectations of yourself. Often you don't have any relating to skill. The only expectations are to have fun. In part, you're exploring. Your aim is to learn, and so there can't be expectations of doing well.

Interestingly, sometimes when playing you'll realize that you did something dumb and then get upset at yourself. In this case, your expectations must have been _suspended_, for whatever reason—autopilot, in the zone, having fun, etc. The mistake becomes obvious in hindsight.

Another part of this might just be how you label and interpret everything. When you fail, you could take it as a reflection of your character. Or, you can just take it as something that happens and move on.

> You don't push him along too fast. Take it slow, so his failure is not public exposure and humiliation. Teach him perspective—that baseball matters but it doesn't matter too much. Teach him that what matters isn't whether I just struck out. What matters is that I behave impeccably when I compete.

> He approached the A's farm teams the way the Marine Corps approached its boot camps. The individual star was less important that the organization as a whole, and the organization as a whole functioned well only if it was uniformly disciplined. Once he decided that hitting was the most important tool and everything else was secondary, Alderson set about implementing throughout the organization, with Marine Corps rigor, a uniform approach to hitting.

I buy this, but when does it fall susceptible to the issues with command and control?

> Scoring runs was, in the new view, less an art or a talent than a _process_. If you made the process routine—if you got every player doing his part on the production line—you could pay a lot less for runs than the going rate.

Another example of running things like a well-oiled machine.

## 4. Field of Ignorance
> "The problem," wrote James, "is that baseball statistics are not pure accomplishments of men against other men, which is what we are in the habit of seeing them as. They are accomplishments of men in combination with their circumstances."

Statistics need to be carefully evaluated to ensure that what we're interpreting from them is correct.

> ...we should at least know how good they are—which means knowing how much they allowed in the field as much as it means knowing how much they created at bat.

People will have different values in different areas. A weakness in one area is fine if it is compensated in others.

> I start with the game, with the things that I see there and the things that people say there. And I ask: Is it true? Can you validate it? Can you measure it? How does it fit with the rest of the machinery? And for those answers I go to the record books...

> I find it remarkable that, in listing offenses, the league will list first—meaning best—not the team which scored the most runs, but the team with the highest batting average. It should be obvious that the purpose of an offense is not to compile a high batting average.

## 5. The Jeremy Brown Blue Plate Special

## 6. The Science of Winning An Unfair Game
> The central insight that led him both to turn minor league nobodies into successful big league closers and to refuse to pay them the many millions a year they demanded once they became free agents was that it was more efficient to create a closer than to buy one.

Under what conditions is it easier to create something than to buy it? What prerequisities for your starting people or materials do you have to have?

> The question wasn't whether a baseball team could keep its stars even after they had finished with their six years of indentured servitude and became free agents. The question was: how did a baseball team find stars in the first place, and could it find new ones to replace the old ones it lost? How fungible were baseball players?

> What AVM's system really wanted to know was: in every event that occurs on a baseball field, how—and how much—should the players involved be held responsible, and therefore debited and credited? Answer the question and you could answer many others. For example: How many doubles does Albert Belle need to hit to make up for the fly balls he doesn't catch?

Likewise, for software engineering.

> _How_ to account for a player's performances was obvious: runs. Runs were the money of baseball, the common denominator of everything that occurred on a field. _How much_ each tiny event on a baseball field was worth was a more complicated issue.

A problem here is that baseball is a game, and thus has very defined parts. How to account for something in software engineering isn't nearly as obvious, although it would probably be tied to commits and deployments.

## 7. Giambi's Hole
> "The important thing is not to recreate the individual," Billy Beane would later say. "The important thing is to recreate the _aggregate_."

It's the team's stats overall that matter, not how much each individual contributes towards it.

## 8. Scott Hatteberg, Pickin' Machine
> Every play Hatty made, including throws he took from other infielders, he came back to the dugout and discussed with Wash. His coach was creating an alternative scale on which Hatty could judge his performance. He might be an absolute D but on Wash's curve he felt like a B, and rising. "He knew that what looked like a routine play wasn't a routine play for me," said Hatty. Wash was helping him to fool himself, to make him feel better than he was, until he actually became better than he was.

## 9. The Trading Desk

## 10. Anatomy of an Undervalued Pitcher
> Baseball executives' preference for their own opinions over hard data was, at least in part, due to a lifetime of experience of fishy data.

## 11. The Human Element

## 12. The Speed of the Idea
Billy's attitude seemed to be, all that management can produce is a team good enough to triumph in a long season.