# Super Thinking

- [Introduction](#introduction)
- [1. Being Wrong Less](#1-being-wrong-less)
  * [Keep It Simple, Stupid!](#keep-it-simple--stupid-)
  * [In The Eye Of The Beholder](#in-the-eye-of-the-beholder)
  * [Walk A Mile In Their Shoes](#walk-a-mile-in-their-shoes)

## Mental Models

- **critical mass**—The mass of nuclear material needed to create a critical state whereby a nuclear chain reaction is possible.

## Introduction
> What is elementary, worldly wisdom? Well, the first rule is that you can't really know anything if you just remember isolated facts and try and bang 'em back. If the facts don't hang together on a latticework of theory, you don't have them in a usable form.
> 
> You've got to have models in your head. And you've got to array your experience—both vicarious and direct—on this latticework of models.

Every discipline has its own set of mental models. Some mental models are also useful for day-to-day problem solving, decision making, and truth seeking.

## 1. Being Wrong Less
#### Inverse Thinking
**Inverse thinking** involves thinking about a problem from an inverted perspective, which can present new solutions or strategies. Applying this model towards trying to get to X typically involves thinking about how to avoid things that prevent you from getting to X.

Inverse thinking is typically attributed to mathematician Carl Jacobi, famous for saying, "Invert, always invert."

#### Unforced Errors
An **unforced error** is where you make a mistake due to your own poor judgment or execution. To improve at something, you need to make fewer unforced errors in that area.

Unforced errors typically come up in sports.

### Keep It Simple, Stupid!
#### Antifragile
> Some things benefit from shocks; they thrive and grow when exposed to volatility, randomness, disorder, and stressors and love adventure, risk, and uncertainty. Yet, in spite of the ubiquity of the phenomenon, there is no word for the exact opposite of fragile. Let us call it antifragile.
>
> Antifragility is beyond resilience or robustness. The resilient resists shocks and stays the same; the antifragile gets better.

Coined by Nassim Nicholas Taleb in his book, *Antifragile*.

#### Argue From First Principles
**Arguing from first principles** involves structuring thought around basic building blocks that you perceive to be true. The *first principles* are self-evident assumptions that are the foundation for your following conclusinos.

Being able to reason from first principles make the difference between being able to tackle and solve problems on your own, versus requiring some sort of template, reference, or crutch to be able to solve a problem. This also allows you to adapt and combine principles to come up with new and sound ideas. This also gives you exposure to how other ideas are reasoned, allowing you to apply that logic to similar situations.

Reasoning with first principles involves deliberately starting from scratch. Starting from scratch allows you to avoid conventional beliefs, which may be incorrect. The end result may match up with conventional wisdom, but doing the reasoning yourself will give you a much stronger understanding of the subject.

#### De-Risking
**De-risking** involves evaluating your assumptions against the real world. This lets you test your assumptions, confirming whether or not they are actually correct. De-risking may also involve further breaking down your assumptions.

When de-risking, it's most important to focus on the assumptions that are crucial for success and that you're least certain about. If that assumption is ignored and ends up being false, your approach will fail. After identifying these critical assumptions, you need to test them.

#### Premature Optimization
**Premature optimization** is a fallacy that occurs when you try to optimize a solution or approach before an optimization is needed. If your approach is incorrect or your optimizations aren't necessary, you've wasted effort.

Premature optimization comes from computer science and refers to when code or algorithms are optimized before they need to be.

#### Minimum Viable Product (MVP)
A **minimum viable product** consists of the very minimum you need in order to launch a product. You frequently won't know what is needed from your product—rather than building functionality that isn't needed, ship the smallest needed and see how your customers respond.

MVP doesn't apply just to products. It can apply towards any venture that you're working on.

Minimum viable products come from startups, which are forced to ship and react quickly in order to find something worth building before they run out of funding.

#### Occam's Razor
**Occam's razor**—the simpler explanation is more likely to be true.

Occam's razor is about ignoring unnecessary assumptions to simplify thinking. It's not always correct—it's just a starting point when reasoning about something complex.

#### Conjunction Fallacy
The **conjunction fallacy** occurs when you believe a combination of conditions makes something more likely, even though it is actually less likely.

> Linda is 31 years old, single, outspoken, and very bright. She majored in philosophy. As a student, she was deeply concerned with issues of discrimination and social justice, and also participated in anti-nuclear demonstrations.
>
> Which is more probably?
>
> 1. Linda is a bank teller.
> 2. Linda is a bank teller and is active in the feminist movement.

(1) is more likely, the conjunction fallacy leads people to say (2). The set of all female bank tellers is larger than the set of all female bank tellers who are also active in the feminist movement. So it's more likely that Linda is a bank teller.

The conjunction fallacy was published by Amos Tversky and Daniel Kahneman.

#### Overfitting
**Overfitting** occurs when using too many assumptions in an attempt to more "precisely" explain events. It results in an overly complicated explanation. This explanation may explain the specific scenario or data that you're looking at, but will likely be a poor explanation for a similar but different set of data.

Overfitting is a concept from statistics.

### In The Eye Of The Beholder
#### Frame Of Reference
Your **frame of reference** is how you see the world. You'll perceive objects, actions, and events in a way that's filtered to your experiences and current situation. Critically, *your frame of reference may differ from others' frames of reference*.

Other people may perceive objects, actions, and events in the same way you do. Or, they may perceive them in a completely different way. When you need objectivity, you need to account for your frame of reference—identify what differs from others' frames of reference.

Frames of reference are a concept from physics, where an object's speed (and other properties) is perceived differently from different perspectives. It's a key concept behind Einstein's theory of relativity.

#### Framing
**Framing** is how a situation or explanation is presented. Framing can occur when one person presents a situation to another, or when someone is perceiving is a situation.

Framing can be used to make something seem more or less palatable. Conversely, things that are presented to you are likely to have been framed as well—you should consider any bias or spin.

#### Nudging
**Nudging** is where subtle cues lead to more substantial behavior. These might be word choice, visual appearances, specials, etc.

Nudges are used to point people in a certain direction. They don't *force* behavior, but they make it more likely by making it more visible. Nudges can be positive or negative—it depends on how they are used.

#### Anchoring
**Anchoring** is the tendency to over-rely on first impressions when making decisions. The first piece of information received skews how we interpret following information.

For example, if a product's suggested price is $100, you're likely to think it's worth around that much.

#### Availability Bias
**Availability bias** occurs when you believe something due to information that has recently been made available to you.

The reason this is bias is that there's often additional information—the remaining context. Because you don't have the full picture, your reasoning and decisions will be skewed.

#### Filter Bubble
A **filter bubble** tends to occur when a feed shows results tailored for you. The feed will remove results that it believes you're less interested in. These also tend to be results that disagree with your point of view. The result is that you have a bubble of viewpoints that support your own beliefs.

#### Echo Chambers
An **echo chamber** occurs when people with similar filter bubbles interact with one another. The same ideas and results will bounce between these people. This reduces exposure to alternate viewpoints and makes it appear that these ideas or results are more widely believed than they may actually be.

### Walk A Mile In Their Shoes
#### Third Story
The **third story** in a conflict is the one that a third, impartial observer would give.

Being aware of the third story helps keep you aware of what is actually going on in a situation. This in turn helps you make fewer biased or incorrect judgments, as well as help you be more self-aware in the moment.

To see the third story, it can help to imagine a recording of the situation, then to think about what an outside audience would think as they saw it.

#### Most Respectful Interpretation (MRI)
The **most respectful interpretation** tactic is to choose to interpret another party's actions in the most respectful way possible. In other words, you give them the benefit of the doubt.

#### Hanlon's Razor
Never attribute to malice that which is adequately explained by carelessness.

#### Fundamental Attribution Error
The **fundamental attribution error** occurs when you attribute a behavior to the incorrect root cause. It often occurs when attributing a behavior to someone's personality or internal motivations, rather than external factors.

#### Self-Serving Bias
The **self-serving bias** is where you have self-serving reasons that justify your behavior, but blame others' intrinsic traits when they show similar behavior.

#### Veil Of Ignorance
The **veil of ignorance** is a technique used when thinking about how society should be. You should imagine yourself ignorant of your current place in the world. Instead, you should imagine how you'd fare if you were in a different social position.

For example, you could consider being of the opposite sex, being a single parent, being old, etc.

The veil of ignorance is an idea from philosopher John Rawls.

#### Birth Lottery
The **birth lottery** refers to the social group you were born into.

A significant portion of your life is determined by luck, based on your birth.

#### Just World Hypothesis
The **just world hypothesis** is a belief that the world is fair, orderly, and predictable. People get what they deserve, good and bad, as a result of their actions.

Believing in the just world hypothesis often results in errors in judgment. This is because you are more likely to make generalizations about why certain things happen, yet these generalizations are wrong at the individual level.

TODO: Add Charlie Munger's take.

#### Victim-Blame
**Victim-blaming** is where the victim of some action is assumed to be responsible for their misfortune.

This belief can stem from the just world hypothesis: because something bad happened to someone, they must have done something to deserve it.

#### Learned Helplessness
**Learned helplessness** is the tendency to stop trying to escape difficult or adverse situations because someone has gotten used to being in these difficult or adverse situations.

Learned helplessness can be overcome when people start to see that their actions can make a difference. It's not just for extreme situations; learned helplessness is also in play whenever someone thinks they can't do or learn something.

Learned helplessness comes from an experiment from Martin Seligman in which he compared reactions of different groups of dogs to electric shocks.