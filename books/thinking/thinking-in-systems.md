# Thinking in Systems
Donella H. Meadows

## Introduction: The System Lens
> A system is a set of things--people, cells, molecules, or whatever--interconnected in such a way that they produce their own pattern of behavior over time.

Systems theory looks at how the structure of a system and its environment result in a behavior. While a system may be acted on by its environment, its behavior stems from the system's characteristics.

## Part One: System Structure and Behavior
### 1. The Basics
> A **system** is an interconnected set of elements that is coherently organized in a way that achieves something.

Based on this definition, a system must have three things: _elements_, _interconnections_, and a _function_ or _purpose_. Without these three pieces, you don't have a system. The system itself is more than the sum of its parts, as it can show behaviors that go beyond each of its three pieces. Typically, the most impactful piece for determining behavior is the function or purpose, followed by interconnections, followed by elements.

To determine whether something is a system or just some stuff, it's helpful to think about:

- Can different parts be identified?
- Do the parts affect one another?
- Is the produced effect different than the effects of each part on its own?
- Does the effect persist in a variety of situations?

A system's elements are easiest to notice and start listing. They're often physical, visible, and/or tangible, but they can also be abstract or intangible. You can often go very deep when listing elements, as you can divide things further and further.

Interconnections are the relationships that hold elements together. They might be physical or chemical, but they can include the information that moves throughout the system. Interconnections are often harder to see, but can be revealed by looking at what is affecting the behavior of various elements.

Functions, or purposes, are harder to see. They are rarely explicitly expressed and are often determined by observing the behavior of the system as a whole. Purposes are based on behavior, not stated goals.

**Stocks** are elements of a system that can be seen, felt, counted, or measured at any time. It's an amount or accumulation of some material or information over time. A stock changes over time through **flows**.

> A stock takes time to change, because flows take time to flow.

The change of a stock over time is determined by the sums of its inflows and outflows. To increase a stock, we could increase the inflow or decrease the outflow. The change in a stock is often slow, and the momentum of this change is often underestimated. The rate of change of a stock determines how quickly other parts of a system will operate. This can produce volatility or stability, depending on what and how fast things are changing.

Because stocks can act as a buffer, they allow inflows and outflows to be decoupled. They also allow inflows and outflows to be temporarily imbalanced.

Systems often monitor their stocks, influencing flows to raise or lower the stock. These form feedback processes. When a stock's change over time is consistent and persists over time, there's likely something that is causing that behavior--a **feedback loop**. A **balancing feedback loop** adjusts the levels of a stock towards some constant or goal. A **reinforcing feedback loop** is self-enhancing, which causes exponential growth or collapse over time. Reinforcing loops occur when a stock can affect itself.

A helpful shortcut: the rate for an exponential stock to double in size is roughly 70 divided by the growth rate, as a percentage.

### 2. A Brief Visit to the Systems Zoo
#### One-Stock Systems
##### Two Competing Balancing Loops (Thermostat)
With a thermostat and a furnace, you won't actually reach the desired temperature. This is because you're always losing heat as it dissipates outdoors. The heating of the furnace dominates the cooling effect so the room still heats up; it just never fully reaches the thermostat setting. This is influenced by the delay between when the furnace is turned on and when the effect is felt. A way to compensate for this is to set your thermostat a bit higher, but there's not a fixed calculation because the rate of heating and how quickly the heat dissipates can change from day to day and from room to room.

Importantly: feedback loops deliver information that affect future behavior, not current behavior. The information can't be generated and delivered fast enough to affect current behavior. This means there will always be a delay. Flows can't react to flows; flows can only react to stocks.

##### One Reinforcing Loop and One Balancing Loop (Populations, Economies)
This system describes populations and economies. For populations, this is through births and deaths. If birth (reinforcing) and death (balancing) rates are constant, the population will grow exponentially or die depending on which rate is dominating.

When rates aren't constant, there can be **shifting dominance**, where different rates will be the dominant rate at different points in time. They consequently are the main factor in determining behavior.

Good questions to ask to confirm if reality matches a model:

- Are driving factors likely to unfold this way?
- If so, would the system react this way?
- What drives the driving factors?

##### Systems With Delays (Business Inventory)
Here, the desired amount of inventory is based on current inventory and perceived rate of sales. In theory, this produces a quickly-adjusting, stable inventory.

In practice, there are also different types of delay in the system:

- Perception delay.  
  This is intentional for this example. Before changing inventory orders based on a change in sales, we'd want to see a change in sale averages over a few days. This helps differentiate trends from spikes.
- Response delay.  
  Here, adjustments are made incrementally. We might only increase inventory orders by 50% of our expected change. This is a form of hedging also intended to confirm that a change is actually a trend.
- Delivery delay.  
  This is the time taken for an order to be fulfilled.

The combination of these delays result in oscillating sizes of inventory orders from even a small increase in sales. This is due to the delay. When a flow is adjusted to balance a stock, there's a delay for the stock to actually be affected. During this delay, the unbalanced stock causes flows to compensate more--the flow has no memory or timeline. This leads for the flow to overshoot, leading to oscillation.

In this example, we want to lengthen our delay to produce a dampening effect in our oscillation. The amount of oscillation determines whether or not our inventory stock will reach equilibrium.

#### Two-Stock Systems
##### Renewable Stock Constrained By Nonrenewable Stock (Oil Economy)
> Nonrenewables resources are _stock_-limited.

Growth in a constrained environment is the **limits-to-growth** archetype. In physical systems, growth can't be infinite--there is always some balancing loop that will eventually constrain something.

A resource can be renewable or non-renewable. This just indicates whether or not the value of a stock can change in one direction or two directions. The type of resource affects how growth is likely to end.

With this example, the end result we see is depletion. As a company increases its capital, and consequently its capital growth and investments, it increases the rate that it depletes a nonrenewable resource. This in turn diminishes the return on the resource, slowing down capital growth. The result is that the nonrenewable resource is depleted much more quickly than the initial projected rate. The resource is never fully depleted because it eventually becomes unreasonably costly to do so.

With these systems, the desired goal of a system affects its behavior. You could try to maximize rate of return over a short period of time, or sacrifice some return for a longer timeline.

If the price of a resource isn't held constant (but demand is), the system generates more capital. There's a higher ceiling on how much capital is generated, which means the system will deplete a resource more quickly. This means the system peaks and collapses more quickly as well. An equivalent effect occurs if price is constant and operating costs decreases.

##### Renewable Stock Constrained By Renewable Stock (Fishing Economy)
> Renewable resources are _flow_-limited.

With a renewable resource, there's often an upper threshold where reproduction can't happen (e.g. due to overcrowding) and a lower threshold where reproduction isn't fast enough. The maximum reproduction rate occurs somewhere in between.

The balance between the relationships here--price per resource, resource regeneration rate, and yield per capital--result in different effects. They can result in growth into equilibrium, oscillations, and even destruction of the system (sustainable fishing, fluctuating amounts, overfishing). Which outcome results is affected by the critical threshold at which the resource can't regenerate itself quickly enough, as well as by how quickly and efficiently the balancing feedback loop adjusts as the resource depletes.

For some populations, the population can be rebuilt even with a very small amount of survivors. This can result in a more macro-level oscillation where the system is destroyed again in the future once the population has grown enough. This isn't always the case; some populations go to extinction. Whether or not the resource survives depends on what happens when the resource is heavily depleted.

## Part Two: Systems and Us
### 3. Why Systems Work So Well
There are three properties of highly functional systems: resilience, self-organization, and hierarchy.

**Resilience** is how well a system can persevere in a variable environment. Feedback loops help this. Systems can also have meta feedback loops, meta meta feedback loops, etc. These help systems become much more resilient, although there are always limits to how resilient a system can become. Because it's hard to observe, especially without looking at a system at a whole, resilience is often sacrificed for stability, productivity, or some more apparent property.

**Self-organization** is a system's ability to add complexity to itself. This allows for new structures, experimentation, and freedom in the system that can lead to new behaviors. Self-organization exists in almost every living system. As with resilience, self-organization is often sacrificed for productivity or stability.

**Hierarchy** is where a system creates subsystems. These subsystems add stability and resilience. Crucially, they also reduce global spread of information--some information is best contained purely within the subsystem. This also enables more modularity and isolation within the system.

Relationships within a hierarchical subsystem are typically stronger than those between subsystems. Sometimes, different subsystems end up emphasizing the performance of their subsystem over the system as a whole. When a subsystem does this it's **suboptimization**. This can also occur from a central part of the system, leading in overcontrol.

### 4. Why Systems Surprise Us

- Events and behaviors.   
  It's easier to understand events when they're accumulated into patterns, or behaviors. A system's behavior is its performance over time. These behaviors are determined by the system's structure.
- Boundaries.   
  When modeling, it can be difficult to establish the right boundaries. Too narrow, and we won't understand what our system affects or is affected by. Too broad, and we include lots of minute details that likely have a negligible effect on the system as a whole. It's important to decide on boundaries every time we model something.
- Limiting factors.   
  To affect a system's behavior, it's important to think about its limiting factors. Changing resources doesn't help if those resources aren't the limiting factor.   
  _How quickly can you eliminate a limiting factor while maintaining quality and standards? How do you "scale" up limiting factor removal?_
- Delays.   
  There's always delay when a system is responding to something, and this delay is often longer than we anticipate. Because we tend to misestimate delays, this can lead to very unexpected (or seemingly no) changes to system behavior.
- Bounded rationality.    
  People make rational decisions based on available information, but no one has perfect information. This results in people making good decisions for themselves in the short run, but that will negatively affect things in the long run or on a larger scale. Sometimes this can be affected by how quickly feedback on the situation as a whole makes it back to individual actors in the system.

### 5. System Traps... and Opportunities
#### Policy Resistance
Policy resistance occurs when a system balances itself, despite many actors wanting to break the system from its current state. Its nature prevents the system from changing from the same type of repeated effort. This can result in lots of resource spent to maintain the same state.

Policy resistance can be overpowered by someone with enough power. However, this tends to cause issues once the power is eased. Another approach is to stop trying to influence the system. This won't be as bad as it sounds because the system itself will also correct since you're no longer trying to push it in a certain direction.

#### Tragedy of the Commons
This occurs when multiple actors act rationally in their best interest, increasingly using up some shared resource. This resource is limited and erodable--at some point, it will reach a threshold where it's likely to be destroyed. The actors are incentivized to increase usage of this resource at a rate that isn't affected by the shared resource--as the resource depletes, actors don't have a reason to change their behavior.

This archetype occurs when feedback takes too long (or never) travels from the resource to the actors.

Three approaches to avoiding the tragedy of the commons:

- Educate people and convince them to moderate themselves.
- Privatize the resource so people are affected by their actions.
- Regulate the resource. The regulator must have the ability and be well-meaning towards the whole.

The third option tends to be what people lean towards. The first is essentially an honor system, and the second isn't always a feasible approach. There are many ways to add this regulation, including:

- Giving each actor a turn.
- Charging each actor for use.
- Adding penalties and restrictions.
- Centrally regulating who gets permission to use the resource.
- Restoring the feedback so each person is affected by their action.

#### Drift to Low Performance
This archetype is where systems perform worse over time. Here, actors focus more on bad news about the system and fixate on them more than good news. The desired state of the system is then influenced by this perceived state, rather than the actual state. This results in goals eroding over time.

To deal with this, either make goals absolute or make goals sensitive to the best performance in the past, rather than the worst.

#### Escalation
This is where actors keep trying to one-up the other.

Escalation can be handled by no longer competing in it, though this may require being able to handle other agents having a short term advantage. It can also be handled by negotiating a new system that will balance the escalation.

#### Success to the Successful--Competitive Exclusion
This is where successful actors are able to use the results of their success to become more successful.

This can be reduced by periodically levelling the playing field. You can also let losing actors get out of the competition, limit how much someone can win, or ensure that winning a current round doesn't unfairly bias future rounds.

#### Shifting the Burden to the Intervenor--Addiction
This occurs when an actor in a system is trying to balance some kind of feedback loop and they use a temporary approach to bring their perceived state of the system closer to their desired state. The dependence shifts to the temporary approach, which then gets used continually. This can be good or bad, depending on the system.

This typically happens to systems when some actor intervenes and provides a solution that doesn't fix the root cause of an issue in the system. The system will still fail, leading to more of the solution being needed. This archetype becomes a trap if the solution makes it harder for the system to be able to handle the issue on its own.

This trap is avoided by not getting into it in the first place. When in it, focus on shifting from short-term relief to long-term solutions.

#### Rule Beating
This is where an actor follows the letter but not the spirit of a rule--they avoid it. This is handled by redesigning the rules to encourage the actual desired behavior.

#### Seeking the Wrong Goal
This often occurs when people confuse efforts with results, resulting in a focus on some metric that doesn't fully make sense. Avoid this by making sure goals actually reflect the welfare of a system.

## Part Three: Creating Change--in Systems and in Our Philosophy
### 6. Leverage Points--Places to Intervene in a System
12. Numbers--Constants and parameters   
    Many parameters within a system are changeable. We often can look at how large a flow is and adjust those numbers. However, while changing numbers can affect local areas of a system, they often don't affect overall system behavior. There are times when critical numbers will trigger another leverage point in this list, but these are rare.
11. Buffers--Sizes of stabilizing stocks relative to their flows  
    Buffers add stability. The larger the buffer relative to its flows, the more stable it is. At the same time, when buffers are too large the system becomes inflexible because it can't react as quickly. Large buffers can also be expensive to build or maintain. While changing buffer size can sometimes have significant effects on a system, buffer size is often not a variable that can be controlled.
10. Stock-and-Flow Structures--Physical systems and their intersecting nodes  
    When a system is laid out poorly and over- or underutilizes a certain node or part of the system, resulting effects can be hard to fix without reworking the system itself. If there's a single point of failure, all the preventative measures taken won't help if that point fails. However, physical restructuring can be slow and expensive. This makes it hard to leverage a system's structure. The most effective path we can typically take is to understand the structure so we don't strain it.
9.  Delays--Lengths of time relative to rates of system change  
    Delays are critical in determining behavior. They often cause oscillation. Delays need to be evaluated relative to the rate of change in the stocks that is trying to be controlled. Delays that are too short tend to result in overreaction and extra effort. Delays that are too long cause an effect to build up unnoticed, sometimes reaching a point where it becomes much harder to fix the issue. Delays are high leverage, but often can't be controlled.
8.  Balancing Feedback Loops--Strength of feedback relative to impacts being corrected  
    This is more an information/control part of the system, and so has higher leverage than the more physical components before. How well a balancing loop works depends on how quickly and accurately it detects a change and then responds to it as well as how much of a response occurs. The strength of feedback loops needs to be evaluated against the strength of the impacts that it tries to correct.
7.  Reinforcing Feedback Loops--Strength of the gain of driving loops   
    Reinforcing feedback loops, when unconstrained, will eventually destroy a system. It's often more effective to reduce how quickly the loop reinforces itself (i.e. slow its growth) rather than trying to add balancing loops.
6.  Information Flows--Who has or doesn't have access to information  
    Missing information is a common way systems can fail. Adding in this information flow adds a new feedback loop to the system, and is often easier or cheaper than building physical infrastructure. This information also needs to be given to the right place and in a meaningful form.
5.  Rules--Incentives, punishments, constraints  
    Rules are one of the most powerful ways to influence a system, as what the system will do depends on the rules it has to follow. A key way to understand how well a system works is to understand its rules and who has the power to change those rules.
4.  Self Organization--Ability to add, change, or evolve system structure   
    The ability to self organize is the one of the strongest ways a system can be resilient.
3.  Goals--System purpose or function   
    All of the lower leverage points will be twisted to meet the goal of a system. The goals of the entire system are much higher leverage than goals of the smaller pieces within the system.
2.  Paradigms--Mindset that leads to system goals, structures, rules, etc.  
    To change a paradigm, keep pointing at anomalies and failures of old paradigms while speaking and acting from a new one. Make the new paradigm visible by ensuring that people believing in that paradigm are visible.
1.  Transcending Paradigms  
    This paradigm is to recognize that no paradigms are true and that every paradigm provides an extremely limited understanding of the universe. Because no paradigm is right, you can pick the one that is most suitable for you to achieve your purpose.

### 7. Living in a World of Systems
- Before changing a system, take time to understand. Understand how it works and it's history. Compare subjective opinions on how the system behaves within actual data. This helps you stay focused on what matters.
- With some understanding of the system, it's important to then test assumptions and make sure they hold water. It's important to be flexible here.
- Think carefully about the language we use to describe systems. Good language makes things clean and enables us to talk about complexity.
- Not all important things are quantitative. It's important to pay attention to qualitative properties as well. It's okay to measure quality poorly--it's there or it isn't--than to try to get it to conform into some numerical measure that doesn't make sense.
- When a system has feedback, define the policy for that feedback.
- Optimize the whole system, not just parts or subsystems.
- There are forces and structures in systems that help it to run itself. Aim to improve those structures, rather than avoiding or diminishing them.
- Focus on where a system can be responsible for itself.
- Expand time horizons--think long term and not just short term.
- Follow systems across disciplines. Don't be limited to staying within just one way of thinking.