# Good Strategy, Bad Strategy

## Introduction: Overwhelming Obstacles

> A good strategy has an essential logical structure that I call the _kernel_. The kernel of a strategy contains three elements: a diagnosis, a guiding policy, and coherent action. The guiding policy specifies the approach to dealing with the obstacles called out in the diagnosis. [...] Coherent actions are feasible coordinated policies, resource commitments, and actions designed to carry out the guiding policy.

## Part I: Good and Bad Strategy

Today, strategy is focused around utilizing "advantages." This is useful, but there are often two overlooked pieces:

> 1. _Having a coherent strategy—one that coordinates policies and actions._ A good strategy doesn't just draw on existing strength; it creates strength through the coherence of its design. [...]
> 2. _The creation of new strengths through subtle shifts in viewpoint._ An insightful reframing of a competitive situation can create whole new patterns of advantage and weakness.

### Chapter 1

- Many organizations don't have a coherent strategy.
- Good strategy is also unexpected from complex organizations, as good strategy is focused. It's often assumed that complex organizations can't get things together to execute something so focused.
- Examples: Apple turnaround, Desert Storm

### Chapter 2: Discovering Power

> The second natural advantage of many good strategies comes from insight into new sources of strength and weakness. Looking at things from a different or fresh perspective can reveal new realms of advantage and opportunity as well as weakness and threat.

- We may not always understand the true strengths and weaknesses at play in a situation. Because of that, we may not be able to form good strategies.
- When a good strategy draws on this, it can seem extraordinary.
- Wal-Mart
  - Consider the competition.
  - > Wal-Mart's advantage must stem from something that competitors cannot easily copy, or do not copy because of inertia and incompetence.
  - Wal-Mart stores function as nodes in a network. This let them break the conventional wisdom of a minimum population size per store.
  - > Walton didn't break the conventional wisdom; he broke the old definition of a store.
- Coherent policies result in specializes pieces that all fit together and complement one another. They aren't interchangeable.
  - > Many competitors do not have much of a design, shaping each of their elements around some imagined "best practice" form.
- U.S. Military—shift to target Soviet weakness
  - > And, most important, it argued that having a true _competitive_ strategy meant engaging in actions that imposed exorbitant costs on the other side. In particular, it recommended investing in technologies that were expensive to counter and where the counters did not add to Soviet offensive capabilities.
  - More accurate missiles, quieter submarines. Expensive to counter, counters don't increase offensive ability.

#### Insight: Decentralization vs. Centralization

Wal-Mart stores operate as nodes in a network, allowing for centralization. Each store benefits from sharing vendors, negotating terms, and shared data/transport.

> Stores that do not share detailed information about what works and what does not cannot benefit from one another's learning.

This also happens _within_ companies, e.g. the software engineering organization.

- Best practices are learnings that add some improvement.
- Centralized network ops, infrastructure, etc. allow teams to benefit without spinning up their own systems.
- What data can be collected to enable more efficient teams?
  - Team composition? Specific people?
  - Product usage?

### Chapter 3: Bad Strategy

> Bad strategy is not simply the absence of good strategy. It grows out of specific misconceptions and leadership dysfunctions.

> To detect a bad strategy, look for one or more of its four major hallmarks:
>
> - *_Fluff._* Fluff is a form of gibberish masquerading as strategic concepts or arguments. It uses "Sunday" words (words that are inflated and unnecessarily abstruse) and apparently esoteric concepts to create the illusion of high-level thinking.
> - *_Failure to face the challenge._* Bad strategy fails to recognize or define the challenge. [...]
> - *_Mistaking goals for strategy._* Many bad strategies are just statements of desire rather than plans for overcoming obstacles.
> - *_Bad strategic objectives._* A strategic objective is set by a leader as a means to an end. Strategic objectives are "bad" when they fail to address critical issues or when they are impracticable.

- Bad strategy is not the same as no strategy or failing strategy.
- Bad strategy often looks at goals but not policy or action.
- Bad strategy does not answer "how."
- If you don't identify and analyze your obstacles, you can't come up with a good strategy.
- Strategies help bridge goals and objectives.
- Underperformance is often not the challenge to be solved—the challenge is the reason for the underperformance. Focusing on underperformance often leads to bad strategy.

> A strategy is a way through a difficulty, an approach to overcoming an obstacle, a response to a challenge.

> A strategy is like a lever that magnifies force.

#### Insight: Code Reviews

Sometimes I see code reviews where the person doesn't know what they're doing. Often it's a particularly complex change, accomplishes the wrong goal or has bad side effects, or even doesn't even have to be made.

This seems like evidence of bad strategy (?) on behalf of the author. They are missing something when it comes to understanding what they're doing and how they're going to accomplish it.

Relatedly, a lot of coding patterns (e.g. DRY) are tactics. Applied blindly, they are typically helpful but there are times where they don't make strategic sense.

It would be nice to be able to define a "good" code review a bit more in terms of a strategic-thinking framework.

Related: What is your strategy for code maintenance? Future code development?

### Chapter 4: Why So Much Bad Strategy?

- Bad strategy exists because the alternative is hard. It is hard to do analysis, logic, and choice. It is also hard to develop and master the fundamentals needed to make those decisions.
- Common reasons the hard work is avoided:
  - Avoidance of pain, difficulty of choice.
  - Ease of template-style strategy.
  - Belief that all you need is a positive mental attitude.
