# The Art of Leadership
Michael Lopp

## Small Things
### Managers
1. Have 1:1s, Learn how to listen for the important signal.
2. When you sign up for a thing, get it done. Every time.
3. Take a measured approach to dealing with disaster.
4. Act last. Read the room. Taste the soup.
5. Listen for one experience that speaks loudly.
6. Each month, ask yourself how you are investing in your growth.
7. Have a monthly conversation with your manager to get feedback on how you are doing.
8. Invest in saving yourself time.
9. Let others change your mind, and tell them when they have. Build a diverse team. Delegate.

### Directors
10. Be patient when things feel broken.
11. Delegate until it hurts.
12. If you're hiring, spend time on it every day.
13. Have a staff meeting that has weekly metrics, includes team-sourced topics, and allows the team to gossip.
14. Well-timed and sincere compliments are free leadership points.
15. Build a team where folks are willing to tell each other hard things.
16. If your team is growing, your ways of working will constantly need to evolve.
17. Draw your org chart for someone else. See if they get it.
18. Invest in reducing the communication tax for distributed team members.

### Executives
19. Act without asking.
20. Build a team that understands itself.
21. Listen to the stories to understand the culture.
22. Protect your unstructured time.
23. Make it clear that leadership can come from anywhere in the team.
24. Find the truth in the rumors.
25. Don't yolo the comms.
26. Find and cultivate high-signal humans.
27. Work to appear not busy.
28. Find and cultivate a mentor.
29. Write down the things you believe as a manager.
30. Be unfailingly kind.

## Part 1. Netscape: Manager
### 1. Assume They Have Something to Teach You
There will be meetings that seem marginal--there's no clear result or outcome, yet they also seem important or relevant enough to go to. Here, assume the other person has a story that will teach you a lesson (that in turn makes the time you're spending meaningful). You need to uncover them.

### 2. Meeting Blur
Meeting blur--when you can't remember what was said by whom, when, and where.

Leaders keep track of the people they're meeting with--their perspectives, their objectives, and how they feel about various topics. When you can't keep track of these profiles you have meeting blur, which is a signal that you have too much to do.

When you have meeting blur, you need to reset. Lopp advises breaking a commitment rather than trying to stretch something out to buy space. That's because people look to your actions and learn from them, whether you intend it or not. You can "teach" them to break commitments but ensure the rest of their work is done well, or do a bunch of things in a mediocre way.

### 3.The Situation
Handling situations/fires.

These are steps to take. Not all of them happen in every situation, some happen multiple times, and exactly which steps, what order, and how much you do them depends on the situation.

> 
1. Am I the right person to handle this Situation?
2. Do I have complete context?
3. What are the track records of my sources of information? Do I trust the sources of information?
4. What inconsistencies in facts have been discovered, and do I understand the nature of those inconsistencies?
5. Can I coherently explain multiple perspectives on the Situation?
6. Do I understand my biases relative to the Situation?
7. Do I understand my emotional state relative to the Situation?
8. Am I the right person to handle this situation?

Just because situations involve adrenaline doesn't mean the decisions should involve adrenaline.

### 4. Act Last, Read the Room, and Taste the Soup

- Act last.
  - Acting last gives you more information and context to form your own opinion.
  - Acting first gives you first mover advantage, which is useful to defining a narrative.
  - Ideas get useful with debate, not agreement.
- Read the room.
  - _What mood is this particular set of humans in?_
  - Talks: ask a question that involves audience participation (e.g. how many people are introverts vs extroverts). The question doesn't matter, the key is how many people answer--this tells you how on guard people are. The more on guard they are, the more you have to warm them up to you.
  - 1-1: _How are you?_
  	- Listen carefully to the answer.
  		- What's the first thing they say?
  		- Is it deflected with humor?
  		- Is it a standard off-the-cuff answer?
  		- Is it different? If so, how so?
  		- What words were used? How quickly did they say them?
  		- How long did they wait to answer?
  		- Did they answer the question?
  	- As before the answer isn't important. It's the mood. And the mood sets the agenda.
  - Meetings (especially ones you aren't running).
  	- Who's running the meeting? How do they open?
  	- Who perks up? Who's buried in their phone?
  	- As topics change, how do demeanors change?
  	- How does my knowledge of the other people help me understand their changes in mood relative to the topic?
- Taste the soup.
	- You have lots of experience. Your job now is to sample critical parts of an idea or project to get an idea of what's been done and what is going to be done.
		- _Why did you choose this design?_
		- _What is this metric going to tell us?_
		- _What do you think the user is thinking at this moment?_

### 5. Spidey-Sense

- You are continually observing the results from your actions and learning from them. You're experiencing different scenarios.
- Spidey-sense is real-time wisdom; a sudden question in the back of your head.
- Spidey-sense spreadsheet--whenever you think there's something that influences an outcome, write it down. This gives you more objective data to look back at when something occurs.

### 6. Your Professional Growth Questionnaire

- You should always/regularly be evaluating your performance and growth.

Questions to evaluate professional growth. Should be answered multiple times a year, with answers written down. How your answers change over time can be as important as the answers themselves.

- What are your strengths? How do you know that?
- What do you need to work on? How do you know that? How are you working on this area? Is your company helping?
- When was your last promotion? How was the promotion communicated to you? What is the one thing you believe you did to earn this promotion?
- When was your last compensation increase? (Compensation = base salary + bonus and/or stock.)
- Do you feel fairly compensated? If not, what would you consider fair compensation? What facts do you based that opinion on? Have you told this to your manager?
- When was the last time you received useful feedback from your manager?
- What compliment do you wish you could receive about your work?
- Are you learning from your manager? What was the last significant thing you learned from them?
- What was the last thing you built at work that you enjoyed?
- What was your last major failure at work? What'd you learn? Are you clear about the root causes of that failure?
- What was the last piece of feedback you received (from anyone) that substantially changed your working style?
- Who is your mentor? When was the last time you met with them?
- When was your last 360 review? What was your biggest lesson?
- When did you last change jobs? Why?
- When did you last change companies? Why?
- What aspect of your current job would you bring with you to a future gig?
- What is your dream job? (Role, company, etc.)
- What is a company you admire? What attributes do you admire?
- Who is a leader that you admire? What are the qualities of that leader that you admire?

### 7. A Performance Question

- Don't think or say the words "performance management."
	- Definition: a well-defined and well-understood workflow leading to an employee's improvement or their departure.
	- The moment you think of this, your engagement changes. The way you interact and communicate becomes structured/unnatural.
- Don't end up in performance management.
- Checklist: Have you had multiple face-to-face conversations over multiple months with the employee where you have clearly explained and agreed there is a gap in performance, and where you have agreed to specific measurable actions to address that gap?
	- Multiple conversations.
	- Face-to-face.
	- Many months.
	- Clearly explained.
- Attitude and demeanor should be a coach's demeanor.

### 8. Rands Information Practices

- Your most precious asset is your time.

#### Browser
	- Make a copy of your bookmarks and store it somewhere safe. Then delete your current bookmarks.
	- Rebuild your bookmarks from memory a bit at a time, over several days.
		- Web-based tools and critical documents belong in the browser bar.
		- News, blogs, and other consumables belong in a feed reader.
	- If you don't have a feed reader, pay for feedly.
	- Install an ad blocker. Be generous about unblocking sites you regularly visit.
	- Pin must-have browser tools (e.g. email, calendar, feed reader) to your favorite browser. 
		- Pin no more than five.
		- Unpin a tool if you haven't used it in a week.
	- Use tabs in your favorite browser. Learn the shortcuts.
	- Strive to have a single browser window open at a time. Strive to have 10 or fewer browser tabs open at any given moment.
	- Place your bookmarks in the cloud.

Win condition: you can clear all your consumables in less than 10 minutes, and you don't have a long tail of cluttered bookmarks that stress you out.

#### Phone
	- Flag VIPs in your contact list. Don't have more than 7.
	- Watch an episode of season 2 of The Office, and turn off all noncritical notifications. Critical notifications are calls from people you know and VIP notifications.
	- Buy and install a spam-blocking utility.
	- Turn on any episode of season 2 of Parks and Recreation, and delete any app that you haven't recently used.

Win condition: When you have three free minutes, you don't instinctively reach for your phone.

#### Email
	- If it's mail you want to read, read it.
	- If it's an external (nonwork) source and you don't want to read it, either:
		- Unsubscribe.
		- Mark it as spam.
	- If the mail is a work source and from a robot, spend a morning learning to filter them out of your inbox to somewhere useful.
	- Learn keyboard shortcuts.

Win condition: getting to inbox zero and staying there.

#### Life
	- Be ruthless about spending time appropriately.