# Slack
Tom DeMarco

## Prelude
- Efficiency-flexibility quandary: the more efficient you get, the harder it is to change.

## Part 1: Slack
> Slack: The unfortunate tradeoff between efficiency and flexibility. How organizations tend to get more efficient only by sacrificing their ability to change. How slack can come to the rescue.

### 1. Madmen in the Halls

- Middle managers tend to get cut for efficiency, resulting in a wider organizational pyramid.
- In actuality, the main activity of middle managers should be reinvention--analyzing, deconstructing, and reassembling the models used to do productive work.
  - This should be a requirement for middle managers. How do you measure it?
- > Slack is the time when you are 0 percent busy.

### 2. Busyness

- The less availability there is, the longer it takes to get something done. Time taken = 1 / availability rate.
- Organizations are networks of interconnected work.
  - Nodes = people.
  - Connections = work in progress that gets passed between people.
- In order to be able to have 100% business, you need to have an inbox/buffer for each person.
- Buffers cause specific pieces of work to take longer to get through the system, even though total throughput is higher.

### 3. The Myth of Fungible Resource

- For efficiency, it's often assumed that people are fungible. But they aren't.
- Matrix management also assumes fungible people.
- Waste from task switching = mechanical time lost + rework required from restarting + immersion time + frustration cost.
- People can't bind with their teams if they have to switch between tasks/team.
- Empirically, there's a 15+% penalty from time sharing someone between two or more tasks.

### 4. When "Hurry Up" Really Means "Slow Down"

- Overimproved organization: one that is so obsessed with efficiency that responsiveness and net effectiveness suffer.
- When people focus on staying busy, they will slow down so that they always look busy.

### 5. Managing Eve

- Knowledge workers need both challenge and pay.
- Hierarchical lines are paths of authority/reporting. They don't work for communication--it's too efficient for information to have to go all the way up and then back down.
- People can't be overly controlled; they must have enough leeway to choose their own direction and make mistakes.
- Control comes up most often when determining the methods to get something done.
- There's both time slack and control slack.

### 6. Business Instead of Busyness

- Intentional slack provides flexibility, better people retention, and a capacity to invest.

#### Change

- Change is an investment, one that costs conceptualization/design and implementation.
- Change has to be done by the people who are to be changed.
- Ability to change has to be an organic part of the organization.

#### Human Capital

- Everytime someone quits on their own schedule, the organization suffers a loss.
- The more important someone's domain knowledge, the less fungible they are.
- Domain knowledge is an investment, paid with money, in the minds of each person. When someone leaves that asset is lost.
- Approximation for value of this human capital: human capital = time to get up to speed * (salary + overhead) * 50%. Time to get up to speed is how long until they are properly filling the shoes of someone who left with the domain knowledge you want.
- For a project, domain knowledge is larger since it involves the business area and the project itself. Untimely loss of someone on a project is also expensive since ramp-up time may be longer than the time budgeted for the project.

## Part 2: Lost, but Making Good Time
> The effect of stress on organizations: It leads them badly off course while causing them to redouble their speed. Causes of and cures for corporate stress.

### 7. The Cost of Pressure

Ways managers add pressure to their reports:

- Setting more aggressive delivery dates
- Adding extra work
- Encouraging overtime
- Being angry when disappointed
- Noting one report's extraordinary effort when others don't have it
- Being severe about any performance that isn't exceptional
- Expecting great things out of everyone
- Preventing any apparent waste of time
- Setting an example yourself of doing a lot of work
- Creating incentives to encourage desired behavior


- People believe that applying pressure decreases the amount of time it takes to do something.
- This isn't true—people under time pressure don't think faster.

### 8. Aggressive Schedules

- People who set/force unrealistic schedules should also be held responsible for the schedule not being met.

### 9. Overtime

- Short bursts of overtime (sprinting) can be okay and even powerful if used at the right time and appropriately.
- Extended overtime is not good.
  - Culturally, people are obligated to hide their "recovery."
  - Even if people are still getting enough sleep, their personal life suffers.
  - Extended overtime reduces productivity.
  - Extended overtime has other side effects as well:
    - Reduced quality
    - Personnel burnout
    - Increased turnover
    - Ineffective normal use of time
- Fatigue negatively impacts quality of thinking.
- Burnout causes people to not want to do anything.
- Turnover is often a hidden cost that companies don't account for.
  - Turnover rates can vary significantly between companies. The best third have half the turnover as the worst third.
- Overtime decreases how much management trims wasted time.
  - Unnecessary or large meetings.
  - Too many interruptions.
- > Overworked managers are doing things they shouldn't be doing.
  - This also causes a downward spiral—overworked managers are doing less management, which causes them to be more overworked.

### 10. A Little Sleight of Hand in the Accounting Department

- Overtime is often not considered/accounted for, causing managers to look more effective than they actually are. This incentivizes managers to encourage overtime.
- Seems like something useful would be the ability to measure these things in the short, intermediate, and long run.

### 11. Power Sweeper

- Improvements over time have enabled people to do work that takes up a lot more of their time, work that could have been offloaded to someone else.
  - Example: people needing to make their own copies of a doc.
- Having a support/clerical person for a team allows the team to reduce a percentage of its upkeep.

### 12. The Second Law of Bad Management

> First Law of Bad Management
> 
> If something isn't working, do more of it.

- First law abuse happens when people without management talent try to be managers—they blindly apply concepts without being attuned to how it affects their organizations.

> Second Law of Bad Management
>
> Put yourself in as your own utility infielder.

- Managers don't perform the actual work, and every time they interact with someone who does they are creating a distraction. This is helpful in the long run but annoying in the short run.
- Why people follow the second law:
  - In some orgs, doing the lowest-level work keeps your job safe.
- Management is hard, not because there is a lot of work but because the skills are hard to master.

### 13. Culture of Fear

- Characteristics of the culture of fear:
  - It isn't safe to say certain things, even if true. Things such as having doubts around a quota.
  - Being right in your doubts means that you're the reason things didn't happen (i.e. you probably sabotaged it).
  - Goals are so aggressive that the odds of achieving them are slim.
  - Power can trump common sense.
  - Anyone can be abused for failing to comply.
  - People who are fired are on average more competent than those who aren't.
  - Survining managers are particularly angry, and people are scared of them.

### 14. Litigation

- Litigation is often more costly than the thing being litigated. So why do it?
  - Litigation can allow for deflection of blame.
  - Litigation might result from a flawed contract, and people subsequently want to litigate on principle and not for profit.
- In healthy organizations, a certain amount of failure is okay. People fail, recover from and analyze their failure, then get another opportunity with a similar amount of responsibility. They then succeed.
- People need room to fail without being blamed.
  - With room to fail, no one will try things that aren't sure bets.
- Employees often know there's something flawed, but their concerns are overruled by authority.
- When engaging in a contract, you want to make sure there's enough slack.

### 15. Process Obsession

- Standards are good and essential to modern life.
  - For one, they enable compatibility across different devices made by different (and potentially competing) vendors.
- Because standards are so good, we might want to develop process standards for knowledge work. This makes people more interchangeable since the work procedures are similar.
  - This can result in process obsession, which is a problem.
- For product/manufacturing standards, the standards describe the final end interface. How a product gets to its final point is left to the vendor; the only important part is if it meets the desired standard.
- How-to standards describe how work should be performed (a.k.a. process).
  - Taylorism: From Frederick Winslow Taylor, a rigorous standardization of process.
  - Still in widespread use for manufacturing processes, but is it good for knowledge processes?
- Manufacturing deviations from Taylorism:
  - Volvo—teams of generalists produce a car from start to finish, rather than the car being continually passed off through specialists. This involves each person knowing and performing a number of jobs.
  - Post cereal factory—in each of three shifts, the teams running the shifts may run the factory in entirely its own way. Decisions on how things get done are left entirely to the individual teams.
- Above examples push process ownership downward—it is a team asset and not a corporate asset.
  - This does reduce flexibility with interchanging team members, as teams can deviate from one another over time.
  - Benefits include a more interesting workday for workers, closer ties to the product & customers, less turnover, and more loyalty to the team/corporation.
- Knowledge work isn't well-suited for Taylorism, because things are not as constant. They involve a lot more subjective judgment.
- Star knowledge workers typically do the same work as their peers; their difference is they have stronger connections with others.
- When work is automated, it reduces the amount of work that needs to be done, but increase the difficulty of the remaining work.
- Often process standards focus on the mechanics of what you need to do, but don't offer help on the actual goal or spirit of the process.
- Owning the process can be used as a means of control (if a boss decides it) or as a means of empowerment (if the people doing the work decide it).
- Empowerment is the transfer of control to someone else. It's not actual empowerment unless there's a chance of failure, or put another way, there's a chance to injure the person above you.

### 16. Quality

- Quality is something that still works great after lots of time and/or use. It doesn't necessarily have to be fancy.
- What makes quality?
  1. It's unique, and is completely unique when it first appears.
  2. It redefines the notion of its genre. (With Photoshop, this is photo processing.)
  3. It redefines how you think of adjacent topics/concepts. (With photoshop, there can still be good parts to overall bad photos—and you can reuse that.)
  4. It lets you do things that were barely imaginable before.
  5. It is deeply thought out. (To some extent, this means its features allow for the emergency of complex behavior through, for example, combination.)
  6. It's fully implemented.
  7. The human interface is easy to use. You rarely need to be trained.
  8. It's extensible.
  9. It's solid. (No defects.)
  - I disagree with some of these. A quality pair of sweatpants, for example, doesn't need to reinvent a lot of things—it just needs to perform its function well and for a long time.
- DeMarco's argument: product quality isn't about lack of defects, it's about what it does for you and how it changes you. Quality is a function of usefulness.
- Corporate quality, on the other hand, focuses on removing defects.
- Quality takes time.
- Companies become more efficient by transferring cost/labor to their customers.

### 17. Efficient and/or Effective

- Efficient: doing something with minimum waste.
- Effective: doing the right something.
- Ideally, we'd always choose to be effective over being efficient.
- Companies tend to avoid risk because that means they will have to discard the efficiency they've built up.

### 18. Management by Objectives

## Part 3
> Change, growth, and organizational learning: The difference between companies that can learn (and profit from their learning), and those that can't.

- You can't grow if you can't change
.
### 19. Vision

- Most common sign of absent vision is the sense of not knowing "who we are."
- While it might be hard to know who you are, it's easier to know who you aren't.
- Peter Drucker's cutlre—the part of the org that cannot, will not, and must not change.
- Successful change can only occur with the context of things that won't change.
- A visionary statement is an assertion of who we are.
  - It must have an element of present truth.
  - There must be an element of future truth.
  - When you have the right middle ground and the future truth is rosy but not impossible, people will accept the vision.

### 20. Leadership and "Leadership"

- Leadership is the ability to enroll other people in your agenda.
- Leadership elements always include a clear direction, admitting the short-term pains, and a lot of follow-up.
- Lack of power is an easy excuse when leadership fails, but really leadership is about making change in the absence of enough power.

### 21. Dilbert Reconsidered

### 22. Fear and Safety

- Safety must always be present in a successful organization.
- Fear often, but not always, inhibits change.
- The fear of being mocked prevents change.

### 23. Trust and Trustworthiness

- Deserved trust: you gain this by demonstrating trustworthiness.
- New leaders don't have time for this. They need "undeserved" (or not-yet-deserved) trust.
  - This involves personal magnetism: be articulate, colorful, attractive, wry.
  - Acquire trust by giving trust.
- Leaders have to discern whom to trust, how much to trust, and when to trust.

### 24. Timing of Change

- Conventionally, people don't change things until they're broken.
- There are times people won't accept change, and times when change will be more readily acceptable.
- As long as people define themselves in terms of the work they do, they'll value that work--and so be reluctant to changing it.
- Change resistance is enormous. To overcome it,  you need some advantages:
  - Sensible approaches to change introduction.
  - Culture that isn't change-phobic.
  - Proper timing.
- Resistance tends to be emotional, not logical.
  - Anxiety makes this worse.
  - Downturns are often a bad time to introduce change because people are already uncomfortable.
- A good time to introduce change is during healthy growth.

### 25. What Middle Management Is There For

- Where does change happen?
  - It doesn't come from the top--the top might give incentives for change, but not the specifics.
  - It doesn't happen at the bottom--they don't have the perspective to reinvent or the power to carry it out.
  - By elimination, it happens in the middle.
- Significant change involves reinvention.
  - Corporate reinvention needs deep involvement in day-to-day business organization, which top management probably doesn't have.
- The key role of middle management is reinvention.
  - Middle managers are agents of change.
- Middle managers need to work together to come up with and then make change happen.

### 26. Where Learning Happens

- Classic model:
  - Learners, facilitators, material, co-learners.
- When people learn/discover soemthing important, they panic--they worry that the thing they've come across is superior to what they've been doing for years.
  - At this point it helps to be supported/reinforced by the facilitator as well as to have others who are experiencing a similar thing.
- Most of team work is done by individuals working separately, not a true team. So why are teams important?
  - They help align goals and keep everyone going in the same direction.
  - It provides community.
  - People on the team who are further ahead of you in a subject are coaches.
- Often teams are only set up at the bottom level of an org. There should probably be teams at other levels, especially as teams help learning happen.
- People often have to learn management skills in a depleted learning environment--there's no facilitator, material, etc.
- Management team meetings often expose that people aren't a team.
  - Everyone's concerned with their own thing--they don't have joint responsibility for the work products.
  - People should be putting their heads together, not talking to the boss.
  - Isolating responsibility and accountability to a specific manager encourages them to be siloed off from other managers.
- Learning and change happens in the "white space" of an org chart--where the middle managers are.

### 27. Danger in the White Space

- Nonlearning organizations have trouble in the white space.
- Internal competition is never healthy.
- Competition involves offense and defensive.
  - People who think some healthy competition can't hurt are only thinking about the offense.
- "Training" at companies involves learning how to do something, then being able to perform it at expert speed. But real training involves slowing down and learning the concept.

### 28. Change Management

- True change is hard to steer and often tries to go off in its own way.
- Authority breaks down during change, so you have to rely on persuasion and your ability to lead without positional power.
- Reward After Performance breaks down--you have to reward first.
  - Sometimes this means calling in your "favors" to get people to embrace change.

## Part 4
> Risk taking and risk management: Why running away from risk is a no-win strategy, and why running _toward_ it makes sense when managed sensibly (and what that entails).

- Risk management: planning for failure.
  - You're planning for many small but expensive failures. You'll still overall succeed by winning a lot of money.

### 29. Uncommon Sense

- Portfolio risk management: don't have too much of one type of asset.
  - Risks aren't inherently bad.
  - Risks don't entirely go away.
  - Managing risk costs something.
  - If risks don't materialize, risk management costs something.
  - Risk management applies to the whole portfolio, not just one of the risks.
- Portfolio risk management applies to your business as well.
- Projects are risks. Portfolios are lots of projects or sufficiently large projects.
- It's hard to take risks without an assessment of the uncertainty/downside.

### 30. Risk Management: The Minimal Prescription

- Aggregate risk: possible overall failures for a venture.
  - If an aggregate risk occurs, the entire venture fails--it would have been better to not have done it at all.
  - You can't manage aggregate risk directly.
- Component risk: things that can go wrong that lead to aggregate failure.
  - You can manage these.
- Risk management includes:
  - Listing and counting each risk.
  - Having an ongoing process for discovering new risks.
  - Quantifying each risk's impact and likelihood.
  - Identifying indicators that a risk is starting to occur.
  - Create a plan in advance for risks that occur.
  - Have a model that shows how component risks affect overall success.
- Dealing with risk depends on its impact, likeliness, and nature.
  - Some can be paid with time/money. This payment comes from some kind of reserve.
  - Unplanned risks involve finding resources to offset them. Planned risks typically have a reserve set aside.
- Risk mitigation: the actions you'll take to reduce the impact of a risk.
  - The plan has to be there before the risk occurs.
  - Some mitigation activities also have to come before the risk occurs.

### 31. Working at Breakneck Speed

- Managing risks requires going at a slower speed.
- Mitigation has a cost that worsens your best case scenario, but improves your medium and worst case scenarios.
- If people can only focus on the best date, they can't mitigate risk.

### 32. Learning to Live with Risk

- We have to take risks, but people are frequently avoiding risk.

Risk Management Assessment (Hard):

- Is there a published list of risks?
  - Does it contain major causal risks?
  - Is it visible to everyone on the project?
  - Are there enough risks on the list to indicate careful analysis?
- Is there a mechanism in place to help discover new risk?
  - Is it safe for everyone to signal a risk?
- Are any of the risks potentially fatal?
- Is each risk quantified for its probability, cost, and impact?
- Does each risk have a transition indicator?
  - Is the transition indicator being monitored?
- Is there a single person responsible for risk management?
  - If everyone is responsible, no one ends up being responsible.
- Are there work tasks that might not have to be done at all?
  - If no, there's no risk management.
- Does the project have a schedule and a goal, and are the two different?
- Is there a large probability of finishing before the estimated date?
  - If not, the schedule is a goal and not an estimate.

