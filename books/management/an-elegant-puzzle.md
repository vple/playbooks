# An Elegant Puzzle: Systems of Engineering Management
Will Larson

## Organizations
> An organization is a collection of people working toward a shared goal.

### Sizing teams
Organizational design raises new categories of questions:

- How many teams should you have?
- Should a new team be created for an initiative, or should an existing team work on it?
- What's the boundary between those two teams?

Will believes the core challenge to organizational design is to size teams. He puts forth these guiding principles:

**Managers support six to eight engineers.** This gives them enough time for tactical work (active coaching, coordinating) and strategic work (writing strategies, leading change).

Managers with four or fewer engineers tend to be _tech lead managers_, who participate more in design and implementation. These TLMs can use their strengths better, but the role has more limited career opportunities. They need to focus more on management skills to progress in that direction. They also don't have enough time to focus on technical details that are needed to progress in a technical direction.

Managers with more than eight engineers act as coaches and safety nets. They don't have enough time to actively invest in their team or their area. The coach is a viable role while transitioning to a better configuration, but it's not an ideal.

**Middle managers should support four to six managers.** This gives them time to coach, align with stakeholders, and invest in their organization. They're also busy enough that they don't create work for their team.

Middle managers can have fewer than four managers while they're transitioning into their role or domain. Like line managers, middle managers who support a lot of managers will act as a problem-solving coach.

**On-call rotations want eight engineers.** Pooling teams together for a shared rotation to hit the needed number of engineers is possible as an intermediate step, but not ideal in the long run. It causes stress for engineers who have to be on-call for systems they're unfamiliar with.

**Small teams (fewer than four members) are not teams.** A team should abstract the people that compose the team. This isn't possible with such a small team; you have to pay attention to every on-call shift, vacation, interruption, etc. They're also more fragile, as team changes have more impact.

### Staying on the path to high-performing teams
How do you increase team performance? To increase performance, should you spread hiring equally across the teams that need more people, or should you focus on one team at a time?

People tend to reach towards hiring as an answer. But hiring should be done more strategically; we want to understand what a team needs to increase performance.

#### Four states of a team
A team can be in one of four states. Each state improves its performance in a different way.

- **Falling Behind**  
	A team is falling behind if their backlog becomes longer each week. You'll often see people working hard but making little progress, low morale, and vocally dissatisfied users. This team benefits from adding people.
- **Treading Water**  
	A team that is treading water is able to get critical work done, but they can't pay down technical debt or begin major new projects. Morale is better, but people are working hard and users know that asking for help won't go anywhere. This team benefits from reducing WIP.
- **Repaying Debt**  
	A team is repaying debt when they can begin paying down tech debt and are benefitting from that repayment--each piece repaid provides more time to repay more debt. This team benefits from having more time.
- **Innovating**  
	A team is innovating when its tech debt is sustainably low, morale is high, and most of their work is satisfying new user needs. This team benefits from having more slack.

#### System fixes and tactical support
Helping a team to transition involves adapting a **system solution** for their current state, then provide tactical support. The strategic solution is most important; do this before trying to do anything tactical.

| State          | System Solution                                                                                              | Tactical Support                                                                                   |
|----------------|--------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------|
| Falling Behind | Hire net new people until the team is treading water.                                                        | Set expectations with users, beat the drum around easy wins, inject optimism.                      |
| Treading Water | Consolidate efforts to finish more things and reduce concurrent work (WIP).                                  | Help people transition from a personal view of productivity to a team view.                        |
| Repaying Debt  | Add time so that paying tech debt has space to compound its rewards.                                         | Find ways to support users while repaying debt so you don't disappear from the users' perspective. |
| Innovating     | Maintain slack so the team can build quality into their work, continuously innovate, and avoid backtracking. | Make sure the team's work is valued, otherwise resources will be taken from the team.              |

### A case against top-down global optimization
### Productivity in the age of hypergrowth
### Where to stash your organizational risk?
### Succession planning

## Tools
### Introduction to systems thinking
### Product management: exploration, selection, validation
### Visions and strategies
### Metrics and baselines
### Guiding broad organizational change with metrics
### Migrations: the sole scalable fix to tech debt
### Running an engineering reorg
### Identify your controls
### Career narratives
### The briefest of media trainings
### Model, document, and share
### Scaling consistency: designing centralized decision-making groups
### Presenting to senior leadership
### Time management
### Communities of learning