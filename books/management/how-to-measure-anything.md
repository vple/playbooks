# How To Measure Anything

## Part 1: The Measurement Solution Exists
### 1. The Challenge of Intangibles

- There are two types of intangibles:
  - Literally not touchable, but generally agreed to be measurable (e.g. time).
  - Things that people aren't measurable at all.
- Claim: The latter type of intangible doesn't exist.
- Companies make irrational decisions around seemingly intangible things.
  - Some categorically refuse to accept projects if the project only improves something unmeasurable.
  - Others have intangibles as their core value, and are happy to make investments regardless of the return.
- Sometimes things are viewed as measurable, but they aren't measured because it's unclear or seems to hard to actually measure.

- Why care about measurements?
  1. Measurements inform key decisions.
  2. Measurements can have market value and be sold to others (e.g. results of a survey).
  3. Measurements sometimes scratch an itch.

> Management needs a method to analyze options for reducing uncertainty about decisions.

#### Universal Measurement Approach

1. Define the decision. (Chapter 4)
2. Determine what you know now. (Chapters 5 & 6)
3. Compute the value of additional information. (Chapter 7)
4. Measure where information value is high. Repeat steps 2 and 3 as needed. (Chapters 9-13)
5. Make a decision and act on it. (Chapterse 11, 12, & 14)

### 2. An Intuitive Measurement Habit: Eratosthenes, Enrico, and Emily

- Sometimes it's easier to get an estimated measurement by instead measuring things that are affected by the quantity you want to measure.
- Fermi question--estimating things that you don't have direct information for, but can indirectly reason about. (number of piano tuners in Chicago)
  - Fermi decomposition gives both an estimate but also helps identify what part of the estimate is most uncertain.
  - The most uncertain components can be measured to most reduce the overall uncertainty.
- While it can sometimes be hard to measure the degree to which something is affected (e.g. quality improvement), there are often related metrics where you can measure if there was any meaningful difference before and afterwards.
- Many seeming intangibles don't need sophisticated measuring techniques to get some useful measurement.

### 3. The Illusion of Intangibles: Why Immeasurables Aren't

There are three reasons people believe something can't be measured:

1. **Concept**--the definition of measurement isn't well understood; people misunderstand what "measurement" means.
2. **Object**--the thing being measured isn't well defined.
3. **Method**--an appropriate measurement procedure isn't known.

Three reasons often given to not measure something:

1. It's too expensive to measure.
2. General opposition to statistics.
3. Ethics--it would be immoral to measure whatever you're measuring.

Only the first one has merit, but it's still overused.

Four useful measurement assumptinos:

1. It's been measured before.
2. You have far more data than you think.
3. You need far less data than you think.
4. Useful, new observations are more accessible than you think.

#### The Concept of Measurement

- Few things can be measured with a near perfect level of certainty.
- **Measurement: a quantitatively expressed reduction of uncertainty based on one or more observations.**
- Information: the amount of uncertainty reduction in a signal.
- Measurement doesn't have to eliminate certainty; reduction can be plenty in many situations.
- The subject of measurement doesn't have to be determined quantitatively, just the uncertainty.
  - Membership of a set is qualitative and usable.
- Scales of measurement:
  - Nominal--set membership.
    - Example: if you have A, B, AB, or O blood type.
    - No implicit order, no relative size.
  - Ordinal--comparable, but we don't know the difference in magnitude.
    - Example: Four-star movie rating system. Four stars is more than two, but not necessarily twice as much.
    - No defined unit of measure so that you can add a fixed, known "1" value.
  - Ratio--ordinal, but we do know the how much two objects differ by.
    - Example: dollars, meters, seconds.
    - Ratio scales can be added, subtracted, multiplied, and divided.
  - Interval--ratio scale, but 0 is an arbitrary point.
    - Example: Celsius scale (0 doesn't mean no temperature).
      - Can't say 20 degrees Celsius is twice as hot as 10 degrees Celsius.
- Treat uncertainty as a feature of the observer, not necessarily the thing being observed.
  - This is Bayesian interpretation--new information can update prior probabilities.
- Probability being a feature of a system is a _frequentist_ interpretation.
- You don't need to eliminate uncertainty with measurement, but you should have some expected reduction to justify measurement.

#### The Object of Measurement

- Clarification chain--a tool to think tangibly about an "intangible."
  1. If X is something we care about, X is detectable.
  2. If X is detectable, it's detectable in some amount--you can detect more or less of it.
  3. If we can observe it in some amount, it's measurable.
- Thought experiment: if you cloned the organization affected by an intangible and gave the "test" group more of your intangible, what would you expect to see?
- It can help to say why you want to measure something in order to help understand what is being measured.

#### The Methods of Measurement

- Direct measurement: where you measure the entire "population" of what you're trying to assess.
- Most seemingly difficult measurements involve indirect deductions and inferences.
- Even small samples can be informative and help to make better decisions (i.e. you can still get information without doing something that is statistically significant).
- Example inference types:
  - Measuring small random samples of a very large population.
    - You can learn about population distribution, especially if there's a lot of uncertainty.
  - Measuring size of a largely unseen population.
  - Measuring when many other, possibly unknown, variables are involved.
    - E.g. if a new program is the cause for an increase in sales, as opposed to some other factor.
  - Measuring the risk of rare events.
  - Measuring subjective preferences and values.
- Rule of five: five random samples from a population will give a 93.75% chance of your median falling between the low and high sample.
  - While the range might still be great, this can still decrease your uncertainty.
  - Math: There's a 50% chance per sample that the sample is above the median, and likewise for being below the median. `1-2*(.5^5) = 93.75%`
- Single Sample Majority Rule: If you randomly select one sample from a large population (even an extremely large one), where you initially believed the population proportion could be between 0% and 100%, there's a 75% chance what you observe in teh sample is the same as the majority.
  - Population proportion: The percentage of a population with a desired attribute.

## Part 2: Before You Measure

### 4. Clarifying the Measurement Problem

Before making a measurement, answer some questions:

> - What is the decision this measurement is supposed to support?
> - What is the definition of the thing being measured in terms of observable consequences and how, exactly, does this thing matter to the decision being asked (i.e.., how do we compute outcomes based on the value of this variable)?
> - How much do you know about it now (i.e. what is your current level of uncertainty)?
> - How does uncertainty about this variable create risk for the decision (e.g., is there a "threshold" value above which one action is preferred and below which another is preferred)?
> - What is the value of additional information?

Chapter 4 focuses on the first two questions.

- If you're going to measure something, you need to know what actions the measurement will inform.
- Decisions have to be defined well enough to be quantitatively measured.
- Uncertainty: The lack of complete certainty. A true outcome isn't known.
- Measurement of uncertainty: A set of probabilities assigned to a set of possibilities.
- Risk: A state of uncertainty where some possibilities result in a loss or other undesirable outcome.
- Measurement of risk: A set of possibilities each with quantified probabilities and quantified losses
.
Requirements for a decision:

- A decision has at least two realistic options.
- A decision has uncertainty; the best choice is not certain.
- A decision has potentially negative consequences if you pick the wrong option.
  - Opportunity loss is also a form of loss (if one option would result in a positive but worse outcome than the other).
- A decision has a decision maker.

Possible forms for a decision:

- Decisions can be one big thing or a bunch of little things.
  - E.g. one-time decisions (a big project), recurring decisions (hiring for a role), portfolio of one-time decisions.
- Decisions can be about a discrete or a continuous choice.
- Decisions can have one or multiple stakeholders, including collaborating and/or competing stakeholders.

### 5. Calibrated Estimates: How Much Do You Know _Now_?

- The knowledge you have now has an impact on how you should measure something or even if you should measure it.
- People will have bias and be over/under confident.
- _Assessing uncertainty is a general skill that can be taught and learned._
- Equivalent bet test: would you rather bet on your confidence interval, or on the odds of a game with exactly the claimed odds?
- If you set the price/payout on a future event, you are _coherent_ if your price is "appropriate"--you don't care if people think the price is too high/low because you think it's appropriately priced (and so won't be arbitraged).
- Some people, when estimating, "anchor" to what they think the right value is when determining their interval. It can help to instead frame as what value do I need for 95% of results to be above/below this value?
- Absurdity test: what values do I know are ridiculous?

## Part 3: Measurement Methods

## Part 4: Beyond the Basics