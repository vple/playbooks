# The Laws of Human Nature
Robert Greene  
[Amazon](https://www.amazon.com/Laws-Human-Nature-Robert-Greene-ebook/dp/B07BJLX414)

## Introduction
People are not always in control of themselves. This book pulls on psychology, science, philosophers, novelists, and biographies to try to classify and explain what influences us. It also aims to help you perceive others and change how you see yourself, via the following:

1. You'll be able to observe people calmly and strategically, allowing you to not get tied up in drama.
2. You'll be better at interpreting cues that others give out, allowing you to better judge their character.
3. You'll be able to handle toxic people.
4. You'll understand what truly motivates and influences people.
5. You'll better understand how you are affected by human nature, allowing you to better yourself.
6. You'll become more empathetic.
7. You'll realize you have more potential that you can bring out of yourself.

## 1. Master Your Emotional Self—The Law of Irrationality
People tend to believe they are behaving rationally, but many times they are actually just driven by their emotions. They believe they are being rational because they don't recognize the influence of their emotions and because they're reluctant to think critically of themselves.

When we do something wrong, we look for an explanation. We often find explanations where we were helpless and we often look for external causes. External factors are at play, but often we that got ourselves into a negative situation.

In order to behave rationally, we have to first understand that *we are fundamentally irrational*. Often our thoughts start as an emotion, which then must be translated into words and abstract meanings. We're not good at doing this, leading us to misunderstand what drives us to do certain things. Additionally, trying to reconcile differences between our emotions and our cognition can result in a positive feedback loop where we continually dwell on an emotion. This can cause it to grow and lead us to behave in ways that we typically wouldn't.

Rational people understand that we are influenced by our emotions and will try to reduce how their emotions affect their behavior. Irrational people are unaware of this influence. This is something that is judged over time—can people sustain success and learn from their failures?

Being rational involves being aware of low-grade irrationality, being aware of high-grade irrationality, and rely on strategies that help our rational brain have more influence over our emotions.

### Recognize the Biases
We have several moods and feelings that influence us subconsciously. They cause biases, many of which are rooted in our desire for pleasure and in our avoidance of pain.

#### Confirmation Bias
Confirmation bias happens when we go in search for evidence to support our view, especially when we discard evidence that refutes our view. This especially happens when people ask for advice—although they want a second opinion on the surface, they are really looking for an opinion that matches their existing viewpoint. Confirmation bias also tends to show up in theories that seem too good to be true.

Confirmation bias is avoided by getting in the habit of trying to disprove your beliefs.

#### Conviction Bias
This bias is where we believe something must be true because we believe in it so strongly. Even if we don't feel we believe in something ourselves, we can still be influenced by this bias—passionate leaders often inspire people to follow them because of how passionate they are. We are also inclined to mistrust people who are hesitant or who aren't confident.

#### Appearance Bias
We often judge people by how they present themselves to us. One, people often present a persona that they want us to believe about them. Two, when we see positive or negative qualities in someone, we tend to project similar qualities onto them.

#### The Group Bias
We are afraid of isolation, which influences us to take on others' ideas and opinions to make us feel as if we belong. However, we then end up believing that we came to our ideas on our own.

#### The Blame Bias
We prefer to blame others than ourselves, since blaming ourselves means admitting that we have faults.

#### Superiority Bias
We tend to believe that we're better than others, that they have more faults than we do. This also shows up in the form of believing that we are motivated by noble reasons, while others are motivated by unethical ones.

### Beware the Inflaming Factors
High-grade irrationality is triggered by external factors. This irrationality is more pronounced than low-grade irrationality. It also weighs on us until we find a way to vent our emotions, often on other people. When we're able to realize the factors that cause this, we're better able to stop tunneling in on our negative emotions.

#### Trigger Points from Early Childhood
Experiences during our childhood leave strong influences. We then incorrectly interpret normal, objective actions through those influences, leading to irrational behavior. Moreover, our irrational behavior often reinforces those influences.

For example, if your parents were distant and made you feel abandoned, you might have "learned" that abandonment meant you weren't as worthy of your parents' love. Later on, when a relationship becomes a bit more distant (which is normal), you might overreact as you're afraid you're being abandoned.

You can recognize when you or someone is being influenced by this behavior when the person reacts in a way that is childish and out of character. You want to try to catch this behavior in yourself, then force yourself to detach and figure out the underlying source. Knowing that will allow you to be better equipped for it in the future.

#### Sudden Gains or Losses
These can cause strong reactions and addictions, especially when we misattribute the underlying cause of the gain or loss. We also tend to discount how much luck affected the outcome. Repeated gains or losses can also cause us to behave differently. Losses especially cause us to choke up in high-pressure situations.

This effect can be mitigated by counterbalancing sudden unusual gains or losses with some pessimism or optimism, respectively.

#### Rising Pressure
When people are stressed, their flaws tend to show up. A good way to judge others' character is to see how they respond under stress.

When you're feeling stressed, you need to watch yourself more carefully. Be more detached, and try to find time and space to be alone in order to decompress.

#### Inflaming Individuals
Some people cause strong reactions, be it positive or negative. The positive ones are often particularly charismatic, being able to stir up emotions in others.

You can recognize these people by how they affect others. Those affected aren't able to reason while around the person. They can also cause you think about them, even when they aren't around.

You can deal with these people by casting them as a normal human, noticing that they have weaknesses like everyone else. Making them human makes them seem more normal.

#### The Group Effect
This is a stronger version of the group bias, above. People behave differently in groups, such as at sporting events or at concerts. You get caught up in an emotion. This is fine for positive emotions, but getting caught up in emotions for nefarious reasons is often bad.

The best way to avoid negative group effects is to avoid the group or to be very skeptical while present. You can also detect when this is going on by noticing when others influence your reasoning ability, either by reducing it or by getting you to stop reasoning altogether.

### Strategies Toward Bringing Out the Rational Self
We become very practical and rational when we have to get something done, often with a timeline. We only pay attention to our excitement and energy; other emotions are ignored. We can get to this state more often through awareness and practice.

- **Know yourself thoroughly.**  
Catch yourself acting on your emotions, especially when you're stressed. Analyze the emotions and weaknesses that you feel at those moments and try to find patterns as to how you behave.
- **Examine your emotions to their roots.**  
Face your emotions head on and figure out what other emotions or factors are underlying what you're feeling at the moment. There can easily be other triggers that lead to your current emotion.
- **Increase your reaction time.**  
Give yourself more time before responding, allowing yourself to reduce the intensity of the emotions you're feeling.
- **Accept people as facts.**  
Instead of wishing that people were something that they aren't, see them as things that just exist. View them instead as puzzles to understand. This helps you to observe them more rationally and to stop projecting your emotions onto them.
- **Find the optimal balance of thinking and emotion.**  
Emotions are inevitable. Emotions are what move us forward, thinking is how we steer and channel that emotion. Think as much as possible before making a decision, then let your emotions guide your actions once you've decided to commit.
- **Love the rational.**

## 2. Transform Self-love into Empathy—The Law of Narcissism
We're all narcisscists—we have to be in order to function. We all need attention, but there's a limited amount of attention that we can get from others. So, we come up with an internal image that we identify with as our "self." We tend to attribute our positive qualities to our self, and that self becomes a way for us to gain attention and validation from ourselves. Balanced appropriately, this gives us self-esteem.

### Types of Narcissists
Although we're all narcissists, we vary in our narcissism. There's a spectrum of narcissism.

#### Deep Narcissists
On the bottom end, you have *deep narcissists*. These are people who were unable to develop a realistic sense of self in their childhood. This forces them to seek external validation later on, since they can't provide the attention they need from themselves. Although they may integrate well with others at first, they need increasing amounts of attention as they grow older (possibly as a result of becoming accustomed to and bored of their current level of attention). 

When interacting with deep narcissists, they need to focus attention on themselves. They often appear self-confident to hide their lack of self-esteem. When they feel insulted or challenged, however, they have no defenses. They don't have a way to self-validate their worth, and often take things personally or overreact to perceived offenses. Deep narcissists also try to see others as objects to control, allowing them to use them for validation. It's best to avoid deep narcissists when you identify them. 

A more dangerous form of the deep narcissist is the *narcissistic leader*. They have more ambition than deep narcissists, which allows them to accomplish work that typical narcissists can't do. They attract others through their accomplishments and their personality. But they're also unstable—they still can't handle being challenged and they also create drama that only they can solve. They force their organizations to operate through them, unlike real leaders who are trying to create cultures that will function without them.

#### Functional Narcissists
On the upper end, there are *functional narcissists*. They are self-absorbed but functional, since they can rely on their sense of self. Because they don't always need to satisfy their insecurities, functional narcissists are able to focus their attention outwards to help others.

#### Healthy Narcissists
These narcissists are at the top of the spectrum, able to recover quickly from insults and needing little external validation. Because they can embrace their flaws, they have an accurate picture of themselves. They don't need to continually prove that their self-image is their actual self because they know it's an accurate.

Healthy narcissists are able to turn more of their attention outwards. This allows them to focus on their work and to become more empathetic. They understand how their own attitude and how they split their attention will affect others, both individually and in a group. They also understand how to engage with people who are feeling negative—they are gentle and find ways to handle them indirectly.

#### Managing Narcissism
There are three things to do better handle narcissism:

1. Understand deep narcissists. We want to be able to identify and handle deep narcissists.
2. Be honest about ourselves. We're all interested in ourselves; we're all on the spectrum of self-absorption. Accepting our narcissism is what allows us to deal with it healthily.
3. Become healthy narcissts.

### Empathy
We're able to be empathetic because we are narcissistic. Narcissism is being absorbed in ourselves. Empathy is being absorbed in others. Additionally, empathy is something that positively reinforces itself—being empathetic allows you to function better socially, which in turn helps develop your empathy more.

There are four components to the empathetic skill set.

#### The Empathic Attitude
Empathy is a state of mind. Believing that you understand others and can quickly judge them shuts down your ability to be empathetic, since you are no longer trying to understand them. Instead, believe that you judge people incorrectly and treat understanding others better as a game. Your ability to be empathic is affected by how well you love and accept yourself.

It's common to assume that others share similar values or traits to you. Don't do this! Instead, view each person as having their own psychology and personality that you'll explore. A good way to practice this is to focus on listening during your conversations, aiming to be able to articulate what the other person has said and even things that they haven't said.

Another part of having an empathetic attitude is treating others as if they're on the same playing field as you. For example, instead of assuming that others make mistakes because they are flawed, consider that they might be acting based on the circumstances they were in. That's how we view our mistakes, so we should view others' mistakes in the same light.

#### Visceral Empathy
This involves reading others in order to understand their feelings, moods, and emotions. We do so by observing how they do things, including their body language and tone. You're trying to figure out the other person's intent, seeing if it's congruent with what they're actually saying or doing.

A key way to understand others in this way is to see how you respond to them. You'll be able to feel their underlying intent by imagining what emotions they're going through. Mirroring others also helps you develop rapport with them.

When observing others, remember that you're not trying to copy them exactly. You'll pick up too much noise and it will be forced & unnatural.

#### Analytic Empathy
You can also have empathy for someone by having a deeper understanding of their background. We tend to bucket people into categories immediately, but instead we want to try to gather information that allows us to better understand the other person.

People are calibrated to interpret the same events in different ways. Something that appears brave to one person might be cowardly to another. Understanding how someone views an event is important to being able to understand what they are feeling.

People's values are often hinted at through various factors:

- The early years of someone's life, when their values are typically established.
- The relationship someone has to their parents and siblings, both past and present.
- How someone reacts to authority figures.
- Their preferences, romantic or otherwise.
- The culture they come from.

If someone isn't opening up, it can be helpful to ask open-ended questions or to share something about yourself first.

#### The Empathic Skill
Developing your skill involves being able to get feedback. You can do so directly by asking what their thoughts are feelings are, then internally comparing it to what you believed. You can also do so indirectly by judging how much rapport you have and how well you feel you understand the other person. Of course, the feedback you get is multiplied by the number of interactions and people that you interact with.

## 3. See Through People's Masks—The Law of Role-playing
People project an appearance that may not always represent how they actually feel or think. We can understand what they're actually feeling by observing their nonverbal communication. Learning to do so involves being able to put yourself in the other person's shoes. You have to be able to understand and feel the underlying causes behind someone's nonverbal actions.

Developing this skill involves observing people more and learning how to interpret what you see. Everyone presents a certain aspect of them, depending on their situation. We want to also play an appropriate role while not mistaking others' appearances for reality. It's important to realize that people's role-playing and mask-wearing is required to function. We're not looking to moralize against that, and we want to wear our own masks as best as we can.

There are three parts to understanding others' motivations: knowing how to observe people, decoding what their actions actually mean, and managing others' impressions of ourselves.

### Observation Skills
As kids, we developed strong observational skills. These deteriorate over time, and we must regain them.

Often we want to look for non-verbal expressions that contradict or enhance what they're saying. Observing people should be done subtly, without them noticing. Aside from normal interactions, this can also be done by people-watching in cafes or parks. There are a number of non-verbal areas to pay attention to:

- Facial expressions, including micro-expressions and forced smiles
- Voice, including pitch and pace
- Body language, including posture, hand gestures, and leg positioning
- Silences
- Clothing choices
- Breathing patterns
- Muscle tension, especially in the neck

Someone's expressions should be judged against their baseline behavior—it's the change in their non-verbal communication that signals what they're thinking or feeling. Additionally, pay attention to how they act under different settings, such as when they're excited or anxious. Cultural background also influences how people express themselves.

It's important to realize that not everyone's non-verbal communication means the same thing. Be careful of misjudging what someone's expressions mean, as this misinterpretation with confirmation bias could lead to you believing they have a motive or feeling that they don't.

### Decoding Keys
There are three main categories of cues to look for: dislike/like, dominance/submission, and deception.

#### Dislike
It's hard for someone to hide their feelings if they dislike you. This will show in their non-verbals, particularly when reacting to something you've done or when caught off guard. These things are often expressed as mixed signals, such as when someone says something positive but has negative body language.

You can help determine if someone has negative body language by comparing their language towards you and towards others. People also tend to react more when stressed, tired, drunk, etc. You can also test people by putting them in situations that would make them uncomfortable if they dislike you.

The earlier you recognize that someone dislikes you, the more room you have to maneuver. You'll have more opportunities and abilities to deal with them, whether that's to treat them as an enemy or to try to win them over.

#### Like
Although we're more open to showing that we like people, we often partially hide it. Signs that someone likes you include relaxed facial muscles, a flushed face, dilated pupils, and raised eyebrows. 

They'll also use genuine smiles, as opposed to fake ones. Genuine smiles are distinguished by how they affect muscles around the eyes and how cheeks get pulled upward. Genuine smiles should also be judged based on the context—are they related to what's going on, is the timing off?

Another important factor is voice. When someone is excited to talk to you, their pitch goes up and their voice is warm and natural.

Nonverbal cues will also show if someone is paying attention to you. Mirroring you is also a good sign.

#### Dominance/Submission
People in a more dominant social position will display it as confidence in their body language. This comes in many ways: being more relaxed, looking more at others, expressing their emotions more openly, owning more space, etc. They typically smile less, since smiling gets interpreted as insecurity.

Sometimes people will try to present themselves as dominant. If they aren't congruent, they will often overexaggerate in some areas, or have some other expression that indicates that they don't feel they way that they're presenting themself.

Dominance can be used negatively by using a negative pattern or symptom, real or imagined, to control others.

Knowing if someone is a congruent leader lets you evaluate how to decide how to act. You can decide how much or how little to attach yourself to someone.

#### Deception
The most common sign of deception is being over-animated or overexaggerated. This happens both with cover-ups, when people are hiding something, and with soft sells, where people are trying to persuade you.

To determine if their expressions are genuine, look for mismatches in their expressions. Usually one part of their face or body will be more expressive, but other parts will be tense or anxious.

People can also try to deceive by having an airtight defense or argument in their favor. If things seem too perfect, be skeptical.

> When you are lying, if you skillfully put in something not quite ordinary, something eccentric, something, you know, that never has happened, or very rarely, it makes the lie sound much more probable. —Dostoyevsky

When you suspect someone of deception, you generally want to encourage them to continue along that line of behavior. At the right moment, you can surprise them with a question or statement that will make them uncomfortable. 

### The Art of Impression Management
We use impressions to "prove" competence. Once people trust us, we are then able to display our more authentic personalities. Sometimes we also have to play a certain role in order to have social success. There are a number of ways to do so.

#### Master the nonverbal cues.
Learn to be likable, have genuine smiles, use welcoming body language, and to mirror people. Know how to project dominance and confidence. Know how to use expressions to convey an emotion, rather than using words.

#### Be a method actor.
This involves training yourself to be able to display proper emotions on command. This is done by recalling previoius experiences. Being able to put yourself in the right emotional mood to match what you're doing.

#### Adapt to your audience.
You may have to play a certain role, but you have room within that role to tailor things to your audience.

#### Create the proper first impression.
Being natural, relaxed, and looking people in the eye is good. Being overly excited usually comes across as insecurity.

#### Use dramatic effects.
This mostly involves knowing how to have the appropriate amount of presence or absence.

#### Project saintly qualities.
There are always positive qualities that society values; project those.
