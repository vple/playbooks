Range: Why Generalists Triumph in a Specialized World
David Epstein
[Amazon](https://www.amazon.com/dp/B07H1ZYWTM)

## Introduction. Roger vs. Tiger
> Deliberate practice, according to the study of thirty violinists that spawned the rule, occurs when learners are "given explicit instructions about the best method," individually supervised by an instructor, supplied with "immediate informative feedback and knowledge of the results of their performance," and "repeatedly perform the same or similar tasks."

In practice, it is very hard to get anyone to do deliberate practice for a field—just look at the above criteria.

First, "explicit instructions about the best method." If this means absolute best, we definitely can't provide that instruction for many things. We don't know the absolute best. In a lot of fields, the "best" thing to do was discovered with decades to centuries of experimentation.

Even when best is relative (the best we know about today), it is still hard for us to do because we may not always have codified those things. It may be instinct or some other thing that we can't properly articulate.

Next, "immediate informative feedback and knowledge of the results of their performance" is hard to provide as well. This is covered later in the book as well. Immediate informative feedback involves knowing what to look for and provide feedback on. This entire piece all depends on _time scale_, as what is good for the short term may not be useful in the long term.

Finally, "repeatedly perform the same or similar tasks" cannot be done in a lot of situations because the tasks are very infrequent or feel very specialized/custom.

> The title of one study of athletes in individual sports proclaimed "Late Specialization" as "the Key to Success"; another, "Making It to the Top in Team Sports: Start Later, Intensify, and Be Determined."

Epstein talks about a "sampling period" that elites go through as they figure out what they want to do. Later on, he also talks about how the experiences from this sampling period help to aid with the field that someone ends up specializing in.

> An internationally renowned scientist (whom you will meet toward the end of this book) told me that increasing specialization has created a "system of parallel trenches" in the quest for innovation. Everyone is digging deeper into their own trench and rarely standing up to look in the next trench over, even though the solution to their problem happens to reside there.


## 1. The Cult of the Head Start
> Chess is very objective and easy to measure.

> The powerful lesson is that anything in the world can be conquered in the same way. It relies on one very important, and very unspoken, assumption: that chess and golf are representative examples of all the activities that matter to you.

> Klein has shown that experts in an array of fields are remarkably similar to chess masters in that they instictively recognize familiar patterns.

> His [Kahneman] findings could hardly have been more different from Klein's. When Kahneman probed the judgments of highly trained experts, he often found that experience had not helped at all.

> Whether or not experience inevitably led to expertise, they agreed, depended entirely on the domain in question.

> The domains Klein studied, in which instinctive pattern recognition worked powerfully, are what psychologist Robin Hogarth termed "kind" learning environments. Patterns repeat over and over, and feedback is extremely accurate and usually very rapid. [...] The learning environment is kind because a learner improves simply by engaging in the activity and trying to do better. Kahneman was focused on the flip side of kind learning environments; Hogarth called them "wicked."

> In wicked domains, the rules of the game are often unclear or incomplete, there may or may not be repetitive patterns and they may not be obvious, and feedback is often delayed, inaccurate, or both. In the most devilishly wicked learning environments, experience will reinforce the exact wrong lessons.

> Through repetitive study of game patterns, they had learned to do what Chase and Simon called "chunking." Rather than struggling to remember the location of every individual pawn, bishop, and rook, the brains of elite players grouped pieces into a smaller number of meaningful chunks based on familiar patterns.

On how elites perform seemingly superhuman (in this case, memory) feats in their domain of expertise.

> When we know the rules and answers, and they don't change over time—chess, gold, playing classical music—an argument can be made for savant-like hyperspecialized practice from day one. But those are poor models of most things humans want to learn. When narrow specialization is combined with an unkind domain, the human tendency to rely on experience of familiar patterns can backfire horribly.

> [...] "rather than obsessively focus[ing] on a narrow topic," creative achievers tend to have broad interests. "This breadth often supports insights that cannot be attributed to domain-specific expertise alone."

## 2. How the Wicked World Was Made
> The farmers and students who had begun to join the modern world were able to practice a kind of thinking called "eduction," to work out guiding principles when given facts or materials, even in the absence of instructions, and even when they had never seen the material before.

> The more they had moved toward modernity, the more powerful their abstract thinking, and the less they had to rely on their concrete experience of the world as a reference point.

> Exposure to the modern world has made us better adapted for complexity, and that has manifested as flexibility, with profound implications for the breadth of our intellectual world.

> "Even the best universities aren't developing critical intelligence," he told me. "They aren't giving students the tools to analyze the modern world, except in their area of specialization. Their education is too narrow." He does not mean this in the simple sense that every computer science major needs an art history class, but rather that _everyone_ needs habits of mind that allow them to dance across disciplines.

Agree with this; most new hires don't know how to think and problem solve.

> They must be taught to think before being taught what to think about.

But how do you do this? Just give a blueprint?

> They were perfectly capable of learning from experience, but failed at learning _without_ experience. And that is what a rapidly changing, wicked world demands—conceptual reasoning skills that can connect new ideas and work across contexts.

## 3. When Less of the Same Is More
> The strict deliberate practice school describes useful training as focused consciously on error correction. But the most comprehensive examination of development in improvisational forms, by Duke University professor Paul Berliner, described the childhoods of professionals as "one of osmosis," not formal instruction.

> While improvising, musicians do pretty much the opposite of consciously identifying errors and stopping to correct them.

I need to learn to design an environment where people can spitball ideas, with the understanding that we'll revisit them later.

> You're just trying to find a solution to problems, and after fifty lifetimes, it starts to come together for you.

## 4. Learning, Fast and Slow
> In every classroom in every country, teachers relied on two main types of questions. The more common were "using procedures" questions: basically practice at something that was just learned. [...] The other common variety was "making connections" questions, which connected students to a broader concept, rather than just a procedure.

> Both types of questions are useful and both were posed by teachers in every classroom in every country studied. But an important difference emerged in what teachers did _after_ they asked a making-connections problem.

> Rather than letting students grapple with some confusion, teachers often responded to their solicitations with hint-giving that morphed a making-connections problem into a using-procedures one. [...] when the students were playing multiple choice with the teacher, "what they're actually doing is seeking rules." They were trying to turn a conceptual problem they didn't understand into a procedural one they could just execute.

Perhaps this means that Dreyfus's model has the right steps, but the wrong order? What if they're parallel tracks and one (experimentation) is more likely to lead to "intuitive" mastery whereas the other leads to rule-defined behavior?

Unrelatedly, I need to find ways to encourage people to be okay with confusion.

> Teachers in every country fell into the same trap at times, but in the higher-performing countries plenty of making-connections problems remained that way as the class struggled to figure them out.

> When a student offered an idea for how to approach a problem, rather than engaging in multiple choice, the teacher had them come to the board and put a magnet with their name on it next to the idea. By the end of class, one problem on a blackboard the size of an entire wall served as a captain's log of the class's collective intellectual voyage, dead ends and all.

> If the teacher didn't already turn the work into using-procedures practice, well-meaning parents will. They aren't comfortable with bewildered kids, and they want understanding to come quickly and easily. But for learning that is both durable (it sticks) and flexible (it can be applied broadly), _fast and easy_ is precisely the problem.

> What you want is to make it easy to make it hard.

> Kornell was explaining the concept of "desirable difficulties," obstacles that make learning more challenging, slower, and more frustrating in the short term, but better in the long term.

> One of those desirable difficulties is known as the "generation effect." Struggling to generate an answer on your own, even a wrong one, enhances subsequent learning.

I can use this by posing a problem and asking people to return to me with ideas on how to approach it at a later time.

> It can even help to be wildly wrong. Metcalfe and colleagues have repeatedly demonstrated a "hypercorrection effect." The more confident a learner is of their wrong answer, the better the information sticks when they subsequently learn the right answer. Tolerating big mistakes can create the best learning opportunities.

> That structure makes intuitive sense, but it forgoes another important desirable difficulty: "spacing," or distributed practice.

> As with the making-connections questions Richland studied, it is difficult to accept that the best learning road is slow, and that doing poorly now is essential for better performance later. It is so deeply counterintuitive that it fools the learners themselves, both about their own progress and their teachers' skill.

> Above all, the most basic message is that teachers and students must avoid interpreting current performance as learning. Good performance on a test during the learning process can indicate mastery, but learners and teachers need to be aware that such performance will often index, instead, fast but fleeting progress.

> [...] "blocked" practice. That is, practicing the same thing repeatedly, each problem employing the same procedure. It leads to excellent immediate performance, but for knowledge to be flexible, it should be learned under varied conditions, an approach called varied or mixed practice, or, to researchers, "interleaving."

> The feeling of learning, it turns out, is based on before-your-eyes progress, while deep learning is not.

> Whether chemists, physicists, or political scientists, the most successful problem solvers spend mental energy figuring out what type of problem they are facing before matching a strategy to it, rather than jumping in with memorized procedures.

## 5. Thinking Outside Experience

> She is probably the world's foremost authority on analogical thinking. Deep analogical thinking is the practice of recognizing conceptual similarities in multiple domains or scenarios that may seem to have little in common on the surface. It is a powerful tool for solving wicked problems, and Kepler was an analogy addict, so Gentner is naturally very fond of him.

> Analogical thinking takes the new and makes it familiar, or takes the familiar and puts it in a new light, and allows humans to reason through problems they have never seen in unfamiliar contexts.

Reasoning questions:

- What is this similar to?
- What everyday analogies can we make?

> It also allows us to understand that which we cannot _see_ at all. Students might learn about the motion of molecules by analogy to billiard-ball collisions; principles of electricity can be understood with analogies to water flow through plumbing. Concepts from biology serve as analogies to inform the cutting edge of artificial intelligence: "neural networks" that learn how to identify images from examples [...] were conceived as akin to the neurons of the brain, and "genetic algorithms" are conceptually based on evolution by natural selection--solutions are tried, evaluated, and the more successful solutions pass on properties to the next round of solutions, ad infinitum.

> The trouble with using no more than a single analogy, particularly one from a very similar situation, is that it does not help battle the natural impulse to employ the "inside view," a term coined by psychologists Daniel Kahneman and Amos Tversky. We take the inside view when we make judgments based narrowly on the details of a particular project that are right in front of us.

> Our natural inclination to take the inside view can be defeated by following analogies to the "outside view." The outside view probes for deep structural similarities to the current problem in different ones. The outside view is deeply counterintuitive because it requires a decision maker to ignore unique surface features of the current project, on which they are the expert, and instead look outside for structurally similar analogies. It requires a mindset switch from narrow to broad.

> Evaluating an array of options _before_ letting intuition reign is a trick for the wicket world.

Reasoning principle:

- Come up with all your options before trying to evaluate them.

> In one of the most cited studies of expert problem solving ever conducted, an interdisciplinary team of scientists came to a pretty simple conclusion: successful problem solvers are more able to determine the deep structure of a problem before they proceed to match a strategy to it. Less successful problem solvers [...] mentally classify problems only be superficial, overtly stated features, like the domain context. For the best performers, they wrote, problem solving "beings with the typing of the problem."

Reasoning questions:

- What is the structure of this problem?

## 6. The Trouble with Too Much Grit

## 7. Flirting with Your Possible Selves
> Instead of asking whether someone is gritty, we should ask _when_ they are.

The implication is that you want to put people in situations where they are naturally motivated to be gritty, as that's what they like and want to stick through.

> Rather than expecting an ironclad a priori answer to "Who do I really want to become?," their work indicated that it is better to be a scientist of yourself, asking smaller questions that can actually be tested--"Which among my various possible selves should I start to explore now? How can I do that?" Be a flirt with your possible selves. Rather than a grand plan, find experiments that can be undertaken quickly. "Test-and-learn," Ibarra told me, "not plan-and-implement."

## 8. The Outsider Advantage
> I realized there was always going to be this somewhat serendipitous outside thinking that was going to make a solution more clever, cost-effective, efficacious, more on the money than anyone else's. And so I went from that idea, how problems are solved, to 'How does one build an organization that solves problems that way?'

Alph Bingham, on the idea behind InnoCentive, a company that provides bounties for specialist-stumping problems.

> Bingham had noticed that established companies tended to approach problems with so-called local search, that is, using specialists from a single domain, and trying solutions that worked before. Meanwhile, his invitation to outsiders worked so well that it was spun off as an entirely separate comapny.

> Bingham calls it "outside-in" thinking: finding solutions in experiences far outside of focused training for the problem itself.

> "I think it happens more often than we'd love to admit, because we tend to view things with all the information we've gathered in our industry, and sometimes that puts us down a path that goes into a wall. It's hard to back up and find another path." Pegau was basically describing the Einstellung effect, a psychology term for the tendency of problem solvers to employ only familiar methods even if better ones are available.

## 9. Lateral Thinking with Withered Technology
> As the company grew, he worried that young engineers would be too concerned about looking stupid to share ideas for novel uses of old technology, so he began intentionally blurting out crazy ideas at meetings to set the tone.

Slightly different intention, but same idea from _The Like Switch_ where he intentionally misspells or mispronounces a word at the start of the semester.

> The higher the domain uncertainty, the more important it was to have a high-breadth team member.

> Their findings about who these people are should sound familiary by now: [...]

Things that make up serial innovators. In list form:

- high tolerance for ambiguity
- systems thinkers
- additional technical knowledge from peripheral domains
- repurposing what is already available
- adept at using analogous domains for finding inputs to the invention process
- ability to connect disparate pieces of information in new ways
- synthesizing information from many different sources
- they appear to flit among ideas
- broad range of interests
- they read more (and more broadly) that other technologists and have a wider range of outside interests
- need to learn significantly across multiple domains
- serial innovators also need to communicat with various individuals with technical expertise outside of their own domain

## 10. Fooled by Expertise

> The [narrow-view] hedgehogs, according to Tetlock, "toil devotedly" within one tradition of their speciality, "and reach for formulaic solutions to ill-defined problems." Outcomes did not matter; they were proven right by both successes and failures, and burrowed further into their ideas. [...] The [integrator] foxes, meanwhile, "draw from an eclectic array of traditions, and accept ambiguity and contradiction," Tetlock wrote. Where hedgehogs represented narrowness, foxes ranged outside a single discipline or theory and embodied breadth.

This also reminds me of Dreyfus. Perhaps the difference between people who are able to move to the intuitive level are people who are able to form connections across ideas.

> A few of the qualities that make the best Good Judgment Project forecasters valuable teammates are obvious from talking to them. They are bright, but so were the hedgehog experts Tetlock started with. They toss around numbers easily, estimating this country's poverty rate or that state's proportion of farmland. And they have range.

> Superforecasters' online interactions are exercises in extermely polite antagonism, disagreeing without being disagreeable. Even on a rare occasion when someone does say, "You're full of beans, that doesn't make sense to me, explain this," Cousins told me, "they don't mind that." Agreement is not what they are after; they are after aggregating perspectives, lots of them.

Values: "polite antagonism", aggregating perspectives, not seeking agreement

> A hallmark of interactions on the best teams is what psychologist Jonathan Baron termed "active open-mindedness." The best forecasters view their own ideas as hypotheses in need of testing. Their aim is not to convince their teammates of their own expertise, but to encourage their teammates to help them falsify their own notions.

> Beneath complexity, hedgehogs tend to see simple, deterministic rules of cause and effect framed by their area of expertise, like repeating patterns on a chessboard. Foxes see complexity in what others mistake for simple cause and effect. They understand that most cause-and-effect relationships are probabilistic, not deterministic.

> In wicked domains that lack automatic feedback, experience alone does not improve performance. Effective habits of mind are more important, and they can be developed.

## 11. Learning to Drop Your Familiar Tools

## 12. Deliberate Amateurs