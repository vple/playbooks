# Insight
Tasha Eurich
[Amazon](https://www.amazon.com/Insight-Surprising-Others-Ourselves-Answers-ebook/dp/B01JWDWP4Y)

### 1. The Meta-Skill of the Twenty-First Century
> Self-awareness is the ability to see ourselves clearly—to understand who we are, how others see us, and how we fit into the world.

Self-awareness is the meta-skill of the twenty-first century. Qualities that are important for success today—emotional intelligence, empathy, influence, persuasion, communication, and collaboration—all come from self-awareness.

Although self-awareness is important, it's rare. It's easier for people to self-delude themselves than to accept the truth. We often accuse others of being unaware, but we rarely ask ourselves if we have the same problem. Most of us think we know ourselves well, but our confidence is often unfounded. Overconfidence also happens when we don't know what we ultimately want, or when we don't understand how we are affecting others around us.

There are two main categories of self-awareness; being truly self-aware means having both types. **Internal self-awareness** is an inward understanding of your values, passions, aspirations, ideal environment, patterns, reactions, and impact on others. Having internal self-awareness enables people to make choices that are consistent with themselves. **External-self-awareness** is knowing how other people see you. Having external self-awareness lets people build stronger and more trusting relationships. There's no proven relationship between internal and external self-awareness.

Gaining self-awareness is a journey, and insights are the aha moments during that journey. Self-awareness unicorns understand that each insight is one of many, and are continually refining themselves to find more insights.

## Roadblocks and Building Blocks
### 2. The Anatomy of Self-Awareness: The Seven Pillars of Insight
Self-aware people have seven distinct types of insights:

- **values**—the principles that guide them
- **passions**—what they love to do
- **aspirations**—what they want to experience and achieve
- **fit**—the environment they require to be happy, energized, and engaged
- **patterns**—consistent ways of thinking, feeling, and behaving
- **reactions**—thoughts, feelings, and behaviors that reveal strengths and weaknesses
- **impact**—the effect they have on others

The key skill to understanding your impact is **perspective-taking**—being able to imagine what others are thinking and feeling.

#### The Importance of External Self-Awareness
Self-awareness is not just about looking inwards. You also need to take into account the outward perspective, how others see you. Self-awareness involves having a clear perspective on ourselves, but also being able to throw away that perspective and see ourselves how others do.

Internal perspective helps for pillars that aren't visible to others: values, passions, aspirations, and fit. External perspective helps for the more visible ones: patterns, reactions, and impact. Still, internal and external perspectives are important for all pillars.

Self-awareness unicorns have an ability to recognize and learn from **alarm clock events**, which are situations that they use to teach themselves self-truths. There are three catgories of alarm clock events.

1. **new roles or new rules**  
	Having new responsibilities leads us to stretch ourselves and brings us out of our comfort zones.
2. **earthquakes**
	These are events that, due to significance and/or severity, are life-shattering. They force us to confront ourselves.
3. **everyday insight**

### 3. Blindspots: The Invisible Inner Roadblocks to Insight
People often believe themselves to be better than average. The people who are most self-deluded are often the last to find out. Without a committed effort, most people only make small gains in improving self-awareness. 

There are three kinds of blindspots:

- **Knowledge Blindness** is where our opinions are based more on our general beliefs about ourselves and our skills, rather than how we actually perform.
- **Emotion Blindness** is where our perceived rational decisions are actually influenced by how we're feeling in the current moment. The danger with emotional blindness is when our emotions affect our decisions without us realizing it.
- **Behavior Blidness** is where we're unable to objectively perceive our own behavior.

To improve our self-awareness, we have to identify our assumptions. A few ways to help do this include:

- Comparing past predictions with actual outcomes.
- Keep learning.
- Seek feedback on abilities and behaviors.

### 4. The Cult of Self: The Sinister Societal Roadblock to Insight
We tend to think we're unique and superior. Initially people valued effort—hard work, grit, and resilience. This then changed to esteem—you don't have to be great, you just have to feel great. However, self-esteem and success aren't related. It's easier to feel good than to become good.

When people are focused on feeling good, they might blind themselves to their shortcomings—the feel good effect. This often sets them up for failure. However, this effect is useful in two scenarios: when we need to bounce back from constant challenges and when we need to succeed through sheer persistence. The feel good effect isn't appropriate in places where failure isn't an option.

Focusing too much on ourselves makes it harder for us to notice things outside of us. It also distorts our self-perception. This happens a lot when people use social media to focus on self-presentation—sharing information about ourselves.

There are three strategies for becoming more self-aware: becoming an informer, cultivating humility, and practicing self-acceptance.

- Being an informer involves sharing non-self-related information on social media. This tends to lead to richer experiences.
- Cultivating humility involves appreciating our weaknesses and keeping our successes in perspective. Humility is hard to find since people can view humility as low-worth and because it involves fighting our own ego.
- Self-acceptance involves understanding the objective reality and choosing to like ourselves anyway. A way to improve this is to watch your inner monologue—focus on being constructive rather than being self-critical.

## Internal Self-Awareness: Myths and Truths
### 5. Thinking Isn't Knowing: The Four Follies of Introspection
Introspection—consciously examining our thoughts, feelings, motives, and behaviors. We tend to believe that introspection can lead to insight. It can, but most introspection is ineffective because it's done incorrectly.

There are a number of introspection follies:

1. Introspection lets us better understand our unconscious. It doesn't.  
  Instead of focusing on the process of introspection, we want to focus on what we can learn and how to move forward. Adopt a flexible mindset—be open to multiple truths and explanations. The role of therapy shouldn't be to find answers, but to explore other perspectives.
2. The causes of our behavior that we discover through inspection may be wrong.  
  We tend to ask *why*, stopping at the first answer we find. Instead of asking why, we should focus on *what*—particularly what we value and want the future to look like. Focusing on what also forces us to name our emotions, which helps with growth.
3. Keeping a journal isn't necessarily effective for self-awareness.  
  Journaling should be focused on exploring negative issues and not overthinking positive ones. It should focus on factual and emotional components. It helps to explore others' perspectives and to not write every day.
4. Rumination—fixation on our fears, shortcomings, and insecurities—is a more harmful version of introspection.  
  Everyone ruminates; the self-aware are good at breaking themselves out of it.  

  - Ask if anyone else cares about this thing as much as you do.
  - Focus on learning something well, rather than on performance.
  - Take a break from whatever you're doing and go do something different.
  - Tell yourself to stop thinking about that topic.
  - Get a reality check; get another perspective.

### 6. Internal Self-Awareness Tools That Really Work
Mindfulness—noticing what we're thinking, feeling, and doing without judgment or reaction. Meditation is a way to mindfulness, but not the only way. Mindfulness isn't the same thing as relaxation. There are a few tools to help do so.

- Reframing—looking at circumstances, behaviors, and relationships from different angles.  
  It's useful to do both when things are going poorly and when they're going well.
- Comparing and contrasting.  
  Observe similarities, differences, and trends throughout your life.
- Reflect on discoveries.  
  Find time to check-in with yourself. Analogous to engineering retros, and you can do these daily.

Life story exercise:

> Think about your life as if it were a book. Divide that book into chapters that represent the key phases of your life. Within those phases, think of 5-10 specific scenes in  your story—high points, low points, turning points, early memories, important childhood events, important adulthood events, or any other event you find self-defining. For each, provide an account that is at least one paragraph long. When you are finished writing your account, take a step back and look at your life story as a whole: What major themes, feelings, or lessons do you see in your story? What does the story of your life say about the kind of person you are and might become—your values, passions, aspirations, fit, patterns, reactions, and impact on others?

Having self-insight isn't enough. We also have to act on it. A tool for this is *solutions-mining*. The **miracle-question**, from *Switch*:

> Imaging that tonight as you sleep a miracle occurs in your life [that] has completely solved this problem....Think for a moment...how is life going to be different now? Describe it in detail. What's the first thing you'll notice as you wake up in the morning?

## External Self-Awareness: Myths and Truths
### 7. The Truth We Rarely Hear: From Mirror to Prism
We also need external self-awareness—understanding how others see us. This is important, as others often see us more objectively than we see ourselves. Even people we don't know well can be a valuable source of feedback. The problem is that other people can be reluctant to give this kind of information. Generally, people prefer telling white lies over hard truths.

Another problem is that people are reluctant to ask for feedback. There are three excuses people make for this:

1. I don't need to ask for feedback.
2. I shouldn't ask for feedback.
3. I don't want to ask for feedback.

One way of getting feedback is 360-degree feedback. You get various people to give anonymous feedback. This helps you see how others' views of us stack up to our own.

Another is the dinner of truth. Invite the person to a meal and ask them to tell you one thing that annoys them the most about you.

#### RIGHT Feedback
Another is getting RIGHT feedback—feedback from the right peoople, from asking the right questions, using the right process.

The right questions to ask are specfic ones. Treat it scientifically, where you're trying to confirm or disprove a hypothesis about yourself. Use examples in your questions. It helps to think about your pillars or previous feedback when coming up with these hypotheses. It also helps to focus on one or two hypotheses at a time.

You want to get feedback from the right people. Avoid unloving critics and uncritical lovers—people who only criticize or who are unable to criticize. Instead, look for loving critics—people who can be critical with your best interests in mind.

There are some additional requirements. There should be mutual trust. The other person should have sufficient exposure to the behavior you want feedback on. They should have a clear picture of what success looks like. They also have to be willing and able to be brutally honest with you.

The right process involves first getting buy-in—make sure your critics actually want to help you and aren't just agreeing out of politeness. Agree on a period of time where they can observe your behavior, then check in periodically to hear their feedback.

### 8. Receiving, Reflecting on, and Responding to Difficult or Surprising Feedback
3R model: receive, reflect, respond.

Receiving feedback mainly involves handling your emotions. Figure out if there's something you can learn by digging more into the feedback.

When reflecting, don't just in right away. Give a few days or even weeks before going into it. When you do, ask if you understand the feedback and how the feedback affects your long-term success and well-being. Decide if you want to act on the feedback, and if so, how?

Then, respond if you have an action you want to take.

We can have self-limiting beliefs. Taking some time to give ourselves self-affirmation about a different area of our lives can help mitigate the "threat" when we have to face feedback that deals with those self-limiting beliefs. Reminiscing is also a powerful mechanism for self-affirmation. Self-affirmation is best when it's done before getting threatening feedback.

Not everything can be changed. Sometimes the appropriate thing is to accept it and learn how to deal with it.

There are lots of different ways we can categorize feedback:

- critical and surprising  
  We can change in response to this feedback, reframe it, or embrace it and be open.
- critical and confirming  
  Use self-affirmation to minimize the impact of feedback that reinforces an insecurity or vulnerability.
- positive and surprising  
  Acknowledge it and further invest in these newly discovered strength.
- positive and confirming  
  This gives confidence to keep going along our path.

## The Bigger Picture
### 9. How Leaders Build Self-Aware Teams and Organizations
Create awareness—make sure everyone knows the plan, the status of the plan, and the challenges being faced. Self-aware teams have an understanding of who they are and how others see them. They understand their objectives, progress, processes, assumptions, and individual contributions.

#### Building Blocks
There are three building blocks that need to be in place for a leader to drive self-aware teams. There needs to be a leader that models the way. There needs to be psychological safety. There also needs to be ongoing process to ensure that feedback is being created. Teams need a direction—if there's no direction, they can't get anywhere.

##### Leaders That Model The Way
Self-aware leaders should be authentic and show that it's both okay and expected to honestly reflect on how things are going. This involves confronting flaws and striving to improve. Make a total commitment to the team's self-awareness. Know and communicate the values and behaviors you expect from yourself and the team. Listen to what your team has to say.

Alan Mulally's principles and practices:

- People first
- Everyone is included
- Compelling vision, comprehensive strategy and relentless implementation
- Clear performance goals
- One plan
- Facts and Data
- Everyone knows the plan, the status and areas that need special attention
- Propose a plan, positive, “find a way” attitude
- Respect, listen, help, and appreciate each other
- Emotional resilience—trust the process
- Have fun—enjoy the journey and each other

The *leader feedback process* is a way to help ensure that leaders get feedback properly. Kick the leader out, and have someone lead a meeting where they get the team to collectively answer several questions, with as many ideas as they can:

1. What do we know about X?
2. What do we want to know about X?
3. What should X know about us as a team?
4. What concerns do we have about X?
5. What expectations do we have of X?
6. What do we want X to stop doing, start doing, and continue doing?
7. What feedback do we have about our vision, our strategy, and our plan?

##### Safety And Expectation To Tell The Truth
Psychological safety—the shared belief that it's safe to ask one another for help, admit mistakes, and raise tough issues. This is cultivate by both building trust and showing vulnerability. It also helps to reate clear norms—these are the rules of engagement as to how the team will behave.

##### Ongoing Commitment And Process To Staying Self-Aware
Peer pirates—a peer that feedback and complaints are shared with, who then takes this to the leader. It helps to have multiple peer pirates, as they're more motivated to provide useful feedback.

*Team feedback exchanges* are also useful. Each team member gets a chance to give their peers feedback. They should be led by someone trusted and socially savvy. This person shouldn't be the person's most senior nor most junior member. It might be useful to hire a skilled facilitator.

Beforehand, have teams think about their teammates' contributions. Outline the process:

- prepare feedback
- deliver question 1 feedback (30s per question)
- deliver question 2 & 3 feedback (30s per question)
- questions for clarification

Each person will have a chance to give feedback to each other person at the table by answering three questions. Each person will also have the chance to ask for clarification on that feedback. This exercise goes in rounds, with breaks in between.

The three questions:

1. What does this person do that most contributes to our success?
2. If this person could change one behavior to be more successful, what would it be?
3. What behavior do I need from this person to help me be more successful?

Feedback should be based on specific behaviors, not generalities.

Ground rules for receiving feedback:

1. No pushback or defensiveness.
2. Take notes and ask questions only for clarification.
3. Be open-minded, assume good intentions.
4. Thank team members.

Ground rules for giving feedback:

1. Avoid generalities ("you always", "you never").
2. Focus on behavior, not on the person.
3. Don't give interpretations, just behavior.
4. Provide examples.

Finally, go around and have each person make a single commitment based on the feedback they heard. Agree on *accountability conversations*—periodically have discussions where people provide updates on what they're doing to progress on their commitment. People should still be pointing out behavior that supports or contradicts commitments in real time.

#### Self-Aware Organizations
Organizational self-awareness involves confronting market realities by seeking feedback from all stakeholders and keeping those stakeholders informed by how the company is adapting to meet needs. The issue is often that organizations can't or won't accept the information they have.

### 10. Surviving and Thriving in a Delusional World
Challenging a delusional person is risky at best and disastrous at worst.

Some people are **lost causes**—they unshakingly believe in their delusion. It helps to deal with them by having compassion without judgment. It also helps to just put up with they are doing to get out of interactions with them as soon as possible, rather than engaging them. You can also reframe, looking at their more positive characteristics.

Other people are **aware, but don't care**. It's helpful to keep things in perspective, realizing that we can manage our reactions. You can also state your needs and establish clear boundaries.

**Nudgeables** want to be better, but don't know that they need to change their approach.

To decide if you should try to help someone, ask:

- Do the benefits of having this conversation outweigh the potential risks?
- Do they know there's a problem?
- Is their behavior counter to their best interests?
- Do I think they will listen to me?

#### 7-Day Insight Challenge

##### Day 1: Select Self-Awareness Spheres
List the three most important areas of your life, e.g. work, school, marriage, friends, etc. For each one, write a few sentences about what success looks like (using the miracle question). Then, rate how satisfied you are from 1 to 10. 

Your opportunities for self-awareness are the areas where you're not as satisfied as you want to be. Pick one or two areas where you want to improve. Think about what's keeping you from success.

##### Day 2: Study The Seven Pillars
Go through the seven pillars of insight. For each one, describe how you see yourself. Have a friend, family, or colleague share how they think about you. Reflect on similarities and differences between your two answers.

##### Day 3: Explore Your Barriers
Figure out what barriers to self-awareness are in play in your life. Spend 24 hours trying to spot them in real time.

##### Day 4: Boost Your Internal Self-Awareness
Pick one of these tools to experiment with. At the end of the day, reflect on how it went and what you learned.

- what not why
- comparing and contrasting
- reframing
- hitting pause
- thought-stopping
- reality checks
- solutions mining

##### Day 5: Boost Your External Self-Awareness
Find a loving critic within each area you're improving. Ask them to share something they value or appreciate about you, as well as one thing that they feel is a weakness for you. Use the 3R model as you receive this feedback.

##### Day 6: Survive The Delusional
Think about the most delusional person you know. Categorize what kind of delusional person they are, then practice a tool below to better manage your relationship.

- compassion without judgment
- float feet-first
- reframing
- what can they teach me?
- laugh track
- state your needs
- clarify boundaries
- walk away
- confront with compassion

##### Day 7: Take Stock
Review your notes over the week and answer these questions:

- What do you now know about yourself and your self-awareness that you didn't know a week ago?
- What one goal can you set for yourself over the next month to help you continue the momentum you have now?