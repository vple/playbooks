# The Art of Seduction
Robert Greene

# Part One: The Seductive Character
## The Siren
Sirens specialize in mythicality and fantasy. They evoke sex, confidence, pleasure, and a hint of danger. They offer adventure and risk. Sirens are most appealing to people who are rigid or lacking in physical pleasure.

As sirens need to stand out, they must be unique. You need to be different from your peers. While any trait can work, physical traits work the most easily as sirens are often a sight to see.

Once you're differentiated you need two other qualities: you need to get people to pursue you in a way that they lose control and you need to seem a bit dangerous. To get people to pursue you, have a sexual presence that is slightly elusive and distant (rather than cheap). It's as if you're a fantasy come to life--be larger than life. Danger adds emotional spice, and is brought about by presenting things that get people to step off a straight and narrow path in their life.

Physical qualities are extremely important.

- 	voice  
	A siren's voice needs to be subliminally erotic. Speak calmly and unhurriedly, rather than quickly, aggressively, or at a high pitch.
-	body and adornment  
	While the voice lulls, the body dazzles. You want to astonish and bewitch. Make the adornments harmonious; don't let one thing overpower another.
-	movement and demeanor  
	Move gracefully and unhurriedly. Hint at something exciting without being obvious. Gestures should be ambiguous, being both innocent and erotic.

Dangers to the siren include:

-	needing to maintain their image over time
-	jealousy from others
-	wanting to escape from the attention they attract

## The Rake

## The Ideal Lover

## The Dandy

## The Natural

## The Coquette

## The Charmer

## The Charismatic

## The Star

## The Anti-Seducer

## The Seducer's Victims--The Eighteen Types