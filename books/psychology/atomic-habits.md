# Atomic Habits
James Clear  
[Amazon](https://www.amazon.com/Atomic-Habits-Proven-Build-Break-ebook/dp/B07D23CFGR/)

## The Fundamentals: Why Tiny Changes Make a Big Difference
### 1. The Surprising Power of Atomic Habits
We tend to underestimate making small, incremental improvements on a daily basis. Instead, we think we need to make a big change to have lots of success. Improving 1% isn't noticeable, but is often more meaningful in the long run. This is why people often fail—they make a few small changes, see no change, then decide to stop.

It doesn't matter how successful or unsuccessful you are now. It matters whether your habits are putting you on the path to success. Outcomes are a lagging measure of your habits.

Instead of focusing on goals, focus on systems and process. Goals are about results; systems are about processes that lead to those results. You set goals to win a game. You build systems to keep playing the game.

When you have trouble changing habits, the problem isn't you. It's your system.

### 2. How Your Habits Shape Your Identity (and Vice Versa)
Changing habits is hard because (1) we try to change the wrong thing and (2) we try to change our habits in the wrong way. 

Change occurs at three levels: outcomes, process, and identity. People typically try to change by focusing on *what* they want to achieve (outcomes). The alternative is to focus on *who* we want to become (identity). When we set goals without changing our inner beliefs about who we are, we sabotage our plans for success.

You have the most motivation when a habit is part of your identity. If you take pride in a part of your identity, you will try to reinforce it. We'll try to protect our perception of our identities. So, we want to align our identities with who we want to be.

Habits are how you embody your identity. The more you repeat a behavior, the more you reinforce the identity associated with that behavior. As you continue to do a behavior, you gain more proof that you have a certain identity. The most practical way to change who you are is to change what you do.

To change your identity, first decide who you want to be. Work backwards to figure out what kind of person would be able to achieve those results.

> The true question is: "Are you becoming the type of person you want to become?"

Habits aren't about *having* something. They're about *becoming* someone.

### 3. How to Build Better Habits in 4 Simple Steps
> Behaviors followed by satisfying consequences tend to be repeated and those that produce unpleasant consequences are less likely to be repeated.

Habits are reliable solutions to recurring problems in our environment. As we rely on a habit, the amount of activity in our brain decreases when we go through that activity. Habits are useful since they free up our conscious mind. Good habits create freedom, as we don't have to spend as much effort to do good things.

Building a habit consists of four steps: cue, craving, response, and reward.

- The **cue** triggers your brain to initiate a behavior.
- **Cravings** are the motivational force behind every habit.  
  Cravings differ from person to person, and are meaningless until they are interpreted. What one person views as a craving may not be interesting at all to another person.
- The **response** is the actual habit you perform.
- The response delivers a **reward**, the end goal of every habit.  
  Rewards serve to satisfy cravings. They also teach us what actions are worth remembering for the future.

The problem phase consists of the cue and craving steps. The solution phase consists of the response and reward steps.

|          | How to Create a Good Habit | How to Break a Bad Habit |
|----------|----------------------------|--------------------------|
| Cue      | Make it obvious.           | Make it invisible.       |
| Craving  | Make it attractive.        | Make it unattractive.    |
| Response | Make it easy.              | Make it difficult.       |
| Reward   | Make it satisfying.        | Make it unsatisfying.    |

## The 1st Law: Make It Obvious
### 4. The Man Who Didn't Look Right
Our habits are triggered by cues, even when we don't consciously notice the cue. To better understand our habits, we should best try to understand the cues that trigger them. We can do this by explicitly calling out the cues and signs that we pay attention to.

The Habits Scorecard is a way to be more aware of our behavior. Create a list of daily habits and mark them with a +, -, or = to indicate what kind of habit it is. Your marks will be based on your situation and goals. Being aware of our bad habits is the first step to being able to change them.

### 5. The Best Way to Start a New Habit
Make it clear how you'll act to accomplish your habit. Create an *implementation intention*—a plan you make beforehand that specifies when and where to act. Be able to say, "When situation X arises, I will perform response Y." It's not motivation that people lack, it's clarity.

You can also rely on *habit stacking*. Identify a habit you already do and use that as a trigger for the habit you want to build. You can also insert new habits in the middle of your current routines. Habit stacking works best when the cue is specific and immediately actionable.

Select appropriate cues to kick off your habits. You won't be effective with a habit if you're preoccupied with something else.

### 6. Motivation Is Overrated; Environment Often Matters More
People usually don't act because of *what* they are, but *where* they are. If you modify the environment, you can naturally bring about desired behaviors. Lots of our behavior isn't done because of a purposeful drive or choice—it's done because it's easy. Make things easy for people.

Create obvious visual cues. Subtle cues are easy to ignore.

Modifying our environment is powerful not just because of how it influences us. It's also powerful because we rarely do it. Our behavior is ultimately affected with the entire environment and context surrounding the behavior. You can train yourself to link a context with a habit. Try to avoid mixing contexts, because this will result in mixing habits.

### 7. The Secret to Self-Control
Addictions can change with radical change in the environment. Disciplined people don't have more self-control; they structure their lives so they don't need that self-control. Keep track of your cues and reduce exposure to cues that trigger habits that you want to stop. Make bad cues invisible.

## The 2nd Law: Make It Attractive
### 8. How to Make a Habit Irresistable
> The more attractive an opportunity is, the more likely it is to become habit-forming.

To make things attractive, we first understand cravings. Dopamine creates cravings. We release dopamine when we experience pleasure and when we anticipate pleasure. Anticipating rewards, not receiving the reward, is what leads us to take action.

We can take advantage of this with *temptation bundling*. Link an action you want to do with an action you need to do. More probable behaviors then help reinforce less probable behaviors.

Temptation bundling can be combined with habit stacking. Create a sandwich—a current habit, the habit you need, then the habit you want.

### 9. The Role of Family and Friends in Shaping Your Habits
We are influenced by the people around us. In particular, we're influenced by the close, the many, and the powerful.

Proximity has a powerful effect. One of the most effective ways to build better habits is to join a culture where desired behavior is the normal behavior. Even better is to join a group where you already have something in common with the group.

We're influenced by majority opinion—when we don't know what to do, we look at how others act. Change is unattractive when it means challenging the tribe. Change is attractive if it means fitting in with the tribe.

We try to copy powerful people because we want success ourselves.

### 10. How to Find and Fix the Causes of Your Bad Habits
Cravings are specific manifestations of meeting an underlying motive. There are lots of ways to address the motive. We just need to use a different solution.

Every time we perceive a cue, we predict what to do next. These predictions lead to feelings. We have cravings when these feelings indicate that something is missing. These cravings then push us to act.

Make hard habits more attractive by associating them with positive experiences.

- Reframe them to highlight benefits rather than drawbacks.
- Use a *motivation ritual*—associate habits with something you enjoy doing.

## The 3rd Law: Make It Easy
### 11. Walk Slowly, but Never Backward
There's a difference between being *in motion* and *taking action*. In motion, you plan, strategize, and learn. These are good, but they don't produce results. Action is behavior that delivers outcomes.

We need to take action to produce results. We don't because being in motion allows us to delay failure.

Start with repetition, not perfection. The habit doesn't need to be perfect, just start practicing it. Repeating an activity adjusts the brain to become more efficient at it. Eventually you get to *automaticity*, where you can perform a behavior without thinking through each step.

### 12. The Law of Least Effort
Conventionally, motivation is what's needed to change. But actually, our motivation is to be lazy. This is a smart strategy, not a dumb one. The law of least effort—when deciding between two similar options, people will pick the one requiring the least amount of work.

Making habits easy involves removing the friction that makes them hard to do. One of the most effective ways to do this is to design your environment. Removing friction enables us to do more.

Organizing a space for its intended purpose primes it to make the next action easy. This can be inverted—prime environments to make bad behaviors difficult. When approaching behavior change, first ask "How can we design a world where it's easy to do what's right?"

### 13. How to Stop Procrastinating by Using the Two-Minute Rule
Habits are the start of our behaviors—we get started with one thing, then keep going on autopilot. We influence where we end up with our *decisive moments*—the decisions we take that set up options available to our future selves. Earlier decisive options affect what we're able to do later on.

Don't start too big when making habits. When starting a new habit, pick something that takes two minutes to do. It should be easy, not a challenge. The following activity might be challenging, but the first two habits should be easy. The point of this is to start establishing your habit.

### 14. How to Make Good Habits Inevitable and Bad Habits Impossible
To get rid of bad habits, make them impossible. Use a *commitment device*—something you do now to control your actions in the future. Change the task so that it's harder to get out of the good habit than it is to get started on it. Make bad habits impractical by increasing the friction.

Automating good habits and eliminating bad habits can be done with technology. Use technology to make good actions easy and bad actions hard.

## The 4th Law: Make It Satisfying
### 15. The Cardinal Rule of Behavior Change
We're more likely to repeat a behavior if the experience is satisfying.

The problem is that we're wired to live in *immediate-return environments*, where our immediate feedback accurately tells us how our actions were. We actually live in *delayed-return environments*, where intended payoffs can take years to materialize. Our brains are *time inconsistent*—they don't evaluate rewards consistently. More immediate rewards are perceived as more valuable. As a result, immediately rewarded actions are repeated and immediately punished actions are avoided.

To help stay on track, have immediate rewards. These should be received at the end of a behavior, as that is the part we remember the most. Use reinforcement learning, make the end of the behavior satisfying.

This can also be used for *habits of avoidance*, where you are trying to avoid a certain behavior. Make the avoidance visible. Whenever you successfully avoid something, reward yourself correspondingly.

### 16. How to Stick with Good Habits Every Day
Make progress by making it visual, such as by the Paper Clip Strategy. In other words, use some form of habit tracker.

Habit trackers make it obvious when you complete something, which can also help you to initiate your next habit. Even just tracking a habit can help reinforce it. Habit tracking also keeps you honest. It motivates you by showing you your progress and can be its own reward.

Measurement should be automated when possible. Manual tracking should be limited to your most important habits.

At some point, you'll miss a habit. This is part of life; just don't miss twice in a row. It's okay if you don't do somethign perfectly, as long as you continue to do something.

Remember that habit tracking has an ultimate goal. Obsessing over a number too much won't help. You can mitigate this a bit by focusing on more qualitative metrics.

### 17. How an Accountability Partner Can Change Everything
Add costs or immediate satisfaction to things. This is best done by ensuring the punishment comes quickly.

This can be done with a *habit contract*—an agreement where you state your commitment to a habit and the punishment that occurs if you don't follow through.

## Advanced Tactics: How to Go from Being Merely Good to Truly Great
### 18. The Truth About Talent (When Genes Matter and When They Don't)
To maximize odds of success, choose the right field. Your genes affect your abilities—they affect the odds that you'll do well in an area. The things you like doing are probably where your genes are influencing you. Choose habits that suit you rather than habits that are popular.

To pick the right habits, pick habits that are easy. You want to make sure it's a habit that will continue to be easy in the long run. You can determine this via explore/exploit. Explore activities, then focus on the best one you've found. Explore occasionally after. The ratio depends on if you're winning or losing.

These questions can help you identify things that are satisfying to you:

- What feels like fun to me, but work to others?
- What makes me lose track of time?
- Where do I get greater returns than the average person?
- What comes naturally to me?

You can combine fields that you're good at to find a field that you can be the best at.

You still need to work hard. Genes only tell you what to work hard at.

### 19. The Goldilocks Rule: How to Stay Motivated in Life and Work
People have peak motivation when things are not too hard and not too easy. Just right. (Same as *Flow*.)

At some point, your ability to succeed and become the best depends on if you can handle the boredom of training every day. The most addictive habits are formed via variable rewards, but no habit will be interesting forever. To continue to improve, you have to enjoy the boredom of continuing with that activity.

### 20. The Downside of Creating Good Habits
When you get good enough, you go on autopilot. Over time, things dip. To counter this, combine automatic habits with deliberate practice. This is done with a system for reflection and review.

Improvements isn't just about learning habits. It's also about fine-tuning them. Reflect and review what's important to you and how well you're moving towards those things.

Your identity can also form pride that prevents you from growing. Counteract this by defining your identity in terms of the important aspects that you want to keep even if other parts of you change. These are typically your principles and talents, as opposed to your results or roles.