# The Like Switch: An Ex-FBI Agent's Guide to Influencing, Attracting, and Winning People Over
Jack Schafer
[Amazon](https://www.amazon.com/dp/B00IWTWO8C)

## 1. The Friendship Formula
The friendship formula has four basic parts: proximity, frequency, duration, and intensity.

**Proximity** is the distance between you and another individual and your exposure to that individual over time. Being around people increases friendship. Proximity has to take place in a nonthreatening environment.

**Frequency** refers to how often you have contact with someone. **Duration** is how long you spend with someone. Interacting with someone more often and for longer increases friendship. However, the two are often inversely related--you spend less time with someone you see often, and vice versa.

**Intensity** is how strongly you satisfy another person's psychological and/or physical needs, via your verbal and nonverbal behavior. There are lots of different kinds of intensity (e.g. curiosity), covered more later.

> To effectively use the Friendship Formula, you have to keep in mind what kind of relationship you are looking to establish and the time you will be required to spend with your person of interest.

## 2. Getting Noticed Before a Word is Spoken
> When you meet people, especially for the first time, ensure that you send the right nonverbal cues that allow others to see you in a positive rather than neutral or negative light.

There are lots of friend signals, but three of them are the most critical: the eyebrow flash, the head tilt, and the real smile. [Video](https://www.youtube.com/watch?v=45Y9lz2-9ZQ).

### Eyebrow Flash
A quick movement where the eyebrows move up and down. They are quick, lasting for about 1/6 of a second. Often not noticed because it's a very unconscious behavior. Eyebrow flashes to a stranger or someone you're not familiar with involve _brief_ eye contact.

Eyebrow flashes often happen when friendly people approach one another. Often happens around 5-6 away.

Eyebrow flashes can be sent long distance, such as in a crowded room. Sending an eyebrow flash and receiving one back often indicates interest. Not receiving an eyebrow flash back doesn't necessarily mean that you can't meet someone, but you should probably look for some other kind of positive signal.

An eyebrow flash with prolonged eye contact indicates intense emotion. With a friend this could be love, but a foe this could be hostility. Unnatural eyebrow flashes, where the eyebrow stays up too long, is also seen as a foe signal.

### Head Tilt
A head tilt is tilting your head to the left or the right. The idea is that it exposes your carotid artery, meaning that you don't perceive the other person as a threat. It's especially important for men to remember to tilt their head in social situations, as they're often used to keeping their head upright in a business context.

Head tilts are strong friend signals. Someone tilting their head is perceived as more trustworthy and attractive. They're also seen as more friendly, kind, and honest.

### Smile
A real smile is a powerful friend signal. Someone smiling is often seen as more attractive, likable, and less dominant. It shows confidence, happiness, enthusiasm, and indicates acceptance. By smiling, you're projecting friendliness.

Generally, people smile at people they like. They won't smile at people they don't like. Smiling is often reciprocated; when you smile at someone else, they'll often smile back. As smiling releases endorphins, this also causes them to feel good about themselves.

There is a difference between genuine smiles and fake smiles. Genuine smiles are used when we want to interact with someone, often someone we already know and like. Fake smiles are used when social or work obligations force us to appear friendly. Smiles should be genuine to be interpreted as friend signals.

Genuine smiles:

-	have uturned corners of the mouth
-	cheeks move upward
-	have wrinkles (crow's feet) around the edges of the eyes
-	bagged skin forms under eyes
-	for some, nose dips downward

Forced smiles:

-	tend to be lopsided, favoring the person's dominant hand
-	lack synchrony; they start later than a real smile and taper off irregularly
-	cheeks often move outward but not upward
-	usually don't have wrinkles around the eyes

### Eye Contact
Eye contact works _with_ other friend signals. It can be done from a distance.

An eye contact friend signal involves establishing eye contact for no more than a second. Longer than that comes across as aggression--a foe signal. End the eye contact with a smile. Smiling back, as well as looking down and then reestablishing eye contact, are good signals.

When two people like each other, they can make eye contact for longer. To do so with a stranger, hold contact for a second and then slowly turn your head, continuing to hold eye contact for a second or two. Your head turning away feels like breaking eye contact.

### Pupil Dilation
Dilated pupils indicate interest. However, they're hard to spot in everyday interactions, making them harder to use as a friend signal. Dilation also occurs when lighting changes, and that has no relation to interest.

### Touch
Touch can express lots of different messages, including: agreement, affection, attraction, guiding, congratulating, etc. It's powerful, subtle, and complex. Even brief touches can have a large influence. 

However, touches, can also produce negative reactions where the person being touched draws away or becomes anxious. These usually indicate dislike or distrust, but also occur in very shy and reserved people.

Light, brief touches on the arm during social encounters often have immediate and lasting positive effects. 

Touches on the hand (except handshakes) are more personal than arm touches, and can be used to gauge romantic interest. If someone pulls away from a hand touch you might not be rejected, but you'll have to build more rapport. People will react unconsciously to even an "accidental" touch or brush against their hand, so you can use this to gauge how receptive they are to being touched.

### Isopraxism (Mirroring)
Mirroring helps to create a favorable impression in the person being mirrored. Sometimes it's impractical to mirror someone exactly; you can cross match where you match an action but perhaps with a different part of the body.

Mirroring is considered normal and won't be consciously noticed. Lack of mirroring is considered a foe signal.

### Inward Lean
People often lean towards people they like and lean away from people they don't like. Similarly, they will tilt their heads slightly backward to create distance between them and someone else. They may also reposition their feet away from someone unwanted.

The signals can be used in business settings to look for who agrees with you, is neutral, or disagrees with you. This allows you to focus your efforts on winning over the people who have yet to agree with you.

### Whispering
Whispering indicates intimacy and is a positive signal. You need a close relationship before you can whisper to someone.

### Food Forking
Food forking, where someone takes your food, is a friend signal when you have a close relationship.

### Expressive Gestures
Gestures vary across and within cultures, from the amount to the intensity of the gestures. Still, people who are friendly are often more expressive. These gestures signal interest and keep attention focused on the speaker.

### Head Nods
A head not indicates that we're engaged with a speaker and that they should keep going. A double nod indicates to go faster. 

Lots of nods are often disruptive, leading to rushed responses. They usually show that the listener wants to speak or isn't interested. You want to avoid this.

### Verbal Nudges
A verbal nudge reinforces head nods. They usually consist of confirmations like "I see" or "go on," or fillers like "umm" or "uh-huh."

There's a lot more stuff in this chapter, but I'm too lazy to copy it all.

## 3. The Golden Rule of Friendship
> The Golden Rule of Friendship--If you want people to like you, make them feel good about themselves.

The biggest obstacle to making others feel good about themselves is our own ego. Our egos lead us to focus on ourselves, rather than the other person.

### Empathetic Statements
Empathetic statements keep the conversation focused on the person you're talking to. They help the other person feel good about themselves, and are one of the most effective ways to do so. They can also be used as conversation fillers when you're struggling for something to say.

To make an empathetic statement, you need to listen to the other person. Then, make a statement about what the other person might be experiencing or feeling. Basic empathetic statements start with "So you..."; you can drop that part as you get familiar with them. 

Avoid repeating what the other person said word for word, as this will trigger a defensive reaction. Also avoid statements of the form "I understand how you feel," as this causes the other person to believe that you don't actually understand them. Instead, focus on the "you."

### Flattery & Compliments
A compliment is intended to praise someone else and acknowledge things that they've achieved. Compliments become more important to help people bond as their relationship develops. When you compliment someone, you're signaling that you're still interested in the other person and what they're doing well.

Flattery has a more negative connotation. Compliments that are or seem insincere are often viewed as flattery, particularly when they're used to exploit or manipulate someone else for a selfish reason. A problem with complimenting people you don't know well is that you can't be sincere (because you don't know them well). You'll come across as trying to flatter the person.

There's a very good way to compliment people that avoids the pitfall of complimenting someone you don't know well. You want the other person to compliment themself, as sincerity isn't called into question here. You do this by providing an _opportunity_ for someone to compliment themselves.

This is done by saying something that leads people to recognize their attributes or accomplishments. They're able to give themselves a pat on the back. These typically take the form of statements implying a certain character or quality based on the recipient's personality or accomplishments. They're essentially implied compliments.

> Ben: Then you've been really busy lately.
> Vicki: Yeah, I worked sixty hours a week for the last three weeks getting a project done.
> Ben: It takes a lot of dedication and determination to commit to a project of that magnitude.
> Vicki: I sacrificed a lot to get that mega project done and I did a very good job, if I may say so myself.

### Third-Party Compliments
A third-party compliment allows you to give a compliment via someone else. This means that you don't have to deliver the compliment yourself, which risks sounding insincere.

To give a third-party compliment, you need to find a mutual friend of acquaintance. This person should be someone that you're confident will pass along your compliment. Then, tell them something positive about the person you want to compliment.

This can be used effectively at work if you know who the gossips are. Gossips value spreading information to the appropriate people. A third-party compliment through them will often make it to your intended recipient.

### Primacy Effect
You can "prime" someone's perception of reality. Just give an opinion about someone or something before a person gets to meet or experience that thing for themselves. This will cause them to be unintentionally prejudiced.

### Ben Franklin Effect
Asking others for a favor will cause them to like you more. Schafer says this is because a person feels good about themself when they do someone else a favor.

Charlie Munger has also recognized this, but he attributes it to consistency--you must surely like someone that you're doing a favor for, otherwise you wouldn't do them a favor.