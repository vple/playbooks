# Multipliers
Liz Wiseman
[Amazon](https://www.amazon.com/Multipliers-Revised-Updated-Leaders-Everyone-ebook/dp/B01KT18416)

## The Multiplier Effect
Multipliers are genius *makers*. They amplify and grow the people around them. Diminishers stifle others, diluting their intelligence and capability.

Multipliers have five disciplines:

1. Attract and optimize talent.  
	Multipliers are talent magnets.
2. Liberators.  
	They create intensity that requires best thinking. They create environments that are comfortable and intense.
3. Challengers.  
	Multipliers lay down challenges to push people beyond what they know.
4. Debate Makers.  
	They make decisions that ready others to make decisions.
5. Investors.  
	They provide necessary resources for success, holding people accountable to their commitments.

There are some other findings as well:

- A hard edge.  
	Multipliers expect great things from their people and drive them to extraordinary results. They're tough and exacting. They make people feel smart & capable, but aren't "feel-good" managers. People appreciate them for the satisfaction of working with them not the actual relationship.
- Great sense of humor.  
	Multipliers don't take themselves or situations too seriously. They often have a self-deprecating wit and can put others at ease.

## The Talent Magnet
Talent magnets recognize talented people, draw them in, and use them at their fullest. They grow their people more than any other manager. Talent magnets have four active practices:

1. They look for talent everywhere.
2. They find people's native genius.
3. They utilize people at their fullest.
4. They remove blockers.

Finding people's native genius involves observing where they light up, figuring out what they do better than anyone around them. Things they do effortlessly, without being asked or paid. Once you find it, you need to label it for them. This helps people identify with their talent and push harder to show this trait. You've gotten it right when they're surprised that others can't do what they do.

### Genius Watching
1. Identify it.  
	Watch what people do easily & freely. Ask 3 whys.
2. Test your hypothesis.  
	Ask peers and the person themself.
3. Work that talent.

## The Liberator
Learn to be direct without being destructive. Distinguish between hard opinions and soft opinions. They disarm people because they act as their peers. Stay calm and help your team feel that things are okay; don't create whiplash.

Liberators focus on intensity. They require concentration, diligence, and energy. People are encouraged to think for themselves and they have a deep obligation to do their best work. They create space for people to step up.

Tyrants create tense environments, where people try to not stand out. They don't give people control over their performance. Other people shrink and hold back.

Liberators create good environments. Key traits:

- Ideas are generated easily.
- People learn rapidly and adapt to new environments.
- People work collaboratively.
- Complex problems get solved.
- Difficult tasks get accomplished.

There are a few different ways to build up these environments.

- Create a learning machine.  
	Enable people to voice their ideas. But hold a high bar for what you need to do to voice your opinion. Give quick feedback, often in small doses, even if it's direct and harsh.
- Set a standard for demanding the best work. Know what people are responsible for, but don't do it for them.
- Be a master teacher.
	Make exepectations clear—you're there to work hard, think, and learn.

### Create Space
You want to create comfort and pressure. You give space, they give their best work. Listen a lot, getting others to talk. Be consistent, this makes things predictable for others and creates safety.

### Demand People's Best Work
Ask people if they're giving their best. Distinguish between someone's best work and the actual outcomes of that work.

### Generate Rapid Learning Cycles
Let people make mistakes, but expect them to learn quickly from them. Focus on learning from mistakes. Create learning cycles.

Diminishers insist on their own ideas and have passive indifference to others' ideas and work. They create anxiety by being temperamental. They make others feel judged. Diminishers believe pressure increases performance. Liberators believe best thinking must be given, not taken.

To become a liberator:

1. Limit your talk time.
2. Label your opinions—let people know about your soft vs. hard opinions.
3. Make your mistakes known. Be personal, be public.

## The Challenger
Diminishers are know-it-alls, they think their job is to know the most and to tell others what to do. They might ask "gotcha" or stalling questions to show off their own knowledge. This limits others to only being able to do what the diminisher knows that they themselves can do.

Challengers find the right opportunities for their organizations, even if they themselves don't know it. They focus on big questions and showing that a solution is possible, not on having all of the answers.

Challengers engage people by:

1. Seeding the Opportunity  
	This might be done by showing someone the need for something. Then the leader can get out of the way and let others solve the problem. You can challenge people's assumptions. Reframe problems as opportunities. Provide a starting point so that people know they're on solid ground, but let them define the opportunity themselves.
2. Lay Down A Challenge
	Set down a challenge that intrigues people. Set a high bar, with a clear and concrete challenge. Ask the important hard questions. But don't fill in the blanks. Help them think as they make progress.
3. Generate Belief
	Establish an improbable but possible milestone. By getting people to a proof of concept, they start to believe that the larger goal can be met. Lay out a path, build the plan collectively. Start with small, early wins and use them to build momentum.

To become more of a challenger:

1. Ask an extreme amount of questions.
2. Have people witness problems first-hand.
3. Get the entire organization to make a small improvement.

## The Debate Maker
Multipliers pull in lots of people and collectively debate to reach a decision. Diminishers only trust a few people to be responsible for a decision.

1. Frame The Issue  
	Debate makers prepare by forming the right team, right questions, and framing issues so that everyone can contribute. They make sure that assumptions are surfaced and challenged, tensions and trade offs are considered, people face reality, and many perspectives address an issue.  
	They bring the right people—those with appropriate knowledge, key stakeholders, and people with responsibility for driving the decision. They define what the issue is, why it's important, and how the decision will be reached.
2. Spark The Debate  
	Good debates are: engaging, comprehensive, fact based, educational. To lead good debates, create safety and rigot. Safety is created by removing fear and ensuring that everyone gets to talk. Rigor is created by challenging opinion and conventional thinking.
3. Drive A Sound Decision  
	Reclarify the decision-making process, making it clear who, how, and when a decision will be made. Get a decision made, then communicate the decision and rationale.

To become more of a debate maker:

- View your contribution as asking questions that produce rigorous thinking and answers.
- Get people to back up their views with evidence.

## The Investor
Multipliers ensure that others become successful. They can step in to teach things, but then they ensure that their people are still accountable for their own decisions afterwards. They teach people to be self-sufficient. Investment happens by giving others the resources and ownership needed to produce results.

1. Define Ownership  
	Put people in charge. Give them ownership for the end goal, not just the individual pieces. Otherwise they'll optimize for just a small piece, not the overall outcome. Give ownership of things a step beyond where the person is, so they grow into it.
2. Invest Resources  
	Give the knowledge and resources people need to fulfill their obligations. Teach and coach, not show and tell. Help people learn what they need to learn. Use questions when you can.
3. Hold People Accountable  
	Although you may step in to help or offer an opinion, you need to return leadership and accountability to others. Expect complete work—expect solutions with problems. Respect the consequences of others' actions. Make sure people have visible measures of success.

To become more of an investor:

1. Let them know who's the boss—they are.
2. Let nature take its course—let people learn from the consequences of their actions. Let the event happen, talk about it, then focus on next time.
3. Ask for solutions. What solutions they have, their proposals, etc.
4. Return responsibility. (One minute manager meets the monkey.)

## Becoming A Multiplier