# Extreme Ownership
Jocko Willink, Leif Babin

- [Part 1: Winning the War Within](#part-1-winning-the-war-within)
    - [1. Extreme Ownership](#1-extreme-ownership)
    - [2. No Bad Teams, Only Bad Leaders](#2-no-bad-teams-only-bad-leaders)
    - [3. Believe](#3-believe)
    - [4. Check the Ego](#4-check-the-ego)
- [Part 2: Laws of Combat](#part-2-laws-of-combat)
    - [5. Cover and Move](#5-cover-and-move)
    - [6. Simple](#6-simple)
    - [7. Prioritize and Execute](#7-prioritize-and-execute)
    - [8. Decentralized Command](#8-decentralized-command)
- [Part 3: Sustaining Victory](#part-3-sustaining-victory)
    - [9. Plan](#9-plan)
    - [10. Leading Up and Down the Chain of Command](#10-leading-up-and-down-the-chain-of-command)
    - [11. Decisiveness Amid Uncertainty](#11-decisiveness-amid-uncertainty)
    - [12. Discipline Equals Freedom—The Dichotomy of Leadership](#12-discipline-equals-freedomthe-dichotomy-of-leadership)

## Part 1: Winning the War Within
### 1. Extreme Ownership
> The burden of command and the deep meaning of responsibility [is that] the leader is truly and ultimately resopnsible for *everything*.

No matter what goes wrong or who messed up, there's only one person to blame: yourself. As a leader, you're responsible for everything that goes on in your team. You have to take **complete ownership**.

All responsibility for successes and failures rests with you. You have to own everything, and there's no one else to blame. You have to acknowledge mistakes, admin failures, take ownership, and figure out a plan to win.

When things aren't working, look at yourself. Take responsibility for explaining the mission, the strategy, the tactics. Make sure your team has the training and resources they need to execute.

If someone isn't performing to the necessary standard, it's your job to train and mentor them. But if they continually fail to meet standards, you have to be loyal to the team over any individual. If they cannot improve, they must be replaced with someone who can get the job done.

Let your team know it's your fault. Ask them whose fault it is, and tell them it isn't theirs. Tell them it's yours, and tell them what you're going to do about it.

Poor performing leaders are those that blame everyone and everything else. These are just excuses.

### 2. No Bad Teams, Only Bad Leaders
Holding all other things equal, it's the quality of the leader that will determine whether or not a team will succeed or fail.

Rather than focusing on a distant goal, it's more effective to focus people on an immediate physical goal. If you can reach an immediate goal that's clearly in reach, you can string together multiple goals and ultimately reach your destination.

Yell less, encourage more.

Embracing that there are only bad leaders means that you need to accept responsibility for problems that face your team. Own problems and develop solutions. Keep the team working towards a focused goal, maintain high standards, and work to continuously improve.

When it comes to standards, *it's not what you preach, it's what you tolerate.* Regardless of the expectations you set, if you accept subpar performance without consequences, that poor performance becomes the new standard.

Pull different elements of the team together and have them support one another. Keep everyone focused exclusively on accomplishing the mission. Most people want to be part of a winning team. They just don't know how to win or need motivation and encouragement. Give people a forcing function to get everyone working together.

### 3. Believe
What do you do when you don't believe in what you are asked to do? Your team will recognize when you don't believe in what you're saying.

You have to address your own doubts. Take a step back, taking a look at your issue from a more strategic level. What's important here? Why are you being asked to do these things?

You have to understand this so that you can believe in the mission. You also have to do understand this so you can explain to your team, so they can believe in the mission. This allows the team to commit and persevere.

When leaders receive an order that they question and don't understand, they must ask: why? If you don't understand or believe in the decisions coming down to you, it's up to you to ask questions until you understand how and why those decisions are being made.

### 4. Check the Ego

## Part 2: Laws of Combat
### 5. Cover and Move
> The enemy is out there.

To get to an objective, have one part of your team provide cover while the other part of your team advances. Then swap so everyone is getting closer.

The idea here is *teamwork*. Everyone is working towards the same mission, so everyone should be able to support each other and pull together. This isn't just true within your team—it also applies to how your team works with other teams. Rely on others to help get you to where you need.

### 6. Simple
Simplify as much as possible. Overly complex plans make it harder for people to understand and execute. Worse, when something goes wrong, the complexity of something can cascade a single mistake into a disaster. The enemy gets a vote, and they will try to disrupt your plans.

Communicate plans in a way that's simple, clear, and concise. Everyone has to understand their role in the mission. You've succeeded in communication only when everyone on your team understands. If this isn't the case, then you failed to keep things simple.

The team also has to be able to ask questions to clarify when they don't understand the mission. Leaders should encourage these questions.

People need to be able to see the connection between actions and consequences to learn to react appropriately. They'll take the path of least resistance.

### 7. Prioritize and Execute
> Relax, look around, make a call.

No matter how good a leader is, there's a limit to how many challenges they can handle simultaneously before getting overwhelmed. At that point, you risk failing all of these challenges. Each challenge is complex and requires a certain amount of attention.

Instead, leaders need to be calm and make the best decisions possible. Determine the highest priority task and execute. This involves:

- evaluating the highest priority problem
- simply, clearly, and concisely lay out the priority for your team
- developing and determining a solution, seeking input from key leaders and from the team where possible
- directing the execution of the solution, focusing all efforts and resources on the task
- moving on to the next highest priority problem, repeat
- passing situational awareness up and down the chain when priorities shift
- not fixating on a priority; be able to see other problems developiong and adjust as needed

### 8. Decentralized Command
A leader can't control everything at once. Dozens of individuals are too many people to manage. Teams need to be broken down to 4-5 people, with each subteam having a designated leader.

The leader must be able to trust thir junior leaders to execute the mission. These subleaders need to understand the overall mission and its ultimate goal—the Commander's Intent. Leaders need to be empowered to make decisions on key tasks to accomplish the mission in an effective and efficient manner.

Each tactical leader must understand both what to do and why they are doing it. When this isn't the case, they must ask their boss to clarify why. You want simple, clear, concise orders that can be understood easily by everyone.

It takes time and effort to master, understand, and apply decentralized command. It's hard to place full faith and trust in your junior leaders. This is especially because these leaders must be able to make tactical decisions that contribute to the strategic mission. These frontline leaders also have to know that they are empowered to make decisions and that senior leaders will back them up.

Developing decentralized command is hard. It comes through preparation and training. The team has to learn from mistakes and get coached on the appropriate principles. Likewise, the leader has to learn that they must trust on their subordinate leaders to execute based on the broader mission.

Junior leaders are expected to make decisions. They can't ask, "What do I do?" They must instead state, "This is what I'm going to do."

Decentralized command doesn't mean that junior leaders and team members can operate fully on their own. They must understand what is within their decision-making authority.

## Part 3: Sustaining Victory
### 9. Plan
As a leader, you need to plan for likely contingencies. Don't take things for granted and aim to maximize mission success while minimizing risk. There are lots of elements to this.

#### Analyze the Mission
Leaders need to identify clear directives for the team. After understanding the directives themselves, they need to then share this information with their team. They need to explain the overall purpose and desired end state. The "Commander's Intent" is the most important part of the brief.

The actual planning involves evaluating what's possible based on your resources. You want to drill in and determine a more thorough plan. Team participation is important here--you should delegate as much as possible to key subordinates. People need to own their tasks within the overall plan and mission. As a leader, you supervise all of this but shouldn't get stuck in the details.

The plan, once completed, needs to be briefed to all participants. The information needs to be given in a simple, clear, and concise format to minimize information overload. This should all be done in a way that encourages discussion, questions, and clarification from everyone involved. If participants are unclear but are afraid to ask questions, the team won't be able to execute. You've successfully bried the team if they understand the plan.

### 10. Leading Up and Down the Chain of Command
### 11. Decisiveness Amid Uncertainty
### 12. Discipline Equals Freedom—The Dichotomy of Leadership