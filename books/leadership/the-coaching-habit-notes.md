# The Coaching Habit
Michael Bungay Stanier  
[Amazon](https://www.amazon.com/gp/product/B01BUIBBZI)

## You Need a Coaching Habit
> Everyone now knows that manages and leaders **need to coach their people.**

Coaching is one of the most impactful leadership styles, as identified in "Leadership That Gets Results." It's also one of the least used, because it's hard to start doing effectively. Most people who received coaching training (if any) fail to follow it because:

1. It was too theoretical, complicated, boring, and/or not closely related to your actual work.
2. You didn't translate the new insights into actions.
3. Behavior changes are hard.

Building a coaching habit lets you avoid three crucial issues. **Creating overdependence**, which is frustrating for everyone and leads to more dependence over time. **Getting overwhelmed**, which causes you to lose focus and become more overwhelmed. **Becoming disconnected**, where lack of engagement and motivation results in not doing impactful and meaningful work.

The bulk of this book is on seven questions to avoid these "vicious circles" and improve your work:

1. Kickstart Question—What's on your mind?
2. Awe Question—And what else?
3. Focus Question—What's the real challenge here for you?
4. Foundation Question—What do you want?
5. Lazy Question—How can I help?
6. Strategic Question—If you're saying yes to this, what are you saying no to?
7. Learning Question—What was most useful for you?

## How to Build a Habit
Building a new habit is simple, though not easy. It's more complicated since there's a lot of misinformation on how to build habits. Building effective habits involves five things: a reason, a trigger, a micro-habit, effective practice, and a plan.

### Reason
Building a new habit involves changing an existing familiar and efficient (but not effective) behavior. You need to be able to answer why you want to do this—what's the benefit. This doesn't mean envisioning success. Instead, you want to connect it to your life. Typically, this means figuring out how it will help people you care about.

### Triggers
You need to know what triggers your old behavior, otherwise you won't be able to behave differently. There are only five types of triggers: location, time, emotional state, other people, and immediately preceding actions.

### Micro-Habits
Make your new habit easy to do. It should be short and specific, taking less than sixty seconds to do. This doesn't mean your entire habit only takes sixty seconds. It's just that this initial step, which will help you complete the rest of the habit, should be easy.

### Effective Practice
There are three parts to effective practice: practicing small chunks of the bigger action, repetition, and being mindful and noticing when you do things well.

### Plan
You'll stumble as you build your new habit, so you want a path to get back on track. Build in steps that will help you recover when you get off-track.

### The New Habit Formula
So, to create a new habit, there are three things you want to do. The more specific you make each step, the more precise and clear it will be as to what you need to do.

1. Identify the trigger.
2. Identify the old habit.
3. Define the new behavior.

Additionally, it's recommended to start with something small and easy. Make sure you have support system(s) and to continue even when you have trouble and feel discouraged.

## Question Masterclass

### Ask One Question at a Time
> After I've asked a question, instead of adding another question, I will ask just one question and wait for the answer.

Bombarding people with questions makes them feel interrogated and is a form of drive-by questioning.

### Cut the Intro and Ask the Question
> When I've got a question to ask, instead of setting it up, I will ask the question.

If you know what you want to ask, just cut to the chase and ask it.

### Should You Ask Rhetorical Questions?
> When I have an answer that I want to suggest, instead of asking a fake rhetorical question, I will ask an actual question or present my idea as a statement.

Rhetorical questions are just advice with a question mark attached. Don't use them.

### Stick to Questions Starting with "What"
> When I'm tempted to ask why, instead of beginning the question with "why," I will reframe the question so it starts with "what."

Don't ask why questions. They make people feel defensive and indicate that you're trying to solve others' problems, rather than helping them solve the problems themselves. Instead, reframe the questions as "what" questions.

### Get Comfortable with Silence
> When I've asked a question and haven't gotten an answer in the first two seconds, instead of filling up the space, I will take a breath and keep quiet for another three seconds.

Silence creates space for learning and insight.

### Actually Listen to the Answer
> After I've asked a question, instead of just looking like I'm actively listening, I will actually listen.

### Acknowledge the Answers You Get
> When someone gives an answer to a question I've asked, instead of rushing to the next question, I will acknowledge the reply.

Acknowledge answers. This can be quick and simple, such as "fantastic" or "that's good."

### Use Every Channel to Ask a Question
> When I get an email requesting advice, instead of writing a long, thorough answer, I will pick an appropriate question and ask that question by email.

## Questions

### 1. The Kickstart Question: What's on your mind?
Conversations can get stuck with boring or superficial starts. This typically happens due to small talk, stale agendas, or assuming that both people are on the same page.

"What's on your mind?" is an open yet focused question, allowing people to get directly to things that matter most to them.

#### Performance vs. Development Coaching

Coaching for performance is addressing and fixing a specific problem. Coaching for development is focusing on the person dealing with the issue. Both are useful, but coaching for development is much rarer and the focus of this book.

#### 3P Model
This is a framework for choosing what to focus on in a coaching conversation. It consists of:

- Projects—The content or stuff that's being worked on. Often you start on projects and include one or both of the other two Ps.
- People—A relationship and how you're performing / behaving on your end of the relationship.
- Patterns—These are behavioral or working patterns that you'd like to change. Typically where you will coach for development.

When someone answers what's on their mind, you can outline these three parts and ask them what they want to talk about.

### 2. The AWE Question: And what else?
This question brings out more options that may lead to better decisions, reins you in, and buys you time. People rarely give you the best answer as their first answer, and will often have additional answers. Asking this question forces you to focus on the other person, rather than automatically going into advice-giving mode.

When asking, be genuinely interested! You can also ask this question multiple times, though you should stop if the person has genuinely exhausted their answers. You can also vary this question by phrasing it a yes/no form—"Is there anything else?" Ideally, you'd want 3-5 answers.

### 3. The Focus Question: What's the real challenge here for you?
When people talk about a challenge, they haven't presented their actual problem. Trying to address their issues prematurely leads to working on the wrong problem, you doing the work for others, or the work not getting done.

This question makes them realize that they have to figure out the challenge that actually matters. It also stays personal. A key part of this question is the "for you" part, which can actually be applied to other questions. The "for you" tends to make conversations development-oriented instead of performance-oriented.

This question also helps you avoid some common pitfalls. You avoid an overabundance of issues, talking about people who aren't present in the conversation, and talking too abstractly.

### 4. The Foundation Question: What do you want?
> The single biggest problem with communication is the illusion that it has taken place. —George Bernard Shaw

People should be responsible for themselves and be able to articulate what they actually want. However, this can be difficult—people might feel uncomfortable, not state their wants clearly, or not be open to hearing the response.

People have wants, the tactical outcomes they want from a situation. Even deeper, they have needs. There are nine universal needs; you should try to identify the likely needs behind peoples' requests.

- affection
- creation
- recreation
- freedom
- identity
- understanding
- participation
- protection
- subsistence

The "What do you want?" question is more powerful when you also add what you want in addition to what the other person wants. This question is also effective because it helps focus people on the ultimate end that they want, rather than the means.

### 5. The Lazy Question: How can I help?
Trying to preemptively help people actually lowers their status, since it indicates that they can't do something. Asking how you can help forces the other person to make a direct and clear request. It also stops you from assuming you know how to help best. 

You can also use a more blunt version of this question: "What do you want from me?" This, and other, questions can be softened with the phrase "out of curiosity."

When people ask questions that are really directed towards getting an answer from you, you can temporarily deflect the question so that you can ask the lazy question. Tell them they have a great question and that you have some ideas, but have them share their thoughts first.

#### Karpman Drama Triangle
When we're not behaving well, we're acting like one of the "seven dysfunctional dwarfs": sulky, moany, shouty, crabby, martyr-y, touchy, or petulant. We're fitting into an archetype—either a victim, persecutor, or a rescuer. In fact, we can rapidly bounce between these mentalities within the same conversation. The lazy question helps break out of this triangle by focusing on helping others.

### 6. The Strategic Question: If you're saying yes to this, what are you saying no to?
People should avoid doing busy work and be responsible for spending their time strategically. This question helps with both of these aspects. It gets people to be committed to what they agree to. It also gets them to think about what types of things they can't do—both because they can't and because they need to make room to accomplish what they committed to.

Sometimes you can't actually say no. Instead, you can say yes more slowly. By asking clarifying questions, you can get people to either decide not to have you do something or to make what you need to do more clearer and explicit. Alternatively, you can say yes to the person but no to the task.

There are other strategic questions you can ask:

- What is our winning aspiration?
- Where will we play?
- How will we win?
- What capabilities must be in place?
- What management systems are required?

#### 3P
Sample questions to get people to identify their boundaries:

- What projects do you need to abandon or postpone?
- What meetings will you no longer attend?
- What resources do you need to divert to the yes?
- What expectations do you need to manage?
- What drama triangle dynamics will you avoid?
- What relationships will you let wither?
- What habits do you need to break?
- What old stories or dated ambitions do you need to update?
- What beliefs about yourself do you need to let go of?

### 7. The Learning Question: What was most useful for you?
People don't learn when they do or are told something. They learn when they recall and reflect on what happened. This question helps people generate their own answers and learn from that.

This question also utilizes the idea of spacing / information retrieval. By asking this question at the end of a conversation, you help to prevent someone from forgetting what you just discussed. A similar style question is to use "What have you learned since we last met?" at one-on-ones.

It's also useful for other reasons:

- It assumes the conversation was useful.
- It forces people to identify the single biggest point.
- It keeps things personal.
- It gives you feedback about what you said that helped.
- It gets people to learn from the conversation.
- It reminds people that you're useful, which can be useful e.g. during performance appraisals.

