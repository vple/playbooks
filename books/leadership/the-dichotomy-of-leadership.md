# The Dichotomy of Leadership
Jocko Willink, Leif Babin

- [Part 1: Balancing People](#part-1--balancing-people)
  * [1. The Ultimate Dichotomy](#1-the-ultimate-dichotomy)
  * [2. Own It All, but Empower Others](#2-own-it-all--but-empower-others)
  * [3. Resolute, but Not Overbearing](#3-resolute--but-not-overbearing)
  * [4. When to Mentor, When to Fire](#4-when-to-mentor--when-to-fire)
- [Part 2: Balancing the Mission](#part-2--balancing-the-mission)
  * [5. Train Hard, but Train Smart](#5-train-hard--but-train-smart)
  * [6. Aggressive, Not Reckless](#6-aggressive--not-reckless)
  * [7. Disciplined, Not Rigid](#7-disciplined--not-rigid)
  * [8. Hold People Accountable, but Don't Hold Their Hands](#8-hold-people-accountable--but-don-t-hold-their-hands)
- [Part 3: Balancing Yourself](#part-3--balancing-yourself)
  * [9. A Leader and a Follower](#9-a-leader-and-a-follower)
  * [10. Plan, but Don't Overplan](#10-plan--but-don-t-overplan)
  * [11. Humble, Not Passive](#11-humble--not-passive)
  * [12. Focused, but Detached](#12-focused--but-detached)

## Part 1: Balancing People
### 1. The Ultimate Dichotomy
> There was no other choice--there was no decision to make.

Leaders are put in a position where they're responsible for their team. They need to ensure results, even if the things they do have a chance of hurting individuals on their team. This is the burden of leadership. You must care for each person on the team, while also accepting risks necessary to accomplish you mission.

If you can't detach from the individuals on your team, you won't be able to make the needed decisions for the team to succeed. At the same time, if you focus only on the result, you might sacrifice some of your individuals. You'd then lose morale and the support of your team.

### 2. Own It All, but Empower Others
A leader can't do everything themselves. They are still responsible, but they must empower the other members of their team.

You do this with decentralized command. Give your people their own missions to own and execute. You can't own it all yourself. The responsibility is yours, but that doesn't mean you have to personally do everything yourself. People can't learn if they don't own it themselves.

When you do empower others, this gives you:

- The ability to step back and see the big picture.
- The ability to see how different operations support or conflict.
- The chance to look at operations at a higher level.

Being too hands off also doesn't work. This causes the team to think too much, and they can drift from higher up objectives.

Micromanagement symptoms:

1. Lack of initiative. Members don't take action unless directed.
2. Team doesn't seek solutions to problems. Members sit and wait to be told about a solution.
3. Even in emergencies, teams don't mobilize and take action.
4. Bold and aggressive action becomes rare.
5. Creativity diminishes.
6. Teams stay siloed.
7. People become passive and fail to react.

To fix micromanagement, the leader needs to pull back. Instead of giving detailed direction, the leader needs to explain the broad goal, desired end state, and reason why the mission matters. Then, let the team execute. The leader should monitor, but avoid giving specific guidance unless extremely necessary. If feasible, let the team do an entire mission on their own.

Hands-off symptoms:

1. Lack of vision about what the team is trying to do or how to do it.
2. Lack of coordination between individuals, competing or interfering efforts.
3. Initiatives overstep bounds of authority. Individuals and teams are doing more things than they have authority for.
4. Failure to coordinate.
5. Team is focused on the wrong priority or soultions that don't match the strategic direction.
6. Too many people trying to lead. Not enough people executing. Too much discussion and debate instead of action.

To fix being too hands-off, give clear guidance. Specify boundaries and what to do when someone runs into those boundaries. If there are multiple efforts going on, the leader needs to make a decision and provide direction.

### 3. Resolute, but Not Overbearing
A leader needs to enforce the standard. Leaders can't be too lenient, but they also can't be too overbearing. There needs to be high standards and the team needs to be pushed to meet those standards, but a leader can't be inflexible for things that have little strategic importance.

Spend your "leadership capital" on things that are important. Focus on areas where standards can't be compromised. Especially when enforcing standards, explain why the standards matter, how they'll help to accomplish things, and what happens when standards aren't met.

### 4. When to Mentor, When to Fire
Leaders need to train, coach, and mentor members to at least the minimum standard, and ideally the highest standards. However, if you've done everything possible and someone isn't able to succeed, they need to be fired.

Most underperformers don't need to be fired. They need to be led.

The point where you've done everything possible is where you've done remedial measures including coaching, mentoring, and counseling. When you've done everything possible to get someone up to speed without seeing results, it's time to let them go. Focusing too much on the individual past this point will show in the team's results.

## Part 2: Balancing the Mission
### 5. Train Hard, but Train Smart
If training is too easy and doesn't stretch people's ability, there won't be improvement. But if training is overwhelming, people will get diminished results. Training needs to make people uncomfortable, but it can't be so overwhelming that it destroys someone's attitude.

As a leader, you can't let people stay in their comfort zone. You need to simulate realistic challenges and apply pressure. Otherwise people will never be able to take on greater challenges.

You want to balance three aspects: realism, fundamentals, and repetition.

### 6. Aggressive, Not Reckless
Have a default stance of aggressive--be proactive about fixing problems. Don't wait to act; understand the overall intent and then execute to move past obstacles. However, balance this aggression with enough thought and analysis to ensure that you've accounted properly for risk.

### 7. Disciplined, Not Rigid
Although discipline empowers individuals and teams, too much discipline prevents people from being able to be creative and adapt to situations. Standard operating procedures and repeatable processes are useful, but it's also useful to understand when to deviate from the standard.

You want to deviate when common sense indicates that you need to change something. You need to decide about alternatives and make adjustments based on the reality of what's happening.

### 8. Hold People Accountable, but Don't Hold Their Hands
You need to hold people accountable to standards. However, it's important to do so by explaining _why_ the standard is in place. This allows them to understand the importance of the standards. This will help you to be able to trust them to do the right thing without oversight. This also helps them hold themselves accountable.

## Part 3: Balancing Yourself
### 9. A Leader and a Follower
A leader needs to be willing and able to lead, but they also have to be able to follow. Lean on others' expertise and ideas, even when they're junior or less experienced. Being able to follow improves relationships and allows you more ways to ensure a mission succeeds.

You want to strive to have the same relationship with every boss you work for, regardless if they're good or bad. That means you have three things:

1. They trust you.
2. They value and seek your opinion and guidance.
3. They give you waht you need to accomplish your mission and then let you go execute.

### 10. Plan, but Don't Overplan
Leaders need to manage risk through contingency planning. Plan for likely outcomes and worst-case scenarios. However, understand that you can't control every risk. Trying to plan for everything will overburden you, making your decisions more complicated. Overplanning reduces flexibility.

Likewise, don't get caught being complacent or overconfident. You can't underplan for contingencies either.

### 11. Humble, Not Passive
The most important quality in a leader is to be humble. Check your ego, take constructive criticism, own your mistakes. See beyond your own needs, including those of the leaders above you.

At the same time, don't be a pushover. There are times where you need to push back, voice concerns, and stand up for your team. Be willing to provide feedback against a direction if you know it will put your team in danger.

### 12. Focused, but Detached
Leaders have to pay attention to details, but they can't lose sight of the bigger picture. Leaders need to pull themselves out of the weeds so they can effectively run their team. You need to find the right balance between being focused and being detached.