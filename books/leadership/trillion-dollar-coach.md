# Trillion Dollar Coach
Eric Schmidt, Jonathan Rosenberg, Alan Eagle
[Amazon](https://www.amazon.com/Trillion-Dollar-Coach-Leadership-Handbook/dp/1473675979)

## 1. The Caddie and the CEO
In fields like football, you need things like "dispassionate toughness," which includes pushing people and being almost insensitive to feelings. In business, compassion is increasingly believed to be a factor for success.

_How Google Works_, talks about the smart creative—someone who helps achieve speed and innovation by combining technical depth, business savvy, and creative flair. It argues that these smart creatives are crucical to building a successful company.

This book focuses on another aspect: teams that act as communities. These teams are able to put aside differences to focus on what's good for the company. This can be hard to bring about, as there will be opinionated people, people with egos, people who engage in "status conflicts."

The trick is to get all of these people moving towards the same goal. When people are moving effectively towards a goal while also being ambitious, competitive, creative, opinionated, etc., you get "tension in the machine" that is good and crucial for success.

But this team still isn't a community. That's where a coach comes in. They balance the team and mold the community. They work with with individuals and with the team to smooth tension, nurture, and align everyone around common goals. To be most effective, this coach works with the entire team.

It's not possible or practical to hire a coach for every team in a company. However, the best coach for any team is the manager of that team—being a good coach is essential to being a good manager and leader. Many management skills can be delegated, but not coaching.

## 2. Your Title Makes You a Manager, Your People Make You a Leader
> If you're a great manager, your people will make you a leader. They acclaim that, not you.

Some people actually enjoy being managed, if they can learn from their manager and their manager helps to make decisions. Teams deliver in large part due to strong managers. Successful people have good processes, hold people accountable, hire great people, evaluate and give feedback, and pay well. A team should come together with its own culture to achieve results.

When Bill coached, the first topic was always management: operations and tactics. At the top, that focused on questions like:

- What are the current crises?
- How quickly can we manage our way out of them?
- How is hiring going?
- How are teams being developed?
- How are staff meetings going?
- Is everyone providing input?
- What is being said, what isn't being said?

### It's the People
> The top priority of any manager is the well-being and success of her people.

Companies rely on their people for success. A manager's primary job is to help people be effective in their job and to grow and develop. They create good environments to work in, via:

- support  
  Give people the tools, information, training, and coaching that they need to succeed. Continuously develop people's skills.
- respect  
  Understand each person's career goals and life choices. Help them achieve their goals while also being consistent with company needs.
- trust  
  Enable people to do their jobs and make decisions. Know that people want to do well and believe that they'll do well.

### Start With Trip Reports
> To build rapport and better relationships among team members, start team meetings with trip reports, or other types of more personal, non-business topics.

Bill would always start staff meetings by asking people what they did on the weekend or, if someone just got back from a trip, by getting an informal trip report. There were two goals to this:

1. Get team members to know each other as people—everyone has families and lives outside of work.
2. Get everyone involved in the meeting in fun way.

Getting people to share stories and be personal helps to ensure better decision making and teamwork later on. Fun work environments also lead to higher performance. Social talk changes the dynamics of meetings, resulting in a better mood. It's jarring to jump directly into your topics.

### Run Meetings Well
Bill believed that 1:1s and staff meetings were two important tools to executives, and that each one should be approached thoughtfully. Get them right.

#### Staff Meetings
Staff meetings are a forum for the most important issues and opportunities. They are used get people on the same page, ensure that people are debating the right issues, and making decisions. It's important to raise awareness and have cross-functional understanding of topics, even topics that could have been resolved in a 1:1.

#### 1:1s


## 3. Build an Envelope of Trust
## 4. Team First
## 5. The Power of Love
## 6. The Yardstick