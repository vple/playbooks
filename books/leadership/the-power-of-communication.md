# The Power of Communication: Skills to Build Trust, Inspire Loyalty, and Lead Effectively
Helio Fred Garcia

## Foreword
> Public opinion wins wars. --Dwight D. Eisenhower

> If you can't communicate effectively, you will not lead.

This book takes concepts from the Marine Corps and applies them to professional communications. Communication is crucial regardless of field--from the military to late night comedy.

## Introduction: Leadership, Discipline, and Effective Communication
Leadership is driven by effective communication, which in turn is driven through discipline.

> War, at its essence, isn't about fighting or killing, at least not for their own sake. Rather, it's about an outcome. A reaction. A change.

Likewise,

> Communication is an act of will directed toward a living entity that reacts.

In particular,

- 	Communication is intentional.  
	You should have a goal. Communication should be part of a strategy to help you accomplish that goal.  
	On the other hand, communication is not impulsive, top of mind, or indulgent. If it's not thought out, it doesn't help you accomplish an aim.
-	Communication is about engagement.  
	It's what you say, but also what you do or are observed doing. It includes action, inaction, and silence.
-	Stakeholders aren't sponges.  
	They have opinions, hopes, dreams, fears, prejudices, attention spans, and appetites for listening.
-	You are not your audience.  
	Audiences don't think or behave the same as you. They have their own preconceptions and barriers that you need to plan for.
-	The only reason to communicate is to provoke a reaction.  
	Effective communication produces that reaction. Ineffective communication doesn't, because it isn't noticed, is confusing, or it causes a different reaction.
-	Your words set expectations that your actions will either fulfill or betray.  
	You lose trust when you betray those expectations and gain trust when you fulfill them.
-	Effective communication is hard and requires discipline.
-	To understand and produce a reaction amongst the people you communicate to, you must understand all you can about them.
-	Do all that is necessary, and only what is necessary, to provoke your reaction.
-	Predict the intended and unintended consequences of your words, silence, inaction, and action.

A commander's intent is a crucial part of the order. It is the receiver's responsibility to understand the intent. It is the deliverer's responsibility to make the intent clear.

I believe there can often be multiple dimensions of intent, and you may need to convey all of them. These are things such as immediate outcomes, personal development, or team operational concerns.

Marines are expected to be spokespersons: **engage**, **tell the truth**, and **stay in your lane**. Each marine is expected to be accountable, but this also shares authority.

## Part I: Leadership and Communication: Connecting with Audiences
### 1. Words Matter
For effective communication, we need to be strategic. Strategy means ordered thinking. With communication, the question of "What do we want to say?" is what we want to answer last. Most people get it wrong by trying to say that first.

The strategic questions we want to ask, in order:

- What do we have? What is the challenge or opportunity we are hoping to address?
- What do we want? What's our goal?
- Who matters? Which stakeholders are important to us? What do we know about them? What information do we need to get about them? What would prevent them from being receptive to us, and how do we get past that?
- What do we need people to think, feel, know, or do in order to accomplish our goal?
- What do we they need to see us do, hear us say, or hear others say about us to think, feel, know, and do what we want them to do?
- How do we make that happen?

Communication is _interactive_. We need to interact with the people we are communicating with. It's two-way, involving a feedback loop.

> Effective communication is an act of will directed toward a living entity that reacts. That reaction is the essential element of essential communication: Was the reaction what we wanted? If not, why not? And how can we provoke the reaction we wawnted?

> We recognize that we're not communicating, but we blame the other person, and we persist. If only we'd stop, acknowledge that we're not communicating, and find some common ground. But it isn't easy.

Part of being interactive is _adapting_. When you aren't understood, you need to shift gears. You can and should react to everything--how the audience initially reacts, what critics or adversaries say, changes in the environment, as facts change, as new developments come up, etc. This is in contrast to digging in, which many people tend to do instead of adapting. We can adapt our behavior, message, form of engagement, and even our language.

It helps to have a plan. This isn't so that you stick dogmatically to the plan. Instead, it's so that you thoroughly understand your goals so you can rapidly adapt once things change.

This is especially important when you remember your audience thinks for itself. It has its own concerns, possibly conflicting with yours, which you need to account for. Your audience will often not care about concerns of things trying to influence it--you. They'll often be concerned with their own immediate interests.

Good communicators meet their audience physically, emotionally, intellectually, spiritually, and/or ideologically. They reveal in both their speech and their action that they understand the motivations and aspirations that drive their audience. This is how they win over the audience.

#### Warfighting
> Effective communication is fundamentally an interactive social process. It is thus a process of continuous mutual adaptation, of give and take, of move and countermove.

> Since communication is a fluid phenomenon, its conduct requires flexibility of thought. Success depends in large part on the ability to adapt--to proactively shape changing events to our advantage as well as to react quickly to constantly changing conditions.

> It is critical to keep in mind that the audience is not an inanimate object to be acted upon but a collection of living, breathing human beings with their own goals, concerns, needs, priorities, attention spans, and levels of desire even to be in a relationship with us.

### 2. Taking Audiences Seriously