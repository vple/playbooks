# Managing Humans
Michael Lopp
[Amazon](https://www.amazon.com/Managing-Humans-Humorous-Software-Engineering-ebook/dp/B01J53IE1O/)

As many of the chapters in this book come from Lopp's blog, I've linked the corresponding articles. The book chapters have likely been edited from the articles; I haven't confirmed.

# The Management Quiver (The Toolkit)
## [Don't Be a Prick](http://randsinrepose.com/archives/dont-be-a-prick/)

> My definition of a great manager is someone with whom you can make a connection no matter where you sit in the organization chart.

Being a great manager involves being able to construct an insightful opinion about someone in seconds, listening to others and mentally documenting how they operate. Importantly, people are messy and complicated. You'll have to constantly assess and adapt in order to best provide what others need.

## [Managers Are Not Evil](http://randsinrepose.com/archives/managers-are-no/)
Many employees don't really know what their managers do. This disconnect is why they often end up not trusting their managers.

The typical frustration that employees have is that they feel like they're busting their ass, but their manager always seems to be slacking off or just chatting with people. Here's the thing—*your manager's job is not your job*. It's not important here to explain *what* that job is, as you'll be biased against any job other than your own. That's because you understand your own job the best.

What is important is if your manager is capable of looking out for the most important person—you. This is a set of 7 questions you should answer to evaluate your manager (and ideally you will answer these questions as soon as you can).

### Where Does Your Manager Come From?
Where does their experience come from? This affects how you should communicate, and also what to expect when things go south.

In particular, another common complaint is that employees feel that their manager has no idea what they do. An important thing for you to do is to manage that expectation.

Your manager might not care what you're doing, just because it's not on their radar. You should find ways to talk to them to let them know. You could coast by without them knowing, but this will also mean no one knows your value when the company runs into trouble.

Or, your manager might not understand what you're doing. Figure out if your manager has the same expertise as you (i.e. engineering). You do this with a specific question, such as what code they're really proud of. Or find out about their education.

They might not have an engineering background, and that's okay. You have to figure out what their background is and talk to them in that language. In that language, tell them what you do and why it matters.

The reason all of this is important is that **your manager is your face to the rest of the organization**. When someone you've never met has something good to say about you, it's because your manager had something to say as a result of you spending a few minutes to explain what you do.

### How Is He Compensating for His Blind Spots?
Everyone, managers included, are deficient somewhere. Their job is to take the skills they've developed and get those skills to scale. This means both accentuating their strengths and reinforcing their weaknesses. In interviews, a good way to get at this is by asking, "Where do you need help?"

### Does Your Manager Speak the Language?
When you talk to your manager, you probably talk differently than how you would when you talk to a friend. That's because when you talk with your manager, you're talking to the organization.

Here, you want to know if your manager can speak to the rest of the organization. Are they capable of interacting with managers from different parts of the organization to get things done?

### How Does Your Manager Talk to You?
Your manager should have some consistent way through which they get information from you. This is typically a one-on-one, but it might be something else such as regularly talking to you in the hallway. This is important because *good managers know the pulse of the organization*. Talking to you is how your manager gathers information and learns.

Your manager should always be interested in getting information, even if you have negative things to say. If they aren't talking to you, they won't learn what's happening in the organization.

### How Much Action per Decision?
Managers need to make things happen. Do they do what they say they're going to do? Do they make things happen? Managers do real work when they take visible action to support their vision for the organization. A manager can't delegate everything—they need to do some delegation to scale, but they also need to continue getting their hands dirty.

### Where Is Your Manager in the Political Food Chain?
Politically active managers are informed managers, and it's better to have a manager who knows what's going on than a manager who just lets things happen to them. These active managers know how to act when change happens in the organization.

It's hard to figure out where your manager is since you're not in their meetings. The next metric you can use is to see how their peers treat them. Are they familiar with them? How does your manager contribute in meetings? How do others view them?

From before, your manager is your face to the rest of the organization. Relatedly, the organization's view of your manager is their view of you.

### What Happens When They Lose Their Shit?
Evaluate these questions again when your manager is in a situation where they need to panic (such as a confirmed layoff). When someone panics, they rely on the skills that have worked for them in the past.

### The Big Finish
The layers in an organization consist of managers whose jobs are to translate between the layers above and below them, in both directions. They know what their employees and what their managers want, and can resolve issues when those wants differ.

The way they navigate depends on how they answer (1) Where did I come from? and (2) Where am I going? Their success is your success.

## [Stables and Volatiles](http://randsinrepose.com/archives/stables-and-volatiles/)
A stable is an engineer that works best with and appreciates a well-defined schedule. They opt towards reliability, in many aspects—haing an efficiently run team, mitigating risk, establishing processes, being calm.

A volatile is an engineer that embraces chaos and risk. They'd rather figure out a strategy than follow one and will have legitimate arguments against authority. They don't conceive of failing and enjoy risk. They have a disdain for feelings and working in large groups, prefering to work in autonomous units. They may not build reliable things, but they build a lot. Reliability only happens when it's in their best interest.

Stables see volatiles as holding nothing sacred and doing whatever they want, regardless of the outcome. Volatiles see stables as lazy and bureaucratic, having turned into "The Man."

A volatile turns into a stable when they get battle scarred from a project. New volatiles come in and then disrupt the organization. *However, you want to nurture this disruption.*

If you don't disrupt, you'll just coast. You need to invest in the disruption, even though it is destructive. You need to find a way to rely on stables to bring predictability, repeatability, and credibility. They need a world where they can survive. Volatiles are needed to remember that nothing lasts.

## [The Rands Test](http://randsinrepose.com/archives/the-rands-test/)
This is the Rands version of The Joel Test, for understanding the health of your team. As teams grow, communication breaks down. This test helps identify if you're clear on what's going on.

- Do you have a one-on-one?
- Do you have a team meeting?
- Do you have status reports?
- Can you say "no" to your boss?
- Can you explain the strategy of the company to a stranger?
- Can you explain the current health of business?
- Does the person in charge regularly stand up in front of everyone and tell you what they are thinking? Are you buying it?
- Do you know what you want to do next? Does your boss?
- Do you have time to be strategic?
- Are you actively killing the Grapevine?

These are scored as follows.

- Do you have a consistent one-on-one where you talk about topics other than status? (+1)
- Do you have a consistent team meeting? (+1)
- Are handwritten status reports delivered weekly via email? (-1)
- Are you comfortable saying "no" to your boss? (+1)
- Can you explain the strategy of your company to a stranger? (+1)
- Can you tell me with some accuracy the state of the business, or could you go to someone somewhere and figure it out right now? (+1)
- Is there a regular meeting where the person in charge gets up in front of everyone and tells you what they are thinking? (+1) And are you buying it? (+1)
- Can you explain your career trajectory? (+1) Can your boss? (+1)
- Do you have well-defined and protected time to be strategic? (+1)
- Are you actively killing the Grapevine? (+1)

## [How to Run a Meeting](http://randsinrepose.com/archives/how-to-run-a-meeting/)
The core difference between a conversation and a meeting is that it needs rules so people know when to talk.

There are two useful types of meetings: *alignment meetings* and *creation meetings*. The former are tactical exchanges. The latter involve diving into and solving a hard problem.

Meetings have two important pieces: *an agenda* and *a referee*. The agenda is what the meeting needs to address. The referee, who is not necessarily the meeting owner, needs to make sure the meeting is always making progress. The referee must always be listening, ensuring that others are still paying attention and not checked out. 

Refereeing is situational. You have multiple people, and each one has a personality, an agenda, and a mood. On top of this, there's the overall meeting topic. Tips on re-engaging people:

- Pull them back. Steer the conversation towards them and ask them a question relating to the current state of the topic.
- Reset the meeting with silence.
- Change the scenery. Even simple changes such as standing up or writing things on the whiteboard can change things.

Referees are also in charge of meeting flow. This means controlling the meeting if needed, but also letting the meeting go off topic if it's productive.

> The definition of a successful meeting is that when the meeting is done, it need never occur again.

## [The Twinge](http://randsinrepose.com/archives/the-twinge/)
As a manager, because you have more people than reports, you simply cannot be aware of every thought and action on your team. You have to delegate. In order to delegate without things crashing down, you need to pay attention to your twinges—those moments where you feel there's a core problem in a project or someone's work.

To find twinges, you use your experience. The skills you developed to build software can't be directly used in your manager role. But the experience you gained developing these skills is crucial for being able to find twinges.

As a manager, your day is filled with stories. Every interaction or observation you have is a separate story. You'll have an incomplete picture of all of these stories—you won't have all the facts, and people will (unintentionally) distort their stories in their favor. Your job is to find out if you believe the story.

To find out if you believe the story, sniff around and demand specifics until the story matches one that you're familiar with.

## [The Update, the Vent, and the Disaster](http://randsinrepose.com/archives/the-update-the-vent-and-the-disaster/)
Your job in a one-on-one is to give every voice a chance to be heard.

Start with a softball—how are you? This is vanilla and content-free, but this also means that the other person has to reveal something about themselves when they answer.

Pay attention to *how* they answer. Their actual answer is not the point—their answer conveys their mood, and their mood sets your agenda. There are three mood buckets:

- The Update (everything's fine)
- The Vent (something's up)
- The Disaster

### The Update
Reports often convey status when they give updates. One-on-ones aren't status reports. They're opportunities to learn something new, outside of the daily business grind. If your employee doesn't have an actual discussion item and you can't start a more meaningful conversation about something the person's said, you need to change the conversation. Here are three techniques:

- Three prepared points, regarding the employee or the team.
- Mini-performance review. Bring up an area that they are working on and ask how it's going.
- Your current disaster. Tell your employee about something you're dealing with. The point isn't to solve your problem, but for them to learn something more than project status.

### The Vent
Vents aren't conversation. They're mental release valves. Your job is to shut up and just listen. The key difference between updates and vents is that vents are driven by emotion.

You finally jump in once the "conversation" reaches one of these points:

- It's done. Your employee's losing steam. Now you actually start talking and triaging.
- It's a rant. If the vent repeats, it's a rant. This can be healthy, and you should let it repeat for some cycles. Give them the benefit of the doubt and try to start steering the conversation after a few rounds.
	- A vent that doesn't want help is a rant.
	- It's okay to jump in in the middle of a rant.
- It's a disaster. The vent is continually progressing and not repeating (it's not a rant) and something's off. Your employee might be out of character.

### The Disaster
Disasters feel like an attack. The most important thing for you to do is to put your emotions away and to not respond emotionally. Your employee wants to fight over something here, and you don't want to give them that chance.

During a disaster, shut up. Your job is to defuse, and this starts by doing nothing. Your employee isn't dealing about their issue, they're dealing with their emotional baggage around the issue. You need to let that initial wave of emotion pass. Once it does, your employee will be a bit more rational and themselves, allowing you to actually have a discussion.

This discussion isn't about the core issue. It's about the emotion around the topic.

Disasters are caused by poor management. Employees only engage in disasters when they feel like losing their shit is their most productive strategy.

## The Monday Freakout
How do you handle when an employee freaks out? These are typically purely emotional, illogical, and rarely grounded on fact. There also aren't clear solutions because there isn't a clearly defined problem.

You want to just listen and maintain eye contact. Be calm, in contrast to them—this helps them realize that their behavior is an extreme.

The core issue of the freakout normally follows the "preamble." The preamble gets your attention, and the real issue often comes after a pause.

Once you get involved in the conversation, ask lots of questions. Specifically, ask questions that engage them logically and rationally—you're trying to shift them from being emotional to being rational. In addition, you'll likely be able to get them to reach an answer of their own.

It's good that your employee is emotionally invested—this shows that they care. But, once you've resolved the freakout, it's important to realize that you have two problems. One, the actual problem you discussed. Two, others think the best way to get your attention is by freaking out.

## [Lost in Translation](http://randsinrepose.com/archives/lost-in-translation/)
Sometimes, you have a complete personality disconnect with someone you lead. Normally, one of the first things you try to understand about an employee is their core motivation. This tells you how to approach working with that person.

With someone that you're disconnected with, you won't be able to get this core motivation. You also won't be able to get them to tell you their secrets. Your first job is to establish basic communication.

Where communication is an issue, use clarification. Whenever you say something, ask your employee what they heard. Similarly, repeat back what you heard to the other person. Don't assume anything—keep pushing until you have an explicitly-stated work commitment.

## [Agenda Detection](http://randsinrepose.com/archives/agenda-detection/)
Agenda detection allows you to get out of meetings faster. It involves the ability to:

1. Determine meeting roles and which participant is in which role.
2. Understand what different roles want out of a meeting.
3. Use this understanding to get out of the meeting.

Tips:

1. Identify the type of meeting.  
	Informational meeting involves talkers sharing information with listeners. Conflict resolution meetings involve someone trying to resolve a problem that they couldn't fix via other means.
2. Classify the participants. Some are players, some are pawns.
3. Identify the players.  
	If no players are at the meeting, leave.
4. Identify the pros and cons.  
	The pros are players on the winning side of the issue. The cons are players on the losing side and are upset about something. Both pros and cons must be represented at a meeting for it to make progress. Sometimes good pros camouflage themselves as cons.
5. Figure out the issue.  
	Specifically, figure out what the cons want.
6. Give the cons what they want.  
	Someone needs to synthesize things into next steps and communicate those steps to the cons. The plan doesn't have to be great, honest, or complete, but it needs to be there to create the perception of progress. Otherwise you'll find yourself in another meeting.
7. Figure out the issue.  
	If it seems like there are too many issues, someone who cares needs to distill things down to coherent issues.

## [Dissecting the Mandate](http://randsinrepose.com/archives/mandate-dissect/)
Sometimes, you have to give a mandate. When you do so, you want to move the team forward without hurting morale. There's three phases to mandating: decide, deliver, and deliver again.

### Decide
Debate is good. But sometimes your team can't move forward in the debate. When they get to the point where they confuse their emotions with the decision and the debate is no longer productive, it's time to decide something.

### Deliver
Here, you explain that a decision has been made. You do not allow the debate to fall back into another debate of issues. Like it or not, your team cannot walk out believing that there's wiggle room.

### Deliver Again
When people walk out, some of them are thinking yay, boo, or yawn. The yawners are the silent majority who just wanted to move forward, and you don't have to worry about them. For the yays and boos—the winners and losers—you need to individually re-express your reasoning. This reinforces your mandate and gives people a chance to respond. Insist on debating, especially with someone on the losing side of the issue.

### Foreign Mandates
Sometimes, you receive the mandate from a different part of the organization. Regardless of your stance, you must figure out the justification behind the morale. You will have to relay the mandate, and so you need to understand how to deliver it with its reasoning.

## Information Starvation
> One of your many jobs as a manager is information conduit, and the rules are deceptively simple: for each piece of information you see, you must correctly determine who on your team needs that piece of information to do their job.

The problem is that you'll mess this up. Gossip results from missing information. When you hear gossip, you need to listen for (1) what is actually being said and (2) what knowledge gap is being filled by the gossip.

There are a few main ways information fails to get conveyed:

- You forget to pass it on.  
	Use a notebook.
- You don't think someone else needs to know that information.  
	Ask them, rather than assuming.
- You think you're already sharing everything.
	Is the context clear of what you're sharing? Does it make sense?

A good way to get people to tell you what they need to know is to simply be silent.

## [Subtlety, Subterfuge, and Silence](http://randsinrepose.com/archives/subtlety-subterfuge-and-silence/)
When dealing with a problem, you need to think and decide what the most effective strategy to deal with that problem is.

### Subtlety
Particularly when you're painting large, top-down strokes, things might not make sense to your employees. This could happen, for example, during a performance review.

To take a subtle approach, you have to push them in the right direction without them realizing it. This means starting small and humble. It could involve, e.g., rephrasing everything you have to say to put it in terms of the objectives that the person should be moving towards.

### Subterfuge
Here, you just go behind someone's back. It's a risk, because you might lose people's trust. Once your work does get revealed, it's important to manage how people see it so that they're receptive to it, rather than feeling you ignored them entirely.

### Silence
In business, people are eager to tell you what they want or need. You just need to listen.

## [Managementese](http://randsinrepose.com/archives/managementese/)
Don't use management terms or metaphors when speaking to your employees. Use the same straightforward language that you use every day. Managementese is very efficient for conveying information between managers, but not for conveying information to your reports.

The main problem with managementese isn't that people don't understand it. They don't trust it. Don't be lazy—explain what you're thinking in a language they understand.

## [You're Not Listening](http://randsinrepose.com/archives/youre-not-listening/)
> Let's start with the most basic rule of listening: If they don't trust you, they aren't going to say shit.

You often have an agenda going into a discussion. Don't start with that. Start with a small, neutral question such as, "How are you?" The point is help you and the other person transition into a quiet, safe conversation. Show your attention with eye contact. Be careful with your gestures, as even checking the time can signal that the conversation isn't your primary concern.

Be a curious fool; assume they have something to teach you. They think you'll talk and not listen, but really you want the opposite. Treat it as a question—ask stupid questions until you find an answer where they light up.

Treat the conversation as a game. Ask stupid questions until you find an answer where they light up. When you understand what the person cares about, you can have bigger conversations and build trust.

Continually try to make sure that both of your words make sense to one another. Ask people to repeat, validate what they're saying, be redundant. You want to avoid miscommunication.

When you restate things, people will either nod or stare. If they nod, you got them. If they stare, you're off the mark. Restate again, but use more of their language.

Use segues and silence to control the tempo of the conversation. Sometimes you need to move it along. Other times you need to draw things out.

These skills are fakeable. The difference is intent—genuinely care about continuing to build trust with your people.

> It takes months of listening, but I want a professional relationship where my team willingly tells me the smallest concern or their craziest idea.

## [Fred Hates the Off-Site](http://randsinrepose.com/archives/fred-hates-it/)
Offsites exist because there's essential work that needs to be done that can't occur under normal conditions in your building. It was different back when the team was small, but now you need something for the team. There are three reasons to have an offsite.

- We need to understand who we are.  
	People lose sight of who they are and where they're going when they're in a hurry. This offsite gives people time away from their hurry to get to know each other.
- We need a new direction and/or fewer disasters.
	Sometimes you're not achieving success. This offsite lets you do deep brainstorming—finding and refining ideas, testing them, and then figuring out how to use those ideas once you come back.
- We are embarking on an epic journey.  
	This is essentially a kickoff meeting. It's about alignment—you want to explain where we're going and why we're going there.

### Offsite Characteristics
- You can't bring everyone. By definition, the team is too big. Bring the people who are capable and willing to solve the problem in front of you. It feels exclusive, but you also can't make decisions without everyone there.
- Have everyone speak. This doesn't have to be structured for everyone—some people are good at making useful spontaneous comments.
- Out of the usual building. It costs money, but you also need to not be distracted by the usual pull of business.
- Have a master of ceremonies. They move the day along, stopping and pivoting as necessary. They want progress, not just good conversations.
- Have a note taker. This is the most important role—they need to capture the *right* ideas that you want to take back and act on. You'll probably only find three ideas.
- No team-building activities, no outsiders.
- At least two days. You want people to have an inflection point where they actually engage with whatever you called the offsite for. The key ingredient here is time—let them spend time here to soak in the ideas and hit that inflection point.

An offsite is successful when you take the inflection points you had and channel them into something you can act on. If you don't drive change from your offsite, you've wasted it.

## [A Different Kind of DNA](http://randsinrepose.com/archives/dna/)
There are lots of engineers that should not be leaders. But they want to grow. How do you handle this? One approach to this is by having a flat organization, where leads or managers are the same as individuals. Are there other approaches?

The good part about the flat approach is that leads and managers are not treated differently. They aren't celebrated. Any other individual has as much right to speak their mind. But it's unclear how this scales.

Leads and managers scale. But as soon as this relationship is formed, there's a power dynamic. People don't speak up anymore, because they don't think they're also responsible for decision making.

### DNA—Design 'n' Architecture
DNA is just a meeting, with the brightest engineers from the company or team with a specific purpose. There are five main things about it:

1. It gets more eyeballs on a topic, and it makes it clear that the topic at hand is important (because so many important people are there).
2. Bring firepower. You want people who not only can design an idea, but also those who can talk about it, criticize it, and explain it to others.
3. It has teeth. To make the meeting impactful, it needs consequences. Those who don't contribute aren't invited back. If you don't bring your A game, people are allowed to mentally kick the shit out of you.
4. DNA meetings are not management related. They focus solely on technical leadership.
5. Getting to these meetings is achievable and aspirational. It's not a popularity contest—you get there on merit. This might be a combination of experience, results delivered, and visible contributions.

## An Engineering Mindset
The first thing on the management must-do list is to *stay flexible*. The second thing is paradoxically inflexible—*stop coding*.

You have to trust others to do the coding work. You need to get yourself to scale. If you're coding, you're not managing.

Actually, this is wrong. Software development evolves, and developers will be able to do more. Software development will evolve faster than you. By not coding, you're not part of the act of creation—which means you get left behind.

Be familiar with your systems—be able to draw a detailed architecture diagram. Own a small feature so you still contribute. Write tests. Be close to how software development is changing in your organization. Remember what it means to be an engineer.

## [Tear It Down](http://randsinrepose.com/archives/tear-it-down/)
There are three types of leaders.

### The Lead
Leads are at the beginning of their career for leading work. Their entire world is their team. Since they're intimately familiar with the work the team does, they are good mentors and good representatives of the team to others. 

Leads are primarily tactical, but are first starting to show strategic thinking. They're learning to delegate and learning that they have authority. They're learning that sometimes they need to interact with other areas, and those areas have their own rules.

### The Lead of Leads
The defining characteristic of a lead of leads is that they are responsible for multiple teams. The *most important* characteristic is that they run the company.

A lead of leads focuses on multiple teams, which means they are able to think across the company. They can identify how healthy different parts of the company are, then communicate this to others and figure out what to do. Leads of leads are part tactical and part strategic.

### The Director
Directors focus outward. They figure out how the company interacts with the rest of the world. They curate the vision of the company. Directors are purely strategic.

## [Titles Are Toxic](http://randsinrepose.com/archives/titles-are-toxic/)
Job names exist so you can easily describe your job. Titles, however, just exist so that someone can make an immediate judgment of your ability, based on your title. Jobs are well-defined, with clear and easy-to-understand responsibilities. Titles have neither. If a title doesn't reflect a job that has real and obvious value, it might be toxic.

As companies grow, they need to provide growth paths. There are two: the lead/management track and the people track.

The lead/management track forms because there's too many people. It's formed to organize communication and decisions. It's created from a need to improve efficiency, communication, and accountability. It's done to scale the company

The people track is driven by the need for growth. Unfortunately, people end up dealing with this by creating artificial titles to give a sense of advancement. This is when titles become toxic.

> The main problem with systems of titles is that people are erratic, chaotic messes who learn at different paces and in different ways. They can be good at or terrible at completely different things, even while doing more or less the same job. A title has no business attempting to capture the seemingly infinite ways in which individuals evolve.

Titles place concrete values on people, when in reality people have varying skills. This leads to people perceiving each other to have different values, and then leads to toxic culture.

## [Saying *No*](http://randsinrepose.com/archives/saying-no/)
Managers become crazy when they are too full of power. This happens when they are no longer questioned in their decisions.

Managers will ask for unreasonable things from the people below them. If you don't say no, you're partly to blame for the problem as well. Nos can travel in any direction—up, down, or sideways.

> Your team is collectively smarter than you simply because there are more of them. More importantly, by including them in the decision process and creating a team where they feel they can say no, you're creating trust.

# The Process is the Product
When people screw up, we end up creating process to provide structure and eliminate guessing/errors. Process creates tension between people who measure and people who create. These conflicts are needed, and process is how you build a product.

## [Understanding 1.0](http://randsinrepose.com/archives/1-0/)
When shipping a 1.0, there's a hierarchy of needs. It's similar to Maslow's hierarchy, but the pyramid is inverted. From bottom up, you need pitch, people, process, and product.

With **pitch**, you have an idea that defines everything else that you're going to do.

1. You're in a hurry. Don't forget it. You need to ship 1.0 before a competitor does.

You use your pitch to find **people**—the people who will build your 1.0 and your engineering culture. These people will slowly change your pitch. You need to be careful that your inverted pyramid doesn't fall over because things are getting too unbalanced.

2. No one is indispensible.

Next you need communication, and **process** defines communication.

3. Process defines communication.

Your pyramid should be constantly adjusting to keep itself balanced.

4. Each layer shapes and moves those near it.
5. You don't have a company until you have a product.
6. The lower (on the pyramid) the failure, the higher the cost.

## [The Process Myth](http://randsinrepose.com/archives/the-process-myth/)
Engineers "hate" process. Or rather, they hate processes that can't defend themselves. Processes can't defend themselves when they can't answer why they are needed.

Small teams don't need process because everyone is on the same page. People get it, or know who to talk to. People are all pulling to get things to succeed. Everyone feels responsible. When you stop someone on this team and ask why they do something a certain way, they can explain why it's important.

At some point the company shifts. The old guard lives and breathes the company's values. The new guard is confused because no one has explained to them the company's values. But the old guard can't put themselves in the new guard's shoes, and can't explain things because they seem obvious.

Eventually someone documents the process. This is intended to document culture and values. It's supposed to explain *why* we do something, not *how*. Process comes across as shitty because people might be good at defining the process, but not at explaining the culture.

Engineers should instinctively ask why. If you can't explain why you have a process, even if it's a good one, you've forgotten what you believe.

## [How to Start](http://randsinrepose.com/archives/a-hard-thing-is-done-by-figuring-out-how-to-start/)
Hard things are done not by starting, but by the details you learn by figuring out how to start.

## [Taking Time to Think](http://randsinrepose.com/archives/taking-time-to-think/)
When you're busy, you can't think. It's not because you're busy. It's because you're reacting.

Rest of this chapter talks about when/how to think. It's related to your projects.

## [The Value of the Soak](http://randsinrepose.com/archives/the-soak/)
Soaking is when you let your thoughts bump around and form connections. Soaks rarely occur when you're busy, because you're busy reacting. It's a protected activity with the goal of producing original thought.

### Active Soaking
Ask dumb questions—acquire information. Don't care if you seem stupid. This is how you figure out what's going on.

Then pitch a stranger. See if what you're thinking makes sense of them. This exposes issues in your thinking.

Write it down, throw it away, and write it down again. Writing gives you another way to verbalize your pitch. This will expose more gaps, so toss your work away and go answer your questions.

### Passive Soaking
Now, you need to stop actively working on your problem. Get rid of all the physical artifacts of active soaking. You won't actually stop thinking about the issue, because your brain won't let you.

## [Capturing Context](http://randsinrepose.com/archives/capturing-conte/)
Every organization has a favorite application. This could be excel, word, salesforce, etc.

An engineer's favorite application is their code editor. But their secret weapon is version control. That's because version control comes with a comment, which means you're capturing context. You should stop and capture context whenever you've created significance.

## [Trickle Theory](http://randsinrepose.com/archives/trickle-theory/)
Your internal critic is important to you because it has made good decisions for you in the past. But sometimes your critic is wrong, and the way you get something done is to jump in and just do a little bit at a time. Progress + momentum = confidence.

When you find yourself working on something too hard or dull and you get mentally blocked, stop and do something else.

## [When the Sky Falls](http://randsinrepose.com/archives/when-the-sky-falls/)
When a disaster happens, how should you handle it? First, some assumptions and clarifications:

- It's not a solo disaster. Many people are involved in resolving this.
- You're going to end up skipping something in this process. Just be aware that it has consequences.
- The steps here are described serially, but often overlap and run in parallel.
- The disaster has a decent chance of being your fault.

### The Situation
Your first job is to figure out everything about what's going on. Develop a mental model about what's happened. Ideally, you should be able to draw a complete picture of everything that's going on. Gather breadth, then depth, with the aim of answering, "Do we understand the situation?"

Understanding typically happens when:

1. The picture you're drawing represents what has actually occurred.
2. Additional research, work, and potential next actions have been figured out and prioritized, even if they haven't been acted on.
3. You get a feeling when you know what you need to do.

When you ignore this, you're making critical decisions with incomplete data. You won't be closer to fixing the disaster.

### Bet Your Car
You've missed something. You can find this out by jumping in, or you can check your math.

1. Vet your model with others—people unrelated to the issue who can understand the broad strokes. Make sure you understand the implications of your solution.
2. Share the current version of your model with those resolving the disaster and ask them if it makes sense.

When you ignore this, you're risking having to deal with unintended consequences. Would you bet your car on the outcome?

### Provide Regular Updates
Your job now is internal public relations. Have a report that you regularly send out to people. Anyone trying to get a status update gets added onto that report.

This serves to overcommunicate and quell misinformation. But it is also a way to periodically vet the progress you're making.

You should always send out status, even if there's no progress. Your goal isn't to say you've made progress—it's to say that we are constantly working to fix this issue.

### Step 0: What, precisely, are you trying to do?
Your immediate desire will always be to unfuck the situation. But that's not what you're really trying to do—figure out a credible answer to this question. Understanding what you're truly trying to fix is a cure.

## [Hacking Is Important](http://randsinrepose.com/archives/hacking-is-important/)
> Hackers are allergic to process not because they don't understand the value; they're allergic to it because it violates their core values.

## [Entropy Crushers](http://randsinrepose.com/archives/entropy-crushers/)
A good project manager elegantly and deftly handles information. They know how to gather information and they know when, how, and who to move that information to.

- **project manager**—responsible for shipping a product
- **product manager**—responsible for making sure the right product is shipped
- **program manager**—combination of both that often handles multiple interrelated projects

Every person on your team increases the cost of communication, decisions, and error correction. When you're being a manager, leading people, and a project manager simultaneously, you're going to do at least one of these roles poorly.

> As a lead, you have three jobs—people, process, and product—and you get to choose how to invest in each of those roles.

If process is the problem, is that what you want to deal with? If the answer is no, find someone to do this for you so you can protect the time that matters to you.

# Versions of You

## [Bored People Quit](http://randsinrepose.com/archives/bored-people-quit/)
When someone decides to quit, they've decided that they no longer believe in the company. At some point before that, they felt that they were bored. It's much easier to fix boredom than an absence of belief.

There are some ways to try to detect boredom:

- Changes in daily routine.
- Asking, "Are you bored?"
- They tell you they're bored.

Boredom can be easy to miss—you'll assume that it's something else. Ignoring someone's boredom causes it to become worse and worse.

You should always be able to answer two questions about each person on your team. This is true regardless of whether or not they're bored.

1. Where are they going?
2. What are you currently doing to get them there?

If you don't have answers to this, you can start to figure them out by doing a few things:

- Keep an interesting problem in front of them.
- Let them experiment.
- Don't let them take one for the team forever.
- Protect their time.
- Aggressively remove noise.
- Tell them what's going on. Let them know what you think about their current trajectory.

## Bellwethers
There are two key groups that need to interview candidates.

The first consists of everyone on the team. It's important to get everyone's perspective. It also shows that you care about everyone's opinion.

The second consists of your go-to interviewers. These are *bellwethers*. You should always respect their opinions. There are three key bellwethers to have for each interview.

The **technical bellwether** should be a technical bully. You want them to actually vet the person's technical ability. These are the people that technically scare you.

The **cultural bellwether** assesses fit within the team and fit within the company. The person for this is the person who will tell you that your candidate will add to the team, rather than detracting from the team. This bellwether best represents the soul of the team.

The **vision bellwether** figures out if the candidate meets your strategic and/or tactical needs. Strategics have their own agenda to push. Tacticals fill a need and aren't interested in pushing an agenda. The manager is often the appropriate bellwether here. They have the vision for the role, and that role defines the career path as well as what is needed to keep a future hire engaged.

Get everyone's feedback, in a collective meeting. As people give feedback, others will add more information and sometimes change their opinion. This is consensus building.

Sometimes the right hire isn't a fit for the team. You can lean on your bellwethers to help find the right people.

Even with all of this, hiring is still a risk. Use as much data as you can to make a decision, including blogs, open source, etc.

## [The Ninety-Day Interview](https://randsinrepose.com/archives/ninety-days/)
You don't know what a new job's going to be like when you accept the offer. Don't consider your interview over until you understand what everyone you work with thinks is the most noteworthy things about the job, as well as the story behind why they think it's important. The industry standard amount of time to make a hire is 90 days. You have another 90 days to finish your interview.

First, relax. You have some time. Trust your instincts less, instead trying to learn more about the job.

1. Stay late, show up early.  
	Understand people and their relation to the day. Figure out when they get there, what they do, their habits, why they do things, etc. All of this helps you understand what each of your team members wants.
2. Accept lunch invitations.  
	People are going out of their way to include you; you should reciprocate. Lunches give you stories, and you don't have any yet.
3. Ask about acronyms.
4. Say something really stupid.  
	This will happen anyway, and it's okay. Your team might find it funny, but they'll also know you're trying.
5. Have a drink.  
	Similar to lunch, but more casual and honest.

Some advanced things:

6. Tell someone what to do.  
	This is a test of your influence and your understanding of the organization. Trust your instincts.
7. Have an argument.  
	This is the most risky. You're aiming to understand how the organization values conflict. How do they deal with issues? How do they make decisions? How passionate are they?
8. Find your inner circle.  
	Find the people who are similar to you. These people will help you refine your stories.

You have to do all of this because the you who interviewed is not the same you as an employee. You've finished your job interviews when you've asked questions, heard stories, understand the organizational and communication structures you've found yourself in, and have a framework for how you're going to interact with people.

## [Managing Nerds](https://randsinrepose.com/archives/managing-nerds/)
Ideally, if you're managing nerds you are a nerd.

Nerds are *system thinkers*. They're motivated by figuring out how something works. They believe that understanding a system brings calm and order to things—so you need to bring calm to things that are chaotic.

They value consistency. They're constantly evaluating your decisions with respect to the other decisions they've seen. Don't make off-the-cuff decisions lightly, as they might conflict with your previous actions. Nerds value this because it creates predictability.

Nerds also value efficiency. They want to be able to find an efficient, consistent way to navigate through the chaos in the world.

Nerds have two highs. One, when they see a knot, they want to unravel it. Two, they want to dominate all knots—when they get upset with the amount of friction involved with something, they'll aim to recreate it entirely without friction.

The first high is the joy of understanding. The second high is the act of creation. Nerds get paid for the latter. You enable them to do so by protecting their time and space.

Getting to either high involves being in the Zone. There are tons of things that can prevent someone from entering the zone, and nerds are constantly trying to manage the stuff that prevents them from getting into it. Every moment someone stays in the zone is a chance for a great idea to occur. Nerds build figurative caves, designed to protect their focus (e.g. headphones, hoodies, etc.).

Nerds suffer from some side effects. They prefer to build things themselves, rather than using someone else's solution. They're well-informed, and will become bitter when they feel like someone is wasting their time. When they're disinterested or drifting, it means they are stuck and either can't or don't want to engage.

## NADD

## [A Nerd in a Cave](https://randsinrepose.com/archives/a-nerd-in-a-cave/)

## [Meeting Creatures](https://randsinrepose.com/archives/meeting-creatur/)
You should understand the types of people who will be in your meetings, and what they will do.

### Anchors
The anchor is the person in charge, ultimately responsible for making a decision. Everyone in the meeting listens to them.

If you need something from a meeting, you must identify this person. You can find them by looking at who everyone looks at when something controversial is said.

If no anchor is present, reschedule the meeting.

### Laptop Larry
Larry is on his computer and trying to be productive. In reality, he's not being productive and not listening. Ask him to put his computer away so he will focus.

### Mr. Irrelevant
This person has nothing to add, and is happy to just be at the meeting. They're mostly harmless.

However, someone invited him. Before you exclude them from future meetings, figure out who that was and why they wanted him there. Figure out if they actually need to be there.

### Chatty Patty
As named, they don't stop talking. They might be irrelevant—figure out if they should be there.

You then need to decide if their signal-to-noise ratio is acceptable. You want to contain her. Do this by asking wordy questions with a precise focus. Otherwise she'll verbally wander. Don't argue with a Chatty Patty at a meeting.

### Translator Tim
Tim is a utility creature—he knows how to talk to everyone at the meeting. He's essential when meetings are very cross-functional, and different types of people need to communicate with one another. 

Figure out if Tim is biased, and in whose favor. If it's not yours, consider finding yourself a Tim.

### Sally Synthesizer
Sally is also a utility creature—she ends meetings. She follows the discussion and understands the core points of what was actually discussed. She knows not just what's been said, but who said it and why that's significant.

If Sally's biased, it's in her favor. Sally is a problem if she thinks she's the anchor.

### Curveball Kurt
You don't understand what Kurt is talking about. Figure out first if he's Mr. Irrelevant.

Ideally you have a Tim or Sally to help out. Also ideally, Kurt is not your anchor.

### The Snake
Some anchors are hidden. The person you thought was the anchor wants to here from someone else's opinion, and their opinion is quite important.

## [Incrementalists and Completionists](https://randsinrepose.com/archives/incrementalists-completionists/)
When solving problems, some people take an incremental approach while others take a completionist approach.

Incrementalists are realists. They understand what's achievable and what constraints are present.

Completionists are dreamers. They know the right way to solve a problem, and want to solve it so it doesn't become a problem again in the future.

You want to meet somewhere in the middle. Exactly where depends on what is needed. 

Incrementalists need vision. They're driven by how much stuff they have to do. They move a lot, but they might not always know what direction they're going in. You want them to see and understand the direction.

Completionists need action. They can see solutions at many different timespans, each of which is radically different. They need help to get a solution started.

One personality has everything to get something done, but doesn't know what to do. The other knows what to do, but doesn't know how to do it. You want to couple these types—when they understand each others' strengths, you have strategy and tatctics on your team.

## [Organics and Mechanics](https://randsinrepose.com/archives/organics-and-mechanics/)
To understand how to talk to your manager, you have to understand how they acquire information. Typically, this is either *organically* or *mechanically*. You need both—mechanics lack inspiration and organics are chaotic.

Mechanics have a very structured approach to problems. Detailed notes, methodical approach, carefully gathering information. They are very predictable.

Organics are all over the places, asking questions and making random connections.

With mechanics, give them information in a structured, well-known, consistent manner. With organics, trust that they have a plan—they have a better picture than it seems because they are better networked.

Mechanical organics are the best possibility—they gather information organically but organize it mechanically. They know everything and don't forget it. 

Organic mechanics are scary—they have lots of knowledge, but there's no clear thread that ties it together. There is a thread, but you'll never know what it is.

## [Inwards, Outwards, and Holistics](https://randsinrepose.com/archives/inwards-outwards-and-holistics/)

## [The Wolf](https://randsinrepose.com/archives/the-wolf/)
Wolves are the engineers that work outside of existing processes, but appearingly suffer no consequences. They understand and use "the system," even if they think it's a joke. They generate disproportionate value with their ability to identify and work on projects essential to the company's future.

Wolves avoid the limitations of a group of people building at scale. They avoid process and have exceptional engineering ability, enabling them to be really productive. 

## [Free Electrons](https://randsinrepose.com/archives/free-electron/)
> The free electron is the single most productive engineer that you're ever going to meet.

They can do anything when it comes to code. Senior electrons are politically and socially aware. CTOs often fall in this category. Junior electrons all have ability, but don't have the experience of dealing with people. Junior electrons are the best possible hires you can make.

Keep free electrons engaged. Their value is in doing research for the organization, not development. They must be defining the bleeding edge for the company. Left to develop, they'll leave.

## [The Old Guard](https://randsinrepose.com/archives/the-old-guard/)

## [Rules for the Reorg](https://randsinrepose.com/archives/reorgs_for_the_average_joe/)

## [An Unexpected Connection](https://randsinrepose.com/archives/an-unexpected-connection/)

## [Avoiding the Fez](https://randsinrepose.com/archives/avoiding-the-fez/)
How can you create an objective opinion of someone's performance? You need to reflect, and the model here is skill vs will.

Skill is an employee's ability to do their job. Are they qualified, overqualified, etc. Are they learning something new? How quickly do they handle tasks, relative to others?

Will is an employee's desire. This is if they like their job, if they're energetic, if they generate great ideas. How much do they talk at meetings?

You want to push employees to be high skill and high will. The two are related—they can form either a negative or a positive feedback loop.

## [A Glimpse and a Hook](https://randsinrepose.com/archives/a-glimpse-and-a-hook/)
Resume design.

## Nailing the Phone Screen

## [Your Resignation Checklist](https://randsinrepose.com/archives/your_resignationlayoff_checklist/)

1. Don't promise what you can't do.
2. Respect your network.
3. Update "The Crew."
4. Don't take cheap shots.
5. Do right by those who work for you and with you.
6. Don't volunteer to do work after you leave (or if you do, make sure you get a lot of money for it).
7. Don't give too much notice.

## [Shields Down](https://randsinrepose.com/archives/shields-down/)
When people leave, figure out why they're leaving and when their shields went down. Ask them when they started looking for a different job.

## [Chaotic, Beautiful Snowflakes](https://randsinrepose.com/archives/chaotic-beautiful-snowflakes/)