# Toyota Kata

## Introduction
> With that in mind, here is my definition of _management:_  
> _The systematic pursuit of desired conditions by utilizing human capabilities in a concerted way._

Management involves adjusting to unpredictable, dynamic conditions and satisfying customers.

## Part I: The Situation
### 1. What Defines a Company That Thrives Long Term?
Despite trying to copy Toyota for 20 years, the industry hasn't been able to catch up to Toyota's levels of performance. People have been trying to _reverse engineer_ Toyota practices, but it hasn't been working well for three reasons.

One, the important things that Toyota does aren't always visible. This doesn't mean secretive; it means things such as the way their managers think and act. Two, reverse engineering the solution to a problem isn't beneficial—you want to reverse engineer the _process_ that helped derive that solution. Toyota constantly derives new production techniques that solve the problems Toyota is facing at the time—it is this ability that you want to replicate. 

Third, reverse engineering puts people in _implementation mode_, where they are trying to follow a clear set of steps to reach an outcome. Instead, you want to be in a _DIY problem solving mode_. Here, you know where you are, where you're going, and the means (approach) to get there. You don't know the problems you will face. The path is unclear, and you will have to discover and resolve those approaches. We are inclined to prefer certainty, but blindly following an implementation plan can lead us somewhere we don't want to be because we don't know how to react to issues we see along the way.

The environments in many organizations tend to have similar elements:

- Conditions inside and outside of the organization are always evolving, even though they can appear steady.
- We can't predict how things will change.
- Catching up to competitors is difficult. If it were easy, people wouldn't fall behind as much or as far.

The way to combat this is to have small, incremental, continual improvement. The sum of these small steps is what leads to greatness. Be aware that the default state of an organization is _not improving_. Even when an organization takes effort to improve, it's either not continuous or it uses standards to prevent backsliding. Improvement needs to be the natural state of things.

There will always be little (or big) problems that people are forced to work _around_ in order to meet their goals, quotas, etc. They are unable to deeply investigate a problem. It appears like the problem is with the people who aren't ensuring quality with their work, but the real problem is with management who isn't designing a conducive system.

Everyone has a different idea of what to improve. Additionally, complex environments can make it hard to predict what should be changed. An organization should use systematic procedures, routines, and methods to help guide people. These methods should provide specific patterns for sensing, adapting, and improving.

#### Kata
These patterns and routines are called _kata_ in Japan. Kata are a way of keeping two things in alignment or synchronization.

Kata differ from principles. A principle does not say how or what to do; it only helps people make choices. Principles are derived from repeated actions. Katas help guide people into doing repeated actions.

We want a kata that brings about continuous improvement and adaptation. What properties would such a kata need to have?

- It has to operate at the process level. Improvements typically happen at the detail or process level.
- The kata must be embedded and inseparable from daily work, as we want improvement every day.
- The kata is content neutral. It has a prescribed method, but not content. This allows the kata to be used in any situation or environment.
- The kata, when possible, relies on facts rather than opinions or judgments. Depersonalizing the kata helps protect against inaccurate or impartial human judgment.
- The kata should survive beyond a particular leader—everyone should use it.

## Part II: Know Yourself
### 2. How Are We Approaching Process Improvement?
Currently process improvement tends to occur via workshops, valuestream mapping, and action-item lists. These all do not contribute much, if at all, to improving processes. This is especially true for action-item lists, which are widely used.

When improving a process, Toyota has people change only one thing at a time. They then chec, the result against the expected result. This allows people to better understand work processes.

### 3. Philosophy and Direction
Some questions to evaluate how important improvement is in your organization:

- Do I view improvement as legitimate work, or as an add-on to my real job?
- Is improvement a periodic, add-on project (a campaign), or the core activity?
- Is it acceptable in our company to work on improvement occasionally?

Many companies view improvement as something to do in addition to daily management. Toyota sees daily management as process improvement. Many of us would be afraid to focus on improvement at the cost of production. However, if processes are improved systematically, desired outcomes should occur on their own.

Direction is important, as it gives perspective to evaluate which conflicting priority is more important. Sometimes there will be a local extrema that conflicts with the desired direction. At many companies they say that they can't change existing processes because that would result in worse efficiency. At Toyota, they say that moving in the new direction isn't optional and instead view this additional inefficiency as an obstacle that needs to be overcome.

This also applies to cost/benefit analysis (CBA). Rather than using CBA to decide whether or not to do something based on financial upside, Toyota uses CBA to determine method. If a proposal is too costly, CBA allows Toyota to say that they need to find a way to do it more cheaply.

In other words, Toyota maintains its vision and views everything else as constraints that must be addressed to get there. Other companies find out there's an obstacle and respond by changing their direction.

### 4. Origin and Effects of Our Current Management Approach
Ford had a vision (contiguous flow) and, as Toyota does, iterated to move closer toward that vision. They couldn't get this working, because of some incorrect approaches, changing demand, changing workflows, etc. As the Model T was no longer what people wanted, experiments to improve factory flow and have continuous improvement ended. Ford had a vision (production flow ideal), but didn't put enough into product development as well as systematic organization and company management.

General Motors focused a lot of developing systematic management and structuring the organization. They had three new concepts, relevant to this book:

- rate-of-return decision making  
  GM used CBA or return-on-investment (ROI) calculations to decide what to do, as opposed to Ford who followed a vision. Making money became the overall direction.
- maximizing output of individual processes  
  GM kept different segments and departments as islands and maximized output for each department. They viewed efficient bulk production as the key to low costs.
- centralized planning and control based on managerial accounting data  
  Divisions operated decentrally, but there was centralized operational decision making and control. This control was done through quantitative targets and performance metrics.

GM's approach led to great success for their market conditions in the 1960s. Their principles have spread and are so pervasive that they are close to invisible today.

- rate-of-return decision making  
  This approach works well in growing markets where you can choose which business opportunities to go after. It's less effective in crowded or low-growth marketplaces. The low-hanging, profitable fruit have already been picked, so management now has to nurture promising processes, products, and situations in order to achieve profitability.
- maximizing output of individual processes  
  - A process become more decoupled from the next process as it tries to produce more.
  - Changeovers interrupt production, resulting in avoidance of changeovers.
  - Excess product must be stored as in-process inventory (work in progress).
  - Defects are discovered too late to effectively trace their causes.
- centralized planning and control based on managerial accounting data  
  Management tends to lose connection with the work floor, resulting in a lack of understanding of the situation. This results in incorrect assumptions and decisions, as well as adaptations that are too slow. Management will also receive numbers that make the corresponding divisions look as good as possible, which means that management might not have the most accurate data to make good decisions with.

You want objectives, but also to have a plan for the means by which you'll accomplish those objectives. Over time, this has shifted into "management by results" where things are good if the quantitative targets set by those above are reached. This worked for a while because their was more waste-tolerance, profitable market choices, room to cut bloat, and slowly-improving competition.

## Part III: The Improvement Kata: How Toyota Continuously Improves
### 5. Planning: Establishing a Target Condition
Target conditions describe a desired future state. It should answer questions such as:

- How should this process operate?
- What is the intended normal pattern?
- What situation do we want to have in place at a specific point in time in the future?
- Where do we want to be next?

There are four techniques that can be looked at from the perspective of target conditions.

#### Takt Time
Takt time: the effective operating time per shift, divided by the quantity of items that customers require from the process during that time period. This gives the amount of time, on average, it needs to take to produce each item. The planned cycle time, or actual intended cycle time, is typically faster.

When takt time is a target condition, there are two ways to reach it: produce more consistently per planned cycle time and move planned cycle time closer to takt time. 

With the former, there is always variation in the amount of time it takes to produce an item each cycle. Measuring this fluctuation allows you to then ask what the range of fluctuation should be. You can then observe the process and figure out how to identify, understand, and eliminate obstacles.

Once that's consistent, you can slow down your planned cycle time to be closer to takt time. This will cause new obstacles to appear, which are the most important ones to fix.

#### 1 x 1 Production (continous flow)
Having buffer allows you to "make production." Toyota views this as negative as problems go unresolved and processes go into a firefighting cycle. Intentionally designing things so that you can't take advantage of buffer will highlight problems that, if solved, will be real process improvements.

#### Heijunka (leveling production)
Leveling production involves leveling two things: mix and quantity. Leveling the mix involves making sure that every item is made. Leveling the quantity involves making sure that the max amount of an item produced is at most the average demand for that period.

In particular, if you have 8 orders for a particular item but your leveling says the max is 7, you would make 7 and have the last item left over for the next cycle. This is because uneven assembly will result in amplified demand in upstream processes—such as inventory. By avoiding demand spikes and keeping things consistent, the inventory on hand doesn't change. In other words, heijunka is the smoothing of production activities.

Using heijunka as a target condition allows you to evaluate whether or not your schedule is possible that day. If no, figure out what's preventing it. You might go off script for the day, but the aim is to fix problems so that eventually you end up back on track.

#### Kanban (pull systems)
The purpose of kanban is to, as with other processes, create a pattern of work moving through the value stream. As such, it's important that you're able to specify things like which parts will be used with which machine. This adds rigidity, but when you're brushing against the rigidity you get exposed to a problem that can be improved.

#### Target Conditions
It's important to distinguish between a target and a target condition. Targets are outcomes. Target conditions are descriptions of how a process is desired to operate. Target conditions help accomplish targets.

Target conditions should be treated as challenges. It's good to have to say that you don't know how to accomplish a condition.

At least some aspects of a target condition should be measurable so that you can determine whether or not they've been reached. Target conditions are not steps—they do not describe how to get to a condition.

Manufacturing process target conditions tend to have four categories of information:

1. Process steps, sequence, and times  
  Sequence of steps to complete a cycle through the entire process, how long each step should take, and who should perform the step.
2. Process characteristics  
  - number of operators
  - number of shifts
  - where 1x1 flow is planned
  - where buffers are held, and intended buffer quantity
  - lot size, changeover times
  - heijunka
3. Process metrics  
  Metrics for checking process condition in short time increments, in real time, while the process is running. E.g. cycle time per step/piece/unit, amount of fluctuation per cycle.
4. Outcome metrics  
  - number of pieces produced per time interval
  - productivity
  - quality indicators
  - cost
  - fluctuation in output from shift to shift

When you have a vague area, it's hard to come up with a detailed target condition. Set up a basic, general one instead and fill in details as you find obstacles. Err on the side of being vague.

#### Standardized Work
Whether or not work is standardized is also a condition—and thus something that can also be managed. A leader should be considering whether or not work is standardized and working towards getting things there. This isn't to drive discipline, accountability, or control. It's to have a _reference point_. Without a standard, you can't make good comparisons.

### 6. Problem Solving and Adapting: Moving Toward a Target Condition
Setting a target condition is the first part of the improvement kata. The other part is figuring out how to get past the obstacles along the way.

Assume the path is unclear. Toyota works through this unclear path by taking small, rapid steps. They use a plan-do-check-act cycle.

1. Plan  
   Define expected actions and outcomes. This is a hypothesis.
2. Do (Try)  
   Run the process according to your plan and see what happens.
3. Check  
   Interpret the results. Did the actual outcome meet the expected outcome?
4. Act  
   Standardize and stabilize if you have something working. Otherwise, do another PDCA cycle.

PDCA can fail if the check step comes too late to learn anything or if the target condition only specifies an outcome. The target condition should specify the steps of a process, the sequence, and the times; process characteristics; process metrics; and outcome metrics.

People often say things along the lines of, "Let's see if this will work." This isn't a good thing to say—because it will probably fail, then you'll return to the status quo. You want to orient people to think beyond that. It likely won't work—let's see what we need to do to make it work.

In fact, we want to find problems. Not having problems means we've stagnated. If we aren't finding problems, it means either people aren't finding them or we should raise our bar so that we find new classes of problems.

At Toyota, the philosophy is to understand a problem so deeply that the solution is obvious. We're often quick to add countermeasures without understanding what it is that we're countering.

#### The Five Questions
These questions summarize Toyota's approach for moving forward toward a target condition.

1. What is the target condition? (the challenge)
2. What is the actual condition now?
3. What obstacles are now preventing you from reaching the target condition? Which one are you addressing now?
4. What is your next step?
5. When can we go and see what we have learned from taking that step?

## Part IV: The Coaching Kata: How Toyota Teaches the Improvement Kata
> How do we teach everyone in the organization the improvement kata?  
>  
> How do we ensure people are engaged in the improvement process and utilize the improvement kata correctly in their daily work? How will we know what skills individuals need to work on? How do we ensure that appropriate challenges/target conditions are developed?  
>   
> How do we ensure that the PDCA cycle is carried out correctly and effectively?  
>   
> How will we ensure that leaders have a grasp of the true situation at the process level in the organization?  
>   
> How will we pass on the improvement kata from generation to generation?

### 7. Who Carries Out Process Improvement at Toyota?
Who should carry out process improvements? Three problematic answers: the process operator, leave it to chance, or have a special team. In particular, process operators aren't able to spend their time fully running a process and also focus on improving it. Instead, Toyota relies on team leaders, group leaders, superintendents, and manufacturing engineers to apply and coach the improvement kata. This takes over 50% of their work time.

How Toyota deals with process problems:

1. The response to process abnormalities should be immediate.  
  - This keeps the trail warm and facilitates learning.   
  - This helps prevent small problems from accumulating.    
  - Responding immediately may still allow us to adjust and meet our goals.   
  - This creates consistency between what we say (quality is important) and what we do.   
2. The response to process abnormalities should come from someone other than the production operators.  
  - The production operators cannot simulatneously make parts and think about the process.
  - Team leaders respond to every abnormality, but typically do problem solving activity for repeating problems.

### 8. The Coaching Kata: Leaders as Teachers
Toyota generally avoids classroom-type exercises for teaching. They believe in learning through actually doing.

Toyota will set up mentor/mentee relationships for every person in the company to learn. This isn't always by hierarchy.

As part of the mentor/mentee dialogue, the mentee is intended to learn to figure things out. This often starts with a vague assignment, need, or challenge from the mentor. The mentor then asks the mentee for how they would approach the issue. This helps the mentor understand the mentee's thinking.

The mentor will then follow up based on what the mentee says. This can sometimes be as simple as asking them to think about something more, or ask why they want to do it a certain way. After cycles of this lead to a point where the mentor is happy, the mentee's role switches to planning and doing PDCA cycles.

The mentor isn't trying to guide the mentee to a particular solution. They're trying to understand how the mentee is thinking and is approaching the situation, guiding them through the improvement kata. The best, although most bittersweet, praise for the mentor is for the mentee to feel that they learned and achieved the target condition on their own.

Toyota's Practical Problem Solving

1. Pick Up the Problem (Problem Consciousness)  
  - Identify the problem that is the priority.
2. Grasp the Situation (Go and See)  
  - Clarify the problem  
    - What should be happening?  
    - What is actually happening?  
    - Break the problem into individual problems if necessary.  
  - If necessary use temporary measures to contain the abnormal occurrence until the root cause can be addressed.  
  - Locate the **point of cause** of the problem. Do not go into cause investigation until you find the point of cause.  
  - Grasp the tendency of the abnormal occurrence at the point of cause.  
3. Investigate Causes  
  - Identify and confirm the direct cause of the abnormal occurrence.  
  - Conduct a 5-Why investigation to build a chain of cause/effect relationships to the root cause.  
  - Stop at the cause that must be addressed to prevent recurrence.  
4. Develop and Test countermeasures  
  - Take one specific action to address the root cause.  
  - Try to change only one factor at a time, so you can see correlation.  
5. Follow Up  
  - Monitor and confirm results.  
  - Standaridize successful countermeasures.  
  - Reflect. What did we learn during this problem-solving process?  

## Part V

## Appendices
### Appendix 1: Where Do You Start with the Improvement Kata?
You can't apply it everywhere at once; you have to start at some process.

Toyota starts with a pacemaker process. A **pacemaker process** is a downstream loop in a value stream that is dedicated to a family of products and where that family of products if finished for the external customer. Toyota focuses on these because variance affects the external customer and produces demand fluctuations for upstream processes.

### Appendix 2: Process Analysis
Process analysis is for understanding current process conditions, not identifying problems or solutions. This analysis gets you the information you need to figure out the next target condition. It's not a hunt for waste.

First, get a basic overview of the value stream.

1. Which value stream (product family) have you selected?
2. What are the processing steps?
3. For each process step, is it dedicated or shared?
4. At what points along the value stream is inventory kept?
5. How does each process know what to produce (information flow)?
6. At what processes are changeovers needed?  
   What is the changeover time, lot size, number of changeovers per day, and estimated EPEI (every product every interval)?
7. What are the "loops" in this value stream?  
   Which loop is the pacemaker loop?
8. With a one to two year time horizon in mind, where:  
   Do you think 1x1 flow should be possible?  
   Do you think inventory should be replaced with a pull or FIFO system?

Now, focus on one process in the value stream—usually the pacemaker loop. This may involve reviewing or recalculating earlier steps as you need a deeper understanding.

- Assess customer demand and determine line pace
  - Customer takt
    - average customer demand rate = effective operating time of process / quantity of customer items required during that time
    - Can change over time
  - Planned cycle time
    - actual speed that line needs to run
    - planned cycle time (Pc/t) = takt time - changeover time, unplanned downtime, scrap/rework rates, etc.
- First impressions of the process
  - Sketch a block diagram of the process.
  - Is thre a 1x1 flow?
  - Are each operator's work steps the same from cycle to cycle?
  - Is line output consistent?
- Is machine capacity sufficient?
  - Can the equipment support the planned cycle time?
  - What is current capacity?
  - How many shifts?
- Is the process stable?
  - Time 20 to 40 full cycles of each operator's work.
- What is the necessary number of operators if the process were stable?
  - Calculate number of operators

