# Poor Charlie's Almanack
Charles Munger
[Amazon](https://www.amazon.com/Poor-Charlies-Almanack-Charles-Expanded/dp/1578645018)

## Chapter Four: Eleven Talks
### 4. Practical Thought About Practical Thought?
> In this talk, Charlie explains how he makes decisions and solves problems by taking us step-by-step through a diverse set of "mental models." He presents a case study that asks rhetorically how the listener would go about producing a $2 trillion business from scratch, using as his example Coca-Cola. Naturally, he has his own solution, apt to strike you as both brilliant and perceptive.
>
> Charlie's case study leads him to a discussion of academia's failures and its record of having produced generations of sloppy decision-makers. For this problem, he has other solutions.
>
> [...]
>
> Editor's warning as suggested by Charlie: Most people don't understand this talk. Charlie says it was an extreme communication failure when made, and people have since found it difficult to understand even when read slowly, twice. To Charlie, these outcomes have "profound educational implications."

Five helpful notions for solving problems:

1. Simplify problems by deciding big "no-brainer" questions first.
2. Use math. Without grounding some of your thought with numbers, you won't be able to make accurate conclusions.
3. Think through problems forwards and backwards.
4. The best and most practical wisdom is elementary academic wisdom, if you think across disciplines. Use the easy-to-learn concepts from intro level courses in every basic subject.  
    Don't rely entirely on others for your thinking, otherwise you will run into trouble. Others' thinking will be subconsciously biased and misaligned, and will lead you astray if you use it without consideration.
5. Lollapalooza effects often only come from large combinations of factors.

