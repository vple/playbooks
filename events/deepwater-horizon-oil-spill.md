# Deepwater Horizon Oil Spill (BP Oil Spill)

## Communication
Tony Wayward, BP CEO, tried to show that he cared. His message wasn't received; he was frustrated and the message was received as callous. This was influenced by comments such as, "You know, I'd like my life back."

## Sources
### Informational
-	[Wikipedia](https://en.wikipedia.org/wiki/Deepwater_Horizon_oil_spill)

### References
-	The Power of Communication