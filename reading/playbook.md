# Reading Playbook

## Reading Effectively

### Learn How to Read

Mortimer Adler's book, [*How to Read a Book*](https://www.amazon.com/How-Read-Book-Classic-Intelligent/dp/0671212095), teaches how to read a book for understanding.

This book covers a number of topics, including:

- [Different Reading Levels](https://fs.blog/how-to-read-a-book/)
- [Being a Demanding Reader](https://fs.blog/2013/06/the-art-of-reading-how-to-be-a-demanding-reader/)
- [Writing in Margins](https://fs.blog/2015/01/marginalia/)

#### Reading Levels

- [Inspectional Reading](https://fs.blog/2013/06/the-art-of-reading-inspectional-reading/) 
- [Analytical Reading]()

### Retention & Understanding Techniques

- [Blank Sheet](https://fs.blog/2013/05/how-to-retain-more-of-what-you-read/)
- [Feynman Technique](https://fs.blog/2012/04/learn-anything-faster-with-the-feynman-technique/)

## Resources

- [*How to Read a Book*](https://www.amazon.com/How-Read-Book-Classic-Intelligent/dp/0671212095), Mortimer Adler
- https://fs.blog/reading/