# Irrationality
These are behaviors or effects relating to irrational behavior or misjudgment.

## Confirmation Bias
This occurs when we aim to seek out things that confirm your beliefs, rather than things that disprove them. Filtering out or not seeking contradicting facts can leave us with a flawed belief that hurts us later on.

Confirmation bias shows up in a few common ways:

- When asking for advice, we might disregard or discredit advice that doesn't match up with what we were hoping to hear.
- When looking to confirm is something is true, we look for more examples that match our expectations.
- When presented with an opposing point, we look to explain it away.

Handling confirmation bias in ourselves involves seeking out ways to disprove our beliefs. We can find ways in which our belief is too broad, which happens when certain implications of our belief are false. Or we can find ways in which our belief is too narrow, where our belief doesn't adequately explain some other related behavior or effect. It can be hard to disprove a belief if we don't understand what factors we can test that contribute to that belief.

Handling confirmation bias in others is tricky, as by definition they are inclined to disbelieve you. You can try a counterexample, which has three possible outcomes: (a) they realize their belief is wrong, (b) they find a way to refute it, or (c) they still hold onto their belief, but have yet to find a way to handle their cognitive dissonance. Regardless, if the outcome wasn't (a), it's unlikely that you'll get anywhere productive with a logical argument.

In that case, it's likely easiest to avoid having to deal with that topic. If it can't be avoided, you might be able to change their mind by influencing their belief—finding ways to either diminish it or to have them start doubting their belief.

## Conviction Bias
This is believing in something because we feel strongly about it. There's no actual justification. In addition to occurring in ourselves, we are influenced by others' convictions, particularly leaders or people in power positions. Their conviction in an idea leads us to believe that it's true or correct.

We can recognize conviction bias when someone isn't able to justify why they believe something. They stubbornly hold on to the belief, giving nonsensical reasons as to why they actually believe it.

When this occurs in ourselves, we address it by facing the conviction head on—why do we believe this? Conviction can easily be motivated by a denial of something else—we believe in something so we don't have to believe or accept some other idea or fact.

Conviction bias can be broken with a counterexample, but it often has to be a very vivid and clear counterexample. If the contradiction isn't staring someone in the face, they can choose to not engage with the counterexample. By not facing facts, their belief stays intact.

## Appearance Bias
TODO—handle this as a result of the inconsistency-avoidancy tendency.

## Group Bias
This is where we take on beliefs from a group, believing that they are our own. The underlying influence behind this bias is that we want to belong to something, rather than being isolated. If you share a belief with a peer group, the group bias may be at play.

The remedy to group bias is just to realize that your belief might come from the group, rather than yourself. You don't necessarily have to change your belief. Instead, externalize it from yourself so that it's something that you can actually critique and examine.

Changing another individual's bias likely involves presenting them with a stronger or more compelling belief than their existing one. Their beliefs may also change if the group's beliefs change, but that can be particular hard to do. 

## Blame Bias
This is where we prefer to blame others rather than ourselves, since blaming ourselves means admitting that we have faults.

This can be addressed in ourselves by looking to first look to ourselves to find why things went wrong. Before looking for external issues, we should genuinely find something that we could have done differently.

Typically, you won't have to change someone else's bias. Leaving them to their beliefs typically doesn't affect you, since you're not acting on their beliefs. You would be affected if either (a) you're the blame target or (b) you need to work cooperatively based on their belief. 

In the first case, you often don't have to change their mind. If it's just a 1-1 situation, you can walk away from the situation. If it involves a group, you will often have to defend / prove yourself to the group, not to the person accusing you.

In the second case, you can sidestep the blame by reframing the situation in terms of a more important goal. In particular, focusing on what can be done to acheive positive results in the future will often force them to look for actionable reasons as to what happened.

## Superiority Bias
We tend to assume that we're better than others. In particular, we tend to believe that others have more faults than we do. We see ourselves as the protagonist of our own stories, so we must have some noble motivations or innate abilities.

The way to address this bias, in both ourselves and others, is to try to humanize the inferior person. Recognize that they are imperfect people trying to make the best decisions that they can, rather than people who are less capable or who are acting for nefarious reasons.

In particular, aim to get both people on the same side, rather than in an us versus them mentality. If you're on the inferior end, this is done by asking *for advice* on how to handle a particular situation. Ideally, the other person will realize that they haven't evaluated your position fairly. However, if they don't, you're parrying their superiority—they'll believe that they're using their superior knowledge or ability to help you improve. This also helps them take your side, since your future success will justify what they taught you.

