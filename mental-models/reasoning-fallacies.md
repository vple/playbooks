# Reasoning Fallacies
Things that lead to mis-reasoning about something.

## Conjunction Fallacy
The **conjunction fallacy** occurs when you believe a combination of conditions makes something more likely, even though it is actually less likely.

> Linda is 31 years old, single, outspoken, and very bright. She majored in philosophy. As a student, she was deeply concerned with issues of discrimination and social justice, and also participated in anti-nuclear demonstrations.
>
> Which is more probably?
>
> 1. Linda is a bank teller.
> 2. Linda is a bank teller and is active in the feminist movement.

(1) is more likely, the conjunction fallacy leads people to say (2). The set of all female bank tellers is larger than the set of all female bank tellers who are also active in the feminist movement. So it's more likely that Linda is a bank teller.

The conjunction fallacy was published by Amos Tversky and Daniel Kahneman.

## Overfitting
**Overfitting** occurs when using too many assumptions in an attempt to more "precisely" explain events. It results in an overly complicated explanation. This explanation may explain the specific scenario or data that you're looking at, but will likely be a poor explanation for a similar but different set of data.

Overfitting is a concept from statistics.