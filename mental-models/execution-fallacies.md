# Execution Fallacies
Ways that you can blunder execution of something.

### Premature Optimization
**Premature optimization** is a fallacy that occurs when you try to optimize a solution or approach before an optimization is needed. If your approach is incorrect or your optimizations aren't necessary, you've wasted effort.

Premature optimization comes from computer science and refers to when code or algorithms are optimized before they need to be.