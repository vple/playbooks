# Perception
These models are significantly influenced by or related to perception. This can be a first party or third party perspective, or even a more manufactured perception. Many of these models can be applied for other activities or concepts, such as reasoning.

## Frame Of Reference
Your **frame of reference** is how you see the world. You'll perceive objects, actions, and events in a way that's filtered to your experiences and current situation. Critically, *your frame of reference may differ from others' frames of reference*.

Other people may perceive objects, actions, and events in the same way you do. Or, they may perceive them in a completely different way. When you need objectivity, you need to account for your frame of reference—identify what differs from others' frames of reference.

Frames of reference are a concept from physics, where an object's speed (and other properties) is perceived differently from different perspectives. It's a key concept behind Einstein's theory of relativity.

## Framing
**Framing** is how a situation or explanation is presented. Framing can occur when one person presents a situation to another, or when someone is perceiving is a situation.

Framing can be used to make something seem more or less palatable. Conversely, things that are presented to you are likely to have been framed as well—you should consider any bias or spin.

## Nudging
**Nudging** is where subtle cues lead to more substantial behavior. These might be word choice, visual appearances, specials, etc.

Nudges are used to point people in a certain direction. They don't *force* behavior, but they make it more likely by making it more visible. Nudges can be positive or negative—it depends on how they are used.

## Anchoring
**Anchoring** is the tendency to over-rely on first impressions when making decisions. The first piece of information received skews how we interpret following information.

For example, if a product's suggested price is $100, you're likely to think it's worth around that much.

## Availability Bias
**Availability bias** occurs when you believe something due to information that has recently been made available to you.

The reason this is bias is that there's often additional information—the remaining context. Because you don't have the full picture, your reasoning and decisions will be skewed.

## Filter Bubble
A **filter bubble** tends to occur when a feed shows results tailored for you. The feed will remove results that it believes you're less interested in. These also tend to be results that disagree with your point of view. The result is that you have a bubble of viewpoints that support your own beliefs.

## Echo Chambers
An **echo chamber** occurs when people with similar filter bubbles interact with one another. The same ideas and results will bounce between these people. This reduces exposure to alternate viewpoints and makes it appear that these ideas or results are more widely believed than they may actually be.

## Third Story
The **third story** in a conflict is the one that a third, impartial observer would give.

Being aware of the third story helps keep you aware of what is actually going on in a situation. This in turn helps you make fewer biased or incorrect judgments, as well as help you be more self-aware in the moment.

To see the third story, it can help to imagine a recording of the situation, then to think about what an outside audience would think as they saw it.

## Most Respectful Interpretation (MRI)
The **most respectful interpretation** tactic is to choose to interpret another party's actions in the most respectful way possible. In other words, you give them the benefit of the doubt.

## Hanlon's Razor
Never attribute to malice that which is adequately explained by carelessness.

## Fundamental Attribution Error
The **fundamental attribution error** occurs when you attribute a behavior to the incorrect root cause. It often occurs when attributing a behavior to someone's personality or internal motivations, rather than external factors.

## Self-Serving Bias
The **self-serving bias** is where you have self-serving reasons that justify your behavior, but blame others' intrinsic traits when they show similar behavior.

## Veil Of Ignorance
The **veil of ignorance** is a technique used when thinking about how society should be. You should imagine yourself ignorant of your current place in the world. Instead, you should imagine how you'd fare if you were in a different social position.

For example, you could consider being of the opposite sex, being a single parent, being old, etc.

The veil of ignorance is an idea from philosopher John Rawls.

## Birth Lottery
The **birth lottery** refers to the social group you were born into.

A significant portion of your life is determined by luck, based on your birth.

## Just World Hypothesis
The **just world hypothesis** is a belief that the world is fair, orderly, and predictable. People get what they deserve, good and bad, as a result of their actions.

Believing in the just world hypothesis often results in errors in judgment. This is because you are more likely to make generalizations about why certain things happen, yet these generalizations are wrong at the individual level.

TODO: Add Charlie Munger's take.

## Victim-Blame
**Victim-blaming** is where the victim of some action is assumed to be responsible for their misfortune.

This belief can stem from the just world hypothesis: because something bad happened to someone, they must have done something to deserve it.

## Learned Helplessness
**Learned helplessness** is the tendency to stop trying to escape difficult or adverse situations because someone has gotten used to being in these difficult or adverse situations.

Learned helplessness can be overcome when people start to see that their actions can make a difference. It's not just for extreme situations; learned helplessness is also in play whenever someone thinks they can't do or learn something.

Learned helplessness comes from an experiment from Martin Seligman in which he compared reactions of different groups of dogs to electric shocks.