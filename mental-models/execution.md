# Execution
Models for successfully executing on something.

## Minimum Viable Product (MVP)
A **minimum viable product** consists of the very minimum you need in order to launch a product. You frequently won't know what is needed from your product—rather than building functionality that isn't needed, ship the smallest needed and see how your customers respond.

MVP doesn't apply just to products. It can apply towards any venture that you're working on.

Minimum viable products come from startups, which are forced to ship and react quickly in order to find something worth building before they run out of funding.