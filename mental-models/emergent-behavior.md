# Emergent Behavior
These are systems, models, and concepts that cause something to develop properties or effects that aren't present in its individual parts. The overall property or effects are often not even codified or explicitly designed for in the system's individual parts.

## Critical Mass
In physics, **critical mass** is the mass of nuclear material needed to create a critical state where nuclear chain reactions are possible.

As a mental model, critical mass refers to hitting a tipping point that enables a process or behavior to occur. Often, it's used to mean hitting a tipping point for a process *that also sustains itself*.