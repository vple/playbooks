# Performance
Mental models relating to how well something performs.

## Antifragile
> Some things benefit from shocks; they thrive and grow when exposed to volatility, randomness, disorder, and stressors and love adventure, risk, and uncertainty. Yet, in spite of the ubiquity of the phenomenon, there is no word for the exact opposite of fragile. Let us call it antifragile.
>
> Antifragility is beyond resilience or robustness. The resilient resists shocks and stays the same; the antifragile gets better.

Coined by Nassim Nicholas Taleb in his book, *Antifragile*.