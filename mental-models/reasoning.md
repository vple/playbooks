# Reasoning
These are methods for approaching and tackling problems.

## Additive Reconstruction
By recreating something piece by piece, we can understand how it works.

Sometimes we have a blueprint and the corresponding materials to build something. But we might not be able to understand how to assemble the blueprint or how to understand the purpose of different parts of the blueprint. With additive reconstruction, we guess and try to put the blueprint together piece by piece. With each step, we check and see if our current result makes sense—does it have the properties we expect in our final result? In this way, going through the process of building something helps us understand how it works.

Fred Sanger used this technique to invent Sanger sequencing, a method for sequencing DNA. He didn't have the ability to add one base at a time. Instead, rather than using only the normal DNA bases, A, T, C, and G, he added some modified bases that would stop additional replication. This way, when proteins replicated DNA, they were forced to stop when they used a modified base. This allowed him to figure out the base at position *n*, thus building up the sequence one base at a time.

This is also the technique used in a thorough reading or debugging of code. Each line of code is stepped through. With each step, the reader pauses to figure out the exact state of the current environment. In this way, they can build up an understanding of a piece of code line by line.

## Subtractive Isolation
By removing bits and pieces, we can simplify complex situations. 

When we remove something, there are two possible outcomes—nothing changes or something changes. When nothing changes, the thing we removed is unnecessary and unrelated to the effect we're studying. When something changes, we get hints as to what the removed piece is responsible for.

In cell biology and genetics, this is used to isolate components that influence cell or genetic behavior. For example, is removing a protein has no effect on a certain cellular function, that protein cannot be responsible for that behavior. Repeated application of this allows us to figure out exactly what component is responsible for what behavior. It might also suggest how the component affects that behavior.

## Inversion
Rather than thinking about a specific problem, consider the inverse of the problem.

By focusing on the opposite of your goal, we can realize solutions or strategies that might not otherwise be apparent. For example, rather than looking how to gain wealth by making more money, you can look at how to gain wealth by losing less money.

Inversion is often credited to mathematician Carl Jacobi, famous for his saying, "Invert, always invert."

## Unforced Errors
An **unforced error** is where you make a mistake due to your own poor judgment or execution. To improve at something, you need to make fewer unforced errors in that area.

Unforced errors typically come up in sports.

## Argue From First Principles
**Arguing from first principles** involves structuring thought around basic building blocks that you perceive to be true. The *first principles* are self-evident assumptions that are the foundation for your following conclusinos.

Being able to reason from first principles make the difference between being able to tackle and solve problems on your own, versus requiring some sort of template, reference, or crutch to be able to solve a problem. This also allows you to adapt and combine principles to come up with new and sound ideas. This also gives you exposure to how other ideas are reasoned, allowing you to apply that logic to similar situations.

Reasoning with first principles involves deliberately starting from scratch. Starting from scratch allows you to avoid conventional beliefs, which may be incorrect. The end result may match up with conventional wisdom, but doing the reasoning yourself will give you a much stronger understanding of the subject.

## De-Risking
**De-risking** involves evaluating your assumptions against the real world. This lets you test your assumptions, confirming whether or not they are actually correct. De-risking may also involve further breaking down your assumptions.

When de-risking, it's most important to focus on the assumptions that are crucial for success and that you're least certain about. If that assumption is ignored and ends up being false, your approach will fail. After identifying these critical assumptions, you need to test them.

## Occam's Razor
**Occam's razor**—the simpler explanation is more likely to be true.

Occam's razor is about ignoring unnecessary assumptions to simplify thinking. It's not always correct—it's just a starting point when reasoning about something complex.