# Ethopian Coffee

## Nura Korate
- Brooklyn Roasting Company
- Origin: Sidama Region, Ethopia
- Altitude: 5500 ft
- Varietal: Indigenous Heirloom
- Process: Fully Washed
- Roast: 3/10
- Notes: Lemon, Flowers, Cocoa
- First tried: October 2020