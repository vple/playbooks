# Japan Things To Do

## Hiroshima
Hiroshima's known for its oysters, sake, and okonomiyaki. Okonomiyaki are savory pancakes, with a variety of ingredients.

Miyajima Island listed separately, below.

### Cultural
##### Shukkei-en Garden
##### Hiroshima Castle
A recreation of Hiroshima Castle. Most of the area is free to walk around, except for the main castle itself.
##### Atomic Bomb Dome
The remains of a building that survived the atomic bomb.
##### Children's Peace Monument
A girl that survived the atomic bomb died around a decade later due to radiation poisoning. Schoolchildren around Japan contributed to this monument in her memory and to speak up for peace.
##### Peace Memorial Park
A memorial for those who died from the atomic bomb. It lines up with the Peace Flame and the Atomic Bomb Dome.

### Food
##### Gariber (Recommended)
Japanese curry shop. Solid, favored by locals.
##### Guttsuri-ann (Recommended)
Cozy restaurant with fresh seafood. The lady running the place is very nice and speaks fluent English (she studied abroad in Oregon). Cash-only, call after 4:30 PM to make a reservation in English. Reservation not required, but the place can be popular.

Although it's open reasonably late, they will run out of certain dishes on busy days.

## Miyajima (Itsukushima) Island
One of the most common things to do near Hiroshima is visit Miyajima Island. You can get there by a combination of train and ferry (free with JR). Good for a day trip.

The main attraction on the island is the Floating Torii Gate. It doesn't actually float, but appears to do so at high tide. Around low tide, the water has receded enough that you can actually walk over to the gate. High tide and low tide times will be posted around the island, but you should check in advance to plan your timing. You don't have to be there exactly at high / low tide.

You should probably skip eating (or eat a really small breakfast) when going to Miyajima. There's tons of street food, many of which feature the oysters that Hiroshima/Miyajima are known for. In addition to oysters, a signature Miyajima dish is anagomeshi—broiled conger eel on rice. 

There's also a mountain if you want to go hiking. The signs say that it's about 2 hours to the top.

## Osaka
### Food
##### Okonomiyaki Mizuno
An okonomiyaki restaurant. One of their signatures is to use yam-based noodles rather than normal noodles. Won a Michelin star in 2016. No reservation; expect around an hour wait to get in.

### Bars
##### *Bar Nayuta*
Cocktail bar.

## Kyoto
Kyoto's most known for its cultural attractions. It's also known for tea—in addition to tea shops, you can find tea ceremonies and even go tea picking. There's also a reasonable amount of nightlife, a lot of which is around the Nishiki Market / Gion area.

### Arashiyama
Arashiyama's located on the western side of Kyoto, and is most famous for its bamboo grove. It's also home to a number of small temples and shrines, as well as a monkey park. There are lots of street food vendors along the main streets—look for bamboo flavors.
#### Arashiyama Bamboo Grove (Highly Recommended)
#### *Arashiyama Monkey Park Iwatayama*
As it sounds, a monkey park. You're able to feed the monkeys here.

#### Azuma Tea Farm (Highly Recommended)
This is a small booth near one of the main streets. They have samples of almost all of their teas, and the teas are quite tasty and cheap. Most of the bags sell for about 500 yen; their roasted green tea bag is 200g. The booth isn't obvious to find; it's near a place called Burari Arashiyama.

#### Steak Otsuka (Highly Recommended)
A steak restaurant, only open for lunch. Although all their steaks are good, they're most known because they (sometimes) have Murasawa beef. Murasawa beef comes from cows raised by a Mr. Murasawa, who only sells 80 cows per year. Since this beef is so hard to find, it's nicknamed phantom beef. Even if they're out of Murasawa beef, you can get premium cuts of Wagyu and Hirai (a famous Kyoto beef).

Supposedly there's an online reservation, but only for 6 people per day. More realistically, you'll have to put your name down on the waitlist. They bring the waitlist out at 9 AM, even though the restaurant only opens at 11. You're highly recommended to go there early to get your name down, then go out and explore Arashiyama. You need to be back by the time your name is called, otherwise you'll lose your spot.

If you show up at 9, you'll be in the first or second wave of people to eat (I put my name down around 10:30 and was in the 3rd wave). There are people who show up before 9 (and just wait for the list to come out). The restaurant seats around 20-30 I think. It probably takes around 30 minutes per meal. You can definitely return later if you're in a later wave, but you should be conservative.

#### *Taisho HANANA*
A very popular restaurant in the area, they're known for their sea bream. There was a huge line for lunch, but supposedly it's not as bad if you can go earlier or later.

### Fushimi Ward
#### *Fushimi Inari Taisha*
One of the iconic places to visit in Kyoto. It's a Shinto shrine, known for its hundreds of traditional gates.

### Higashiyama Ward
#### Bar Ixey (Highly Recommend)
A cocktail bar. The bartender's specializes in homemade liqueurs.
#### Gion

#### Kiyomizu-dera (Recommended)
One of the big temples to visit. It's a Buddhist temple, quite large, and has some great views.

#### *kyocafe chacha*

#### Yasaka Shrine

### Nakagyo Ward
#### *Fukujuen Kyoto Main Store*
A tea shop, with a cafe, buying area, and ceremony area.

#### Ippodo Tea (Highly Recommended)
One of the biggest tea names in Japan; this is their flagship store. Inside, there's a buying area, a cafe, and a tasting area. In the cafe, they'll teach you how to make the tea you've selected, including a technique for estimating water temperatures. I believe you're limited to how much tea you can taste in the tasting area. Ippodo is a premium brand, and their tea prices reflect that.

#### *Jiki Miyazawa*
A popular kaiseki restaurant. I found about this through word of mouth—I believe it has/had a michelin star. You can make English reservations online, and probably have to reserve a bit in advance.

#### Nijo Castle
A castle in the middle of the city. I don't know how nice it is during the day, but I'd recommend dropping by if they happen to have a nighttime illumination.

#### *Nishiki Market*
A street market. One of the biggest things to do in Kyoto, outside of the temples and shrines.

#### WEEKENDERS COFFEE TOMINOKOJI (Recommended)
A small coffee shop tucked away in the back of a parking area. It's a few blocks away from Nishiki Market.

### Shimogyo Ward
#### Wajouryoumen Sugari (Recommended)
A popular noodle shop, mainly known for its tsukemen (dipping ramen). There's no English sign explaining where the restaurant is, but you can see through the (slatted) storefront that it's a noodle shop. The door is directly to the left of the storefront window; the door's on the short side. Open for lunch and dinner.

### Coffee, Snacks, Desserts

## Nara
Nara is one of the most common places to visit near Kyoto. It's most known for the free-roaming deer that have learned to bow (in the hope of being fed).

When visiting Nara, avoid keeping papers / bags out in the open—some of the more aggressive deer may steal it from you. Most of the deer are nice, but the more aggressive ones may nip at your clothes or try to grab the food in your hand prematurely. You can buy some wafers to feed the deer at various stands throughout the area—they're 150 yen for 10 wafers, which is plenty for one person.

There are no food vendors in the main park area. There are lots of vendors on the streets between the station and the park; most of the recommendations below are there as well.

### Cultural
#### Isuien Garden
A nice, small garden on the western side of the main park area. The entrance is from the west, outside of the main park. It's a little pricey compared to other Japanese gardens, but it's still nice.
#### *Kasugataishashinen Manyo Botanical Gardens*
#### *Todaiji*
Probably the most popular cultural attraction in Nara. It's a Buddhist temple known for the largest bronze Buddha statue in Japan.
#### *Yoshikien Garden*
A garden next to Isuien Garden.

### Food
#### Edogawa Grilled Eel (Recommended)
A well-known restaurant in the area for its unagi (freshwater eel) dishes. Delicious, but a bit pricier. Accepts credit cards.

### Snacks
#### *Kitamachi Tof N' Donuts*
Tofu donuts. Supposedly the tofu gives the donuts a lighter taste.

#### *Kotaro*
This shop sells taiyaki—a Japanese sweet shaped like a tai (Japanese seabream). The inside is filled with azuki (red bean paste).

#### Nakatanidou (Recommended)
Freshly-made, sweet red bean mochi. The full production process is on display here—notably, people are excited to see the dough-pounding process. Nakatanidou is located on one of the main streets from the station to the park, so it's pretty easy to find.

## Tokyo
Because Tokyo is so large, you'll probably want to plan based on which area you want to spend (part) of your day in. It's easy to get around by subway, but you'll usually want to knock out things in a general area.

### Harajuku
An area most known for Japanese teenage fashion. Also known for crepes. Harajuku's also next to a park and Meiji Jingu.

#### Cafes, Snacks, Desserts
##### Deus Ex Machina Cafe Harajuku
Hip cafe with a surfer aesthetic. Looks like there's also a bar with live music in the downstairs seating area.
##### GOMAYA KUKI (Highly Recommended)
Rich, sesame-flavored ice cream shop. They have six total flavors, three black and three white. Really, really good.
##### *The Zoo*
Ice cream cones designed to look like animals.

### Ikebukuro
#### Pokemon Center Mega Tokyo (Recommended)
#### Sunshine Aquarium (Recommended)
A rooftop aquarium. 
#### Yong Xiang Sheng Jian Guan (Highly Recommend)
A small dumpling spot located just northwest of Ikebukuro station with pan-fried soup dumplings that's popular for the locals. A good spot to grab some food to go.
#### *Nakiryu*
Michelin-starred ramen restaurant. Recommended to try the tan tan men.

### Shibuya

#### Cafes, Snacks, Desserts
##### *MiLKs*
Dessert restaurant known for its ice cream. Has some boozy offerings as well.
##### Nanaya
Ice cream shop known for its matcha gelato, advertised as the most matcha-intense ice cream in the world (probably true). It's good, but if you don't like matcha it won't be your thing.
##### SILKREAM (Recommended)
Shop that sells Cremia, a really silky, smooth ice cream. Cremia is actually sold at various places, so you don't actually have to go here if you happen to see it elsewhere. Worth dropping by if you're in the area.

#### Food
##### Niku Yokocho (Meat Alley)
This is actually several meat-focused izakayas. The staff will try to get you to sit at their table, so you might want to look around to figure out what you want. There's a cover charge, which varies per restaurant.

#### Bars
##### *Bar Tram*
Cocktail bar. Sister bar with Bar Trench and Bar Triad.
##### Bar Trench (Highly Recommended)
Cozy cocktail bar. I believe their specialty is absinthe. Can get filled since it's so small, but there's not really a line either if you want to wait a bit. Sister bar with Bar Triad and Bar Tram.
##### *Bar Triad*
Cocktail bar. Sister bar with Bar Trench and Bar Tram.

#### Shopping
##### Shibuya 109
Department store.

### Shinjuku
#### Cultural
##### Shinjuku Gyoen National Garden (Recommended)
Large park with a few gardens, lakes, and lots of different types of trees / flowers. I would assume they have certain seasonal flowers blooming during every season.

#### Cafes, Snacks, Desserts
##### Hoshino Coffee
I think they're known for their namesake coffee, which looked like some premium or gourmet coffee. Seemed a bit pricey, but also seems decently popular.

#### Bars
##### Golden Gai
Actually a street with quite a large number of bars.
##### Bar Asyl (Highly Recommended)
Dive-y whiskey bar in the Golden Gai. The owner, Abe-chan, has a number of (Japanese) whiskeys at a surprisingly low price. (In retrospect, it's probably due to alcohol being cheaper in Japan.) He has his tasting notes written on many of his bottles. Rather than trying to pick your own whiskeys, I'd recommend just telling him the flavor profile you like and letting him pick. Also has some nice/fun music mixes.

### Sugamo
#### Food
##### Tsuta Ramen (Recommended)
Michelin-starred ramen. They have a ticketing system—you show up in the morning and buy a ticket for an available time slot for 1000 yen. You return at that time, where you thing exchange your ticket back for your 1000 yen and order your ramen as usual. I showed up around 7:30 on a weekday (Monday?) and there were plenty of tickets left. There is still a return wait (for me, about 45 minutes).

The ramen itself is really good. They use truffle in their ramen, and the (sho-yu) dish is designed to be very clean-tasting. They use soba for their noodles.

Definitely worth doing, but too much of a hassle to do multiple times (it doesn't help that there's not much in the immediate area). You can buy kits of their ramen to eat at home, which (I hear) are actually quite good. These have to be ordered at the same time that you're placing your order.

### Toyosu
#### The Bowl Steakhouse
Restaurant located right next to teamLab Planets. They get their seafood from the fish market.

#### *teamLab Borderless*
An extremely popular interactive museum. Reserve tickets in advance. Technically not Toyosu, but it's nearby.

#### teamLab Planets (Highly Recommended)
Another interactive museum from teamLab. Although it's not as popular as Borderless, it's definitely worth checking out. This is a temporary exhibit.

#### *Toyosu Fish Market*
Japan's largest fish market, formerly located / known as Nishiki Fish Market. Known for their tuna auction and the nearby vendors that use fresh seafood from the market.

If you're going to go here, you likely need to plan your entire morning around it. The hours are very early and the touristy parts are very popular.

##### *Sushi Dai*
Located in Toyosu Fish Market, this is one of the most well-known sushi restaurants in Japan. They don't take reservations and you'll need to wait in line, in addition to getting there very early in the morning.