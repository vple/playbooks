# Japan Overview

## Logistics
### Money
Most places in Japan operate on a cash-only basis. Big credit cards should work.

Japan also uses IC cards, a refillable payment card. These can be bought at stations. They're primarily used for public transit, but can also be used at a variety of vendors (such as at convenience stores).

### Intercity Travel
#### Japan Rail (JR) Pass
It's recommended to get a Japan Rail (JR) pass, which will give you unlimited access to all JR lines in Japan.

Public transit in Japan is run by different companies. JR is a pretty ubiquitous one, and getting a JR pass will allow you to easily take trains between cities. JR also runs some subway lines, so you'll be able to take some subways for free.

You can get a pass by purchasing online. You'll receive an exchange voucher that you then have to exchange at a station (you'll need your passport at the station). You may also be able to buy the pass at a station.

At all of the JR gates, there will be a gate staffed by someone from JR. Show them your JR pass and they'll let you through.

#### Train
Japan runs several trains between major cities. It's recommended to get a JR pass (above) if you'll be traveling between cities.

You should install the JapanTravel app, which is useful for trips between cities. It's less useful within a city (Google maps is usually good there). In the JapanTravel app, you can set departure times as well as which lines to search for. (You probably want to set that to JR if you have a JR pass.)

Japan also runs bullet trains (shinkansen), which are faster trains between cities. These aren't really different from normal trains, but they'll often have their own section in the station.

Most trains have a reserved portion and a non-reserved portion. The non-reserved cars (often 1-3 or 1-5, depending on train) will be shown on the electronic boards at each platform. Non-reserved cars may be crowded at peak hours. You can reserve a seat by talking to an agent, either at the gate or at the ticket office. It's free (with the JR pass).

### Intracity Travel
#### Public Transit
Japan's public transit is quite good and well-priced, though they can get packed at times. In major cities, you should be able to easily get by with walking, subways, and buses. There are often airport shuttles as well.

Public transit is pay-on-exit. Subways operate on distance, so you'll need to tap your IC card on entrance and exit. Buses are often flat fare, and you can pay via IC card or cash on exit. You don't get change back on the bus, so having some smaller coins can be useful. If you're out of coins, the pay machine should also have a bill slot for coin exchanges.

Some subway lines are operated by JR. You can use your JR pass to take those for free.

#### Cabs
Cabs are quite reliable and trustworthy in Japan. You don't need to worry about getting scammed. Most cab drivers don't really speak English, so you should have the address you want to go to.

The rear left passenger door can be opened/closed remotely by the driver, and they'll often do so. Other doors are opened by hand.

Uber and other ride-sharing services aren't allowed in Japan. There is a cab hailing app, but it's not really needed.

### Communication
You can get by easily in major cities without knowing any Japanese. There are English signs/translations practically everywhere. Most public transit announcements are done in both Japanese and English.

Vendors will know varying degrees of English, and restaurants will often have an English menu. Regardless, pointing at what you want works fine.

### Stations
In Japan, train/subway stations are often also shopping/eating areas. Eating at the station is quite reasonable—the food can actually be quite good and will be mediocre at worst.

## Cultural
### Customs
- Japan is mostly left-oriented (drive on left side of the street, walk on the left, etc.). There are some exceptions, but they don't really follow any convention.
- You typically pay at the register. Restaurants will typically leave your check at the table; bring it to the register to pay.
- You can ask for the check by making an X with your index fingers. The "signing your receipt" motion to ask for the check isn't really a thing in Japan.

### Etiquette
- There's no tipping in Japan.