# Travel Playbook

## Logistics
### Money
Do you need cash? How much? Will your credit card work?

Does the country have its own region-specific payment system?

### Intercity Travel
How are you going to travel between cities? Will you fly, or will you use some other sort of transit such as trains? Is there a pass you can get that gives you flexible travel?

### Intracity Travel
How will you travel within the city? Is it preferred to walk, drive, cab, take the subway, etc.? Which methods are reliable and/or trustworthy?

What are the typical rates you'll have to pay for a trip?

### Data
How will you be online during the trip? Will you get a SIM card, rely on wifi, etc.? How much will this cost and where will you buy the needed supplies?

### Lodging
Where will you be staying? How will you get to your place from an airport or your arrival point?

### Group
Does everyone in your group have similar travel preferences? If not, how will you handle this? There are a number of dimensions here:
- Locations: urban, rural
- Activities: urban, cultural, touristy, nightlife
- Price point
- Ideal sleep schedule
- Pace: lots of activities, no plans from day to day, etc.

Additionally, is your group an appropriate size for your trip?

### Communication
How will you communicate with locals? How much of the language will you need to know, and how much of your language will they know?

## Itinerary
### Cities
Which cities are you going to visit, and in what order? Will you have enough time in each city? How much time will you lose to travel?

### Activities
Do your activities require any prep, such as reservations? What days or times are they available?

## Cultural
### Customs
Are there any everyday customs or behaviors you should be aware of?

### Etiquette
Is there anything polite / rude you should be aware of?