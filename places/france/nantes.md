# Nantes

### Cultural
#### Les Machines de l'île
Has large, mechanical animals. Not sure what's inside if you pay, but you can watch the mechanical elephant for free. Just need to find it. The elephant is one of the biggest things Nantes is known for.

#### Rue Crébillon
A street with a bunch of shops.

#### Passage Pommeraye
Covered passageway off of Rue Crébillon, also with a bunch of shops.

#### Talensac Market (Highly Recommend)
One of the best markets in Nantes. Has several buchers, cheese shops, fruit shops, bakeries, etc. One of the good/popular cheese shops there is **Beillevaire**. We also got some prosciutto from La Jambonnerie Nantaise.

These were the cheeses/meats that we had, all of which we liked:

- Jambon du Parma (Parma prosciutto?), La Jambonnerie Nantaise
- Camembert di Buffala, La Jambonnerie Nantaise
- Picodon d'Ardeche, Beillevaire
  - picodon = goats milk cheese, made in southern France near the Rhône
  - picodon d'ardeche = most common variety, with more acidity
  - https://en.wikipedia.org/wiki/Picodon
- Le Beret Savoyard
  - I think it's a brand of a fairly popular (?) cheese. Couldn't find much info in English online.

### Restaurants
#### COQUE
It's decent but I didn't think it was special. However, it is open on Sundays.

#### ***Cuit Lu Cru***
Needs a reservation.

#### *L Uni*

#### Le Crabe Marteau (Recommend)
A concept restaurant where you break crabs with hammers (wooden mallets). Pretty fun, and the crab seems pretty fresh—has good flavor out of the shell.

#### ***L'Atlantide 1874***
Michelin starred.

### Sweets
#### Amorino (Highly Recommend)
Probably known for their flower-shaped cones, the ice cream here has very strong flavors.