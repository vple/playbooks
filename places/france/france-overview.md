# France Overview
## Terminology
- **arrondisement**—borough
- **gare**—station

## Cultural
### Closed Dates
Many places are closed on Sundays and Mondays. Many places are also closed or have different hours from late July throughout August—you probably don't want to visit in August if you're looking for certain shops/vineyards. These different hours aren't reflected online.

### Tipping
- Tipping is not expected in most places. You might tip for a good meal, either rounding up or 5-10%. You might also tip a cab driver who helps you with your bags, a euro or two per bag.

## Old Planning Docs
- https://docs.google.com/document/d/1w2PYZf75G5wuG-zu9jbdCp4_tqw6TOr-Nrp7fo7LlxI/edit#heading=h.egwkqx43315v

## Food
### Markets
Many cities have markets (marchés), open in the morning until around 1 PM. They'll typically have bakeries, cheeses, meats, seafood, fruits and vegetables, etc. The quality is great and most things are cheaper than you'd find in the US. These are definitely worth going to, particularly if they're near—you can get a solid, quality breakfast and try out the local foods.