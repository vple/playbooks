# Paris
## Overview
Paris zip codes are all 750XX, with the last two digits indicating the arrondisement (borough).

## 1st Arrondisement
### Bakeries
#### Le Meurice (Recommend)
This is actually a hotel, but on the side there's a bakery. They make small cakes that look like fruits. Quite expensive, but probably worth taking a look inside if you're in the area.

#### *Pierre Hermé Paris*

#### *Sébastien Gaudard Pâtisserie*

## 2nd Arrondisement
### Bakeries
#### Fou de Pâtisserie
Pastry shop with pastries from several famous chefs.

#### Stohrer (Recommend)
Oldest bakery in Paris. They serve more traditional pastries—very rich. Known for their baba rum. They also have a good croissant.

## 3rd Arrondisement
### Restaurants
#### Candelaria
Small Mexican restaurant. Solid if you want Mexican food.

### Sweets
#### *Jean-Paul Hevin*
Closed in early August.

#### La Chocolaterie de Jacques Genin (Recommend)

#### Scoop Me A Cookie (Recommend)
Pricey but delicious cookies.

## 4th Arrondisement
### Cultural
#### The Centre Pompidou
Variety of art exhibits, including the National Museum of Modern Art.

## 5th Arrondisement
### Bakeries
#### Odette Paris
Known for their cream puffs. Tasty, but I didn't find them that amazing. They have a few shops here and there.

### Other
#### Shakespeare & Company

## 6th Arrondisement
### Bakeries
#### Poilâne (Recommend)
Seemed popular with the locals. Known for their boule, although the bread we got seemed kind of dry. Also known for their shortbread cookies.

### Restaurants
#### *La Jacobine*
Was closed in early August.

#### Le Procope
Solid place that you can get into without a reservation. Slightly pricey, ended up at €90 for a glass of wine, two appetizers, and two mains. Food was pretty good—we had the snails, duck breast, and beef cheek. They were all good, although the beef cheek was very underseasoned. Worth going to if you're looking for dinner in the area and don't mind spending a bit more.

## 7th Arrondisement
### Cultural
#### Eiffel Tower

### Bakeries
#### *La Pâtisserie des Rêves*

## 8th Arrondisement
### Cultural
#### Arc de Triomphe
#### Champs-Élysees Avenue

## 9th Arrondisement
### Bakeries
#### *Gontran Cherrier*

## 10th Arrondisement
### Bakeries
#### ***Du Pain et des Idées***
Known for their snail-shaped croissant pastry, but their croissants are also supposed to be good. Closed in early August.

## 11th Arrondisement
Supposedly this borough has a lot of good food.

### Cultural
#### *Atelier des Lumières*
Museum, online tickets only.

### Bakeries
#### *Boulangerie Utopie*
#### Dupain (Highly Recommend)
3rd best baguette in Paris in 2017. At €1.10 a baguette, it's worth getting a few. Nice and crusty.

### Cafés
#### DonAntónia Pastelaria
Café near Ten Belles. Looked like they had several nice sandwiches in addition to pastries, if you're looking for a lunch stop.

#### *Muscovado*
Good place for a hearty breakfast?

#### Ten Belles (Recommend)
Worth dropping by if you're in the area and looking for coffee. Known for their chocolate chip cookies as well, but they were out when we got there.

### Restaurants
#### ***Astier***
Old-timey bistro fare. Known for their cheese course. Probably need a reservation.

#### ***Chez Aline***
Sandwich shop, open only for lunch. Apparently has amazing, hearty sandwiches.

#### *Clamato*
More casual, seafood-oriented restaurant. Might need a reservation, but you might be able to get in if you're there at opening.

#### ***Le Clown Bar***
Supposedly their foie gras / duck pastry thing is quite good. Not sure if reservations are needed.

#### ***Le Servan***
Simple, seasonal food with exceptionally-valued pricing. Has a Michelin star as well? Reservation probably required.

#### *Septime*
Must book exactly 3 weeks in advance for a reservation.

### Sweets
#### *Chocolate Alain Ducasse*
Closed in early August.

## 12th Arrondisement
### Bakeries
#### ***Ble Sucre***
The owner is a former head pastry chef at a 5 star hotel. Their signatures include iced madeleines, millefeuilles, and kouign amann. Also has one of the best croissants. Closed in early August.

## 16th Arrondisement
### Bakeries
#### Pastry Cyril Lignac
Signature pastry is their Equinoxe.