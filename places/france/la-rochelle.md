# La Rochelle
Smaller, slower-paced seaside town. I believe it's a popular place to vacation to for the French, but it's not so well-known otherwise.

There's two main things here—La Rochelle and the Île de Ré, the isle nearby. The island is probably a day trip, and ferries don't run continuously throughout the day. You will probalby need to plan your trip around which places you want to go to, and on which days.

One of the things they make on Île de Ré is pineau des charentes—it's a wine and cognac blend. The one we had was with rosé and was actually quite sweet/nice, kind of reminiscent of Japanese fruit wines.

The tourist center has some maps with places worth checking out.

You'll usually have to call a cab company to get a cab, and there aren't many companies.

### Cultural
#### La Rochelle Aquarium
Seemed like a fairly standard aquarium. If you're into it and have 2-3 hours, it's nice to go to. The line was not so bad when we went around 11 or 12, but it was pretty long when we left. Lots of kids here.

#### Le Parc Charruyer (Recommend)
A park a bit west of the central part of the town. There's a mini-zoo towards the southern end. It mostly has birds, but there's a few other animals too.

### Markets
#### Artisans Craft Market
Summer (mid-June to mid-September) artisan market along the waterfront. Takes place daily in Vieux Port, on these streets:

- Place de la Chaîne
- Cours des Dames 
- Quai des Sardiniers

#### Central Market (Highly Recommend)
1 Rue Gambetta

The central market is the biggest one and has a daily food market. On Wednesdays and Fridays it spills out onto the main street. Regional specialties include:

- Chabichou du Poitou (creamy goat cheese)
- Marans red eggs
- seafood (mussels, oysters, clams, squid)

La Bergerie (fromagerie) was staffed by a nice, friendly guy who was fluent in English.

#### *Minimes Market*
Avenue de la Capitainerie

During the summer, Fridays only.

#### *Place de Verdun Market*
Friday market. Look out for local honey, sweet brioche, homemade yogurt, homemade cognac.

### Restaurants
#### BDKAF
Coffee shop.

#### CM (Recommend)
Nice fish & chips spot that's close to the station. Friendly staff.

#### Coquillages et Crustacés (Highly Recommend)
Nice seafood restaurant near a rocky beach. It's actually in L'Houmeau, which is a bit away from La Rochelle.

### Sweets
#### ***La Martinière***
Ice cream shop on the island with apparently some quite interesting flavors.