# Bordeaux
## Vineyards
### Saint-Émilion
Named after some monk, Saint-Émilion is a village that's now been declared as a UNESCO World Heritage Site. 

All Saint-Émilion wines are (required to be) reds, and typically consist of merlot, cabernet franc, and cabernet sauvignon grapes. Saint-Émilion wines are typically blends. A significant part of the terroir here relates to the clay and limestone in the area.

There is a Saint-Émilion wine classification system, done about every 10 years. At the top is *Premier Grand Cru*, divided into *Classé A* and *Classé B*. Next are *Grand Cru Classé* vineyards. The lowest level is *Grand Cru*, which just means that the winery follows the rules and regulations. *Grand Cru* wines are generally not as good as the other tiers, but this isn't always the case—some wineries choose not to be classified. The classification is based off of the actual wine, terroir, and reputation in the marketplace.

#### Château Soutard
Corperation-owned winery, rated Grand Cru Classé as of 2012. They're aiming to get to Premier Grand Cru Classé B at the next classification.

They make two labels a year. Their first label is called Château Soutard, and their second label is called (I think) Château Petit Soutard. Their first label is made of merlot, carbernet franc, cabernet sauvignon, and a very small percentage of malbec. It's fermented in wooden vats and aged in wood barrels. Second label grapes are aged in stainless stell vats.

#### Château Côte de Baleau
Family-owned winery, promoted to Grand Cru Classé as of 2012. Part of their philosophy is to make wines that you don't have to wait long to drink.

This winery uses concrete fermentation vats.

Their first label is Château Côte de Baleau. I enjoyed their pairing with sausage; it would go well with meats. 

Their second label is Château les Roches Blanches, and is made entirely without woodden containers. This results in fewer tannins, making it lighter and fruitier. I enjoyed the pairing with comte cheese.

### Cultural
#### Basilique de Saint-Michel (Basicila or St. Michael)
#### Le Miroir d'eau
Water mirror. It's actually some kind of surface with a thin layer of water on top, which then reflects stuff. Pretty overrated; not nearly as exciting as it sounds. Lots of kids playing around.
#### Porte de Bourgogne
It's an arch.
#### Porte Cailhau
Historic city gate.

### Markets
#### Marché des Capucins (Recommend)
One of the bigger markets in Bordeaux. It has a few restaurants inside, so you can also come and eat here. Open daily.

### Restaurants
#### La Tupina (Highly Recommend)
Pretty rich, classic French cooking.

#### Le Cabanon Marin (Recommend)
River-facing seafood restaurant, known for their seafood platters and soup. You'll probably need a reservation unless you show up around when they open (we got there at 7:30). Service can be slow as they don't have that much staff front of house.

#### Sel et Sucre
Crêperie. We grabbed a crêpe to go.

### Sweets
#### La Fabrique Givrée (Recommend)
Ice cream shop. Flavors we tried included chocolate from Vietnam and saffron pistil.

### Misc
#### *LaLune Brasseurs Bio à Bordeaux*
Brewery across the river.

