# New York City Things To Do

## Bowery
### Food
- *[Momofuku Ko](https://ko.momofuku.com/)* (Kaiseki)  
  At Ko we are guided by the Japanese kaiseki tradition as well as seasonality, the innovation of our mentors and peers, food memories, and the breadth of cuisine available in the melting pot of New York City.

## East Village
### Food
- *[Huertas](https://www.huertasnyc.com/)*  
  Huertas serves Basque-influenced fare, evoking the lively eating and drinking culture of Northern Spain and creatively inspired by our home in NYC.
- [Ippudo](https://ippudony.com/) (Ramen)

## Lower East Side
### Bars
- *[Yopparai](http://www.yopparainyc.com/)* (Izakaya)  
  Yopparai is an intimate Japanese sake bar and restaurant featuring a wide variety of sake along with traditional foods and snacks that pair well with sake.

### Food
- [Dirt Candy](https://www.dirtcandynyc.com/)  
	Vegetarian restaurant.

## Nolita
### Food
- *[Pasquale Jones](https://pasqualejones.com/)*  
  Pasquale Jones is a restaurant on the corner of Mulberry and Kenmare Streets featuring wood-fired food, Neo-NY style pizza, and an extraordinary wine program.

## Soho
### Food
- *[Le Coucou](https://lecoucou.com/)*
- *[Bo Ca phe](http://bocaphe.com/)*

## Tribeca
### Food
- *[Tamarind](http://tamarindtribeca.com/)* (Indian)  
  Indian fine dining.

## West Village
### Food
- *[L'Artusi](https://www.lartusi.com/)* (Italian)  
  L'Artusi presents a modern take on traditional Italian cuisine, combining comfort and creativity.

## Resources
- https://www.bonappetit.com/story/nyc100