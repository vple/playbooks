# People & Culture
- How do you establish culture on a team?

# Project Execution
- How do you get the right signal to noise in terms of amount of telemetry?
- How do you evaluate mean time to resolution (MTTR)?