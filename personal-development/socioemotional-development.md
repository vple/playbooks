# Socioemotional Development

A critical part of maturing as a person is understanding and managing emotional and social expectations. 

## Uninvestigated Topics
Topics relating to socioemotional development that I believe are important but have no supporting sources for.

- Developing a sense of self
- Recognizing that you have opinions and can influence your environment
- Learning that everything, including inaction, has a consequence
- Self-acceptance

## Ordering
A rough guess at the order in which people have to learn and develop these skills. 

This ordering is most certainly wrong, mainly because it's linear. Everyone develops several of these skills in parallel, and to varying degrees. We also can return to certain topics as we learn that we need to grow more in that area. Additionally, it's likely that the nuance in certain areas can't be properly understood until we have a certain level of development in other areas.

Currently, this timeline starts some point around / after adulthood. Childhood is important, but I don't know anything about that period of time.

1. Self-acceptance.  
   Here we become comfortable with the fact that we have strengths and flaws.
2. Understanding irrationality.

## Understanding Irrationality
Everyone behaves irrationally. That's how the human brain is wired. Many of our thoughts or actions start with a subconscious emotion that we then (mis)translate into actions or language. People are irrational, and we must accept and be comfortable with that. 

Of course, we tend to need rationality in order to function in the world. The real challenge here is learning to behave rationally when we and others are inclined to behave irrationally.

### Inner Irrationality
First, we have to accept that we ourselves are irrational. If we can't accept this, we can't progress further.

Second, we have to learn to recognize when we behaved irrationally and why we did so. For me, it really helped to reflect on situations where I felt upset and to re-evaluate my actions. Many times I acted out of self-interest, which I recognized by asking if my actions were fair to the other person.[^no]

[^no]: It took me a bit longer to recognize a related concept: If you ask someone for something and give them an option of saying yes or no, you must be willing to accept the response you don't want. In particular, this is especially important when someone rejects you or your proposal.

Third, we have to be able to start recognizing when we behave irrationally in real time. From understanding the situations where we don't behave acceptably, we can identify triggers that tend to cause our undesired behavior. We then need to identify when these triggers are happening and change our behavior.

A critically important part of this is knowing what we want to change our behavior to. An implied part of this is that we understand what acceptable responses are. More socially-savvy people will already know what is acceptable. Those who are less savvy will have to figure out what is okay, e.g. by asking or observing people in real life, observing behaviors on TV and in movies, or by researching online.

### Outer Irrationality
We also want to be able to handle irrationality in others.

First, we have to understand that others are irrational. It's critical to realize that people may be acting based on their emotions, even when they might seem to be acting on reason.

Second, we have to understand that we're not aiming to change others. It's not possible to change everyone else. It's already difficult to change a single behavior in another person. 

Third, realize that irrational behavior in others triggers a response from us. It's important to realize that our gut reaction is liked based off of others being irrational, and so we should reevaluate how we want to respond. We may want to fix or correct others, which is difficult and not recommended. We may also want to respond back immediately, but this can result in compound irrational behavior—we're acting poorly because we were triggered by someone else acting poorly.

Fourth, we have to determine how to act. There are often several actions; the one we pick depends on the circumstances we're in. 

- We can remove ourselves from the situation, choosing to avoid it altogether. 
- We can reroute the other person's thinking. This doesn't change their mind, but it sidesteps their irrational behavior.
- We can identify and address the emotion underneath the irrational behavior. We'd likely do this is we can't sidestep the issue and must deal with someone for an extended period of time. Doing this is tricky, since the other person may not be aware of their underlying emotions. It also requires that we're good at reading others.
- We can choose to address the irrational behavior directly. This is often not a good way to go, as people may not be receptive to criticism or changing their behavior. It's most likely to result in change if you have good rapport or are in an authoritative position. Either way, it helps more if the other person understands why their behavior was poor and that changing it helps them in the long run.

### Resources
- The Laws of Human Nature, Robert Greene. Chapter 1.
- The Psychology of Human Misjudgment, Charlie Munger