# Molasses-Spice Cookies
Everyday Food, December 2005  
https://www.marthastewart.com/341117/chewy-molasses-spice-cookies

## Ingredients
- 2 cups all-purpose flour
- 1.5 tsp baking soda
- 1 tsp ground cinnamon
- .5 tsp ground nutmeg
- .5 tsp salt
- 1.5 cups sugar
- .75 (1.5 sticks) unsalted butter, softened
- 1 large egg
- .25 cup molasses

## Directions
1. Preheat oven to 350 F.
2. Whisk flour, baking soda, cinnamon, nutmeg, and salt.
3. Reserve .5 cup sugar. Beat butter and 1 cup sugar until combined.
4. Beat in egg and molasses until combined.
5. Reduce speed to low and mix in dry ingredients. Mix until a dough forms.
6. Pinch and roll dough into 1 tbsp balls. Roll/coat balls in reserved sugar.
7. Arrange balls on sheets, about 3 inches apart. Bake until edges are just firm, 10-15 minutes.
8. Cool 1 minute on sheets, then transfer to rack.

Recommended to bake one sheet at a time so cookies crackle uniformly.