# Croissants
Peter Reinhart  
https://bewitchingkitchen.com/tag/peter-reinhart-croissants/

## Ingredients
### Detrempe
- 595 g all purpose flour
- 11 g salt
- 56.5 g sugar
- 9 g instant yeast
- 198 g milk
- 227 g (1 cup) cool water
- 28.5 g melted butter

### Butter Block
- 340 g cold butter
- 16 g flour