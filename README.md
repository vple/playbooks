# README

Hi! I'm Vincent. These are my playbooks.

There have been countless times where I've wanted to be able to do something, but I wasn't sure how. Searching around would sometimes help, but what I really wanted was some clear guidance on how to learn or do that thing. I'm making these playbooks as a way of documenting my beliefs and the steps I took to get somewhere.

Because it's much easier to do something than to write about how it's done, these playbooks are unfortunately likely to lag significantly behind what I've learned. I also try to learn ideas from many sources. Although I'll try my best to stick to making playbooks from my actual experiences, there will almost certainly be influences from external ideas I've found and believe in but don't have as much experience with myself.

Finally, I apologize in advance for what will probably become a semi-structured mess. Organization is hard!