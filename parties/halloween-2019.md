# Halloween 2019

## Drinks
### Punches
- Mary Rockett's Milk Punch
- Glasgow Punch

### Cocktails
#### Trick
Spirit-forward.

- Whiskey
    - Manhattan (?)
    - Old-fashioned
    - Rusty Nail
    - Sazerac
- Gin
    - Martini
    - Negroni
- Tequila
    - Don Julio Blanco (neat, rocks, or shot)
    - Olmeca Altos Tequila Reposado (neat, rocks, or shot)

#### Treat
Not as spirit forward.

- Whiskey
    - Gold Rush
- Gin
    - Bee's Knees
- Rum
    - Daiquiri
    - Honeysuckle
- Tequila
    - Short Rib

### Food
- [Croissants](https://bewitchingkitchen.com/tag/peter-reinhart-croissants/)
- [Deviled Eggs](https://www.seriouseats.com/recipes/2017/03/canal-house-deviled-eggs-asparagus-caviar-olive-recipe.html)
- [Molasses-Spice Cookies](https://www.marthastewart.com/341117/chewy-molasses-spice-cookies)
- [Pumpkin Mac and Cheese](https://www.seriouseats.com/recipes/2013/12/pumpkin-mac-n-cheese-from-melt-the-art-of-mac-and-cheese.html)
- [Pumpkin Pie](https://www.seriouseats.com/recipes/2012/11/extra-smooth-pumpkin-pie-recipe.html)
- [Salted Caramel Apple Snickers Cake](https://www.halfbakedharvest.com/salted-caramel-apple-snickers-cake/)

## Prep Notes
### Ingredients
- [x] Mary Rockett's Milk Punch
    - [x] 8 lemons
    - [x] 1 gallon alcohol
    - [x] 1 gallon water
    - [x] 2 pounds demerara sugar
    - [x] 2 quarts milk (whole or raw)
    - [x] 2 nutmegs

- [] Glasgow Punch x2
    - [x] 15 oz. fine-grained raw sugar
    - [x] 15 oz. water
    - [x] 10 oz. lemon juice
    - [x] 40 oz. cold water
    - [x] 25 oz. Jamaican-style rum (750 ml)
    - [x] 5 limes

- [] Cocktails
    - [x] Beefeater's
    - [x] Elijah Craig Small Batch
    - [x] Flor de Cana Extra-Dry White Rum
    - [x] Old Overholt Rye
    - [x] Tanqueray London Dry Gin (using beefeaters)
    - [x] Carpano Antica sweet vermouth
    - [x] Dolin dry vermouth
    - [x] Rittenhouse 100 Rye
    - [x] Pierre Ferrand 1840 cognac (have it but using ambre)
    - [x] Vieux Pontarlier Absinthe (substitute)
    - [x] Siembra Azul Blanco Tequila
    - [x] lemon juice
    - [x] lime juice
    - [x] honey
    - [x] maraschino cherries
    - [x] sugar
    - [x] jalapeno
    - [x] pomegranate molasses

- [] Croissants
    - [x] 595 g all purpose flour
    - [x] 11 g salt
    - [x] 56.5 g sugar
    - [x] 9 g instant yeast
    - [x] 198 g milk
    - [x] 227 g (1 cup) cool water
    - [x] 28.5 g melted butter
    - [x] 340 g cold butter
    - [x] 16 g flour

- [] Deviled Eggs x8

- [] Molasses-Spice Cookies x2
    - [x] 2 cups all-purpose flour
    - [x] 1.5 tsp. baking soda
    - [x] 1 tsp. ground cinnamon
    - [x] .5 tsp. ground nutmeg
    - [x] .5 tsp. salt
    - [x] 1.5 cups sugar
    - [x] .75 (1.5 sticks) unsalted butter, softened
    - [x] 1 large egg
    - [x] .25 cup molasses

- [] Pumpkin Mac and Cheese x6

- [] Pumpkin Pie x2

- [] Salted Caramel Apple Snickers Cake

### Prep Schedule
#### T-3+
- [x] simple syrup
- [x] honey syrup
- [x] infuse jalapeno tequila

#### T-2

- [x] bake cake
- [x] boil eggs
- [x] make detrempe
- [x] make pie dough
- [x] make ice
- [] bake cookies

#### T-1
- [x] laminate dough
- [x] bake pies
- [x] prep mac & cheese
- [] prep deviled eggs
- [x] make ice
- [x] assemble cake 1

#### T-0
- [] cocktail prep
- [x] bake croissants
- [] prep pumpkins
- [] pre-bake pumpkins
- [] assemble cake 2
- [] juice fruits
- [] fill water