# Communication Practice

A list of things to practice to get better at communication.

## Exercises
- eyebrow flashes
- head tilts
- smiles
	- differentiate between genuine and fake smiles
- extended eye contact turn

## Observations
- count number of eyebrow flashes in a conversation
- classify smiles between genuine and fake
- observe leans, backward head tilts, and foot positioning

## Applied Practice
- flash eyebrows when greeting someone
- flash eyebrows and smile to random people, aiming for a positive response
- touch people on the arm when talking to them
- mirror others when talking to them
- food fork (may need to ask) on dates
- nod when listening to someone else
- use empathetic statements
- use implied compliments
- use third-party compliments