# Verbal Communication

### Whispering
Whispering indicates intimacy and is a positive signal. You need a close relationship before you can whisper to someone.

### Verbal Nudges
A verbal nudge reinforces head nods. They usually consist of confirmations like "I see" or "go on," or fillers like "umm" or "uh-huh."

### Empathetic Statements
Empathetic statements keep the conversation focused on the person you're talking to. They help the other person feel good about themselves, and are one of the most effective ways to do so. They can also be used as conversation fillers when you're struggling for something to say.

To make an empathetic statement, you need to listen to the other person. Then, make a statement about what the other person might be experiencing or feeling. Basic empathetic statements start with "So you..."; you can drop that part as you get familiar with them. 

Avoid repeating what the other person said word for word, as this will trigger a defensive reaction. Also avoid statements of the form "I understand how you feel," as this causes the other person to believe that you don't actually understand them. Instead, focus on the "you."

Resources:

-	_The Like Switch_, Jack Schafer

### Flattery & Compliments
A compliment is intended to praise someone else and acknowledge things that they've achieved. Compliments become more important to help people bond as their relationship develops. When you compliment someone, you're signaling that you're still interested in the other person and what they're doing well.

Flattery has a more negative connotation. Compliments that are or seem insincere are often viewed as flattery, particularly when they're used to exploit or manipulate someone else for a selfish reason. A problem with complimenting people you don't know well is that you can't be sincere (because you don't know them well). You'll come across as trying to flatter the person.

There's a very good way to compliment people that avoids the pitfall of complimenting someone you don't know well. You want the other person to compliment themself, as sincerity isn't called into question here. You do this by providing an _opportunity_ for someone to compliment themselves.

This is done by saying something that leads people to recognize their attributes or accomplishments. They're able to give themselves a pat on the back. These typically take the form of statements implying a certain character or quality based on the recipient's personality or accomplishments. They're essentially implied compliments.

> Ben: Then you've been really busy lately.
> Vicki: Yeah, I worked sixty hours a week for the last three weeks getting a project done.
> Ben: It takes a lot of dedication and determination to commit to a project of that magnitude.
> Vicki: I sacrificed a lot to get that mega project done and I did a very good job, if I may say so myself.

Resources:

-	_The Like Switch_, Jack Schafer

### Third-Party Compliments
A third-party compliment allows you to give a compliment via someone else. This means that you don't have to deliver the compliment yourself, which risks sounding insincere.

To give a third-party compliment, you need to find a mutual friend of acquaintance. This person should be someone that you're confident will pass along your compliment. Then, tell them something positive about the person you want to compliment.

This can be used effectively at work if you know who the gossips are. Gossips value spreading information to the appropriate people. A third-party compliment through them will often make it to your intended recipient.

Resources:

-	_The Like Switch_, Jack Schafer

### Primacy Effect
You can "prime" someone's perception of reality. Just give an opinion about someone or something before a person gets to meet or experience that thing for themselves. This will cause them to be unintentionally prejudiced.

### Ben Franklin Effect
Asking others for a favor will cause them to like you more. Schafer says this is because a person feels good about themself when they do someone else a favor.

Charlie Munger has also recognized this, but he attributes it to consistency--you must surely like someone that you're doing a favor for, otherwise you wouldn't do them a favor.

Resources:

-	_The Like Switch_, Jack Schafer
-	_Poor Charlie's Almanack_, Charlie Munger