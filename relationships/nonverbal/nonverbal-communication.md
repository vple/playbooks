# Nonverbal Communication

## Positive Signals
These indicate some form of interest (friendly, romantic, etc.).

Of these signals, the eyebrow flash, head tilt, and real smile are the most critical.

### Eyebrow Flash
A quick movement where the eyebrows move up and down. They are quick, lasting for about 1/6 of a second. Often not noticed because it's a very unconscious behavior. Eyebrow flashes to a stranger or someone you're not familiar with involve _brief_ eye contact.

Eyebrow flashes often happen when friendly people approach one another. Often happens around 5-6 away.

Eyebrow flashes can be sent long distance, such as in a crowded room. Sending an eyebrow flash and receiving one back often indicates interest. Not receiving an eyebrow flash back doesn't necessarily mean that you can't meet someone, but you should probably look for some other kind of positive signal.

An eyebrow flash with prolonged eye contact indicates intense emotion. With a friend this could be love, but a foe this could be hostility. Unnatural eyebrow flashes, where the eyebrow stays up too long, is also seen as a foe signal.

Videos:

-	https://youtu.be/45Y9lz2-9ZQ?t=41
-	https://www.youtube.com/watch?v=GG7bNGQoOt8

Resources:

-	_The Like Switch_, Jack Schafer

### Head Tilt
A head tilt is tilting your head to the left or the right. The idea is that it exposes your carotid artery, meaning that you don't perceive the other person as a threat. It's especially important for men to remember to tilt their head in social situations, as they're often used to keeping their head upright in a business context.

Head tilts are strong friend signals. Someone tilting their head is perceived as more trustworthy and attractive. They're also seen as more friendly, kind, and honest.

Videos:

-	https://youtu.be/45Y9lz2-9ZQ?t=53

Resources:

-	_The Like Switch_, Jack Schafer

### Smile
A real smile is a powerful friend signal. Someone smiling is often seen as more attractive, likable, and less dominant. It shows confidence, happiness, enthusiasm, and indicates acceptance. By smiling, you're projecting friendliness.

Generally, people smile at people they like. They won't smile at people they don't like. Smiling is often reciprocated; when you smile at someone else, they'll often smile back. As smiling releases endorphins, this also causes them to feel good about themselves.

There is a difference between genuine smiles and fake smiles. Genuine smiles are used when we want to interact with someone, often someone we already know and like. Fake smiles are used when social or work obligations force us to appear friendly. Smiles should be genuine to be interpreted as friend signals.

Genuine smiles:

-	have uturned corners of the mouth
-	cheeks move upward
-	have wrinkles (crow's feet) around the edges of the eyes
-	bagged skin forms under eyes
-	for some, nose dips downward

Forced smiles:

-	tend to be lopsided, favoring the person's dominant hand
-	lack synchrony; they start later than a real smile and taper off irregularly
-	cheeks often move outward but not upward
-	usually don't have wrinkles around the eyes

Videos:

-	https://youtu.be/45Y9lz2-9ZQ?t=64

Resources:

-	_The Like Switch_, Jack Schafer
-	https://nicolasfradet.com/smile-body-language/

### Eye Contact
Eye contact works _with_ other friend signals. It can be done from a distance.

An eye contact friend signal involves establishing eye contact for no more than a second. Longer than that comes across as aggression--a foe signal. End the eye contact with a smile. Smiling back, as well as looking down and then reestablishing eye contact, are good signals.

When two people like each other, they can make eye contact for longer. To do so with a stranger, hold contact for a second and then slowly turn your head, continuing to hold eye contact for a second or two. Your head turning away feels like breaking eye contact.

Resources:

-	_The Like Switch_, Jack Schafer

### Pupil Dilation
Dilated pupils indicate interest. However, they're hard to spot in everyday interactions, making them harder to use as a friend signal. Dilation also occurs when lighting changes, and that has no relation to interest.

### Touch
Touch can express lots of different messages, including: agreement, affection, attraction, guiding, congratulating, etc. It's powerful, subtle, and complex. Even brief touches can have a large influence. 

However, touches, can also produce negative reactions where the person being touched draws away or becomes anxious. These usually indicate dislike or distrust, but also occur in very shy and reserved people.

Light, brief touches on the arm during social encounters often have immediate and lasting positive effects. 

Touches on the hand (except handshakes) are more personal than arm touches, and can be used to gauge romantic interest. If someone pulls away from a hand touch you might not be rejected, but you'll have to build more rapport. People will react unconsciously to even an "accidental" touch or brush against their hand, so you can use this to gauge how receptive they are to being touched.

Resources:

-	_The Like Switch_, Jack Schafer

### Isopraxism (Mirroring)
Mirroring helps to create a favorable impression in the person being mirrored. Sometimes it's impractical to mirror someone exactly; you can cross match where you match an action but perhaps with a different part of the body.

Mirroring is considered normal and won't be consciously noticed. Lack of mirroring is considered a foe signal.

Resources:

-	_The Like Switch_, Jack Schafer

### Inward Lean
People often lean towards people they like and lean away from people they don't like. Similarly, they will tilt their heads slightly backward to create distance between them and someone else. They may also reposition their feet away from someone unwanted.

The signals can be used in business settings to look for who agrees with you, is neutral, or disagrees with you. This allows you to focus your efforts on winning over the people who have yet to agree with you.

### Food Forking
Food forking, where someone takes your food, is a friend signal when you have a close relationship.

### Expressive Gestures
Gestures vary across and within cultures, from the amount to the intensity of the gestures. Still, people who are friendly are often more expressive. These gestures signal interest and keep attention focused on the speaker.

### Head Nods
A head not indicates that we're engaged with a speaker and that they should keep going. A double nod indicates to go faster. 

Lots of nods are often disruptive, leading to rushed responses. They usually show that the listener wants to speak or isn't interested. You want to avoid this.