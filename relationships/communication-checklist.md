# Communication Checklist
> Communication is an act of will directed toward a living entity that reacts.

## Effective Communication
-	What is my intent? Goal? How does my communication help accomplish this goal?
-	Where or when does my communication become impulsive, top of mind, or self-indulgent?  
	This is to be avoided.
-	Are my words, doings, actions, inactions, and silence congruent?

### Strategy
These questions allow us to think strategically when determining what we want to communicate. Answer these, and use them to figure out what we want to say.

- 	What do we have? What is the challenge or opportunity we are hoping to address?
- 	What do we want? What's our goal?
- 	Who matters? Which stakeholders are important to us? What do we know about them? What information do we need to get about them? What would prevent them from being receptive to us, and how do we get past that?
- 	What do we need people to think, feel, know, or do in order to accomplish our goal?
- 	What do we they need to see us do, hear us say, or hear others say about us to think, feel, know, and do what we want them to do?
- 	How do we make that happen?

## Audience
-	How might my audience react?  
	They have their own opinions, ideas, hopes, dreams, fears, prejudices, attention spans, and appetites for listening.
-	How does my audience think? Behave? In what ways will this be different from my own thinking and behavior?
-	What preconceptions or barriers will my audience have?
-	What do I know about my audience? What do I need to learn?

## Reactions
-	What is the reaction I want to provoke?  
	The only reason to communicate is to provoke a reaction.
-	Will my communication be missed, confusing, or cause a different reaction?  
	Effective communication leads to the desired reaction.
-	What expectations will my words set? How will I fulfill those expectations?
-	What do I need to do to produce this reaction? What do I not need to do to produce the reaction?
-	What intended or unintended consequences will come from my words, silence, inaction, and action.
-	How should I adapt my communication based on the reactions I've received?