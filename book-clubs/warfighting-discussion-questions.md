# Warfighting Discussion Questions
Questions to drive critical thinking and discussion about _Warfighting_.

## 1. The Nature of War
-	What is this chapter about?  
	The nature of war.
-	Why is it important to understand the nature of war?  
	Understanding how to go about war comes from understanding the nature of war itself.
-	What are elements of the nature of war?  
	Moral, mental, and physical characteristics and demands.

### War Defined
