# 00001: A Learning Model

2018-09-30  

## Context
This doc describes my basic model as to how learning works.

As an aside, this doc doesn't fit well into the framework of decision records—it's not a decision, but rather a learning / belief.

## Model

The basic learning model I have in mind is simple. We all start with a certain understanding of the world. We have a mental model. Eventually, our mental model fails to support what we see. We then revise the model. That revision is us learning. I believe this is also known as mental model alignment theory.

The model's straightforward, but I want to elaborate on a few points.

Understanding of the world can mean a lot of different things in different scenarios. It can mean that we have a good model of something, but there's an inaccuracy somewhere. It can mean that we have a wildly incorrect model. It can also mean we have no model at all, which happens when we don't know about something (or don't value it).

Model failure happens all the time. It's just that we often don't think consciously about what failed, why, and what we can learn from it. It's also fairly common that we don't even have a good understanding of our model in the first place.

Finally, not all learning is good learning. Models are infinitely flexible in how we revise them. "Good" learning occurs when we revise a model in a way that accurately explains the case where it failed before. But it's also quite easy to come to wrong conclusions—to misattribute the underlying issue, and so revise our model in an incorrect way.

## Consequences
There are no ground-breaking consequences here. By knowing this model, you can be more cognizant of what's happening when something doesn't match up with how you expect. You'll also be more aware that the revisions you make may not be accurate.

This idea also raises a few interesting questions:

- How can you understand what someone else's mental model is? How can you understand which parts of their model are incorrect or differ from yours?
- What causes people to make bad revisions to their models?
- Is there a way to get people to make better revisions?
- Is there an effective way to show someone when they've made a bad revision and then get them to buy into a good revision?