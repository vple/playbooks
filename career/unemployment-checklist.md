# Unemployment Checklist

Things to do when becoming unemployed.

- Get healthcare.  
  https://www.healthcare.gov/coverage-outside-open-enrollment/special-enrollment-period/  
  Losing healthcare qualifies for special enrollment.
- Unemployment benefits.