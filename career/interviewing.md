# Interviewing: Evaluating Businesses

When joining a company, I want to join a company that will help me achieve what I'm aiming for. As a result, I need to discover what the business can offer me during the interview process.

## Personal Objectives

Samples:

- skill development
- responsibility
- learning about an industry
- specific types of projects
- money
- reputation

## Long-Term Thinking

Can I trust a business to make effective long-term decisions? Is the business unified in those decisions? Do the business's long term beliefs align with mine?

A concern here: what if a business hasn't been forced to deal with a certain problem? To what degree do they get to slide?

### Techniques

#### Strategic Thinking & Alignment

With as many leaders as appropriate, start a discussion that reveals their strategic thinking.

This could include questions such as:

- Who's your leading competitor?
- What's your main priority at the moment?
- What's your current strategy? (How are you going to accomplish that?)

This allows me to see if a person understands their current situation and is capable of strategic thinking. Furthermore, by cross-referencing between different people, I can see if the business is on the same page. Each person's individual strategies should connect and make sense in terms of the business's overarching strategy.