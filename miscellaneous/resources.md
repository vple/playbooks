# Miscellaneous Resources
Interesting stuff that I haven't classified at all.

- [How to assess the quality of garments: A Beginner's Guide {Part I}](https://anuschkarees.com/blog/2014/05/01/how-to-assess-the-quality-of-garments-a-beginners-guide-part-i/), Anuschka Rees
- [Ask HN: What did you do when you suddenly got rich?](https://news.ycombinator.com/item?id=20521902)