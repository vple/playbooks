# Recruiting Communications

Hello, and thanks for being a part of recruiting! Whether it's through interviewing, info sessions, career fairs, or other means, you play an extremely important part in our company's future: you're helping to define our future generations of engineers.

A key part of recruiting is, well, _recruiting_. As much as possible, we hope that every interaction with someone gets them more interested in us. That's where we rely on you—your interactions with candidates and potential candidates affect how they feel about us. When you get someone excited about us, they're more likely to want to work with us.[^benefits]

[^benefits]: As a bonus, when we have a higher acceptance rate we don't have to spend as much effort to recruit new engineers. Among other things, this means fewer interviews for you!

So, how can we get more people interested in working with us?

## Conversation Goals
At a high level, there are two results we'd like to accomplish when we talk to someone.

One, we want them to walk away feeling that this is a good place to work! This is true no matter who we're talking to—a candidate, a potential candidate, a curious bystander, etc. Whenever someone talks about us with their friends, it would be great if they have something positive to say. If we can leave them with a positive impression, they're more likely to say something nice about us!

Two, we want people to (figuratively) see themselves working here. This is just for candidates and potential candidates. Our first goal helps candidates see us as a good company. This goal helps candidates see us as a good company _for them_.

This is where you come in! Your interactions with a candidate determine whether or not we'll hit these goals. It's simple—all you need is a bit of structure!

## Be a Spokesperson
First up, remember that you're a spokesperson! Odds are, the person you're talking to will have talked to very few (if any) other people. In their mind, you are the most authoritative source about what it's like to work here. We're relying on you to make us look good!

Surprisingly, we can actually draw from the Marine Corps for some useful guidelines on being a good spokesperson. Here's what a senior Marine officer said about Marines interacting with news media:

> Make sure each of your Marines knows this: If you're deployed to a war zone and there's a reporter around, we expect you to do three things:
>
> - Engage. Speak with the reporter.
> - Tell the truth. Don't lie, but also don't reveal confidential, classified, or sensitive operational information.
> - Stay in your lane. If you drive a tank, talk about your tank. If you fly a plane, talk about your plane. Don't talk about anything that isn't your direct responsibility.

We can draw some parallels for when we're talking to others.

### Engage
Speak with the person!

There are plenty of little details that you can fuss over to become more engaging when you talk to someone. While these can help, there's a much simpler quality that will help you be engaging: be excited!

This doesn't mean faking enthusiasm. Instead, let your enthusiasm naturally show when you're talking about something you like! When you can, find ways to talk about experiences that you enjoyed.

Everyone will have different things that excite them; what motivates someone else won't necessarily motivate you. While you'll have to figure out what you like on your own, we've observed many people and seen the unique topics and scenarios that motivate them. We encourage you to do the same!

Here are some categories we've observed:

-   Human interaction.  
    Some people enjoy meeting and talking to others, such as at a career fair. These people are naturally more excited when they get to interact face-to-face with potential candidates.
-   Technologies.  
    Some people light up when they talk about a specific technology. It might enable cool use cases, be interesting conceptually, or something else entirely.
-   Projects & work items.  
    We've seen people get really excited about a piece of work that they did or the project their team is doing. The reasons for this vary a lot—some people are excited to be tasked with important pieces of a project, others like how their project impacts customers, and some just really like the challenges they solve as part of their work.
-   Community.  
    Others get excited about the community here. Exactly what about the community varies from person to person. They include company-sponsored events and groups, playing games, being around motivated and driven people, and going out together to restaurants or bars. A good way to figure out if this is you—where do you spend time with your coworkers, outside of work itself?

Once you've figured out what excites you, find ways to share your enthusiasm with others—you can steer the conversation towards topics that you like. This is especially easy to do when someone asks you what you like about us.

### Tell the Truth
We want people to realize we're a great company. However, we don't want to do this by lying or by stretching the truth just to make ourselves look better. We do a lot of things well—we don't have to invent facts!

Not telling the truth just makes things bad, both for the candidate and for us. If they realize someone is lying to them, they'll see us as dishonest and (rightfully) not a place they'd want to work at. If they don't realize, they'll become disillusioned once they actually start working here and figure out that it's not as rosy as it seemed.

We know it can be awkward to be in a situation where you don't know the answer to a question. It's okay to say that you don't know! After the conversation, great engineers will figure out how to better answer the question in the future.

Where possible, it's nice to still get an answer for the candidate. You might be able to ask someone else or send a follow-up email once you've found the answer.

There are also situations where an honest answer might make us look bad. While you can sometimes get away from the topic, there's another effective way to talk about a negative: explain how the situation is getting (or going to get) better!

An example: there are plenty of times where we have some kind of performance problem—queues too full, a careless mistake breaks everything, some of our old code is inefficient or doesn't scale well. These things can cause real problems for our customers where we effectively don't work for hours or days. You can probably remember a time when this happened to your team.

While this is bad, we actually have very good processes for handling these issues. Naturally, we fix the bug, but more importantly we take steps to help us do better in the future. We talk about issues within our teams, teach about them at all-hands, and (when reasonable) write blameless postmortems.

The real value in explaining how things will be better is that it _builds confidence_ in us becoming better over time.

On a related note—you don't have to make us sound like we're all sunshine and rainbows. We of course don't want to look terrible, but every organization has its own issues and we aren't an exception. Ideally the grass is actually greener where we are, but even then there will still be some dead, brown patches here and there.

We know things aren't perfect. More importantly, candidates know this! It can actually help when we're able to speak candidly about areas where we can do better.

### Stay In Your Lane
Staying in your lane boils down to one thing: speak from your own experiences!

The main benefit here is that this helps you speak better. You're more knowledgeable about the subject. This lets you speak more accurately, but it also makes it easier for you to go in depth about a topic or for you to handle follow-up questions.

## Conversation Milestones
Now that we have an idea of how to be a good spokesperson, how do we accomplish our goals: show others that we're both a good company and a good company for them? It's unlikely that a potential candidate will tell us exactly how they're feeling about us.

We do this by aiming to hit a few milestones in our conversation. In rough, increasing order of difficulty:

- Provide factual, accurate information about us.
- Share something about us that you value.
- Share something about us that the person you're talking to values.
- Make a connection with the person you're talking to.

Just to be clear: you won't hit all of these milestones in every conversation! However, you should always aim to hit at least one. Here's how each milestone relates to our goals.

### Factual, Accurate Information
Whether or not they realize it, every person has their own set of values and beliefs about what makes a good company. Recruiting someone doesn't always mean convincing them that we're the best option.

Sometimes, we don't have to do the convincing—we just need to share info about us. The other person then has the data they need to figure out if we're the right company for them. They convince themselves.

This milestone is probably the easiest to achieve—just know how to answer a potential candidate's questions.

### Share Something You Value
The next step up is to share something relating to work that's important to you. The hope here is that the candidate will realize there's something great about us, based on your experiences.

This is also discussed above (see Engage), but you'll have your own unique set of things you value. It could be a flexible work environment, the freedom or ownership you have at work, how you like eating/gaming/socializing with your coworkers, etc.

### Share Something They Value
While it would be great if the person you're talking to shares your values, that's not always the case. If you can speak towards what the other person values, that's even better than speaking about what you value.

This is also harder because you often don't know what the other person values! Occasionally they might tell you, but typically someone won't spontaneously bring it up.

An easy thing to do here is to just ask questions! Here are a few ideas:

- 	Directly ask what someone's looking for in a job or in an internship!  
	A fair number of people will have something they're hoping to get out of a job, and you can tailor your answer to meet what they're looking for.
- 	Expand on details that they've mentioned.  
	If they have a lot of friends in D.C., you could ask if it's important for them to work in D.C. If they mention that they really enjoyed their class on databases (or systems, or security, etc.) you can follow up and ask for more specifics as to why they liked that topic.
-	Ask about what they liked from previous jobs.  
	Many candidates will have worked at a previous job. Asking what they liked (or didn't like) about that job will immediately give you an idea of what topics they care about.

### Make A Connection
The last milestone is to make a connection with the person you're talking to. This one tends to be the hardest purely because of logistics. If you don't have a lot of time to chat or if you don't have a lot of shared interests, it's much harder to find something to connect about.

Making a connection, however, is likely the most impactful milestone. At the end of the day, a large part of someone's decision will just be how they feel about the people they've met. If you're able to make a connection, that will be one of the most memorable things that a candidate experiences.

## Practice, Practice, Practice
When you watch someone who's been recruiting for a while, they might make all of these things seem effortless. In reality, they had to figure out how to recruit effectively. We hope that you'll also take some time to polish your skills! While you'll inevitably have some conversations where you don't come across as well as you would've liked, the most important thing is to figure out how you can have a better conversation with the next person you speak with!