# Cocktail Planning

- 	Beekeeper, NoMad
	a smoky, floral gin sour
- 	English Heat, NoMad
	a spicy gin sour with hints of vanilla
-	Fig and Thistle, NoMad
	a fig-and-honey-flavored tequila sour
-	Jitney, NoMad
	a mezcal negroni variation with coffee and absinthe
-	Maize Runner, NoMad
	a corn-and-blueberry cobbler
	fall?
-	Paris Is Burning, NoMad
	a smoky gin sour with elderflower
-	Bee Lavender, NoMad
	a scotch sour with hints of lavender and honey
	winter/early spring?
-	Bitches Brew, NoMad
	a bitter-chocolate-and-coffee-tinged manhattan
-	Cafe Con Leche, NoMad
	a rum-and-aquavit sour with the flavors of a cappuccino
- 	Ciampino, NoMad
	a herbaceous, spiked hotel chocolate
	winter
-	Mott and Mulberry, NoMad
	a whiskey sour that tastes like mulled cider
	fall, winter
-	North Sea Oil, NoMad
	a spirit-forward cocktail with smoke, bitter orange, chocolate, and caraway...doesn't quite fit any box
-	Satan's Circus, NoMad
	a rye whiskey sour with chile, cherry, and bitter orange
-	Sons Of Liberty, NoMad
	a fall sour with apple and earl grey
-	Start Me Up, NoMad
	a whiskey sour with honey and ginger