# Spirits
Recommended spirits for punch, by David Wondrich.

## Rum
### Pirate Juice (Jamaica rum)
"Hogo," high proof.

- Inner Circle Green Dot
- Smith & Cross
- Wood's
- Wray & Nephew White Overproof
- Lemon Hart (mix equal parts 80-proof and 151-proof)
- Sea Wynde
- Bundaberg

### Planter's Best
Older, mellower, smoother, lower proof.

- Angostura 1919
- El Dorado (5 or 12 year)
- Chairman's Reserve
- J.M VSOP
- Plantation Barbados 5-year Grande Reserve
- English Harbour 5-year
- Scarlet Ibis

### Stiggin's Delight
Older, richer, darker, more expensive.

- Mount Gay Extra Old
- Angostura 1824
- El Dorado (15 or 21 year)
- most Plantation vintage rums