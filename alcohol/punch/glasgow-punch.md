# Glasgow Punch
_Punch: The Delights (and Dangers) of the Flowing Bowl_, David Wondrich

## Ingredients
- 6 oz. fine-grained raw sugar
- 6 oz. water
- 4 oz. lemon juice
- 16 oz. cold water
- 10 oz. Jamaican-style rum
- 2 limes

This is a slightly tweaked ratio from the original, which has a 1:3 ratio of alcohol to other ingredients. You could instead use 20 oz. cold water and 6-7 oz. of rum, resulting in a 1:6 ratio. Wondrich says this tweaked version is better.

Scaled up by 2.5 to match a full bottle of rum:

- 15 oz. fine-grained raw sugar
- 15 oz. water
- 10 oz. lemon juice
- 40 oz. cold water
- 25 oz. Jamaican-style rum (~750 ml)
- 5 limes

## Directions
1. In serving bowl, dissolve sugar into water.
2. Add lemon juice and cold water.
3. Stir in rum.
4. Cut limes in half, rub cut sides around the rim of the bowl, and hand-squeeze in the juice.