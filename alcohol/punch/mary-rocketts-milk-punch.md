# Mary Rockett's Milk Punch
_Punch: The Delights (and Dangers) of the Flowing Bowl_, David Wondrich

## Original Recipe
> To make Milk Punch. Infuse the rinds of 8 Lemons in a Gallon of Brandy 48 hours then add 5 Quarts of Water and 2 pounds of Loaf Sugar then Squize the Juices of all the lemons to these Ingredients add 2 Quarts of new milk Scald hot stirring the whole till it crudles [_sic_] grate in 2 Nutmegs let the whole infuse 1 Hour then refine through a flannel Bag.

## Ingredients
- 8 lemons
- 1 gallon alcohol
- 1 gallon water
- 2 pounds demerara sugar
- 2 quarts milk (whole or raw)
- 2 nutmegs

There are a few different types of alcohol that are known to work well:

- brandy (traditional)
- rum (use a mild, mellow rum of Planter's Best grade)
- scotch/irish whiskey
- mezcal

My preferred alcohol is Pierre Ferrand cognac (ambre or 1840).

## Directions
1. Peel the lemons. Infuse the rinds in alcohol for 24 hours. Save the lemons.
2. The next day, remove the lemon rinds.
3. Juice the lemons.
4. Combine lemon juice, infused alcohol, water, and demerara sugar.
5. Heat the milk until scalding hot. Once hot, add it to the punch.
6. Grate in the nutmeg.
7. Let sit for 1 hour.
8. Strain the punch, removing all curds. Curds can and should be squeezed to extract extra punch. Refrigerate.
9. After refrigerating, the sediment in the punch will have setlled. Siphon off the punch. Refrigerate and repeat as needed.
10. Rebottle the punch.

Punch can be stored at cellar temperature. Chill before serving.

## Log
### 2019-10-29
- Pierre Ferrand 1840
- Two siphonings. You can leave the siphon at the bottom of the bucket for both siphonings; no babysitting needed.