# White Wine Preferences

## Like

### Côtes de Provence
#### [BY.OTT](http://www.domaines-ott.com/en/the-wines/by-ott/blanc)
> BY.OTT is an elegant extension to the Domaines Ott* range and a truly Provençal wine with conviviality and friendship at its heart.

> A pale yellow brilliant with sea green hints and white gold fire. The strong yet subtle nose reveals notes of ripe mango, bush peach, pine sap and acacia flowers. The taste starts with freshness, marked by notes of lemon, before easing into a full, perfectly balanced body. The smooth and sensual finish brings out the heady nose. An elegant and true Provencal wine open to a wide range of food pairings.

Dinner @ Le Crabe Marteau, Nantes, France. August 2019. Vintage 2018.

### Côtes de Gascogne
#### Domaine Chiroulet—Terres Blanches
http://www.charlesnealselections.com/domaine-chiroulet1.html

Dinner @ Coquillages et Crustacés, L'Houmeau, France. Vintage 2018.

## Neutral

### Coteaux du Layon
#### Les 4 Villages—Chenin Moelleux
Dessert wine.

Dinner @ COQUE, Nantes, France. August 2019. Vintage 2014.