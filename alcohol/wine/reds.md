# Red Wine Preferences

## Like

### Haut-Médoc
#### Château Caronne Ste. Gemme
Dinner @ La Tupina, Bordeaux, France. August 2019. Vintage 2014.

### Saint-Émilion
All Saint-Émilion wines are (required to be) reds, and typically consist of merlot, cabernet franc, and cabernet sauvignon grapes. Saint-Émilion wines are typically blends. A significant part of the terroir here relates to the clay and limestone in the area.

There is a Saint-Émilion wine classification system, done about every 10 years. At the top is *Premier Grand Cru*, divided into *Classé A* and *Classé B*. Next are *Grand Cru Classé* vineyards. The lowest level is *Grand Cru*, which just means that the winery follows the rules and regulations. *Grand Cru* wines are generally not as good as the other tiers, but this isn't always the case—some wineries choose not to be classified. The classification is based off of the actual wine, terroir, and reputation in the marketplace.

#### Château Côte de Baleau
Uses concrete vats.

##### Château Côte de Baleau
Wine tasting @ winery. August 2019. Vintage 2015. Pairs well with sausage, [meats].

##### Château les Roches Blanches
Light, fruity. Fermented and aged without wood. Pairs well with comte cheese.

Wine tasting @ winery. August 2019. Vintage 2015.

#### Château Soutard
Corporation-owned winery. Rated Grand Cru Classé as of 2012.

##### Château Soutard
Varietals: merlot, cabernet franc, carbernet sauvignon, malbec. Fermented in wooden vats, aged in wooden barrels.

Wine tasting @ winery. August 2019. Vintage 2014.

##### Château Petite Faurie de Soutard
Varietals: merlot, cabernet franc, carbernet sauvignon. Fermented in stainless steel vats, aged in wooden barrels.

Wine tasting @ winery. August 2019. Vintage 2014.