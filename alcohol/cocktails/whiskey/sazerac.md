# Sazerac
## Death & Co
- Vieux Pontarlier Absinthe
- 1.5 oz Rittenhouse 100 Rye
- .5 oz Pierre Ferrand 1840 Cognac
- 1 tsp demerara syrup (2:1)
- 4 dashes Peychaud's
- 1 dash Angostura
- lemon twist

Rinse rocks glass with absinthe and dump. Stir and strain into glass, no ice. Express lemon oils and discard twist. No garnish.