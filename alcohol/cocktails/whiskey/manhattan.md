# Manhattan
## Home (?)
- 2.25 oz Old Overholt rye
- 1 oz Carpano Antica sweet vermouth
- 2 dashes Angostura
- 1 maraschino cherry

Stir and serve up.

## Death & Co
- 2.5 oz Rittenhouse 100 Rye
- .75 oz House Sweet Vermouth
- 2 dashes Angostura
- 1 brandied cherry

Stir and strain into a coupe. Garnish with cherry.