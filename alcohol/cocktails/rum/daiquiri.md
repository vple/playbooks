# Daiquiri
## Death & Co
- 2 oz Flor de Cana Extra-Dry White Rum
- 1 oz lime juice
- .5 oz cane sugar syrup (2:1)
- lime wedge

Shake and strain into a coupe. Garnish with lime wedge.

### Notes
Ideal when very cold. I'm normally lazy and use normal simple syrup.
