# Honeysuckle
## Death & Co
- 2 oz Flor de Cana Extra-Dry White Rum
- .75 oz lime juice
- .75 oz acacia honey syrup (2:1)
- lime wedge

Shake and strain into a coupe. Garnish with lime wedge.
