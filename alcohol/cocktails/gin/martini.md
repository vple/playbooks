# Martini
## Home (?)
- 2.25 oz Beefeater's London Dry
- .25 oz Dolin Dry Vermouth
- 1 dash orange bitters
- lemon twist

Stir and strain into a martini glass. Garnish with lemon twist.

## Death & Co
- 2.5 oz Plymouth, Beefeater London Dry, or Tanqueray London Dry
- .75 oz Dolin Dry Vermouth
- 1 dash House Orange Bitters
- lemon twist

Stir and strain into a martini glass. Garnish with lemon twist.