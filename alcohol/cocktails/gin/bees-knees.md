# Bee's Knees
## Death & Co
- 2 oz Tanqueray London Dry Gin
- .75 oz lemon juice
- .75 oz acacia honey syrup (2:1)
- 3 drops Scrappy's Lavender Bitters
- 1 brandied cherry

Shake and strain into a coupe. Garnish with cherry.

### Notes
I normally make this with Beefeater's and without the bitters.