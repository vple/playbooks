# Short Rib
## Death & Co, Phil Ward 2008
- 2 oz jalapeno-infused Siembra Azul Blanco Tequila
- .75 oz lime juice
- 1 oz simple syrup (1:1)
- .75 tsp pomegranate molasses (Al-Wadi recommended)

Shake and straine into a coupe. No garnish.