# Sake Preferences

## Like
### [Ozeki Osakaya Chobei](http://www.ozekisake.com/products/product_detail.php?product=14)
> Ozeki Osakaya Chobei is a premium sake named after the founder of Ozeki. This is a Daiginjo sake, brewed from the highly polished rice. The 50% of the rice grain is polished away. Ozeki Osakaya Chobei has a rich and fruity aroma and a delicate flavor. This sake is excellent with light food such as Sashimi and steamed fish.

Self-described as slightly sweet and slightly light-bodied.