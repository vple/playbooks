# Research Topics
## Management
- How do I identify strengths?
	- watching when someone takes pride in being best at something
	- being good at something
	- when they pay attention/make good decisions

## People & Culture
- How do you establish culture on a team?

## Project Execution
- How do you get the right signal to noise in terms of amount of telemetry?
- How do you evaluate mean time to resolution (MTTR)?

# Teaching Topics
- Coachability
	- Coachability doesn't mean to have a good attitude and work hard. It's the capacity to digest feedback and make immediate corrections.

- Self-directed growth
- Team Players
	- admit to mistakes
	- argue about what's correct without worrying about offending people
	- stick to commitments
	- hold each other accountable
	- build trust
	- focus on team results
	- socially smart
	- hungry, smart (socially/about people), humble