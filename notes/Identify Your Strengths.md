# Identify Your Strengths
20200716083233
#management #manager-identity

Your strengths as a manager are typically the things you are inclined to handle. It's important to identify them so that (1) you know what you're good at, and (2) you can start training other people in those skills so you can move on.

> - How do I make decisions?
> - What do I consider a job well done?
> - What are all the responsibilities I took care of when it was just me?
> - What's easy or hard about working in this function?
> - What new processes are needed now that this team is growing?

[@zhuoMakingManagerWhat2019, 45]