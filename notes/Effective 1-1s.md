# Effective 1-1s
20200717095141
#one-on-ones #effective-one-on-ones

The key to good 1-1s is preparation. You want to focus on what's most important for your reports.

## For Reports
### Top Priorities
What are the top most critical outcomes for your report? How can you help them tackle those challenges?

- What's top of mind for you right now?
- What priorities are you thinking about this week?
- What's the best use of our time today?

### Calibrate On "Great"
Do both of you have the same vision of what you're working towards? Do you have the same goals or expectations?

- What does your ideal outcome look like?
- What's hard for you in getting to that outcome?
- What do you really care about?
- What do you think is the best course of action?
- What's the worst-case scenario you're worried about?

### Share Feedback
What feedback do you have that can help your report? What can they tell you that would make you a more effective manager?

### Reflect On How Things Are Going
How is your report feeling overall? What makes them satisfied or dissatisfied? Have their goals changed? What have they learned recently? What do they want to learn going forwards?

- How can I help you?
- What can I do to make you more successful?
- What was the most useful part of our conversation today?
- Are [[20200723090647]] Workplace Core Needs being met?

## For You
While 1-1s are focused on what's important for your reports, there are also times you need to gather information. Often, this information is related in some way to things that are _long-term_ important for your reports (they might not realize it). They can sometimes be worked into other conversations.

### Peer Relationships

- How does it feel working with someone else on the team?
- What is the working relationship with someone on the team?
- Do your reports see evidence of the strengths or feedback that you're seeing? Do they see evidence of different strengths or feedback?

Resources:
[@zhuoMakingManagerWhat2019, 66-67]