# Get To Know Your Reports
20200716083921
#management #report-relationship

[First 1-1 Questions](https://docs.google.com/document/d/1ALVJFA8gp7H53iIyPQqoBN1IvZku-5YYcjKpMx6HCXE/edit)

> - What did you and your past manager discuss that was most helpful to you?
> - What are the ways in which you'd like to be supported?
> - How do you like to be recognized for great work?
> - What kind of feedback is most useful for you?
> - Imagine that you and I had an amazing relationship. What would that look like?

[@zhuoMakingManagerWhat2019, 49]

- Do you prefer to be given a lot of space, or do you prefer to be regularly pushed?