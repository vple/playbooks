# Team Building
20200716083453
#management #team-building

Things to consider when building a team:

> - What qualities do I want in a team member?
> - What skills does our team need to complement my own?
> - How should this team look and function in a year?
> - How will my own role and responsibilities evolve?

[@zhuoMakingManagerWhat2019, 46-47]
