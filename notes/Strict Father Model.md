# Strict Father Model
20200709091322
#conservatism #frames

> The strict father model begins with a set of assumptions: The world is a dangerous place, and it always will be, because there is evil out there in the world. The world is also difficult because it is competitive. There will always be winners and losers. There is an absolute right and an absolute wrong. Children are born bad, in the sense that they just want to do what feels good, not what is right. Therefore, they have to be made good. 
> 
> What is needed in this kind of a world is a strong, strict father who can: 
> • protect the family in the dangerous world, 
> • support the family in the difficult world, and 
> • teach his children right from wrong.

[@lakoffAllNewDon2014, 4-5]

This model leads to a lot of black and white, us vs. them thinking. It's "correct" to think in your own self-interest, and being prosperous happens when a strict father figure teaches you what is right and wrong. For that reason, helping others to take care of themselves is considered a weakness and suboptimal.