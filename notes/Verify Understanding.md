# Verify Understanding
20200710105002
#management
Could probably use a tag about how this relates to helping people understand higher direction.

When you give someone a task, their tendency is to think in terms of the confines of the task. It's important to verify that they understand why the task is important. They need to understand how a task fits in with a larger project, as well as how completing that project enables other improvements. Verifying understanding gives a way to confirm whether or not they understand this, as well as gives an avenue to explain the bigger picture when someone doesn't see the connection.