# Task-Specific Feedback
20200718114744
#management #feedback

Provide task-specific feedback as frequently as possible.

- Focus on what was done, rather than who did it.
- Provide feedback when the action is still fresh.

[@zhuoMakingManagerWhat2019, 86-87]