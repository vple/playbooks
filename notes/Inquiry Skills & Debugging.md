# Inquiry Skills & Debugging
20201003152816
#inquiry-skills #learning #debugging #teaching

Goals of inquiry skill development, grades 5-8 in the US:

- identify questions that can be answered through scientiﬁc investigations
- design and conduct a scientiﬁc investigation
- use appropriate tools and techniques to gather, analyze, and interpret data
- develop descriptions, explanations, predictions, and models using evidence
- think critically and logically to make the relationships between evidence andexplanations

via National Science Education Standards (1996)
    - https://www.nap.edu/catalog/4962/national-science-education-standards
    - https://www.nap.edu/read/4962/chapter/8?term=%22identify+questions+that+can+be+answered%22#145

These are the same skills used for debugging. So if someone both has these skills and understands how they can express them with code, they should be good at debugging.