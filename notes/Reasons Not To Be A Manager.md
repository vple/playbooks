# Reasons Not To Be A Manager
20200715101606
#management #manager-fit

- You prefer a specific role over accomplishing a particular outcome.
    - If there's a specific activity you like (writing code, designing, etc.) then it will hard to be a manager since you won't get to do what you like doing the most.
- You don't enjoy talking with people.
    - If you don't like or struggle to talk to and understand people, it will be hard to ensure your team members are effective.
    - If you hate meetings or context switching, it will be hard to be a manager as most of your day will be meetings.
- You aren't able to provide stability in emotionally-challenging situations.
    - If you have trouble empathizing with people or providing support, it will be hard for you to handle difficult and delicate situations well.
- You want to progress your career.
    - Becoming a manager is more of a transition than a promotion. In tech, there are individual career paths that are just as valued as management paths. Solely wanting to progress your career doesn't align well enough with what a manager needs to do.
- You want free rein to call the shots.
    - As a manager, you have to inspire people rather than tell them what to do. Additionally, you are still held accountable by various people in the org. If you can't make decisions in the interest of the team, you'll lose trust.
- You were asked to be a manager, but don't want to be one.

Resources:
[@zhuoMakingManagerWhat2019, 28-34]