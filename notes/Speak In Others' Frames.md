# Speak In Others' Frames
20200709091136
#frames #persuasion

> Facts matter enormously, but to be meaningful they must be framed in terms of their moral importance. Remember, you can only understand what the frames in your brain allow you to understand.

[@lakoffAllNewDon2014, Preface]

> The word is defined relative to that frame. When we negate a frame, we evoke the frame.

[@lakoffAllNewDon2014, Framing 101]

> This gives us a basic principle of framing: When you are arguing against the other side, do not use their language. Their language picks out a frame—and it won’t be the frame you want.

[@lakoffAllNewDon2014, Framing 101]

Whenever you talk to someone else, figure out what frames they think in so you can use appropriate terminology and speak in ways they understand.

Questions:
- How can you quickly determine what someone's frames are?
- What are common frames?
