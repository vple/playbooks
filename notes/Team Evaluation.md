# Team Evaluation
20200715104934
#management #team-development

Things to look at when assessing your team.

## Current State
- Does everyone get along?
- Are processes efficient?
- Is the team known for rigorous and high-quality work?

## Improvements
- How able is the team to hit deadlines?
- Are priorities always shifting?
- Is there a meeting that people always avoid?

Resources:
[@zhuoMakingManagerWhat2019, 41]