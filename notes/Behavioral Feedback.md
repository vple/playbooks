# Behavioral Feedback

Share behavioral feedback thoughtfully and regularly. Behavioral feedback is a statement about how you view someone, so you need to thoughtfully consider what you're going to say. Support your feedback with specific examples.

You can look at behaviors to determine some of a report's strengths or areas of development. Look at themes for task-specific feedback around a report.

- Are decisions made quickly or slowly?
- Are they really good at process? Are they an unconventional thinker?
- Do they move towards pragmatic or idealistic solutions?

[@zhuoMakingManagerWhat2019, 87-88]