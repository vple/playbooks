# Hypocognition
20200709091836
#hypocognition

> When the frames are there, the words come readily. There’s a way you can tell when you lack the right frames. There’s a phenomenon you have probably noticed. A conservative on TV uses two words, like tax relief. And the progressive has to go into a paragraph-long discussion of his own view.

[@lakoffAllNewDon2014, 21]

> In cognitive science there is a name for this phenomenon. It’s called hypocognition—the lack of the ideas you need, the lack of a relatively simple fixed frame that can be evoked by a word or two.

[@lakoffAllNewDon2014, 21-22]

Questions:
- How do you teach someone something that they don't yet have terms for?
- How do you invent and popularize a term so it catches on?