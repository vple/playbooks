# 360 Feedback
20200718120843
#management #feedback

Companies that do 360 feedback often run the process once or twice a year. You can also gather feedback yourself. 360 feedback is especially useful when you don't have a lot of context for your report's day-to-day work.

A sample to collect feedback, asked of closest collaborators:

- What is X doing especially well that X should do more of?
- What should X change or stop doing?

When delivering 360 feedback, you'll want to deliver it in person since it's comprehensive. You'll also want to document the feedback so both you and your report can reference it in the future.