# Set Clear Expectations
20200718113422
#management #feedback

When providing feedback, the first thing you want to do is set clear expectations.

- Agree on what success looks like.
- Get ahead of expected issues, lay foundations for future feedback sessions.
- Address what a great job looks like, rather than a mediocre or bad job.
- Provide advice to help reports start off well.
- Mention common pitfalls.
- Determine the mechanism/schedule on how you'll get updates on and provide feedback for the project.

> In your first three months on the job, I expect that you'll build good relationships with your team, be able to ramp up on a small-scale "starter" project, and then share your first design iteration for review. I don't expect that you'll get the green light on it right away, but if you do, that would be knocking it out of the park.
> Here's what success looks like for the next meeting you run: the different options are framed clearly, everyone feels like their point of view is well represented, and a decision is made.

[@zhuoMakingManagerWhat2019, 85-86]