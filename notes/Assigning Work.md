# Assigning Work
20200715101157
#management

As a manager, you're incentivized to have people work towards things that need to be accomplished.

You can also have them work towards initiatives defined solely by you. This of course shouldn't be abused—the work should still be relevant towards some company-relevant goal. But you shouldn't feel burdened to have to figure out how to do everything exclusively yourself.