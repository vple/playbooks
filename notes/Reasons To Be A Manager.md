# Reasons To Be A Manager
20200715101555
#management #manager-fit

- You prefer adaptability to accomplish an outcome over playing a specific role.
    - Managers have to do what it takes to get a team to succeed. This can involve training, hiring, dealing with people problems, creating a plan, etc.
    - You'll need to enjoy, or at least not mind, adapting to the current situation and changing what you do day to day.
- You enjoy talking with people.
    - As a manager, you have to support your people. You'll need to talk with and listen to them in order to accomplish this.
    - You'll have a lot of meetings—you should be okay with this.
- You can provide stability in emotionally-challenging situations.
    - You'll have to deal with hard conversations—telling people they're not performing, firing someone, dealing with someone's personal matters. You'll need to be stable, empathetic, and able to help defuse or resolve situations rather than contribute to them.

Resources:
[@zhuoMakingManagerWhat2019, 28-34]
