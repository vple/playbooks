# Quality Severity
20200803194631
#software-engineering

(Lack of) quality is relative to how severe the impact is.

- How many users does this affect?
- Does this affect reliability?
- Is this a critical functionality?