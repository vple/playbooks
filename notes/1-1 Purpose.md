# 1-1 Purpose
20200717094630
#one-on-ones

Your time and energy is your most valuable resource; spending it on your team helps to build good relationships. They give you a time to discuss things that wouldn't normally come up during work.

One-on-ones should be focused on what will help make reports more successful, not on what you need from them. They shouldn't be used for status updates. Topics that can be discussed in a group or over email should be kept in those mediums.

Resources:
[@zhuoMakingManagerWhat2019, 65-66]