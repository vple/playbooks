# Determine Team Norms
20200716084124
#management #calibration

> - What does it mean to do a great job versus an average or poor job? Can you give me some examples?
> - Can you share your impressions of how you think Project X or Meeting Y went? Why do you think that?
> - I noticed that Z happened the other day....Is that normal or should I be concerned?
> - What keeps you up at night? Why?
> - How do you determine which things to prioritize?

[@zhuoMakingManagerWhat2019, 50]