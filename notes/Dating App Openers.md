# Dating App Openers
20200716184758
#dating #dating-apps #conversation-openers

- What are your favorite things to do on weekends?
- What are three words that you'd use to best describe yourself?
- When was the last time you really laughed, and what made you laugh this hard?
- What life accomplishment are you most proud of?
- If you had a theme song, what would it be?
- What is the most meaningful thing someone could do for you?
- Would you rather inherit money or work for it?
- What is the first thing you do when you wake up in the morning?
- What is more important--doing what you love, or doing what you're good at?

## Templates

- Ask for a recommendation. Books, bars, coffee shops, etc.
- Share a random fact.

## Food

- What is your favorite type of food, and why?
- Fellow foodie here. If you could eat only one meal again for the rest of your life, what would it be?
- What three things are always in your fridge?
- Favorite ice cream flavor?

## Travel

- Where is your favorite place in the world, and why?
- I just got back from XYZ. Stunning. What's next on your travel bucket list?

## Experiences

- What was it like to XYZ? Where did you do that?
- What is the most spontaneous thing you've ever done?