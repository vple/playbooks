# Types Of Unknowns
20201107112237
#management #problems #decision-making

What do you want out of engineers (or potentially other knowledge workers) in a day?

They do knowledge work, so you want people to be able to make high-quality decisions. There are many types of decisions people will have to make during the day.

There are knowns and unknowns. Known problem the person already knows how to solve, and unknowns the person doesn't. You'd expect faster, and hopefully routinely good, decisions for known problems. Unknown problems take more time and involve more uncertainty, so they require longer and better thought.

Whether or not a problem is known or unknown depends also on the person. So some problems might be unknown problems for person A, but known problems for person B. You can increase the quality of person A's thinking if they're able to get the knowledge and decision making process from person B.

The person may also not be in the company. A problem might be an unknown problem for everyone at company A, but a known one for someone at company B. So ideally you have some way of obtaining that information.
