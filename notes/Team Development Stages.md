# Team Development Stages
20200716133848
#management #team-development

[Tuckman's stages of group development](https://en.wikipedia.org/wiki/Tuckman's_stages_of_group_development)

## Forming
When a group first comes together. This team likely has a name and a loose understanding of a goal, but there are very few processes or patterns. Team members are typically motivated, but focus on themselves. To move on to the next stage, team members need to be able to risk conflict when discussing topics.

## Storming
During storming, there's friction in the team. This results from conflicts of opinion, leading to the team sorting out power, status, working styles, hierarchy, etc. Not all teams having a storming phase, while others may never leave their storming phase. Teams may also re-enter the storming phase when issues come up.

While storming, people will form opinions about character and integrity of other members. Sometimes, leaders themselves are questioned. Supervisors may be more accessible in this stage, but are typically more directive.

## Norming
When norming, things start to work out on the team. The team is aware of conflicts, but moves past them to work towards a common goal. Norming has a risk—if people are too concerned about preventing conflict, they won't share ideas.

## Performing
At this point, teams are highly productive. Everyone is motivated, knowledgeable, competent, autonomous, and able to handle the decision-making process.

Resources:
[@hoganResilientManagement2019, 4-5]

Can this also be related to directive/supportive quadrants from The One-Minute Manager?