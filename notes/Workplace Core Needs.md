# Workplace Core Needs
20200723090647
#management #biceps #team-development

BICEPS model.

## Belonging
Belonging involves having a connection to a community of people. This includes being close with the group, the group being cared for, and there being a connection with other people.

Belonging can feel threatened when people aren't included or left in the dark about things, especially things that their peers are aware of.

## Improvement / Progress
People want to feel improvement towards something, be it for a company, a team, or for yourself. Impact means that your work affects others. People also want to see that they are learning and improving skills that matter to them, at a pace they are happy with.

## Choice
People want to be able to make decisions that affect them and their world. This should be balanced so people aren't overwhelmed by choice, but they also aren't constrained by it either.

## Equality / Fairness
People want equal access to resources, information, and support. Everyone has access to the things that are relevant, and decisions are made and communicated fairly.

## Predictability
This is also something to balance. There should be enough certainty so people can have a baseline to expect. At the same time, things can't be stagnant since that would be boring. People should be able to have an idea of when to expect things (time). They should also be able to know and plan for the future. The direction that people are heading in should rarely change faster than what people are comfortable with.

Things that can cause lack of acceptable predictability include:
- change of strategy
- always working ong bugs
- performance cycles or standards keep changing

## Significance
This is where you are relative to others. This includes having status, having your work be visible to people that matter, and that your work gets recognized.

Resources:
- [https://www.palomamedina.com/biceps/](https://www.palomamedina.com/biceps/)
- [@hoganResilientManagement2019, 9-13]