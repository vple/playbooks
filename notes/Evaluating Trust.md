# Evaluating Trust
20200716085121
#management #report-relationship #trust

You have a trusting relationship with your reports when:

1. Your reports regularly bring their biggest challenges to your attention.
2. You and your reports regularly give critical feedback without it being taken personally.
3. Your reports would gladly work for you again.

[@zhuoMakingManagerWhat2019, 61-62]