# Interview Archetypes
20201003155757
#interviews

Potential types of interview questions.

- Inquiry skills. [[20201003152816]] Inquiry Skills & Debugging
    - Test people's ability to reason and deduce/identify things.
- Debugging.
    - Related to inquiry skills, but focused specifically tested via debugging.