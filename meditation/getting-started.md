# Getting Started

## Resources

- [Meditation: Why You Should Do It](https://markmanson.net/meditation), Mark Manson
- [Meditation for Beginners: 20 Practical Tips for Understanding the Mind](https://zenhabits.net/meditation-guide/), Leo Babauta

## Purpose

- Meditation helps you disidentify with your mind and your emotions.
- Gain perspective and clarity on your internal issues.
  - Doesn't fix them for you.

## How

There are lots of different styles and techniques. They're just different; there's no right or wrong.

- Sit cross-legged on the floor. Be comfortable.
  - Keep your back straight.
  - Relax your diaphragm.
  - Eyes can be closed or open.
- Set a timer.
- Clear your mind and think about nothing.
  - Count your breaths (inhale + exhale). If you get distracted, restart at one.
  - Later on, you can follow thoughts, feelings, or sensations.