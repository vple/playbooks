# Problem Solving

## Basics Checklist
A checklist to ensure that you've thought things through.

- Have you figured out the "no brainer" parts of your problem first?
- Have you run some numbers to make sure your idea holds water?
- Have you thought forwards toward and backwords from your end goal?
- Have you applied concepts from other disciplines?
