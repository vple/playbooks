# Crimea Openings

## Industrial (1)

Attempt 1, likely not optimal.

| # | Action      | Wood | Food | Metal | Oil | Workers | $$$ | Power | Popularity | Stars | Combat | Notes                       |
|--:|-------------|-----:|-----:|------:|----:|--------:|----:|------:|-----------:|------:|-------:|-----------------------------|
| 0 |             |    0 |    0 |     0 |   0 |       2 |   4 |     5 |          2 |     0 |      0 |                             |
| 1 | Produce     |      |    1 |       |     |       3 |     |       |            |       |        |                             |
| 2 | Trade       |      |      |       |   2 |         |   3 |       |            |       |        |                             |
| 3 | Bolster     |      |      |       |     |         |   2 |       |            |       |      1 |                             |
| 3 | **Upgrade** |      |      |       |   0 |         |   5 |       |            |       |      0 | Bolster (Combat) -> Upgrade |
| 4 | Trade       |      |    2 |       |   1 |         |   4 |       |            |       |        |                             |
| 5 | Bolster     |      |      |       |     |         |   3 |       |            |       |      2 |                             |
| 5 | **Upgrade** |      |      |       |   0 |         |   6 |       |            |       |      1 | Move -> Enlist              |
| 6 | Trade       |      |    2 |     2 |     |         |   5 |       |            |       |        |                             |
| 6 | **Enlist**  |      |    0 |       |     |         |     |       |            |       |      2 | Enlist -> Combat            |
| 7 | Produce     |      |    1 |       |     |       5 |     |       |            |       |        |                             |
| 7 | **Deploy**  |      |      |     0 |     |         |   7 |       |            |       |      1 | Speed                       |