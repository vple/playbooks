# Crimea // Agricultural

## Board Strategy
This board has some nice synergies going for Zehra & Kar.

Upgrading also comes naturally on this board, as it's paired with move. Zehra & Kar have no starting access to oil, but fortunately the upgrade cost is exactly two oil. This lets you upgrade after a single trade, and it gets even better once you have some combat cards to spend.

The real strength of this board, however, comes from the bolster and enlist pairing. The Crimeans always want combat cards and this makes it ridiculously easy to pump them out. It gets even better if enlist has been upgraded once or twice. With one upgrade, you get to trade a food for a combat card and an enlist. With two upgrades, the enlist is effectively free. And if that wasn't enough, the enlist action provides three coins every time.

Conveniently, we also start at 4 popularity. After a one-time enlist bonus, we'll be sitting at 6 and just need to find a single popularity somewhere.

With no easy access to wood, building is never easy. But here there's a silver lining—produce is paired with build. This allows you to quickly max out on workers, then never worry about paying the produce cost since you'll have no plans to build.

## Fast Encounter
The idea here is that we can use our trade into move and upgrade combo to knock out a quick two upgrades and grab an encounter. Ideally we get an encounter to accelerate us even further, but even if we don't we can fall back to our single-action enlist engine.

The quick encounter results in our mechs and workers being a bit delayed. However, this was likely to happen with this player mat anyways.

### Opener
| # | Action      | Wood | Food | Metal | Oil | Workers | $$$ | Power | Popularity | Stars | Combat | Notes                                        |
|--:|-------------|-----:|-----:|------:|----:|--------:|----:|------:|-----------:|------:|-------:|----------------------------------------------|
| 0 |             |    0 |    0 |     0 |   0 |       2 |   7 |     5 |          4 |     0 |      0 |                                              |
| 1 | Trade       |      |      |       |   2 |       2 |   6 |       |            |       |        |                                              |
| 2 | Move        |      |      |       |     |         |     |       |            |       |        | Character -> Anywhere  Worker (Food) -> Metal |
| 2 | **Upgrade** |      |      |       |   0 |         |   7 |       |            |       |        | Bolster (Combat) -> Enlist                   |
| 3 | Trade       |      |      |       |   2 |         |   6 |       |            |       |        |                                              |
| 4 | Move        |      |      |       |     |         |     |       |            |       |        | Character -> Encounter                       |

### Encounter
From here, our encounter options decide how we can get the most out of our combat card engine. Generally, our preference is to get some resources and, if possible, popularity. Additionally, because the encounter happens before our second upgrade, we'll hopefully be able to use that upgrade even more effectively.

Gaining money is pointless for us, as we naturally generate a lot. With our high starting popularity, we would prefer to not lose any so that we can quickly get to 7.

#### Food
Encounters: 1,

Gaining food makes things much easier—we no longer have to upgrade our enlist. This lets us reduce the cost of our deploy instead. Gaining 2 food and 1 popularity is preferred to paying for 4 food; 2 food should be enough time to delay our last enlistment. More importantly, it gives us the extra popularity we're looking for.


## Gain 2 Food, 1 Popularity

## Gain 2 Metal, 1 Popularity

## Gain 2 Oil, 1 Popularity

## Gain 1 Combat Card, 1 Popularity

## Pay 4 Coins, Deploy A Mech

## Pay 3 Coins, Build A Structure

## Pay 2 Coins, Gain 1 Worker, 3 Food

## Pay 2 Coins, Gain 1 Worker, Any 2 Resources

## Pay 2 Coins, Gain 1 Upgrade

## Pay 2 Coins, Gain 2 Power, 2 Combat Cards

## Pay 2 Coins, Gain 4 Metal

## Pay 2 Coins, Gain 4 Oil

## Pay 2 Coins, Gain 4 Food

## Pay 2 Coins, Gain Any 3 Resources

## Pay 2 Popularity, Gain 4 Metal

## Pay 2 Popularity, Gain 4 Oil

## Pay 2 Popularity, Gain 4 Food

## Pay 2 Popularity, Build A Structure

## Pay 2 Popularity, Gain 2 Food, 2 Metal

## Pay 3 Popularity, Gain Any 5 Resources

## Pay 2 Popularity, Gain 1 Worker, 3 Metal

## Pay 2 Popularity, Gain 1 Worker, 3 Food

## Pay 2 Popularity, Gain 1 Upgrade, Any 2 Resources
