# Mac Setup

## General
1. Install browsers.
	* [Chrome](https://www.google.com/chrome/)
	* [Firefox](https://www.mozilla.org/en-US/firefox/new/)
2. Install [homebrew](https://brew.sh/).
3. Install [Spotify](https://www.spotify.com/us/download/mac/).

## Programming
### Machine Configuration

Use locally installed binaries:

```
$ echo 'export PATH="/usr/local/bin:$PATH"' >> ~/.bash_profile
```

### IntelliJ

<https://www.jetbrains.com/idea/download/#section=mac>

### Sublime Text
<https://www.sublimetext.com/>

Symlink the Sublime Text CLI into `/usr/local/bin`:

```
$ ln -s /Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl /usr/local/bin/subl
```

### Git
#### Installation
To install git, run:

```
$ brew install git
```

Alternatively, OSX may prompt you to install git when you try to run a git command in the terminal.

#### Configuration
```
$ git config --global user.name "Your Name Here"
$ git config --global user.email "your_email@youremail.com"
```

Set up git to [store your credentials on the OSX keychain](https://help.github.com/articles/caching-your-github-password-in-git/).

```
$ git config --global credential.helper osxkeychain
```

Create a [personal access token](https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line/). Git will remember this token the first time it's used as a password.

TODO: Add instructions for adding token directly to keychain.

```
git config --global alias.st status
git config --global alias.ci commit
git config --global alias.br "branch -vv"
git config --global alias.co checkout
```

#### Resources
* <https://help.github.com/articles/set-up-git/>

### Java

1. Install [Java](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).
2. Add `JAVA_HOME` to your environment variables by adding the following line to `~/.bash_profile`:
```
export JAVA_HOME="`/usr/libexec/java_home -v 1.8`"
```

### Bazel

```
$ brew install bazel
```

### MacDown

<https://macdown.uranusjr.com/>

Symlink the MacDown CLI:

```
$ ln -s /Applications/MacDown.app/Contents/SharedSupport/bin/macdown /usr/local/bin/macdown
```

### sed
OS X comes with FreeBSD sed.
https://unix.stackexchange.com/questions/13711/differences-between-sed-on-mac-osx-and-other-standard-sed

To use GNU sed instead:

```
brew install gnu-sed
```

Add this line to `~/.bash_profile`:

```
export PATH=/usr/local/opt/gnu-sed/libexec/gnubin:$PATH
```

## Miscellaneous
1. Install [Steam](https://store.steampowered.com/about/).

## Resources
* <http://sourabhbajaj.com/mac-setup/Java/README.html>