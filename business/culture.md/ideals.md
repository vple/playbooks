# Culture Ideals

Stuff I'd want at my company.

## Slack
See Slack (book). Sacrifice a bit of efficiency for flexibility and efficiency.

## Misc
- Minimize isolated domain knowledge--people with this knowledge are more relied on. It's harder for the org if they need to leave, take a vacation, or switch roles. It's harder on the person because they're relied on a lot.