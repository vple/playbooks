# Manuals

- 8BitDo SF30 NES Controller
  https://download.8bitdo.com/Manual/Controller/SN30+SF30/SN30_Analogue.pdf
- Miroco MI-EK001 Electric Kettle
  https://image.hootoo.com/s3/cHJvZC8yMDE5LzAyLzE3LzU0NTlmNTM2ZjgucGRm
- Javelin Pro Duo
  https://www.manualslib.com/manual/1156685/Lavatools-Javelin-Pro-Duo.html#manual