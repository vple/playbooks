A checklist of items to get effective devops going. Priorities of items will vary based on how impactful the item is and what your organization is good/bad at.

## The DevOps Handbook
The items in this section come from *The DevOps Handbook*.

## Part II—Where To Start
### 5. Selecting Which Value Stream to Start With
- Start with sympathetic and innovative groups.
- Demonstrate early wins and broadcast success.
- Grow credibility, influence, and support.
  - Start with innovators and early adopters. Ideally, find those who are also respected and have influence.
  - Build a critical mass and a silent majority. It's okay if teams aren't visible.
  - Identify holdouts. Finally, tackle the high profile detractors.

### 6. Understanding the Work in Our Value Stream, Making it Visible, and Expanding it Across the Organization
- Create a value stream map.
  - Involve appropriate parties.
  - Include lead time, processing time, and %C/A (percent complete & accurate).
  - Focus on areas with bottlenecks or where there's a lot of rework.
- Create a dedicated transformation team.
  - Have members be focused 100% on devops transformation. Don't split time between devops and current responsibilities.
  - Select team members who are generalists, with skills across many domains.
  - Select team members with longstanding, mutually respectful relationships with the org.
  - Create a separate physical space for the team, giving them some isolation.
- Keep planning horizons short.

### 7. How to Design Our Organization and Architecture with Conway's Law in Mind
- Aim for market-oriented organizations.
- Enable team members to be generalists.
- Cross-train and grow engineering skills.
- Fund services and products, not projects.
- Have small, decoupled, independent, productive teams.
- Keep team sizes small.

### 8. How to Get Great Outcomes by Integrating Operations into the Daily Work of Development
- Created shared/blessed ops services and libraries that are more convenient to use than alternatives.
- Enable developers to self-serve.
- Embed ops onto dev teams, or assign ops liasons.
- Integrate ops into dev rituals.

## Part III—The First Way: The Technical Practices Of Flow
### 9. Create the Foundations of Our Deployment Pipeline
- Enable self-serve, automatically configured and created, production-like environments.
- Can you accurately test your code in a non-production (but production-like) environment?
- All environments are created through a common build mechanism.
- All code, dependencies, environments, scripts, etc. are checked into a single shared repository.
- Make infrastructure easier to rebuild than to repair.
- "Done" includes tested and working in production.

### 10. Enable Fast and Reliable Automated Testing
- Testing is done regularly and frequently, by the developers.
- Tests are automated.
- All changes have accompanying automated tests.
- The continuous build is always kept passing.
- Test coverage monitoring is added and continually increases over time.
- Policy and testing guides are written, shared, and used throughout the organization.
- There is a deployment pipeline that handles building, testing, and integration.
  - Deployment pipeline processes run in dedicated environments.
- Have unit, acceptance, and integration tests.
- Add and use test coverage.
- Errors are caught in the earliest possible testing stage.
- Faster and more reliable testing stages are run first.
- Ensure tests can run in parallel.
- Use the latest passing build for manual testing.
- Use test-driven development.
- Automate as many manual tests as possible.
- Tackle flaky or unreliable tests as soon as possible.
- Use performance testing to understand system performance.
- Validate all important attributes of the system. This includes non-functional attributes: availability, scalability, capacity, security, etc.
- Notify and respond to broken deploy pipelines.
- Make broken pipelines very visible.
- Add tests at earlier stages to catch regressions.

### 11. Enable and Practice Continuous Integration
- Use trunk-based development.
- Have developers regularly check their changes into master.
- Consider using gated commits.

### 12. Autoname and Enable Low-Risk Releases
- Automate the deployment process.
  - Identify, simplify, and automate as many manual steps as possible. Remove steps where possible.
  - Deploying should be done in the same way in every environment.
  - Should automatically smoke test deployments.
  - Ensure consistent environments are maintained.
- Enable self-serve deployments.
- Ensure deploy pipeline can support deployment.
  - Ensure that packages created during continuous integration are suitable for production deploys.
  - Show readiness of production environments at a glance.
  - Provide a push-button, self-service method to get any suitable version of packaged code into production.
  - Automatically record which commands, which machines, who was involved, and outputs. This is for auditing and compliance.
  - Run smoke tests.
  - Provide fast feedback for the deployer so they can see if their deploy succeeded.
- Have a one-step "deploy code" button.
- Decouple deployments from releases. See release patterns for approaches.

### 13. Architect for Low-Risk Releases
- Rewrite architectures as needed.
- Use a strangler application to help migrate existing functionality.
  - Use versioned APIs.

### Part IV—The Second Way: The Technical Practices Of Feedback
#### 14. Create Telemetry to Enable Seeing and Solving Problems
- Collect telemetry.
  - Enable developers to add telemetry as part of their daily work. This should be easy and self-service.
  - Are you collecting data at the business logic, application, and environment level?
- Collect significant application events.
  - See notes for important events to log.
- Make sure that monitoring and logging is centrally available, rather than being siloed.
- Graph/visualize metrics.
  - Overlay telemetry graphs with deployment times.
- Detect anomalies.
- Proactively alert and escalate.
- Use telemetry to troubleshoot issues.
- Make sure information is accessible to everyone.
- Make sure information is visible to everyone.
  - e.g. with TVs

- How do you evaluate mean time to resolution (MTTR)?

#### 15. Analyze Telemetry to Better Anticipate Problems and Achieve Goals
- Use outlier detection to automatically detect issues.
  - Means and standard deviations are good to focus on for Gaussian distributions.
  - Other techniques can be used for non-Gaussian distributions, including:
    - Smoothing
    - Fast Fourier Transforms
    - Kolmogorov-Smirnov tests
- Configure alerts based on outliers.
- Fine-tune alerts.
  - Minimize false-positives.
  - Focus on outliers that matter.
  - Increase signal-to-noise ratio.
- Find ever-weaker failure signals.

#### 16. Enable Feedback So Development and Operations Can Safely Deploy Code
- Actively monitor metrics when deploying.
- Use telemetry to make deployments faster.
- Handle deployment issues with fix forwards or roll backs.
- Overlay deployment times on metric graphs.
- Have dev also responsible for downstream pager duty.
- Have dev see how their work is used downstream.
- Have dev be initially responsible for managing production services.
- Use a service handback mechanism to return service-ownership to dev when there are issues.
  - Don't let operations manage a fragile service while tech debt buries them.

#### 17. Integrate Hypothesis-Driven Development and A/B Testing into Our Daily Work
- Validate feature impact quickly, such as via A/B testing.
- Use feature toggles to control which version customers see.
- Product owners should view their features as hypotheses, backed by data from A/B testing.
- Kill feature development quickly if they aren't fulfilling their intended result.

#### 18. Create Review and Coordination Processes to Increase Quality of Our Current Work
- Bring implementers close to change authorizers.
- Code review all changes.
- Engineers should monitor other team members' commit streams.
- Define high-risk changes; pull in subject matter experts.
- Break up complex, difficult to understand commits.
- Track code review metrics to ensure they aren't getting rubber stamped.
- Sample and inspect code reviews to ensure quality.
- Pair program to improve quality and transfer knowledge.
- Cut or streamline bureaucratic processes.

## Part V—The Third Way: The Technical Practices Of Continual Learning And Experimentation
### 19. Enable and Inject Learning into Daily Work
- Establish a just culture—accidents are caused by processes, not people.
- Conduct blameless post-mortems.
  - See notes for important qualities in post-mortem meetings.
- Post-mortems are easily accessible by anyone in the org.
- Look for weaker failure signals to act on.
- Ensure that failures are things that people are comfortable with and will surface.
- Failure modes are defined.
- Faults are added in production.
- Large-scale failures are rehearsed.
- Introduce Game Days.
  - Identify, address, and re-test issues.
- Game days are regular events, not extraordinary ones.

### 20. Convert Local Discoveries into Global Improvements
- Create and use chat bots to automate regular, repeatable work.
- Turn documentation and standards into shared executables.
- Use a single, shared repo.
- Codify non-functional requirements, including:
  - Sufficient production telemetry in applications and environments.
  - Being able to track dependencies.
  - Services that are resilient and degrade gracefully.
  - Forward and backward compatibility between versions.
  - The ability to archive data to manage the size of production data sets.
  - Ability to search and understand log messages across services.
  - Ability to trace requests from users through multiple services.
  - Simple, centralized runtime configurations.
- Automate and document recurring operations work.
- Define the technologies that operations will explicitly support.

### 21. Reserve Time to Create Organizational Learning and Improvement
- Have regular improvement blitzes.
  - Tech debt should be regularly paid down.
- Dedicate time towards learning and teaching.
- Ensure that people are learning from their peers.
- Encourage engineers to go to conferences, then share their learnings.
- Set up internal consulting and coaching responsibilities. Have subject matter experts.