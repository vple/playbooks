# Management Roadmap

This is a roadmap for me to establish what I need to master as a manager, as well as to develop my own management style.

## What Makes A Great Manager

- Being a great manager is hard. It requires discipline, focus, trust, and individualizing.

## To Do

### Improve Q12 Score
From *First, Break All The Rules*:

> 1. I know what is expected of me at work.
> 2. I have the materials and equipment I need to do my work right.
> 3. At work, I have the opportunity to do what I do best every day.
> 4. In the last seven days, I have received recognition or praise for doing good work.
> 5. My supervisor, or someone at work, seems to care about me as a person.
> 6. There is someone at work who encourages my development.
> 7. At work, my opinions seem to count.
> 8. The mission or purpose of my company makes me feel my job is important.
> 9. My associates or fellow employees are committed to doing quality work.
> 10. I have a best friend at work.
> 11. In the last six months, someone at work has talked to me about my progress.
> 12. This last year, I have had opportunities at work to learn and grow.

### Create Report Dossiers
For each report, have a doc that summarizes and tracks who they are and how they're doing. This would likely involve:

1. First page: summary of where this person is and where they're going. Significant strengths/weaknesses, where they are focusing on, significant/recent personal details, how to deliver feedback to this person, etc.
2. Second page/section: data-based summary/picture. e.g. time at each tier. The idea here is that it's supplementary info that can be used to help flesh out details for helping someone.
3. Notes

- identifying how people would like to be treated

### Be On Stage

- What should it look like when I'm on stage?
- How do I handle when I'm not in the right mood to be on stage?
- How can I shift my normal behavior so I'm on stage even when I'm not thinking about it?

### Clarify my role in finding & retaining talent

- I don't have much of a say as to who joins my team.

### What is my management style?

What does style even mean?

### What are my strengths & weaknesses?

How do I incorporate these things?

### Personalizing Per Individual

What's important to pay attention to, and how do you do it?

- What are their strengths?
- How do they take praise/criticism? How does that affect your delivery?

### Talent

- identify/classify different common talents
- determine which talents team members have
- figure out how certain talents can be applied
- identify which talents are best for Yext & best for my team
	- identify the factors in culture, people, etc. that affect this

### Outcomes

- define the outcomes I want
- growth framework?
- day-to-day expectations

### Strengths

- things to pay attention to to identify strengths

### Learning from the best

- identifying the best people at yext
- observing them to figure out what makes them the best

## Questions / Sticking Points

- How can you display your capability as a manager when recruiting people?
- How to get people to open up if they're naturally closed up and / or afraid? (JS, EC)

## Misc Ideas/Concepts

- Reports don't see things through the lens of a manager. Things likely should be structured in a way that doesn't require them to have this perspective.