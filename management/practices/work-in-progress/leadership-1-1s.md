# Leadership 1-1s

1-1s are a common practice with reports. But it's also important to build teams of leaders that are functioning well together.

While there are several directions to help build these teams, the angle here is to start having 1-1s between these leaders. This gets people talking and understanding what other people do.

Here's some initial direction:

- What does the other person do?
- What challenges does the other person face?
  - Do you face these too?
    - What might you want to try to improve the situation?
  - Do you not face these?
    - Did you previously deal with it, and you now have an approach to dealing with it?
    - Do you have something else going on that causes you to sidestep this entirely?
- What technical things are on the other person's mind?
- What people things are on the other person's mind?