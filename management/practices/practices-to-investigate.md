# Practices To Investigate & Implement

Practices that I don't follow today that seem interesting and worth some investigation.

## Existing Practices

- Brag sheets.

## Philosophies

- Make it possible for others to take the recognition, reward, and work that got you to where you are.
- Find the people who are the most upset, then ask them why they're upset.