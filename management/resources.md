# Management Resources
These aren't necessarily resources I agree with. It's just a dump.

## Hiring & Interviewing
- [Hiring is Broken And Yours Is Too](https://software.rajivprab.com/2019/07/27/hiring-is-broken-and-yours-is-too/)
  - Pros and cons of various software interviewing practices.
- [Interview Questions](https://www.jcheng.org/post/interview-questions/)
  - Rating technical interview questions based on their effectiveness.
- [How we interview engineers at CircleCI](https://circleci.com/blog/how-we-interview-engineers-at-circleci/)

## Diagnosing Problems
- [Spidey-Sense](https://randsinrepose.com/archives/spidey-sense/), Michael Lopp (rands)
  - Seeing a common behavior but not sure what the cause is? Track potential leading indicators, then use your data to better understand what's happening.
  
## Performance Reviews
- [What you should really measure in your annual performance reviews (and why)](https://www.atlassian.com/blog/hr-teams/our-performance-reviews-framework), Bek Chee (Atlassian)
  - TLDR: Expectation of role + contribution to team + demonstration of company values, weighted equally.
  - Weights can probably be adjusted based on company priorities.
  - Doesn't include many implementation details.
  
## Career Development
### Career Navigation
- [There are no adults in the room](https://letterstoanewdeveloper.com/2019/08/12/there-are-no-adults-in-the-room/)
  - Explains that people are well-intentioned, but nothing is clear cut.

### Developing Employees
- [How to Help Your Employees Learn from Each Other](https://hbr.org/2018/11/how-to-help-your-employees-learn-from-each-other), Kelly Palmer, David Blake; Harvard Business Review
  - [Outline](https://outline.com/u2EPNz)
  - The importance of P2P learning.
  - [The Expertise Economy](https://www.amazon.com/Expertise-Economy-smartest-companies-learning-ebook/dp/B078W5VTCN)
- [Making Learning a Part of Everyday Work](https://hbr.org/2019/02/making-learning-a-part-of-everyday-work); Josh Bersin, Marc Zao-Sanders; Harvard Business Review
  - [Outline](https://outline.com/KHsTMr)
  - It's hard to make time for learning. The best way to approach this is to fine ways to "inline" learning with daily work.
  - Bottom-up: metacognition/mindfulness, to-learn list, tech-enabled tips, add learning time to your calendar, subscribe to a few high-quality and relevant newsletters, contribute actively and expertly to a channel.
  - Top-down: make knowledge systems accurate and easy to use, share content internally, leverage apps and APIs to bring in content, devote a communication channel to learning, put learning in the inbox.

### Career Conversations
- [Career Conversations With Employees: Questions And Tips For Managers](https://www.fellow.app/blog/2019/career-development-conversations-with-employees/), Manuela Bárcenas, Fellow
  - Kim Scott's career conversations, from *Radical Candor*

### Career Path
- [Thriving on the Technical Leadership Path](https://keavy.com/work/thriving-on-the-technical-leadership-path/), Keavy McMinn
  - Thoughts on senior engineer positions outside of the management track.

## Prioritization & Productivity
- [One Thing](https://randsinrepose.com/archives/one-thing/), Rands
  - Managers end up thinking that the act of managing their workload equals productivity, but it's actually doing that workload that is productivity.
  - Start/stop non-focused time makes it hard to get big things done.
  - Use a forcing function to focus on the things that matter—have a limit to how many items you can keep on your plate.

## Team Building
- [Icebreaker](https://icebreaker.range.co/)
  - Tool that provides icebreaker questions to help with team building.
- [Atlassian Team Playbooks](https://www.atlassian.com/team-playbook)
  - Atlassian's playbooks for unleashing team potential.

## One-on-Ones
- [One-on-One](https://a16z.com/2012/08/30/one-on-one/), Ben Horowitz, a16z
  - Horowitz's tips on effective 1:1s.

## Book Lists
- [The Best Leadership Books For New Managers in 2019](https://www.forbes.com/sites/johnhall/2019/08/15/the-best-leadership-books-for-new-managers-in-2019/)
- [Books that every engineering manager should read](https://medium.com/free-code-camp/books-that-every-engineering-manager-should-read-7a053e296d11)

## Reorgs
- [Getting Reorgs Right](https://hbr.org/2016/11/getting-reorgs-right), Stephen Heidari-Robinson & Suzanne Heywoord, Harvard Business Review
  - [Outline](https://outline.com/7DEFAv)
- [What to Do and Say After a Tough Reorganization](https://hbr.org/2015/10/what-to-do-and-say-after-a-tough-reorganization), Rebecca Knight, Harvard Business Review
  - [Outline](https://outline.com/yxw8rB)

## Remote
- [The Art of Async: The Remote Guide to Team Communication](https://twist.com/remote-work-guides/remote-team-communication)