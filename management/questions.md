# Management Questions

- Information spread:
  - When should official information come from the top, vs. go through reporting lines.
  - When do you want to make sure your message is heard from you, vs. translated/interpreted/diluted through others?