- What's your favorite way to treat yourself?
- What do you need from your teammates?
- What’s something another engineer does well that you’d like to learn?
- What do you think is your current role on the team? What do you want your role to be?
- What’s something our team does inefficiently?
- Do you feel that you’ve gotten enough feedback over the past month?

## Lists
- https://getlighthouse.com/blog/one-on-one-meeting-questions-great-managers-ask/
- https://jasonevanish.com/2014/05/29/101-questions-to-ask-in-1-on-1s/
- https://help.small-improvements.com/article/264-24-questions-to-ask-in-your-next-11-meeting