# Middle Management Expectations

_What is your key role?_

- Reinvention.  
  - Via Slack, Tom DeMarco. Middle managers are agents of change.

- Middle managers should have time on their hands.
  - It looks like excess time, but it's actually what they need to be able to change things.