# Management Skills

What are the skills you need/want/should have to be a good manager?

## Strategic Skills
I haven't reflected on these yet.

- Hire well
- Delegate effectively
- Foster a growth mindset
- Build psychological safety
- Understand team members individually
- Ask for and give Feedback
- Keep employees engaged
- Grow employees’ careers
- Set goals and motivate
- Manage up, down and sideways
- Serve as role models
- Drive accountability
- Communicate effectively
- Run effective meetings
- Unblock issues

### Sources
- https://www.fellow.app/blog/2019/10x-managers-supermanagers/

## Tactical Skills