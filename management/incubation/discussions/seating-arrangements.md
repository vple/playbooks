How do you organize your teams' seating arrangements for effectiveness?

## Rands Leadership Slack, 2019-07-19
https://rands-leadership.slack.com/archives/C595C5DM5/p1563373735251900

> Charlyn Gee  
> do you have an open plan office and also teams? how do you layout the space?

> mendel   
> I’m not 100% sure this is what you mean but I am a very strong believer in “teams sit with their backs to each other” over “teams sit across the table from each other”

> Charlyn Gee   
> Oh @mendel you mean ppl don't have to look past their monitors to talk? Instead turn around to talk?

> mendel   
> Yeah exactly! but I made a diagram so i’m sharing it anyhow :slightly_smiling_face:
> ![Seating Arrangement](seating-arrangement.png)

> mendel   
> being able to trivially pull a chair over is :chefkiss:

> Kevin   
> We have triangular "pods" of three desks each.  People here also work more closely with the people behind them, and hide behind monitors from people at their pod.

> moj_hoss   
> we have cubicles with IC facing _away_ from each other in pods of 4 cubicles

> kenliu   
> This is a very big topic @Charlyn Gee — do you have a specific problem you are trying to solve?

> Charlyn Gee   
> I have an opportunity to re-do the layout in part of our open plan office, for 5-6 teams = ~60 people. mostly I was curious whether there's standards or known best practices that would likely work well

> matt.b   
> The best practices include walls

> kenliu   
> Understanding the constraints around current desk arrangements would be helpful.

> kenliu   
> Are you able to purchase new furniture, or do you have to work with what’s there?

> masonoise   
> We do it like @mendel described. Larger teams have to span pods/areas unfortunately. We’re in a historic building so we can’t mess around too dramatically. Plus it’s an odd triangular building so the area widths are limited.

> Charlyn Gee   
> I have to work with what's there to some extent.  I can probably get more/less of the kinds of furniture that are there (like more desks, fewer sofas). But I can't e.g. switch all the desks for bigger ones.

> moj_hoss   
> Another best practice: consult the team. They may have suggestions that work for them (you _may_ also be able to sit different teams differently based on their needs). You won't make everyone happy but consulting them might be good practice

> Charlyn Gee   
> hm I do have some specific problems already:
> 
> - teams are cross-functional and some functions like being closer (e.g engineers pair program, FE engineers like sitting next to design, everyone wants to be near PM)
> - it's noisy
> - ppl have standups by their desks b/c conf rooms are too far and too scarce... even more noisy
> - team members change kinda often, and some people are on multiple teams

> Charlyn Gee   
> what do you do when you add someone to a team?

> Luca   
> The same thing you would do when adding unplanned work to your board: prevention by making sure you don't commit to 100% capacity

> kathkeating   
> We have islands of teams with spaces between.  If possible, we put a collaborative space between teams (chairs, whiteboard, etc).
> 
> Our desks are all moveable and power, when possible, comes from the ceilings. So it allows teams to reconfigure to however they want.
> 
> We also have two suites connected by a large open hangout space (open conference room, essentially). So between the major parts of the org, we aren't all crammed together.
> 
> It doesn't always work this way, and as we run out of space the collaborative areas will get smooshed, but that's our goal.

> Charlyn Gee   
> I like the idea of having a team area that the team can reconfigure. I'll have to see if that's possible w/ our furniture

> Noemi   
> Fwiw, I really dislike the body language of back to back seating. Staggering the desks can make it easier to talk past monitors, fwiw.

> masonoise   
> The new _Resilient Management_ book calls out seating as an example of something new managers don’t expect to be fraught...and are surprised about.

> Simon Gerber   
> How about plants.

> Charlyn Gee   
> I was thinking of maybe plants as dividers between team spaces