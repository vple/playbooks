All things onboarding.

## Rands Leadership Slack, 2019-07-08
https://rands-leadership.slack.com/archives/C052PTMCA/p1562599345261700

> Christine Jones  
> :question:Hey everyone, next month I have a new hire starting. This is the first new hire since I became a manager. I want to create an on-boarding cheatsheet because our on-boarding has mostly been informal and in person, which is becoming a scaling issue as we hire people in other offices. Does anyone have advice for what they would include? I've worked here for a long time, so my onboarding issues were too long to ago to be relevant.

> Prabodh  
> Are you looking for something for pre-joining too? We had come up with some thing recently which included few steps pre joining and detailed post joining onboarding

> Pat Kua  
> Hey! Great topic. My colleague @milan wrote a number of articles and talks a lot about this: https://medium.com/insiden26/onboarding-engineers-1-2-5fedecba5d5d 

> listrophy  
> i always try to start with Kate Heddleston's stuff: https://github.com/heddle317/onboarding

> Cate Stock  
> My structure tends to look like this:
> 
> - Company overview, values, and organization structure
> - Department organization structure
> - Department policies and procedures (mainly PTO, working hours, etc)
> - Communication methods
> - Regular meetings that the new hire will be attending
> - Regular meetings that the new hire will not be attending (focusing on key decision-making meetings)
> - Main daily tools and how work gets tracked
> - Team culture and personality, my work and management style (I also ask some questions around this to try and determine how the new hire likes to learn/communicate)
> - Going forward 
> - feedback/coaching opportunities, where to get help, initial assignments 

> Christine Jones  
> @Prabodh I haven't heard of pre-joining. What does that entail for your team?

> Prabodh  
> This typically is after the offer is sentout and accepted....We send across a thankyou email from the EM, setup a phone call (and if possible a lunch) to welcome the individual and answer any potential questions about opportunity in detail..This is to ensure the understand the details and from my prev experiences i observed that - they ask more questions during this phase about company, what to learn on tech etc where they are not afraid of being evaluated 

> Prabodh  
> Also one last thing about onboarding is to put a regular feedback loop 
>
> - Setup dedicated slot to collect feedback on "onboarding experience" from every new joinee after they spent 2 weeks to 4 weeks  in the system and potentially incorporate their feedback into the system

> Marc Adler  
> I wrote something a few weeks ago that was specific to onboarding senior developers.
> 
> https://ctoasaservice.org/2019/05/09/onboarding-senior-developers-keys-to-success/

> moj_hoss  
> 
> Separating the various buckets of onboarding can help:
> - HR onboarding (org chart, facilities, social events)
> - IT onboarding (email account, vpn, git access, JIRA access)
> - overall org onboarding: 'why' (the problem the org solves for customers) 'what' (the product categories and names to serve customer problems) and 'how' (main features of products that deliver the solution)
> - technical onboarding: setup dev env, dev process, testing, high level architecture

> moj_hoss  
> The thing I've seen work is if a manager puts in place the *structure* of above, and then every new hire goes through and a) edits and updates information that is there b) adds missing information.
> So over time it becomes a living document that is being updated with each new addition.

> moj_hoss  
> Other tidbits:
> 
> 1- you can put easter eggs in the doc and awards for finding them
> 2- you can have quizzes at the end of each section to help them better understand the material
> 3- you can gamify the process (e.g. person to find 5 errors gets donut, 10 gets sandwich, etc)