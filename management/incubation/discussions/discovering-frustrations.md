Figuring out what frustrates your reports.

## Resources
- https://trustedadvisor.com/why-trust-matters/understanding-trust/understanding-the-trust-equation

## Rands Leadership Slack, 2019-07-08
https://rands-leadership.slack.com/archives/C04VAEVLD/p1562615829044400

> Jon  
> Q: Does anybody have tips/tricks to find out your team members’ frustrations?
> Context: One of my team members is leaving company. I had regularly asked him stuff like “how are you feeling about your role these days”  and “do you have any feedback for me” during our 1:1s, but never got back anything substantial.
> While I was on PTO he put in his notice and told my boss some of his frustrations, some of which is useful and actionable feedback for me. I just wish that I could’ve gotten him to tell me that stuff earlier and more directly.

> Noemi  
> This isn't effective with everyone in every situation, but I find that "if you could change one thing about $topic, what would it be?" can help elicit critical feedback from people who are usually hesitant to share it.

> masonoise  
> @Jon do you have any ideas about why this team member shared the frustrations with your boss and not with you during 1:1s?

> jason  
> This sounds like a fantastic question to ask during a 1:1 during their last week

> Jon  
> @masonoise - I wish I had en easy answer, trust of course plays a role and I assume the solution is a long-term thing and not just better phrasing on my part.

> alden.peterson  
> Something which might be helpful to consider is your questions place a lot of the responsibility for getting you the information you want on your team member.
> 
> If the teammate is frustrated, that question requires them to bring up the frustration/discontentment in some capacity. not everyone is comfortable doing that (the trust thing is relevant here).
> 
> While subtle, @Noemi’s phrasing is changing that, you are taking responsibility for “scoping” the response and making it easier to respond. It’s not a guarantee of anything but the phrasing of a question absolutely can make a significant impact 

> masonoise  
> I'm sure we've all had similar situations; I know I have. "But why didn't so-and-so tell me about that??" Everyone's different of course so some will respond to different questions; some will respond if you share some of your own frustrations; some will respond only if you specifically show interest in the thing that's frustrating them; and some will, honestly, never share. In this case I'd be asking myself whether I somehow sent a signal that I wasn't interested or sympathetic -- but it's not necessarily so!

> wesjossey  
> @Jon was your supervisor doing skip level 1:1s?

> Jon  
> @wesjossey yes. Apparently the complaining only happened at the end, he assured me that he would’ve looped me in otherwise.

> cliffh  
> @jon It’s hard to say, but I general find a good portion of a manager:direct 1:1s has to be focused on building trust in the manager by the direct. If they don’t trust you they won’t tell you want is really wrong. Exactly how you build that trust will vary based on the direct as everyone is a little different. I spend a lot (more than half) of my 1:1s trying to get the direct to trust me and to reenforce that trust. Also, some people just won’t trust you no matter what, so there may not have been much you could have done differently.

> ray  
> There's also no guarantee that the reasons this person gave your boss were the real reasons. They might have just been convenient things to say. Maybe the don't trust you, maybe they don't trust your boss, maybe they don't trust anyone at your company. Maybe they just wanted to a new challenge or a shorter commute. In 1:1s I feel it is my obligation to provide a space for my report to communicate, but it's up to them to say what they want.

> wesjossey  
> :thumbsup:
> 
> Two more questions:
> 1. If you’re familiar with the trust equation (https://trustedadvisor.com/why-trust-matters/understanding-trust/understanding-the-trust-equation) I’m curious if you had to guess which of the four you think might have been lacking in your relationship (not saying it was your fault, but maybe on both sides).
> 2. Where’s your paranoia meter now on your other team members? Do you think they’re also not sharing what they feel, or are you getting regular feedback from them?

> moj_hoss  
> #radicalcandor also has a good framework of being able to give and take very honest feedback

> rkoutnik  
> Hoo boy, that is a _phenomenal_ question, and one I’ve got a lot of opinions on.  Major props for having the guts to ask it.  Apologies if this post ends up sounding a bit half-cocked or strange, I became a dad last week and I’m not sure what this sleep you speak of is.
> 
> It sounds to me like you lost the trust of this team member.  This doesn’t mean you’re bad or untrustworthy!  There’s a lot of reasons trust can get lost, and not all of them involve you.  Every employee carries scars from previous jobs, and there might have just been aggressive pattern matching on their part.  They could also have lost trust with the company as a whole.
> 
> There’s two things that an employee needs to believe in order to share frustrations:
> 
> - If I share this, I won’t face negative consequences
> - If I share this, there’s a good chance something will change
> 
> Let’s start with the first one.  “Negative consequences” here doesn’t just mean fired (but it certainly includes it!).  It could mean aggressive questioning about the feedback (not great for the conflict-averse), being seen as “not a team player”, losing out on an opportunity for a cool new project, etc.  There are _lots_ of companies out there where you need to drink the “everything is perfect here” kool-aid to get anywhere with anyone and people who give constructive feedback get ostracized.
> 
> When first joining a team, most people will offer up some small bits of feedback to test the waters.  If there’s pushback, they’ll clam up and you’ve probably lost them already.  You need to see these small bits of feedback and leap at the chance to demonstrate that you’re willing to listen.  Make sure to praise (publicly, if contextually appropriate) the person giving feedback.  You want them to feel like giving the feedback was the right thing to do.
> 
> I’m willing to bet that the team member probably gave small bits of feedback in the past.  They may not have even been to you or at this company!  But something went wrong, there was pushback, and they decided keeping quiet was the better option.
> 
> The second thing, “a good chance something will change”, is a slower death but also happens with incredible frequency.  I can think of one boss in particular in my past who would take feedback in a 1:1, think about it, have a great discussion around the feedback, and then do absolutely nothing about it.  I’d ask them about it a few weeks later and they’d react as if it was the first time they’d heard about it.  I just gave up and told them they were doing a great job because what I had to say didn’t matter to them.  They were more interested in playing the “good boss” role instead of actually walking the walk.
> 
> I don’t think this is your situation.  You got feedback, and already are trying to make things good, despite having essentially lost the employee.  You’re already doing much better than my old boss in the above example.
> 
> Hope this wall of text helps.  If it doesn’t, well, I’ll give you your money back :wink: .

> jgoldschrafe  
> So, on that "good chance something will change": over your career, you will encounter situations where someone is upset about something you have no control over. It may be something that's embedded in the culture of the company ("we're an early-stage startup, we fly more fast and loose than the kinds of environments you're used to") or something that's just not likely to become an organizational priority in the near future ("we know the state of testing sucks right now, but we're not going to address it until the next major refactor of this component/interface").
> 
> And it's okay to have these answers. What you should be doing in these situations is helping your direct a) make good decisions for themselves about their work and b) finding closure on the matter so they can move their attention onto something else. This might mean asking some very uncomfortable leading questions, like: "Think about what's important to you, and what you want to be able to say about your contributions when you look back on your work a year from now. Is the way we do things right now something you're okay with?" Because there's nothing worse for trust than dragging someone along in the hopes that something might get better when there is essentially no chance of it ever changing. It's like the work equivalent of carrying on a relationship where someone wants kids and someone else doesn't.