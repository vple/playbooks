How do you develop team members? (Typically, beyond required day-to-day.)

## Rands Leadership Slack, 2019-07-01
https://rands-leadership.slack.com/archives/C052PTMCA/p1562022940422600

> Jaya B  
> Not sure if this is the right channel to ask this, but I am curious how engineering leaders/managers are encouraging professional development of team members with today’s dynamic skill requirements. This is a broad topic. We talk about soft skill development (presentation, teamwork, mentoring), I also want to know about hard skill development (taking courses, conferences). Depending on org size, where do budget come from? HR or eng manager budget? How do you tie that into job performance? Meaning, a team member performing very well, but not really learning new skills vs. a team member is advancing his/her skillset, but currently an average performer (although when a new project comes up that requires new skillset, he/she would be very useful). Love to know what works or not from this community.

> masonoise  
> We have a company-set PDP budget of $5000/employee/year which is assigned to each manager's budget. It is not tied into performance at all (since measuring "performance" is a questionable practice).

> mendel  
> Same approach here, but we’re a little less generous (USD 3k, or CAD 4k). I think I might push to have that re-evaluated for next year, since it hasn’t changed in several years.
> 
> The problem with our approach is that outside of _going to conferences_ — which are a great perk but not always the best alignment with learning objectives — we don’t have a good system for providing enough slack in the day-to-day of an engineer to learn new skillsets ahead of needing them. For instance, one of my objectives for this year is to get an AWS SA certification, but I have got precisely nowhere on that even though I can spend money against it, because there’s always more immediate things to do: project work, org-facing “side hustles”, etc.

> masonoise  
> We also have a small team that handles "development" -- we run several semesters a year with people teaching classes for others at the company, as well as bringing in outside folks to teach classes periodically. PDP budget can be used of course for courses, Safari subscriptions, etc and we have some online learning programs available (video classes etc).

> mendel  
> Conference-as-travel-perk vs. conference-as-learning opportunity is a tough one to balance too

> masonoise  
> And to echo what Rich said, the company has to make the time available. Our internal "university" classes for example happen during business hours -- no expectation to stay late or anything like that in order to participate!

> steve.rubin  
> If it’s a big deal that requires schedules to be juggled, then going to a conference/class/whatever, becomes less attractive to an employee. They’re made to feel like it’s a hassle and it’s their fault.

> steve.rubin  
> Ideally X hours each week and a 1-2 week learning session per year would be baked into the schedule from the start.

> Kerrie Luginbill  
> We have a single developer who works sort of in isolation that I am running into this with. He gets roadblocked because of a skillset cap and doesn't know how to work through it. We offer to pay for courses, we hired a mentor (a dev that used to work for us and has moved on to a much larger dev agency) who worked with him and billed us a retainer monthly, and we send to conferences. When sending to conferences, we require the team member to pitch the value of the conference (they have to sell us the benefit to offset the cost) and we ask them to present to the team afterward. We also allow for professional development on company time.

> atporter  
> We have a number of peer led employee groups that focus on a specific language, technology or skill

> okterok  
> Late to the discussion a little, but before we had actual professional development money and were a start-up, my start-up encouraged public speaking and would send us to any conference (in the US, I later found out :smile: ) which we got a talk accepted at. This gave me a cool opportunity to engage in conferences across the US. As a manager, I encourage people to set goals both inside but also outside their normal day-to-day and then give them time in their sprints to accomplish those tasks. One example would be online Scala courses or data science courses which didn't cost the engineer any money.

> okterok  
> We had study groups, too, where groups of us would learn topics over lunch and collaborate with "homework" assignments. It was really fun and a nice way to team-build. Several engineers who took the Scala course, for example, still use those skills professionally now :slightly_smiling_face:

> masonoise  
> Oh yes, we have an engineering book club too.

