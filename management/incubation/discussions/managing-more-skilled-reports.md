How do you manage someone who is more skilled than you?

## Resources
- https://blog.coleadership.com/being-a-tech-lead-with-teammates-more-experienced-than-you/

## Rands Leadership Slack, 2019-07-01
https://rands-leadership.slack.com/archives/C052PTMCA/p1562001074359600

> adam  
> does anyone have any suggestions for how to continue to provide value as a manager to software engineers who have surpassed your personal technical ability? this person wants to continue down the engineer path and not move into management. i have 1 report who i'm sure has surpassed me and another who is likely about to.

> adamhobson  
> Listen to them. Help them find the right opportunities. Encourage them to find a mentor.

> matt.schellhas  
> clear roadblocks, help on the project where your strengths lie, shield them from the wider org where appropriate

> niks  
> My preferred way to provide value is to think of myself as their coach - I'm not their teacher, I'm there to help them figure out their best path. Becoming a 'thinking partner' allows you to remove your knowledge from the equation and focus on what will help them.
> 
> I've been managing more technically-knowledgable people than me for over 11 years. It's entirely possible to do.

> atporter  
> Let them focus on details, keep a broader view of the project/org/company. Warn them of pitfalls or problems out side of their immediate scope. Be a sounding board.

> adam  
> i guess if the situation was completely reversed and i was the technically superior one, i'd want my manager to help me grow on the technical path. so it sounds like i can still do that but not directly, more of indirectly.

> Alex Martynov  
> Hi Adam, not sure it would work for everyone, but how about sharing this with this person. Something like "the younger version of me has been very reluctant to report to manager who wasn't a better engineer than me. And now I feel that I can't teach you anything technically. How do I think I can serve you best in such circumstances?"

> jgoldschrafe  
> If you focus your attention on the things you're good at, you will:
>
> - ensure that nobody on the team grows beyond the current highest level on that team
> - prevent the mission of the team from growing laterally to accommodate the business needs you aren't any good at
> - necessarily create you-shaped directs

> masonoise  
> "Better" technical skills are less important for engineer growth, past a certain point, than non-technical skills are. Help them learn that.

> masonoise  
> Things like communication, learning to be customer-focused, marketing ideas and getting buy-in, selling a solution to someone who has their own horse in the race, presenting to upper management, coaching and mentoring juniors, etc. Those sorts of skills are needed to get to senior and beyond in most (maybe all) organizations.