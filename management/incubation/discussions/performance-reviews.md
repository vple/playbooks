Anything performance reviews.

## Rands Leadership Slack, 2019-07-22
https://rands-leadership.slack.com/archives/C04UK88AG/p1563802669103800

*How do you deliver 360 review peer feedback?*

> Chris Seifert  
> Question for everyone - if your org does 360 reviews, do the people being reviewed see the raw feedback? Or do you receive it and consolidate it for them?

> Steven Bourke   
> Consolidate: Usually, I pull out the theme, adapt the message to the person in question. Pay more attention to overlap in their self appraisal and 360 themes that do not appear in their self appraisal. If I know the person is ok with direct feedback and can extrapolate themselves I’ll usually just print it and go over it with them 

> Mike Pappas   
> For us, I, as an employee,  receive a completed review form from each individual who reviewed me. However, the manager sees them first, and it's their job to ensure that I can actually absorb that feedback effectively - so they have the right, if they deem it necessary, to either redact pieces (which they either feel confident are fully nonsense, which is super rare, or which they'd simply prefer to only discuss in person), or very carefully tweak language to help me understand the feedback more easily. My manager can only submit such feedback, though, after getting approval for their changes from the original reviewer, to ensure they don't put words in their mouth.

> David Thompson   
> I usually consolidate raw feedback and group it into themes unless the comment is clear and actionable enough to stand on its own.
> 
> I also strongly encourage a culture of feedback so the managers on my team are regularly getting their own feedback directly from their peers and stakeholders.

> Mike Wales   
> Another consolidation here (although I will likely pull out specific direct quotes from their peer’s reviews to share).

> Mike Wales   
> It’s less consolidation and more editorializing and polishing. Not all peer reviews end up with actionable feedback, so it’s me trying to get those big, powerful, themes/ideas out and packaged up in a useful way.

> moj_hoss   
> When we did it $lastjob-2, it was raw feedback with some consolidated and averaging *added*

> matt.schellhas   
> The reviewers have the option to send it directly or not.

> matt.schellhas   
> Managers don’t see their feedback from reports until after they’ve finished the perf cycle for their reports.

> Mike Sullivan   
> We did raw but anonymous feedback.  Your manager would see it ahead of time and provide summaries / commentary on it and they whole package was released to you at one time - either before or after it was discussed.

> Cristiano Balducci   
> consolidated, the theory being that you can map who gave you feedback anonymously through the writing style

> madicap   
> Raw but without names attached, although it's usually fairly easy to determine who wrote what from content and writing style, so not really the anonymous it pretends to be. Some teams suggest signing names to reviews to remove this strange in-between state.
> 
> There is a final section at the end for truly anonymous feedback, though, which gets directed to the manager only for them to consider and not communicate directly if there's anything extra-sensitive to convey.