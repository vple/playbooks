How do you tune your interviews to provide a useful signal?

## Resources
- https://slack.engineering/refactoring-backend-engineering-hiring-at-slack-b53b1e0e7a3c

## Rands Leadership Slack, 2019-07-11
https://rands-leadership.slack.com/archives/C051J57DD/p1562828697109000

> Gargs  
> https://www.linkedin.com/feed/update/urn:li:activity:6554474861329293312

> Noemi  
> I can’t tell you how many “experienced engineers” I’ve interviewed in the last 2 months who swiftly demonstrated an utter inability to code their way out of a paper bag. 
> 
> And if you start making exceptions for people based on pedigree, what sort of bias does that introduce to your process?

> kolektiv  
> I was going to say the same thing - I don’t know how easy it is to “hide” at Google but I’ve certainly ended around people who on paper have done all kinds of things, but in reality… Not so much. Sure, it’s a balancing act, and should you use the same approach for a junior grad as for an experienced dev? Probably not, but if you all you check for is self-proclaimed reputation, prepare to have your view of human nature taken down a few notches.

> Simon Gerber  
> When my last company was trying to scale rapidly we did hundreds of phone screenings and gave everyone the same set of questions - but changed our expectations based on the candidates reported experience.
> 
> We divided out screening questions into four key areas:
> 
> 1. Core Java
> 2. General programming concepts (algorithms, Big O notation, polymorphism, SOLID principles, etc)
> 3. Communication (i.e. could communicate technical answers simply)
> 4. General technical knowledge (i.e. knew about the existence of other JVM languages, could name more than one database server (you'd be surprised!), etc.)
> 
> We always wanted '3' and '4' to be high. But '1' and '2' exhibited the most interesting variation. 
> 
> Developers with not much industry experience, but often fresh out of University, would score low for '1' but high for '2'.
> 
> They hadn't had time to learn the deep, dark corners of Java - to earn their battle scars - but they could still remember their university lectures on Big O notation
> 
> As time goes by - we saw 'core Java' go up and 'general concepts' go down. People gained more practical knowledge but started to forget their theory.
> 
> Then at a certain point one of two things happened. Either both '1' and '2' go up - or they both go down.
> 
> Sometimes as engineers achieved mastery they were able to recover their theoretical knowledge and slot it together with practical knowledge of Java.
> 
> Other times they hit a level where they become a bit more "hands off" and technical skills all start to degrade.
> 
> Once we'd seen that pattern over and over again with our screen questions, and had a good view on who went on to pass their in-person interviews or not - we started to look for outliers. The most interesting candidates were the ones who were scored better than we would otherwise expect for their experience level.

> Noemi  
> So, how should we test a senior developer’s hands on software development skills without insulting them?

> Simon Gerber  
> I'm a fan of doing some pair programming in the interview,
> 
> "The aim is not to finish the exercise - the aim is just to spend 30 minutes working together so we can get a feel for your approach to problem solving"

> Noemi  
> What kind of problem do you work on with them?

> Simon Gerber   
> We gave them a choice. We had a pool of many exercises and asked our engineer who would be pairing with the candidate to pick three they were comfortable with before the interview.
> 
> During the interview the candidate would be shown the three excerices, given five minutes to read through them and pick the one they liked the look of.

> Simon Gerber   
> The exercises were crowd-sourced internally.
> 
> Things like:
> 
> -  calling the score of a tennis match.
> - Scoring a Yatzee dice roll
> - 'line-wrapping' a string
> - Converting decimal numbers into roman numerals

> Simon Gerber   
> We had a couple at one point that were derived from actual real-world code we had in our code-base. But they were discontinued because they actually ended up being too easy to solve.

> masonoise   
> We did something similar at my last company: we developed a couple of git repos with an app that was partially done, and then paired on adding some features. The pairing included deciding what to work on, so they could choose something that interested them (hopefully).

> Gargs  
> At senior levels does the quality of code really impact productivity, though? I feel that recruiting is being optimized for the wrong things at that level. Maybe because most hiring managers suffered from insane interviews and so kind of think it’s the norm to do bubble sort on a whiteboard without any standard library features. I mean really. Is that what’s holding your prod down.

> marq  
> I’ve reviewed interview packets for Staff-level egineering positions — so five hours of onsite interveiws plus usually a couple of phone screens, plus internal references and other materials. That’s still very hard to assess.

> Gargs  
> Once had an interviewer get pissed because I knew something in swift they haven’t heard of. Their comeback - we build scalable apps here so we write our own functions. What the hell is a scalable app.

> Noemi  
> @Gargs - we don’t do algorithm interviews. I agree that those are dumb. But “don’t do algorithm interviews” doesn’t answer the question of how you do screen for a senior developer who can both write code and design APIs and systems well enough to not only create maintainable and extensible software but also improve our codebase and help teach our juniors how to do those things as well.

> Noemi  
> How do you go about assessing their fundamentals, familiarity with frameworks, thinking outside the box, and generic concepts, etc? (I mean this as an honest question, not an attempt to argue - because figuring out how to accomplish that is an ongoing challenge for me.) 

> Gargs  
> @Noemi that’s easy. You’d test for what you know. Generally people doing interviews know nothing more than the problem they copied from some interview guide and so they get candidates like that. You can tell familiarity and problem solving skills mostly by hearing people talk about their current projects.
> 
> Don’t let the senior most person interview candidates. Involve the one that knows your product inside out. They’re the ones that’d be asking the relevant stuff.

> Tony Tam  
> During technical interviews, I make an assumption most people can code.  I want to assess problem solving, able to communicate and defend technical work they have done before, and to be critical of their previous work, if I ask them what they would have done differently.

> Matt Gioe   
> I find the "assess problem solving" idea to be so pie in the sky (while also admitting that it's a reasonable thing to want to evaluate). At the absolute best, it tests their ability to solve a problem of a certain kind under contrived circumstance. Sometimes the "problem" isn't even relevant to their workload past, present or future. More often than not, the answer to solving a "problem" is "ask someone who knows the answer" and that's not an option afforded to you in the moment.

> Matt Gioe   
> So, yeah, there's that protection which is sort of the "talk through the problem" version which, as you've pointed out, is actually really tough in practice (both for the interviewee who may have never coded with another person in real time before AND the interviewer who has to constantly question their own judgements of the questions asked).
> 
> But I guess even more fundamentally, when I'm posed with a problem I can't answer, in reality, I just go ask the person/Internet who might know the answer. And then maybe next time I remember how to solve that problem on my own (or not. Maybe I ask again). And, IMHO, that's still problem solving. It's just not the puzzle solving we're looking to evaluate.
> 
> It's almost like the option to look up the answer on Stack Overflow vs knowing CS concepts (or whatever). Resorting to Stack Overflow is still "problem solving", I feel.

> gjiva  
> eh, i can’t find exactly the article i read on this (i think it was last year), but this seems in the ballpark: https://www.cnbc.com/2017/11/30/google-uses-this-scientifically-proven-method-to-hire-employees.html
