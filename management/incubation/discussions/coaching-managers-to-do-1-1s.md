How do you coach/teach a manager on how to run a 1-1, if they have no experience with them?

## Rands Leadership Slack, 2019-07-02
> Noemi  
> Has anyone here coached a new manager through learning to do good 1-1s?

> Noemi  
> @Alex Martynov - I’ve got two new leads starting this summer who have never done any sort of people management before and whose experiences in 1-1s from the report side may have been mixed. We’ll be gradually turning over responsibility for weekly 1-1s with some of the team to them (still doing skip levels too, of course).
> 
> I want to make sure they’re doing a good job and giving our junior developers the 1-1s they deserve and crave.
> 
> But unlike almost anything else where I can observe directly (or fairly direct results) and give feedback on what they’re doing effectively or if I see them faltering, 1-1s are by definition private. So I’m not sure how to a) detect problems or b) help them improve if they’re being ineffective but aren’t themselves aware that that’s the case.

> Alex Martynov  
> To be honest, I have a belief that if people have time to reflect they are capable of giving themselves quite good feedback. For example, by asking them questions like: What do you think is working during your 1-1s? What's not working? How open is the other person with you? Why? Why not? If you were that person, what kind of words would have helped you to open up? And so on.

> Haley  
> Assuming you already have relationships with the people you’re transitioning, would it be out of place to ask their feedback on the new managers after a couple weeks? Assuming also you’ll be giving the new managers a nice instructional talk on how you conduct them. When I was starting 1:1s I learned a lot from reading tons of blogs (like the balance) and cribbed some question ideas from this slack.

> Haley  
> Thinking back, I think it would have been most helpful for me when I was starting out if someone had just succinctly explained the purpose of 1:1s, and gave me a list of starter questions to use until I figured out what worked best for me and my team

> Noemi  
> Yeah, I had a similar experience, Haley, and am trying to spare both the managers and reports from that.
>
> Your questions are very helpful, and reading them had helped me think through what specifically about this situation is making me anxious. I think one of the leads doesn’t really fully understand what the job is or what the purpose of 1-1s is. I’m concerned that rather than being receptive to feedback on that point he’ll interpret it as my not having confidence in him in general.

> Haley  
> Totally makes sense. I feel it’s worth remembering that 1:1s are pretty fluid depending on the personalities involved - what works for you and I might not work for your new managers. You’ll do your best to set them up with success, and a certain amount will be up to them to learn. Having someone like you coach them on effective 1:1 strategies from the beginning is already giving them a huge head start in competency!

> royrapoport  
> @Noemi To be precise, I think what you might be talking about are two separate challenges:
> 
> 1. How do you know how a lead under you is doing 1o1s?; and
> 2. How do you coach them on doing better?
> 
> While originally you were talking about (2), right now you're talking about (1) (and it makes sense, of course, to start with (1)).
> 
> 1o1s are not "by definition" private, I'd argue -- most of them are not . I think it's very reasonable, for example, to ask the skips how their 1o1s with their new leads are going, as well as asking the leads how the 1o1s are going, and what they talk about (there's a difference between "what do you talk about?" which is abstract and "what did you talk about this week?" which might feel like prying -- find the right balance).
>
> Separately from this, do please seriously consider having regularly-scheduled skip-level 1o1s.  They'll be invaluable to you.

> Ashby (they/them)  
> do you know what the outcomes are that you hope these folks will achieve by > running decent 1-1s?

> Noemi  
> So, pretty much the same things I hope to achieve in my own:
> 
> 1) developing and maintaining relationships
> 2) finding out what concerns and challenges the report is dealing with right now or that we've recently discussed, and deep diving into them; as much as appropriate asking them questions to guide them towards learning from the experience and/or finding solutions for themselves; adding ideas if they're struggling to come up with good ones
> 3) making sure we're communicating both upwards and downwards about what's going on, what's important, and especially what's changing in the organization; gathering the report's feedback; answering their concerns; getting buy-in as necessary. Often this involves discussing in more depth an initiative or org change that we recently brought up or are about to bring up in a group venue.
> 4) discovering, moving on, and providing feedback on the report's professional development goals; setting specific targets and checking in on them; in some cases, practicing specific skills together.
> 5) sharing feedback in both directions
> 
> With the outcomes of that ideally being:
> 
> 1)  Maintaining good relationships with psychological safety -- communication, engagement
> 2) The report knows what's going on in the organization; the context of their work; and feels heard about concerns they have -- autonomy; morale
> 3) developing people professionally and making sure that as much as possible we're giving them work they're interested in and that helps them grow -- growth, engagement
> 4) understanding (and hopefully providing) what people need from us in order to stay here; and knowing / being able to help them when it's time for them to move on -- retention

> Ashby (they/them)  
> are you in a position to verify any of these outcomes directly, rather than verifying the quality of the 1:1s specifically?

> Noemi  
> Ashby, you keep asking questions that make me have to think!
> 
> I can measure parts of outcomes 2, 3, 4 but not the entirety.  And those feedback cycles are long and subjective and indirect enough that it can be very difficult to detect problems before they become crises.  But I'd be interested in your ideas for better measuring them.  (FWIW, this is something I struggled with mightily myself my first couple of times managing people, because it's really hard to figure out what you're doing well and poorly [well, that and I didn't even have any good frameworks at the time for thinking about what the effects of good or bad management would look like])

> Ashby (they/them)  
> It looks to me like all those outcomes are things you could ask about in skip 1:1s and have some very interesting conversations
> Not so coincidentally, questions around those outcomes are almost exactly the ones that Investors in People auditors ask when they come round to redo the accreditation, certainly 2 and 3 are things I've been explicitly asked about by an auditor.

> wesjossey  
> @Noemi reading up on your outcomes and what you hope to achieve question, you can survey this effectively without asking any team members to “rate” their managers (as you’ll skew the metrics once you ask people to rate one another).
> 
> Using whatever likert scale format you prefer:
> _How strongly do you agree with the following statements_
> 
> 1. I feel safe sharing my opinions and ideas with those around me.
> 2. I feel engaged in my work --- This question can be asked on a bi-weekly or monthly basis as an alternative
> 3. My relationship with my colleagues meets my expectations
> 4. I have a consistent understanding of what is happening in our business
> 5. I have a consistent understanding of what is happening within our organization
> 6. I understand how my work impacts our other members of our organization, and our business
> 7. I am given the autonomy needed to do my best work
> 8. I am given the support needed to do my best work
> 9. I have regular conversations about growing professional at Acme Co
> 10. My team knows how to keep me motivated and engaged
> 11. My workload is _____ (too light, light, just right, heavy, too heavy) --- This can also be asked more frequently, and there’s research that talks about how work + engagement is a better measure of retention than engagement alone
> 
> These aren’t perfectly worded questions (I don’t have our library in front of me at the moment), but basically you’re trying to back into *are your managers achieving your expectations* without specifically asking someone, “Are you having effective 1:1s with your manager.” Most people can’t articulate clearly what makes an effective 1:1s, so you need to decompose it to be able to get feedback on what’s potentially missing.
> 
> The big thing in here that’s hard to really measure is whether or not each manager truly understands what *motivates* each person. Motivations are remarkably diverse on even a fairly homogenous team, and if you miss the boat on motivation, a lot of other efforts wont’ make as big a difference.

> Dan Turkenkopf  
> @wesjossey Do you have any recommendations on resources to help discover what motivates each person?

> wesjossey  
> Good question!
> 
> I bet there is something out there… I tend to honestly find once you’ve built up some basic levels of trust, you can basically just flat out ask. I don’t tend to find people are super cagey about it.
> 
> I’ll ask on mentoring calls after about 10 - 15 minutes, once the person is warmed up, if I get the sense they’re not engaged in their current work. People tend to not balk at it.
> 
> Let me ask my colleague though. She tends to have lots of resources around this stuff, as she builds out our curriculums on this sort of stuff.

> wesjossey  
> @Dan Turkenkopf
> Ok! Per Michelle’s recommendations:
> 
> 1. Daniel Pink’s “Drive” --- https://www.amazon.com/Drive-Surprising-Truth-About-Motivates/dp/1594484805
> 2. She recommends using RIG as the framework for focus on motivation, which stands for *Relationships*, *Impact*, and *Growth*.
> 3. Here’s two article that, while they don’t say “Go ask this” to your reports, have different components / questions you could use to try to sort out motivation:
> 
> https://www.fortunyconsulting.com/purpose-action-series-2-hi-im-rig-nice-fulfill/
> 
> https://yscouts.com/community/assessing-fulfillment-at-work-through-rig/