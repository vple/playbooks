How do you do Good Things when there's organizational friction?

## Rands Leadership Slack, 2019-07-10
https://rands-leadership.slack.com/archives/C052PTMCA/p1562623130285500

> alexis  
> :question: Not sure where to ask this, so I’ll try here and follow any :raccoon: that pops up…..
> Has anyone ever built/described an “acceleration committee”.  A designated group of senior people who can be contacted to try and get organisational obstacles (processes / red tape) out of the way?  I’d like to give folks who are faced by tasks which are:
> 
> - systematic (ie not once-offs), and
> - slow them down (in their eyes needlessly), and
> - they can’t ‘fix’ themselves (e.g. because they are A RULE THAT IS IMPOSED)
> 
> The idea is that senior folk can only help if they know about the issues, and they need to commit to a (fast) process of trying to help where possible.  This process should also be as public as possible (mainly to celebrate wins).
> I’m trying to write up a scope description, ideas/inputs welcome.  This is for a business unit of ~200 folks (growing by 50 or so a year) spread across the world and part of a ~1k person org.  I’m thinking to have some kind of ticketing system at its core, however the tech is secondary for me — it’s the practice I’m looking for input on.

> Jeff Bryner  
> I was part of a thing at $lastjob that sounds similar, but the thing I was in was focused on business-wide problems. Sounds like you are going specifically after engineering group, process-level red-tape?

> Kimberly  
> We have a Kaizen Committee which I think is similar. Basically, people submit ideas to the committee to change processes, problems, fix inefficiencies etc

> Kimberly  
> the team ended up building an internal portal for submissions and we talk about any current initiatives in our weekly company meeting. We are <50 people currently though. 

> alexis  
> There is some assumption (likely valid) that we can fix our own internal processes relatively easily (I plan to include a step like “Have you spoken to your team lead about this?” because that should sweep most of these up).  It’s the ‘do this complicated thing for $corporate_dept’ which will need senior management to get change implemented.

> steve.rubin  
> Copious skip level 1:1s.
> “Suggestion” box.

> Abbas  
> We did an ideas email alias that routed to a committee of leaders who had great cross functional instincts. In our case that was CEO, head of people, CTO, and CMO.
> 
> Every idea was reviewed in a monthly “Do Better!” meeting of these people, discussed with other people as needed, and if determined to be of merit, popped in as an OKR for the right part of the org in the right timeframe.