How do you ensure interviews/new hires are calibrated correctly?

## Rands Leadership Slack, 2019-07-22

> cfank  
> Hi all, I'm in the position of needing to evaluate whether someone we've hired as a Senior Software Developer is, in fact, Senior-level. I was involved in the technical interview along with another and we were agreed that the candidate was intermediate, not senior, but I guess they made a convincing argument to the person making the hiring decision.
> 
> Now it falls to me to determine before the end of their probationary period whether we would continue employing them or cut them loose and continue looking for a Senior-level person.
> 
> Does anyone have some feedback for how to perform skill-level evaluations? I've spoken to one of their peers and they placed the new hire at the intermediate level but admitted they didn't have much context to work with. Are further peer reviews the way to go here? :thinking-face-rotating:

> ocho   
> Does your org have defined job requirements for Senior level devs?

> masonoise   
> That's what I was going to say. Unless you have a good, clear framework that sets expectations at the various levels, you cannot objectively evaluate someone. Which sounds like the reason for the original failure at hiring time.

> masonoise   
> Without that, peer reviews will be purely subjective and not something you can show this person to explain that they're not meeting expectations at their current level.

> DanB   
> A lot of this comes down to negotiation and BATNA. I'd also encourage you to re-evaluate a recruiting system that incorporates a probation period. That pipeline must surely resemble a market for lemons. 

> cfank   
> >Does your org have defined job requirements for Senior level devs?
> 
> No, and that's something I've been banging the drum about for a while now -- more clearly defined roles. I'll bring it up again within this context and see if I can at least get a framework started that we can use.
> 
> I agree, the decision would be pretty subjective and I feel pretty poorly about letting someone go because they don't "feel" Senior-enough.
> 
> >I'd also encourage you to re-evaluate a recruiting system that incorporates an probation period.
> 
> This is an artifact of employment law in British Columbia, where there are more strict requirements of the company for terminations after the 90 day probationary period ends .