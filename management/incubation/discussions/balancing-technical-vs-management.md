How do you approach the technical vs management career tracks?

## Rands Leadership Slack, 2019-07-17
https://rands-leadership.slack.com/archives/C4H2VTR7Y/p1563382127120000

> nirav  
> What are your thoughts around the Engineering/Manager Pendulum (2-3 years as a manager, 2-3 years as an engineer then swinging back into management)?  I know you've advocated for keeping your technical skills up to date in the past, do you believe that someone who participates in this type of pendulum is creating a more well rounded leader or is this rather a sign of someone who may be losing out on expertise by hopping back/forth?  Feel free to tie it into the new role that you have created at slack (IC > Manager of 3 trial run) 

> rands  
> I’ve seen this in play a lot. It’s a fine idea and a great way to keep both sides of the brain engaged.

> rands  
> However, I believe there is likely a cap on career progression. You’re likely not going to be a VP of Engineering at a large company (or an the converse, an Architect/Principal/Etc.) or it’s going to take you a lot longer.

> rands  
> Which is fine. Different strokes for different folks.

> nirav  
> Yeah that makes sense from an experience standpoint, breadth vs. depth and sacrificing career progression for the opportunity to move between roles.  Thanks Rands!