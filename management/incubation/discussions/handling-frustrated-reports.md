How do you manage a report that is frustrated?

## Resources
- https://randsinrepose.com/archives/the-update-the-vent-and-the-disaster/

## Rands Leadership Slack, 2019-07-01
https://rands-leadership.slack.com/archives/C052PTMCA/p1562003695369800

> htmikelee  
> how do you respond when a report expresses general frustration in a 1:1? nothing specific. very few things are actionable. but just a general frustration that things aren’t changing fast enough and they don’t have the ability to just do it themselves.

> atporter  
> Sometimes folks just need a safe forum to vent.

> jgoldschrafe  
> I often ask directly if they're looking for me to listen and support, or to provide feedback that might help them navigate the situation

> jgoldschrafe  
> In a sense, this is a rephrasing of the Mark Horstman/Mike Auzenne model where you ask "can I give you feedback?" before actually proceeding to give feedback

> Rob Donoghue  
> Beyond empathy, they are also often attempting to find out if they are *seen* - that is, if they are alone in seeing these problems or not.   Could just be venting, but can be an opportunity to convey whether you’re both on the same side of the line.

> matt.schellhas  
> in my experience, this is a cry for help. "This is painful, and I would like to not be in pain any more. Please help."
> 
> Listening and sympathy is good, but such a situation is unsustainable. You have to stop the pain. Otherwise they _will_ leave to be free of it.

> Abbas  
> Being a manager does carry some therapy like work as well. So if this is general frustration being vented, sometimes a simple “I hear you” and a patient listening ear is all that’s needed. I would strongly advise following up on the feeling in a couple of weeks, explicitly referencing this discussion to see if any specific frustrations have been distilled, or if the cloud has passed and things are back to normal.
If it happens more frequently, you’ll need a more structured approach to extract the core issue

> htmikelee  
> yeah. i had another report a few months ago who vented at me for an hour and then asked if we could schedule more. that was a more clear “i want you to start these conversations with the higher ups”. i told him i’d work on it and was able to check back in and say “we took these steps. these new things are a direct result of what you told me. the rest is going to take a while but how do you feel now?” this one is tougher because they’re not things i can reasonably do anything about.