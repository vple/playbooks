How do you train your interviewers?

## Resources
- https://medium.engineering/mediums-engineering-interview-process-b8d6b67927c4

## Rands Leadership Slack, 2019-07-12
https://rands-leadership.slack.com/archives/C051J57DD/p1562951727226800

> feifan  
> :wave: does anyone have docs/links they can share about how they train technical interviewers (phone screen and/or onsite)? I’m going to be helping some friends practice for (IC) interviews, and it would be great to get some insight into how I can be an effective practice partner!


> Toma M  
> My experience is that the most important thing is having extremely clear, detailed requirements for the job, so that you know what you're trying to find out in the interview. If those things are unambiguous, and you've defined what count as bad, good, and excellent answers, then training interviewers is straight-forward


> feifan  
> That’s a good point! Would it make sense to think there are tactical concerns (like “communication ability”) that are role-agnostic?


> Toma M  
> What is "communication ability" in this context? Is your company multi-lingual?
> 
> My company has employees that only speak Mandarin, so I couldn't interview them


> Toma M  
> Otherwise, a lot of it is around things like persuasive speaking, as in "how do you go about making the case for something that you know is important, when others do not agree with you by default"
> 
> I use that as a test to see if people fight/argue, vs if they look for data or other proof to back up their view, and how willing they are to compromise and/or "disagree and commit"


> feifan  
> Not multi-lingual. I just went through a bunch of interviews, and all the prep material stressed how it was important to communicate clearly and “explain your thoughts”. IME that came naturally to me, but what might that mean in terms of specific things to look for?


> feifan  
> The general gist of my question is to try taking what I would consider unknown knowns (things I do naturally, without thinking) and turn them into known knowns (explicit criteria)


> Toma M  
> In terms of training interviewers, I would actually make sure they are aware of the reverse problem: if you make thinking-out-loud too much of the test, you could screen out people who are excellent potential hires but either don't know they're supposed to do this, or have trouble with the artificial nature of the interview context


> Toma M  
> If I want candidates to explain their thought process, I explicitly tell them so, and usually the point is to pick out something specific, like if they think of a possibility and then rule it out for a good reason. But to construct that scenario, you have to know exactly what knowledge and reasoning abilities you're testing for


> mendel  
> I don’t have materials I can share, but we have formal interviewer training on
> 
> - legal requirements
> - avoiding bias
> - behavioral interviewing techniques
> 
> and then as for learning specific slots, especially technical ones, we have a process where a new interviewer shadows an experienced one, and then reverse-shadows, before they’re ready to do the slot themselves.


> angelocordon  
> I remember reading through Medium’s interview process and they were pretty transparent how they conduct their interviews - this might help: https://medium.engineering/mediums-engineering-interview-process-b8d6b67927c4