How do you coach/convince a report to do 1-1s, if they have no experience with them?

## Resources
- https://www.manager-tools.com/manager-tools-basics
- https://www.manager-tools.com/map-universe/one-ones

## Rands Leadership Slack, 2019-07-10
https://rands-leadership.slack.com/archives/C04VAEVLD/p1562744446092500

> Gabriel  
> Dear all, I want to ask for some input on the following challenge I have : I just started this week in a new place as an Engineering manager and one of my direct reports is very reluctant to the idea of having 1-1 with me - he never had in the past a manager to actually take care / support him, how could I influence him to accept meeting with me? He pushed back on me asking for some links or references regarding why should he do this, I told him will look into it so that I can win some time to think about this - it’s a situation I wasn’t expecting at all.

> Grzegorz Piwowarek  
> Trick him into a meeting and don’t tell him it’s a `1-to-1` :slightly_smiling_face: 

> Marco  
> Same here. And depending on the work environment, you might opt for a lunch or a walk outside, instead of a meeting.

> jgoldschrafe  
> You might be interested in the Manager Tools series on rolling out 1:1s, which was created specifically for situations like this one 

> Grzegorz Piwowarek  
> I’be been on the other side - usually it was because some manager was making these meeting way too official, stiff, and felt like it’s an obligation. It was a huge turn off for me 

> Alex Martynov  
> Apart from not wanting to have 1-1, what do you think about the relationship with this person? I have a suspicion that something else might be going on and this is just a symptom.

> Robyn  
> +1 on the manager tools series. They also address common questions/troubleshooting rolling out 1:1's with scenarios like this.
> 
> I would start with the one on one podcasts from their Manager Tools Basics series:
> 
> https://www.manager-tools.com/manager-tools-basics
> 
> And then if you want more:
> 
> https://www.manager-tools.com/map-universe/one-ones

> moj_hoss  
> Give it time.
> I had this exact same situation. When I started at $lastjob 2 ICs explicitly told me: "we don't want 1:1s". I said "ok".
> Time went by. They got to know me. We had informal 'drop-in' 1:1s and then I suggested 'topic based 1:1' (something like: "get together to talk about why project A is :dumpsterfire: "). Then these became regular. Then we started talking about how they're doing, their career plans. Voila! We were having regular 1:1s.
> Give it time.

> James Cerrato  
> Welcome to management where your reports (being humans) will make you say that last sentence over and over again as you grow into the role. lolol `it's a situation I wasn't expecting at all`

> James Cerrato  
> I personally had a similar situation and what I did was work closely with the engineer (by pairing and being involved as technical support on his projects), over time he began to respect me and we started to already have natural adhoc discussions til one day he came around to the idea of life and career discussions.

> James Cerrato  
> I had a CPO once explain to me his definition of authority: that it is either granted or earned. With title sometimes folks are reluctant to the "given" authority of you as a manager, but it takes earning it over time to truly build organic trust in you. Starting at a new place largely is "granted" authority in that it's bestowed on you with your title, but your reports don't actually know you so that's where servant leadership and leading from beside helps build the "earned" authority to ask for things like 1:1s.

> Matt Kantor  
> I always buy new people in the office a coffee outside (starbucks or where ever) just to say hi.  Then I just start a "getting to know you" process.  See where they came from, how this is different, answer any questions, then tell them I do this with everyone all the time repeatedly.  Also that it's confidential.  Which is needs to be.

> Robyn  
> I found the discussion above super valuable.
> 
> Are there behaviours that help with "earning" the authority? I believe in servant leadership, as an example. What kinds of things do I need to do to earn authority?
> 
> Obviously I don't want to think about it as transactional (if I do behaviour x, I'll gain +y% authority)...I guess I'd just like to know what kinds of behaviours are useful and what I should avoid.

> billmorein  
> I'd ask yourself whether you know what matters to him and what you/the company need from him. I am of the belief that 1:1s are an almost universally good thing but the question of *why* they are good actually depends a bit on the person and situation.

> billmorein  
> I like this link but wish that there was actually a better sense of what the study was: https://rands-leadership.slack.com/archives/C04VAEVLD/p1552057709101800

> steve.rubin  
> I aim to remove uncertainty:
>
> 1. In the first team meeting, explain 1:1s. Let them know why you feel they’re valuable, and what you hope they (and you) can get out of them.
> 2. For your first round, the information you’ll be trying to extract will be highly similar, so send the team all (most?) of the questions you’ll be asking in your part of the meeting.