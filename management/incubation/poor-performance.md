# Poor Performance

> Chris acknowledges in #1-1s that they are not as productive as they expect themselves to be, and we’ve talked multiple times about the need to keep improving when they were, and at least once on the continued need to improve (since they plateaued) (both improving and plateaued my judgement without much concrete to back it up)

> Your employee has acknowledged they're not performing to the level they need to, has not pointed out anything you could do to help, and has not improved? Is that about right?

Addressing poor performance:

1. Agree on both sides that the performance isn't up to standard.
  - Precisely articulate what is wrong and what is expected.
  - Have both sides explain the issues and expectations to confirm things are clear.
2. Agree on how much responsibility is on both sides for improvement.
  - Ultimately, the report is responsible for an improvement in *outcomes*.
  - However, the manager should have some responsibility for providing the right support and environment for the report to be able to improve.
  - Make it clear that the report is expected to monitor and improve their own performance. The manager is there to help as needed and should be used as a resource.
3. Check in regularly. You should expect that the report:
  - Knows whether or not they are performing at the appropriate level.
    - If they don't know or are mistaken in assessing themself, it is a problem (for various reasons).
  - Has taken advantage of resources available to them.
    - Although there are many resources, a key one is the manager
  - Is improving.