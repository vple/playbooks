# Onboarding Mentorship

Some initial directions:
- When is someone ready to mentor?
- How do you train them?
  - It's there first time and there will be a lot of mismatch between what they expect and what you expect.
  - They may also be very new to a non-IC role and probably can't think very well in that capacity.
    - Explicit expectations?
- Qualities of a good mentor?
  - Strong intermediate/senior employee.
  - Doesn't mind being interrupted a lot.
