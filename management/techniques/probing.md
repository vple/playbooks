# Probing

As a manager, you must delegate. It's not possible for you to know every single thing that's going on. But this raises a problem: how can you be confident that a task or project will complete successfully if you're not intimately familiar with it?

Every time someone tells you what's going on, they're telling you a story. These stories might not accurately reflect what is or will happen in reality. You want to ask: do you believe this story? When Gary Goto says a project is on task and will be done by Thursday, do you believe him?

The technique you use here is *probing*. You poke around for more details, making sure that the details match up with the story you were given. Demand specifics. If Gary is out of office on Wednesday and hasn't thought about how he will test his new feature, how can it possibly be done by Thursday?

As you probe, there will be times when your gut tells you that something is off.[^the-twinge] You may not be able to consciously explain what's wrong. Trust your gut—you're subsconsciously pattern matching based on all the details you've been given and all of the experience you've built up as an individual contributor.

[^the-twinge]: Rands calles this the [Twinge](http://randsinrepose.com/archives/the-twinge/).