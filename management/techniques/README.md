# Technique Toolkit

These are various tools that you might find useful for management. They are just tools—each one can be incredibly effective when used in the situation, but completely useless when used in a wrong one.

## Project Management / Execution
### Avoiding Pitfalls / Preventative Maintenance
- Probing

## Training
- Demonstrate (The Right to be Shown)

## Roadmap
These are in no particular order.

- Meetings
	- What makes a good meeting?
- Measuring where things are
	- Rands Test
	- Are you a good manager?
- 1-1s (feedback, relationships)
- Providing direction

information conduit

communication
	- personality disconnects

emotional
	- freakouts