# Daily Check-Ins
Part of leading a team includes understanding what's going on. 

One of the most straightforward ways to do this is to just observe, but that only works if your team's work is easily observable. This is true for areas like manufacturing or construction. It's not true for knowledge or digital work, such as software engineering.

When this is the case, there's a simple solution—just ask! The idea of a daily check-in is to just spend a few minutes every day asking your team how things are going. This also gives your team members a natural chance to raise questions or issues.

It's a simple question, but you're still wearing your usual planning/management hat. You're checking if the person is making appropriate progress, if they understand what they're doing, if they're on the right track, if they need guidance, etc.