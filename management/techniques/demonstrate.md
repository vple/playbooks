# Demonstrate

In a perfect world, your team members would be able to instantly pick up and excel at any task that you need from them. In reality, this is never the case. 

There may be *some* areas where your team members will be able to excel with minimal or no guidance from you. More likely, however, is that people won't know what to do. This is normal—how can someone perform well at something if they don't know what they're supposed to do or what the results are supposed to be?

This is where demonstration comes in. You show how you expect something to be done. Demonstrating allows you to transfer needed skills and knowledge between team members.

The idea here is that **everyone has the right to be shown** how to do a task that you expect from them. It's okay to ask someone to do something that they're not familiar with, but once they show that they are struggling you need to take a step back and walk through things with them.

To show someone how to do something, you (or a subject matter expert who can also teach) do the task with the other person observing. If possible, point out in real time what you're thinking, observing, and deciding. Debrief after the task to reinforce any key points and answer any questions that your team member might have.