# Interview Question Long List

Potential questions for designing an interview. These aren't approved or sanctioned in any way--they still need to be evaluated to decide if they are helpful and provide a meaningful signal.

- What is something you'd like to improve?
  - Mainly intended for internal transfers.
  - If someone is to join a team responsible for improvements, they should have something they've seen that they want to change.
- What is something we do that's bad?
  - Can someone identify poor practices?
  - Can they articulate why?
  - Are they able to present negative things to you?
- Write me something.
  - Evaluates writing ability.
  - Likely needs a bit more direction.
- Convince me of something.
  - Can this person get buy in?
