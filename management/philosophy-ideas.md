# Management Philosophy Ideas

- Find your weaknesses by failing.
  - Put people in situations where they can fail without being blamed.
  - Treat failures as things that expose your weaknesses.
  - Room for failure means people know they can try things that aren't sure bets.
- Principle of fair exchange: any agreement you arrive at should be equally acceptable to you from either side.
- People should be able to take pride in their work.
  - Volvo (1980) had a team of people per car. Upon completion, each person scratched their signatures under a fender.
- People like ideas more when it comes from them. (Socrates) But as a manager, sometimes you need certain things to happen.
  - First, how should you get the thing to happen? Should you just tell/ask people? Or should you try to get them to discover it?
  - If you do try to get them to discover it, how do you go about that? How much do you _engineer_ their environment or the problem they're thinking about? How much do you try to steer them towards a particular answer?
  - What about with a group? Is it fine, for example, to have someone bring up a specific point at a group meeting?