# Types Of People

## Plays By The Book
If you're a person who likes to play by the book, you thrive when they get to repeat the same, clear-cut tasks over and over again. You do well when you get to just follow the same process over and over again. Others see you as someone who puts out a steady stream of high-quality, albeit "cookie-cutter," work. You do your best when you get put in a lane and just need to stay in that lane.

You likely struggle in one way or another when you have to do something that isn't on your beaten track. There can happen in different ways.

In one scenario, you are tasked with something that is completely new to you. You might flounder and be completely unable to figure out how to make progress towards a solution. Or, you might rely on your tendency to stick to an existing process and try to find an existing solution. You might try to mold that solution to work for your current task without understanding why the solution does or doesn't work well for your particular case.

Another scenario is where you need to do something that is slightly different from the work you're already familiar with. You could struggle here because you don't understand the fundamentals behind your existing approach. The result is that you're not sure how to appropriately tweak what you already do in order to adapt it to your current problem.

To better handle these situations, it helps to develop a stronger understanding of the context/environment of your work. Going by the book often allows you to ignore these things, but this understanding is what helps you to develop solutions when faced with unfamiliar situations.

Playing by the book can also lead to a very insidious negative effect: you lose your ability to drive innovation. When your default mode of thinking is to just follow an existing track, you focus almost exclusively on _replicating_ what already exists. You're unable to step back and see how a solution helps (or doesn't help) solve the problem it's being applied towards. Because of this, you aren't able to evaluate what could be better and drive long-term improvements.

Your leaders will appreciate being able to put you in situations where you can handle lots of recurring, similar tasks. However, they will be reluctant to put you into situations that require creativity or that have lots of ambiguity. They will also struggle to view you as someone who can become a leader. If this is something you want to change, you need to be proactive about making sure your leaders put you in situations where you be challenged. While they may occasionally assign you to something out of your comfort zone, there's a decent chance that they are doing so reluctantly if you haven't made it clear what you want.

## Strong Individual Ability
Some people are very strong at doing things on their own. Regardless of whether it's something they know how to do or something they're new at, these people rely on their own strengths to solve and complete tasks. They're often seen as independent and/or self-reliant.

Having strong individual ability is not the same as preferring to work on your own. People with strong individual ability will execute their tasks well and quickly (or at least, on time). They can often be trusted with things they're unfamiliar with. Someone who prefers to work on their own but lacks strong individual ability will struggle in one or more of these areas (correctness, quality, speed).

If you're strong individually, you're often quite welcome on a team. This is because you can often be relied on as someone who will complete their tasks well. If you've familiarized yourself with your role, odds are that you're also contributing more than just your individual share. Strong individual talent isn't required for a team to perform well, but it often makes it much easier for the team to do so.

While you may be genuinely well-rounded, it's more common for you to have a few areas that you're exceedingly good at. You likely lean on these strengths, using them to help you complete work that would be otherwise difficult for you. Playing to your strengths is great; this is definitely something that you should keep using to your advantage. However, it's also important to understand the weaker skills that you're covering for. It's possible to skew too much towards your strengths, to the point where you don't take advantage of other, simpler solutions. 

Especially as an individual contributor, it's easy for someone with strong individual to rely _only_ on themselves. This is effective at first, but there will always be a point where individual ability alone won't cut it. You want to reach a point where you can recognize when to rely on your own strengths and when to pull from others around you. Your individual ability isn't the only resource you have at your disposal; others can help you as well.

Your leaders will generally be happy with your work, and things will generally go pretty smoothly for you. They'll get more frustrated as you start to bump into areas where you can't rely just on your own ability. You'll have a moderate-to-high ceiling, which will get higher once you learn how to balance acting as an individual with acting as part of a team.

## Ownership

## Lurkers
Lurkers are those who show up to things but don't participate. During meetings and discussions they are silent and possibly disengaged, only saying something when directly prompted. If you are a lurker, you are likely harming yourself without being aware of it.

There are many reasons you might be a lurker:

- You're afraid to say something that would make you look wrong or dumb.
- You are completely lost and don't understand what's going on.
- You digest information differently. It's hard for you to follow along (for example) in a meeting and contribute.
- You think what you have to say isn't important, because someone else has probably already thought of it.

Loosely, you're either afraid or overwhelmed. These are natural feelings. The problem is, by lurking, you are implicitly making a tradeoff. You're decreasing the chances that your team will perform well, in exchange for some peace of mind in the moment. In the long run, this is a bad trade for everyone involved.

Many meetings are informational meetings. They're present to share information amongst everyone at the meeting. You may view meetings as things that you reluctantly go to. Your leaders (or whoever is calling the meeting), however, view it completely differently. For them, they are expecting everyone to walk out of the meeting with a much better understanding of is going on and what needs to be done. When you lurk, you're preventing this from happening. There's two sides to this.

On the one hand, you might not be receiving information very well. Maybe there's a confusing point that you need re-explained. Maybe you're completely lost. Regardless of the situation, you need to do something to improve your understanding.

If possible, ask during the meeting. If it's confusing to you, it's probably confusing to someone else as well. If you believe answering your questions would derail the meeting too much, you need to take action after the meeting to ensure that you understanding things. This might be, for example, setting something up one-on-one for someone to explain the topic to you. Doing nothing is not an appropriate response.

On the other hand, you might recognize that something you're hearing doesn't make sense. It's not that you aren't following along, it's more that something sounds wrong. You are obligated here to speak up and get your concern clarified. If it's a misunderstanding on your end, this helps you get your information sorted out. It could just as easily be a real problem that hadn't yet been considered. By speaking up, you help deal with concerns before they becoming problematic.

In both cases, the medium that you communicate with doesn't have to be face-to-face, group meetings. Meeting one-on-one or hashing things out online can also work. You don't always have to speak, but you do always have to communicate.

## Pragmatism
### Perfectionists

### Speed Demons
Speed demons are defined by how quickly they finish their work. You're bent on completing your task as quickly as possible in order to move on to the next task. It's often fairly surprising how quickly you complete tasks. This is great if your work is high quality, but that is typically not the case with speed demons.

The risk of being a speed demon is that you're compromising other important qualities in order to be fast. Your work will often end up being incorrect, brittle, and/or unreliable. Of course, this is almost never intentional. But it still happens because of how quickly you're going.

There aren't really any shortcuts to ensuring high quality here. You will have to make sure you follow processes and tools that catch the mistakes you're likely to make. Of course, this requires understanding the types of things you tend to make mistakes on. It's usually better to just slow down and ensure your work is high quality.

Your leaders will be hesitant to continue working with you if you're speed demon. They know that it's likely that you'll inadvertantly create time bombs that become larger problems for them. You need to change your approach so that producing high quality work becomes your number one priority. Otherwise, your leaders will either find a way to get you off the team or put you in a position where they can limit the amount of damage you can do. You'll have a very low ceiling.