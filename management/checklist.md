# Manager Checklist
Are you a good manager? The aim of the checklist is to help identify things you're doing well or things you could be doing.

## People

### Empowerment
Empowerment is how you're helping people to develop their skills and gain more responsibility.

- [ ] Do your people have control over something where they'll make you look bad if they fail?
  - You empower people by trusting them with something where there's a chance they'll fail. By doing this, you show you trust them and are willing to take a risk on them. Hopefully, this trust also encourages and motivates them to do their best work for you.

## Team Building & Development
_These probably need more context/explanation of what they mean._

- Create a self-aware team.
- Create an emotionally intelligent team.
- Create psychological safety.
- Know and communicate the values and behaviors you expect from yourself and the team.
- Communicate the:
  - objectives
  - progress
  - processes
  - assumptions
  - individual contributions
- Have team feedback exchanges, with accountability conversations. (once per year)
