# QOTW Log
1-1 Questions of the Week.

- 2019-11-18  
  I’m curious about how you’re doing with your check-in goals. How have you made progress? What would help you make progress?
- 2019-10-28  
  What’s a topic that you’ve been learning about over the past two weeks? What motivated you to learn about this?
- 2019-10-21  
  Do you feel that you’ve gotten enough feedback over the past month?
- 2019-09-30  
  What’s something our team does inefficiently?
- 2019-09-09  
  What do you think is your current role on the team? What do you want your role to be?
- 2019-08-26  
  What’s something another engineer does well that you’d like to learn?
- 2019-08-12  
  What was good/bad/interesting about the team dynamics from last sprint?
- 2019-07-29  
  Have you felt comfortable taking risks, making mistakes, and/or raising issues over the past month?  
  See also this article on psychological safety and the seven questions in it:  
  https://medium.com/@mcleanonline/measuring-psychological-safety-81dd1da91915
- 2019-07-22
  Have you felt comfortable taking risks, making mistakes, and/or raising issues over the past month?  
  See also this article on psychological safety and the seven questions in it:  
  https://medium.com/@mcleanonline/measuring-psychological-safety-81dd1da91915
- 2019-07-15  
  How’s your research project going?
- 2019-07-08  
  What’s something you do that you’re proud of?
- 2019-06-24  
  What comes naturally to you?
- 2019-06-17  
  Are you happy?
- 2019-06-10  
  What is getting in the way of you being (more) successful?
- 2019-06-03  
  What’s a takeaway from the offsite that we can/should do?