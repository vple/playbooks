# Individual Improvement

Miscellaneous things that can be tweaked individually. The idea here is that these things can use by individuals for self-improvement, or for managers to help offer suggestions to make someone more effective.

- Do error-prone work earlier, as you may be more likely to make mistakes later and/or when tired.