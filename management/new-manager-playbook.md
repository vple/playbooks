# New Manager Playbook
So you're a new manager. Congrats! What are you supposed to do now?

Well, one of the first things you should probably do is figure out: _What does it mean to be a manager?_ If your situation is like mine, you might not have been told what is expected of you in your new role!

Here's the answer. Management means using and coordinating people to accomplish desired objectives in a systematic way.

There are a few parts here. Overall, your goal is a manager is to accomplish goals. However, you can't do this directly yourself, and you can't use tools. You must accomplish these goals indirectly, by getting others to work together towards that goal. Additionally, this all needs to be done systematically—in a way that is reliable and predictable.

This is easy enough to say, but it's much harder to _understand and do_. Becoming a manager means figuring out what _you_ will do in order to systematically accomplish goals, via other people. 

## Defining Management
There are two big questions that you need to learn to answer now that you're a manager.

First, what makes a great manager?

Second, what's your management style?

### What Makes A Great Manager?
There are many answers to this question. Although they are loosely similar, each points out a different aspect to management.

#### Rands
> A great manager is someone with whom you can make a connection no matter where you sit in the organization chart. —Michael Lopp[^dont-be-a-prick]

[^dont-be-a-prick]: http://randsinrepose.com/archives/dont-be-a-prick/

This involves being able to quickly construct an insightful opinion on someone, listening to them and mentally documenting how they're built.

## Your Job

- "A manager's job is to take what skills they have, the ones that got them promoted, and figure out how to make them scale."

If you came to management by climbing an engineering career ladder, you likely got to here by developing many essential engineering skills. These skills allowed you to be great at building *software*.

As a manager, these skills will not help you build *people*. Management is a new career, and one where you are just starting out. This isn't to say that your engineering skills are useless—it's just that you need an entirely new skillset to be a great manager.

## Evaluating Yourself
As you'll find, one of the most important things you'll do with your employees is to teach them to evaluate themselves. You want them to be aware of and responsible for their own development.

You're no exception to this! How do you evalute how you're doing as a manager? 

### Metrics
Here are some different criteria used by various people and organizations. You should pick one that resonates with you and work on it improving yourself against those criteria, as much as reasonably possible. When you feel you're not making as much progress as you should, come back and evaluate yourself against another set of criteria—it might show some other directions where you can continue to improve.

### Information Gathering
- one on ones
	- "pulse of the organization"

### Delegation