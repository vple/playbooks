# Evaluating Managers
In school, you might've picked a class based primarily on who was teaching it. You evaluated your professors, likely on factors such as prestige, how clearly they communicate concepts, how much they enjoy teaching, and your friends' word of mouth.

It's no different at work. Instead of teachers you have managers. And managers are much more important, because you will interact with them over a longer period of time and because they have much more impact on your career trajectory. It's not important to understand what your manager does as their job, but it is important to understand if they are capable of looking out for you.

Companies are almost always hierarchical. They are organizations made up with layers of people. Each of the intermediate layers consists of managers who translate between the layers above and below them. Your manager needs to understand your wants and your manager's wants, and they need to be able to navigate when those wants differ.

There's an important consequence to this: **your manager is your face to the rest of the organization**. They represent you, particularly to the layers above you. Relatedly, **the organization's view of your manager is their view of you**. Because your career is somewhat coupled to your manager's career, you want to evaluate your manager and make sure that they're capable of looking out for number one—you.

## Rands Questions
Michael Lopp (Rands) has a set of seven questions for evaluating your manager. They're reproduced here.[^managing-humans]

[^managing-humans]: These questions come from his book, Managing Humans. You can also find a few of my notes on the book [here](https://bitbucket.org/vple/playbooks/src/master/books/managing-humans.md).

### Where does your manager come from?
You want your manager to be able to speak well on your behalf, as they will be in situations where they'll need to speak to how you're doing. Your job is to give your manager something to say about you. You do this by ensuring that they know what you're doing.

*Your manager doesn't always know what you do.* There's often two main reasons this happens.

One, your work just isn't on their radar.[^work] Not because your work is good or bad, just because it doesn't happen to be what they're looking at. The fix to this is easy—just tell them whenever you talk to them.

[^work]: We use the word "work" here, but this isn't limited to your raw deliverables. You want your manager to know how you contribute and the good things that you do.

Two, your manager may not have the same background as you. Meaning that they got to where they are via a different career path than you, and are actually not able to do the things the things required of your job.[^different-job] You still need to tell them what you do, but you need to tell them in their language. Figure out their background, and then teach them about your work using words that they understand. You want them understand *what* you do and *why it matters*.

[^different-job]: This isn't necessarily a problem, as they probably have other skills that got them into that situation. Your manager's job is not your job, so they don't necessarily need the same skillset to do what you do.

### How do they compensate for their blind spots?
Your manager, like everyone else, has strengths and weaknesses. As a manager, they need to take their skills and scale them. They need to be able to build a team that highlights their strengths, but also handles where they're weak. This isn't possible for them to do if they are unaware of their weaknesses.

### Does your manager speak the language?
Your manager doesn't only need to speak to you—they need to speak to other managers at the company. Are they capable of doing so in a way that others understand them?

### How does your manager talk to you?
Here, you want to see if your manager is regularly talking to you. Frequently, this is done via one-on-ones.

It's important that your manager regularly talks to you because this indicates that they want to learn. They should want to know what you have to say, even (and especially) when you have something negative to say. A manager who doesn't regularly talk to their employees won't learn what is going on within their group.

### How much action per decision?
Managers need to delegate, but they also need to do real work—visible action taken to support their vision for the organization. Does your manager do what they say they're going to do? Do they make things happen?

### Where is your manager in the political food chain?
It's better to have a politically active manager than one who just passively takes organizational changes. These politically active managers both know when change is happening and they know what to do to best represent their organization when that change happens. Again, the organization's view of your manager is their view of you.

This is the hardest question to evaluate, because you are likely not in the meetings where your manager interacts with those above them. The next best way to evaluate is by seeing how your manager interacts in meetings with their peers. Observe how they're treated by their peers. Are they familiar with them, or are they only getting to know them? Are they able to actively contribute to others' meetings?

### What happens when they lose their shit?
The good end of the management spectrum is pride. When your manager is doing well, you'll want to see if they take care of the people who helped them get there and if they have a plan for what's next.

For this question, however, we're interested in the other end—panic. How does your manager react when things are going wrong? You want to how they answer the above questions now—how do they talk to you, with the organization, are they staying politically active, etc. These observations will give you indicators as to how well things are going for your organization and, consequently, you. It also gives you more insight into how your manager behaves under stress—this is when they'll most likely rely on the skills that worked for them in the past. Is the person you find the kind of person you want to work for?