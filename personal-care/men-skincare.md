# Men's Skincare

## Identification

Determine your:

- skin type
- skin concern

## Routine

### Basic Skincare Routine

- Cleanser
  - Can double clense (oil + water based)
- Toner
- Serum
- Eye Cream
- Moisturizer

https://www.gq.com/story/everything-you-need-to-know-to-start-a-skincare-routine

### Korean 10 Step

1. Eye Makeup Removal
2. Cleanse (oil cleanse + water cleanse)
3. Exfoliator
  - Frequency?
    - Seems like 2x/month for women, 2x/week for men?
4. Refresher (Toning?)
5. Essence
6. Ampoule / Serum
7. Sheet Mask
8. Eye Cream
9. Moisturizer
10. Night Cream

https://intothegloss.com/2014/04/korean-beauty-skincare/

### Current Routine

1. Oil-based cleanser
  - https://www.amazon.com/BANILA-CO-Clean-Cleansing-Original/dp/B07BSVJ4H8/
    - via https://sokoglam.com/blogs/news/15817332-the-tutorial-skincare-for-men
2. Water-based cleanser
  - https://www.amazon.com/Facial-Cleanser-Men-Kyoku-Products/dp/B003040T0Y/ref=cm_cr_arp_d_product_top?ie=UTF8
    - Is this actually water-based?
    - Amazon says for oily/combination.
    - Seems out of production?
  - https://www.amazon.com/COSRX-Good-Morning-Cleanser-150ml/dp/B016NRXO06/
    - listed on many lists
    - Soko Glam listing: https://sokoglam.com/collections/mens-shop/products/cosrx-low-ph-good-morning-cleanser
    - best for sensitive & dry skin, supposed to be for any type
    - pH 5
  - https://www.amazon.com/BANILA-CO-Cleanser-cleanser-natural/dp/B07BSKR5CG/ref=sr_1_1?dchild=1&keywords=banila+co+clean+it+zero+foam&qid=1609626815&sr=8-1
  - https://sokoglam.com/collections/skincare/products/banila-co-clean-it-zero-foam-cleanser
3. Exfoliator
  - Wednesday + Saturday
  - Kyoku
    - probably need to find a replacement once it's out
4. Toner
  - BENTON Aloe BHA Skin Toner
    - https://www.amazon.com/Benton-Aloe-Skin-Toner-Ounce/dp/B00GAOBG3A
    - https://sokoglam.com/collections/mens-shop/products/benton-aloe-bha-skin-toner
      - all skin types
5. Essence
  - https://sokoglam.com/products/neogen-real-ferment-micro-essence?variant=41009913673
6. Serum
  - https://sokoglam.com/products/neogendermalogy-real-ferment-micro-serum?variant=2437965709321
9. Moisturizer
  - Kyoku
    - has SPF
  - https://sokoglam.com/collections/skincare/products/danahan-ginseng-seed-secret-emulsion

## Future Stuff

- What chemicals/compounds do what?
- Deeper dive into each step.
- Expand on routine
  - https://intothegloss.com/2014/04/korean-beauty-skincare/
  - https://www.fashionbeans.com/article/korean-skin-care/
- How does pH affect skin care?
- Exfoliator frequency?

## Other Guides

- https://verygoodlight.com/2017/12/28/the-easiest-guide-ever-to-korean-beauty/